package Prerequisite;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.gs.pages.Branch;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BranchCode_Verification extends LoginLogout_ACH{
	@Test

	public void branchCode_Verification() throws Exception {
		try {

			Log.startTestCase(log, "START");
			
			((ExtentTest) test).log(LogStatus.INFO, "<h6><b>Prerequisite : Check Business Date & Previous Business Date in Bank Data > Branch</b></h6>");
			
			log.info("Navigate to Bank Data");
			((ExtentTest) test).log(LogStatus.INFO, "Navigate to Bank Data");
			WebElement bankData = SideBarMenu.bankData(driver);
			BrowserResolution.scrollToElement(driver, bankData);
			WaitLibrary.waitForElementToBeClickable(driver, bankData, Constants_ACH.avg_explicit).click();

			log.info("Click on Branch");
			((ExtentTest) test).log(LogStatus.INFO, "Click on Branch Tab");
			WebElement branch = SideBarMenu.branch(driver);
			BrowserResolution.scrollToElement(driver, branch);
			WaitLibrary.waitForElementToBeClickable(driver, branch, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Branch Page with Branch Heading");
			Branch.branchHeading(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);

			//Navigate to Branch table
			log.info("Navigate to Branch Table"); 
			BrowserResolution.scrollUp(driver);
			
			// Fetch Module name(s) and assign branch code to verify business date
			Properties prop = FilesUpload.readPropertiesFile(System.getProperty("user.dir") + "\\TestData\\GlobalData.properties");
			
			List<String> BranchCodeList = new ArrayList<String>();
			String[] ModuleNameArray = prop.getProperty("ModuleName").split(",");
			for (int i=0 ; i < ModuleNameArray.length ; i++) {
				switch(ModuleNameArray[i].toLowerCase()) {
				case "fed":
				case "iccm":
					BranchCodeList.add("GSBI");
					break;
				case "chaps":
				case "fps":
					BranchCodeList.add("GS01");
					break;
				}
			}
			
			// Scroll table to last row to get all values from table
			js.executeScript("arguments[0].scrollIntoView();", Branch.branchTableLastRow(driver));
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Verify Branch Code");
			List<WebElement> branchTableListTemp = Branch.branchTableList(driver);
			List<String> branchTableList = new ArrayList<String>();
			for(WebElement branchTableValue : branchTableListTemp) {
				if(!branchTableValue.getText().equals(null) && !branchTableValue.getText().equals("")) {
					branchTableList.add(branchTableValue.getText());
				}
			}
			
			// Remove duplicate elements in Branch Code list
			Set<String> BranchCodeSet = new HashSet<>(BranchCodeList);
			BranchCodeList.clear();
			BranchCodeList.addAll(BranchCodeSet);
			
			int branchTableListCount = branchTableList.size();
			
			// Assign Current Business Date and Previous Business Date
			String BusinessDate = CommonMethods_ACH.getCurrentDate("yyyy-MM-dd").toString();
			String PreviousBusinessDate = CommonMethods_ACH.subtractBusinessDays(CommonMethods_ACH.getCurrentDate("yyyy-MM-dd"), 1, Optional.empty()).toString();
			
			// Loop over Branch Codes
			for(int i=0 ; i < BranchCodeList.size(); i++) {
				
				int branchCodeCount = 0;
				
				// Loop over Table Records for respective Branch Code
				for (int j = 0; j < branchTableListCount; j++) {
					String BranchCode = Branch.branchTableData(driver, j, 0).getText();
					String BusinessDateUI = Branch.branchTableData(driver, j, 12).getText();
					String PreviousBusinessDateUI = Branch.branchTableData(driver, j, 13).getText();
					
					if (BranchCode.equals(BranchCodeList.get(i))) {
						
						branchCodeCount++;
						
						((ExtentTest) test).log(LogStatus.INFO, "<b>Branch Code: <font size='+1'>" + BranchCode + "</font></b>");
						
						if(BusinessDate.equals(BusinessDateUI)) {
							((ExtentTest) test).log(LogStatus.PASS, "Business Date for <b>" + BranchCode + "<br>EXPECTED: " + BusinessDate + "<br>ACTUAL: " + BusinessDateUI + "</b>");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Business Date for <b>" + BranchCode + "<br>EXPECTED: " + BusinessDate + "<br>ACTUAL: " + BusinessDateUI + "</b>");
						}
						
						if(PreviousBusinessDate.equals(PreviousBusinessDateUI)) {
							((ExtentTest) test).log(LogStatus.PASS, "Previous Business Date for <b>" + BranchCode + "<br>EXPECTED: " + PreviousBusinessDate + "<br>ACTUAL: " + PreviousBusinessDateUI + "</b>");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Previous Business Date for <b>" + BranchCode + "<br>EXPECTED: " + PreviousBusinessDate + "<br>ACTUAL: " + PreviousBusinessDateUI + "</b>");
						}
					}
				}
				
				if(branchCodeCount == 0) {
					((ExtentTest) test).log(LogStatus.FAIL, "Branch Code <b>" + BranchCodeList.get(i) + "</b> not found in the table");
				}
			}
			
			Log.endTestCase(log, "END");
			
			}catch(Exception et){
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "Prerequisite", TestCaseRow, Constants_ACH.Col_status, "Failed");
				exceptionerror=et.getMessage();
				CaptureScreenshot_ACH.captureScreenshot();
				((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			}
	}
}
