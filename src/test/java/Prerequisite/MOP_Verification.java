package Prerequisite;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.gs.pages.MethodOfPayment;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class MOP_Verification extends LoginLogout_ACH{
	@Test

	public void mOP_Verification() throws Exception {
		try {

			Log.startTestCase(log, "START");
			
			((ExtentTest) test).log(LogStatus.INFO, "<h6><b>Prerequisite : Check Business Date in Distribution Data > Method of Payment</b></h6>");
			
			log.info("Navigate to Distribution Data");
			((ExtentTest) test).log(LogStatus.INFO, "Navigate to Distribution Data");
			WebElement distributionData = SideBarMenu.distributionData(driver);
			BrowserResolution.scrollToElement(driver, distributionData);
			WaitLibrary.waitForElementToBeClickable(driver, distributionData, Constants_ACH.avg_explicit).click();

			log.info("Click on Method of Payment");
			((ExtentTest) test).log(LogStatus.INFO, "Click on Method of Payment Tab");
			WebElement methodOfPayment = SideBarMenu.methodOfPayment(driver);
			BrowserResolution.scrollToElement(driver, methodOfPayment);
			WaitLibrary.waitForElementToBeClickable(driver, methodOfPayment, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Method Of Payment Page with Method Of Payment Heading");
			MethodOfPayment.methodOfPmtHeading(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);

			//Navigate to Method Of Payment table
			log.info("Navigate to Method Of Payment Table"); 
			BrowserResolution.scrollUp(driver);
			
			// Fetch MOP name to verify business date
			Properties prop = FilesUpload.readPropertiesFile(System.getProperty("user.dir") + "\\TestData\\GlobalData.properties");
			String[] MOPArray = prop.getProperty("MOP").split(",");
			
			// Scroll table to last row to get all values from table
			js.executeScript("arguments[0].scrollIntoView();", MethodOfPayment.methodOfPmtTableLastRow(driver));
			Thread.sleep(Constants_ACH.short_sleep);
					
			int startingTableIndex = 0;
			boolean foundStartingTableIndex = false;
			log.info("Verify Method of Payment");
			List<WebElement> methodOfPmtTableListTemp = MethodOfPayment.methodOfPmtTableList(driver);
			List<String> methodOfPmtTableList = new ArrayList<String>();
			for(WebElement methodOfPmtTableValue : methodOfPmtTableListTemp) {
				if(!methodOfPmtTableValue.getText().equals(null) && !methodOfPmtTableValue.getText().equals("")) {
					if(startingTableIndex==0 && foundStartingTableIndex==false) {
						startingTableIndex = methodOfPmtTableListTemp.indexOf(methodOfPmtTableValue);
						foundStartingTableIndex=true;
					}
					methodOfPmtTableList.add(methodOfPmtTableValue.getText());
				}
			}
			
			int methodOfPmtTableListCount = methodOfPmtTableList.size();
			
			// Assign Current Business Date and Previous Business Date
			String MOPBusinessDate = CommonMethods_ACH.getCurrentDate("yyyy-MM-dd").toString();
			
			// Loop over Branch Codes
			for(int i=0 ; i < MOPArray.length; i++) {
				
				int MOPCount = 0;
				
				// Loop over Table Records for respective Branch Code
				for (int j = 0; j < methodOfPmtTableListCount; j++) {
					String MOP = MethodOfPayment.methodOfPmtTableData(driver, startingTableIndex+j, 0).getText();
					String MOPBusinessDateUI = MethodOfPayment.methodOfPmtTableData(driver, startingTableIndex+j, 14).getText();

					if (MOP.equals(MOPArray[i])) {
						MOPCount++;
						
						((ExtentTest) test).log(LogStatus.INFO, "<b>Method Of Payment: <font size='+1'>" + MOP + "</font></b>");
						
						if(MOPBusinessDate.equals(MOPBusinessDateUI)) {
							((ExtentTest) test).log(LogStatus.PASS, "Business Date for <b>" + MOP + "<br>EXPECTED: " + MOPBusinessDate + "<br>ACTUAL: " + MOPBusinessDateUI + "</b>");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Business Date for <b>" + MOP + "<br>EXPECTED: " + MOPBusinessDate + "<br>ACTUAL: " + MOPBusinessDateUI + "</b>");
						}
					}
				}
				
				if(MOPCount == 0) {
					((ExtentTest) test).log(LogStatus.FAIL, "Method Of Payment <b>" + MOPArray[i] + "</b> not found in the table");
				}
			}
			
			Log.endTestCase(log, "END");
		} catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
		}
	}
}
