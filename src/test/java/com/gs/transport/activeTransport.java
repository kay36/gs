package com.gs.transport;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.gs.pages.OnboardingData;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;

import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class activeTransport extends LoginLogout_ICCM{
@Test
	
	public void active() throws Exception {
	try {
		Log.startTestCase(log,TestCaseName);
		
		log.info("Navigate to Onboarding Data");
		WebElement onBoardData = SideBarMenu.onboardingData(driver);
		BrowserResolution.scrollToElement(driver, onBoardData);
		WaitLibrary.waitForElementToBeClickable(driver, onBoardData, Constants.avg_explicit).click();
		
		log.info("Navigate to Transport ");
		WebElement transportTab = SideBarMenu.transportTab(driver);
		BrowserResolution.scrollToElement(driver, transportTab);
		WaitLibrary.waitForElementToBeClickable(driver, transportTab, Constants.avg_explicit).click();
		
		log.info("Click on Grid View");
		WebElement gridView = OnboardingData.gridView(driver);
		String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants.avg_explicit).getAttribute("class");
					
		if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
			WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants.avg_explicit).click();					
		}
		
		Thread.sleep(Constants.short_sleep);
		WaitLibrary.waitForAngular(driver);
		CommonMethods.getTransport(driver, "SWIFTOUT_MOP_SIMULATED");
				
		WebElement editBtn = OnboardingData.editBtn(driver);
		WaitLibrary.waitForElementToBeClickable(driver, editBtn, 30).click();
		WaitLibrary.waitForAngular(driver);
		System.out.println("Edit button clicked");
		
		// Verifying Edit page with Title
		WebElement editTransTitleElem = OnboardingData.getEditTransportTitle(driver);
		WaitLibrary.waitForElementToBeVisible(driver, editTransTitleElem, Constants.avg_explicit);
		
		// Click on Status select box
		WebElement statusSelectBox = OnboardingData.getStatusSelectBox(driver);
		BrowserResolution.scrollToElement(driver, statusSelectBox);
		js.executeScript("window.scrollBy(0,-120)");
		Select transpStatus = new Select(statusSelectBox);
		transpStatus.selectByVisibleText("ACTIVE");
		BrowserResolution.scrollDown(driver);
		WaitLibrary.waitForAngular(driver);
		
		//Submit Button
		WebElement submitTransport = OnboardingData.submitBtn(driver);
		WaitLibrary.waitForElementToBeClickable(driver, submitTransport, Constants.avg_explicit).click();
		WaitLibrary.waitForAngular(driver);
		
		// Approver Transport
		CommonMethods.approveStatement(driver, "SWIFTOUT_MOP_SIMULATED");
		
		WaitLibrary.waitForAngular(driver);
		
		log.info("Click on Grid View");
		WebElement gridView1 = OnboardingData.gridView(driver);
		String gridViewClass1= WaitLibrary.waitForElementToBeVisible(driver, gridView1, Constants.avg_explicit).getAttribute("class");
					
		if (gridViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
			WaitLibrary.waitForElementToBeClickable(driver, gridView1, Constants.avg_explicit).click();					
		}
		
		Thread.sleep(Constants.short_sleep);
		WaitLibrary.waitForAngular(driver);
		CommonMethods.getTransport(driver, "SWIFTOUT_MOP_SIMULATED");
		
		WebElement getStatus = OnboardingData.getStatusText(driver);
		String OriginalStatus = getStatus.getText();
				
		String ExpStatus = "ACTIVE";
		if (ExpStatus.equals(OriginalStatus)) {
			((ExtentTest) test).log(LogStatus.PASS, "Transport is Active");
		}
		else {
			((ExtentTest) test).log(LogStatus.FAIL, "Transport is Suspended");
		}		
		
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
	}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}
}
