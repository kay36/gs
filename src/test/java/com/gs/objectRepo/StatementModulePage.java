package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StatementModulePage {
	public static WebElement element = null;

	// Locating Received Instructions Tab
	public static WebElement statementModule(WebDriver driver) {
		By statementModule = By.xpath("//span[contains(text(),'Statement Module')]");
		return driver.findElement(statementModule);
	}

	public static WebElement receivedStatements(WebDriver driver) {
		By receivedStatements = By.xpath("//span[@class='ng-binding'][contains(text(),'Received Statements')]");
		return driver.findElement(receivedStatements);
//"//span[contains(text(),'Received Statements')]"));
	}
	
	public static WebElement issueInformation(WebDriver driver) {
		By issueInformation = By.xpath("//i[contains(@class, 'fa fa-exchange')]");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(issueInformation));
		return driver.findElement(issueInformation);
	}
	
	public static WebElement listView(WebDriver driver) {
		By listView = By.xpath("//i[contains(@class, 'fa fa-list fa-fs14')]");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(listView));
		return driver.findElement(listView);
	}

	public static WebElement matchingLogicStatementStatus(WebDriver driver) {
		By matchingLogicStatementStatus = By.xpath("//table[1]/tbody[1]/tr[1]/td[20]");
		return driver.findElement(matchingLogicStatementStatus);
	}

	public static WebElement reconMsgpaymentId(WebDriver driver) {
		By reconMsgpaymentId = By.xpath("//td[contains(text(),'UNMATCHED')]/preceding::td[3]/a");
		return driver.findElement(reconMsgpaymentId);
	}

	public static WebElement reconMsgpaymentIdFoRNotMatch(WebDriver driver) {
		By reconMsgpaymentIdFoRNotMatch = By.xpath("//table[1]/tbody[1]/tr[1]/td[17]/a[1]");
		return driver.findElement(reconMsgpaymentIdFoRNotMatch);
	}

	public static WebElement forceComplete(WebDriver driver) {
		By forceComplete = By.xpath("//button[@class='btn btn-sm pull-right ng-scope']");
		return driver.findElement(forceComplete);
	}

	public static void getSelectAccount(WebDriver driver, String text) {
		element = driver.findElement(By.xpath("//select[@id='Account']"));
		Select sel = new Select(element);
		sel.selectByVisibleText(text);

	}

	public static WebElement submit(WebDriver driver) {
		By submit = By.xpath("//button[@class='btn btn-success btnStyle pull-right ng-scope']");
		return driver.findElement(submit);
	}

	public static WebElement cancel(WebDriver driver) {
		By cancel = By.xpath("//button[@class='btn btn-success btnStyle pull-right ng-binding']");
		return driver.findElement(cancel);
	}

	public static WebElement getIssueInformationTab(WebDriver driver) {
		By getIssueInformationTab = By.xpath("//b[contains(text(),'Issue Information')]");
		return driver.findElement(getIssueInformationTab);
	}

	public static WebElement refreshstatement(WebDriver driver) {
		By refreshstatement = By.xpath("//i[@class='fa fa-refresh fa-fs14']");
		return driver.findElement(refreshstatement);
	}

	public static WebElement selectStatementByName(WebDriver driver, String sName) {
		By refreshstatement = By.xpath("(//li[contains(text(),'" + sName + "')])[1]");
		return driver.findElement(refreshstatement);
	}

	public static WebElement originalPaymentMessageId(WebDriver driver) {
		By originalPaymentMessageId = By.xpath("//*[@id='tab1']/tbody/tr/td[19]/a");
		return driver.findElement(originalPaymentMessageId);
	}
	
	public static WebElement reconPaymentMessageId(WebDriver driver) {
		By originalPaymentMessageId = By.xpath("//*[@id='tab1']/tbody/tr/td[17]/a");
		return driver.findElement(originalPaymentMessageId);
	}

	public static WebElement statementMatchingStatus(WebDriver driver, String sName) {
		By refreshstatement = By.xpath("//a[contains(text(),'" + sName + "')]/../following-sibling::td[last()-1]");
		return driver.findElement(refreshstatement);
	}
	
	public static WebElement getStatementStatus(WebDriver driver, String fileName) {
		By getStatementStatusName = By.xpath("//li[contains(text(),'" + fileName + "')]/following::span[@tooltip='Instruction Status']");
		return driver.findElement(getStatementStatusName);
	}
	
	public static WebElement getInsIdByFileName(WebDriver driver, String fileName) {
		By getInsIdByFileName = By.xpath("//li[contains(text(),'" + fileName + "')]/preceding::li[@tooltip='Instruction ID'][1]");
		return driver.findElement(getInsIdByFileName);
	}

	public static WebElement viewAction(WebDriver driver, String sName) {
		By viewAction = By.xpath("//a[contains(text(),'" + sName + "')]/../following-sibling::td[last()]");
		return driver.findElement(viewAction);
	}

	public static WebElement btnCloseViewAction(WebDriver driver) {
		By btnCloseViewAction = By.xpath("//div[@id='rawData1_0']//button[@class='close'][contains(text(),'�')]");
		return driver.findElement(btnCloseViewAction);
	}

	public static WebElement btnListView(WebDriver driver) {
		By btnListView = By.xpath("//div/button[@tooltip='List View']");
		return driver.findElement(btnListView);
	}

	public static WebElement receivedStatementSearchBox(WebDriver driver) {
		By btnListView = By.xpath("//*[@id='searchBox']");
		return driver.findElement(btnListView);
	}

	public static WebElement btnSearch(WebDriver driver) {
		By btnListView = By.xpath("//span[@id='SearchFontIcon']");
		return driver.findElement(btnListView);
	}

}
