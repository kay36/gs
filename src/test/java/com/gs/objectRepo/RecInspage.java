package com.gs.objectRepo;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gs.utilities.WaitLibrary;

public class RecInspage {
	public static WebElement element = null;
	
	public static WebElement showAdvancedSearchButton(WebDriver driver) {
		By adSearchBtn = By.xpath("//span[@ng-if='advancedSearch'][contains(.,'Show Advanced Search')]");
		element = driver.findElement(adSearchBtn);
		return element;
	}
	
	public static WebElement origInstId(WebDriver driver) {
		By origInstId = By.xpath("//input[contains(@placeholder,'Original Instruction ID')]");
		WaitLibrary.waitForElementPresence(driver, 30, origInstId);
		element = driver.findElement(origInstId);
		WaitLibrary.waitForElementToBeVisible(driver, element, 20);
		return element;
	}
	
	public static WebElement searchButton(WebDriver driver) {
		By searchButton = By.xpath("//button[@id='AdSearchBtn']");
		WaitLibrary.waitForElementPresence(driver, 30, searchButton);
		return driver.findElement(searchButton);
	}
	
	public static WebElement getReportAttachMsgStatus(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//td[contains(text(),'COMPLETED')]"));
		return element;
	}
	
	public static WebElement getRFI_FedAdditionalInfo(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//textarea[@id='Fed Additional Info']"));
		return element;
	}
	
	public static void getReceiver(WebDriver driver, String option ) throws InterruptedException
	{
		element = driver.findElement(By.xpath("//select[@id='Receiver']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(option);
		 //sel.selectByVisibleText("S_BIC - CITIUS33XXX");
	}
	
	public static WebElement getReconId(WebDriver driver)
	{
		element= driver.findElement(By.xpath("//td[contains(text(),'Recon Credit Transfer')]//preceding::td[1]/a"));
		return element;
	}
	
	public static WebElement searchIcon(WebDriver driver)
	{
		By recInsTab = By.xpath("//span[@id='SearchFontIcon']//i[@class='fa fa-search']");
		return driver.findElement(recInsTab);
	}
	
	public static WebElement searchBox(WebDriver driver)
	{
		By recInsTab = By.xpath("//input[@id='searchBox']");
		return driver.findElement(recInsTab);
	}

	public static WebElement getAdditionalInfROF(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//textarea[@id='Additional Information']"));
		return element;
	}
	
	public static void getReasonCodes(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//select[@id='Reason Codes']"));
		Select sel = new Select(element);
	    sel.selectByIndex(5);
	}
	
	public static void getRequestType(WebDriver driver, String option) throws InterruptedException
	{
		element = driver.findElement(By.xpath("//select[@id='Request Type']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(option);
	}
	
	public static WebElement generateROF(WebDriver driver)
	{
		element= driver.findElement(By.xpath("//span[contains(text(),'GenerateROF')]"));
		return element;
	}
	
//Locating Received Instructions Tab
	public static WebElement recInsTab(WebDriver driver) {
		By recInsTab = By.xpath("//span[contains(text(),'Received Instructions')]");
		WaitLibrary.waitForElementPresence(driver, 30, recInsTab);
		return driver.findElement(recInsTab);
	}

	public static WebElement listView(WebDriver driver) {
		By listView = By.xpath("//i[contains(@class, 'fa fa-list')]");
		WaitLibrary.waitForElementPresence(driver, 30, listView);
		return driver.findElement(listView);
	}

	public static WebElement outputInstructionId(WebDriver driver) {
		By outputInstructionId = By.xpath("//span//a[contains(text(),'LTERMID')]");
		return driver.findElement(outputInstructionId);
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'Output')]//following::a[1]"));
	}

//To get first instruction Id 
	public static WebElement getIntructionId(WebDriver driver) {
		By getIntructionId = By.xpath(
				"html/body/div[2]/div/div[3]/div/div[2]/div/div[4]/div[6]/div[1]/div/div[1]/div/div[1]/ul/li[1]");
		return driver.findElement(getIntructionId);
	}

//To get file name from the instruction Id
	public static WebElement getFileName(WebDriver driver) {
		By getFileName = By.xpath(
				"html/body/div[2]/div/div[3]/div/div[2]/div/div[4]/div[6]/div[1]/div/div[1]/div/div[1]/ul/li[2]");
		return driver.findElement(getFileName);
	}

//To get file name by passing instruction Id dynamically
	public static String getFileNamefrmInsId(WebDriver driver, String InsId) {
		WebElement element = driver.findElement(By.xpath("//li[contains(text(),'" + InsId + "')]//following::li[1]"));
		String fName = element.getText();
		// String fName1 = element.getAttribute("value");
		// System.out.println("value"+fName1);
		return fName;
	}

//To get all instruction Id's from the Received instruction section
	public static List getAllInsIdFromRecIns(WebDriver driver) {
		By getAllInsIdFromRecIns = By.xpath("//li[@title='Instruction ID']");
		return (List) driver.findElement(getAllInsIdFromRecIns);
	}

	public static WebElement getPaymentIdReturned(WebDriver driver) {
		By getPaymentIdReturned = By.xpath("//tbody[1]/tr[1]/td[1]/span[1]");
		return driver.findElement(getPaymentIdReturned);
	}

	public static WebElement getInsType(WebDriver driver) {
		By getInsType = By.xpath("//li[@tooltip='Instruction Type']");
		return driver.findElement(getInsType);
	}

	public static WebElement getTransportName(WebDriver driver) {
		By getTransportName = By.xpath("//li[@tooltip='Transport Name']");
		return driver.findElement(getTransportName);
	}

	public static WebElement getTransportNameStatusInRecInsPage(WebDriver driver) {
		By getTransportNameStatusInRecInsPage = By.xpath("//span[@tooltip='Instruction Status']");
		return driver.findElement(getTransportNameStatusInRecInsPage);
	}

	public static WebElement getTransportNameStatusByFileName(WebDriver driver, String fileName) {
		By getTransportNameStatusByFileName = By.xpath("//li[contains(text(),'" + fileName + "')]/following::span[@tooltip='Instruction Status']");
		WaitLibrary.waitForElementPresence(driver, 30, getTransportNameStatusByFileName);
		return driver.findElement(getTransportNameStatusByFileName);
	}

	public static WebElement getTransportNameStatus(WebDriver driver) {
		By getTransportNameStatus = By.xpath("//span[@class='caption-subject bold ng-binding']");
		return driver.findElement(getTransportNameStatus);
	}

	public static WebElement getInsIDS(WebDriver driver) {
		By getInsIDS = By.xpath("//li[@tooltip='Instruction ID']");
		return driver.findElement(getInsIDS);
	}

	public static WebElement getInsIdByTransportName(WebDriver driver, String transName) {
		By getInsIdByTransportName = By.xpath("//li[contains(text(),'" + transName + "')]/preceding::li[@tooltip='Instruction ID'][1]");
		WaitLibrary.waitForElementPresence(driver, 30, getInsIdByTransportName);
		return driver.findElement(getInsIdByTransportName);
		/*
		 * String
		 * ramana="//li[Contains(text(),'"+recInspage.getTransportName(driver).getText()
		 * +"')]/preceding::li[@tooltip='Instruction ID']";
		 * System.out.println("ramana:"+ramana); element=
		 * driver.findElement(By.xpath(ramana));
		 */
	}

	public static WebElement getInsIdByValue(WebDriver driver, String instructionIDValue) {
		By getInsIdValue = By.xpath("//li[contains(text(),'" + instructionIDValue + "')]");
		return driver.findElement(getInsIdValue);
		/*
		 * String
		 * ramana="//li[Contains(text(),'"+recInspage.getTransportName(driver).getText()
		 * +"')]/preceding::li[@tooltip='Instruction ID']";
		 * System.out.println("ramana:"+ramana); element=
		 * driver.findElement(By.xpath(ramana));
		 */
	}

	public static WebElement instructionIdRow(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//table[@class='table table-striped table-bordered table-hover floatThead stickyheader']//td[@class='bold cursorPointer']//a"));
		return element;
	}

	public static WebElement instructionId(WebDriver driver, String insid) {
		element = driver.findElement(By.xpath("//li[contains(text(),'" + insid + "')]"));
		return element;
	}

	public static WebElement parentInstructionId(WebDriver driver, String insid) {
		element = driver.findElement(By.xpath("//td[contains(text(),'" + insid + "')]"));
		return element;
	}

	public static WebElement paymentId(WebDriver driver, String paymentid) {
		element = driver.findElement(By.xpath("//span[contains(text(),'" + paymentid + "')]"));
		return element;
	}

	public static WebElement getPaymentId(WebDriver driver) {
		By getPayId = By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[1]/span");
		WaitLibrary.waitForElementPresence(driver, 30, getPayId);
		element = driver.findElement(getPayId);
		return element;
	}

	public static WebElement getMOP(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[3]"));
		return element;
	}

	public static WebElement getPaymentStatus(WebDriver driver) {

//	element = driver.findElement(By.xpath("(//span[contains(text(),'Status :')]/following-sibling::text()[1])[1]"));
		element = driver.findElement(By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[9]"));
//	element = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div[8]/text()[4]"));

		return element;
	}

	public static WebElement getPaymentCommonStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Status :')]/following-sibling::text()[1])[1]"));
		return element;
	}

//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[9]

	public static WebElement getAccountPostingTab(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'Account Posting')]"));
		return element;
	}

	public static WebElement getLinkedMessagesTab(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(),'Linked Messages')]"));
		return element;
	}
	
	public static WebElement getPaymentEventLogTable(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//div[@id='tableExport']"));
		return element;
	}
	

	public static WebElement getpaymentAction(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//span[contains(text(),'PaymentAction')]"));
		return element;
	}
	
	public static WebElement getButtonRepair(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//button[contains(text(),'Repair')]"));
		return element;
	}
	
	public static WebElement getCreditorSection(WebDriver driver) 
	{
//		element = driver.findElement(By.xpath("//div[@id='Creditor']"));
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[24]/div[1]/div[1]/h4[1]"));
		return element;
	}
//	/html[1]/body[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[24]/div[1]/div[1]/h4[1]
	
	public static WebElement getCreditorAccountNumber(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//div[@id='Creditor']/div[10]/div[1]//input[@name='Account']"));
		return element;
	}
//	/html[1]/body[1]/div[2]/div[1]/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[24]/div[1]/div[1]/h4[1]
	
	
	

	public static WebElement getGenerateROF(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'GenerateROF')]"));
		return element;

	}

	public static void getSelectAction(WebDriver driver, String text) throws InterruptedException {
		element = driver.findElement(By.xpath("//select[@id='Action']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(text);

	}

	public static void getSelectRequestType(WebDriver driver, String text) throws InterruptedException {
		element = driver.findElement(By.xpath("//select[@id='Request Type']"));
		Select sel = new Select(element);
		Thread.sleep(2000);
		sel.selectByVisibleText(text);

	}

	public static void getReasonCode(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Reason Code']"));
		Select sel = new Select(element);
		sel.selectByIndex(8);

	}

	public static void getROFReasonCode(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Reason Codes']"));
		Select sel = new Select(element);
		sel.selectByIndex(5);

	}

	public static void getAttachMsgId(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Attach Message Id']"));
		Select sel = new Select(element);
		sel.selectByIndex(8);

	}

	public static void getFieldCausingREJT(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Field Causing REJT/RETN']"));
		Select sel = new Select(element);
		sel.selectByIndex(8);
	}

	
	

	public static WebElement getAdditionalInf(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Additional Information']"));
		return element;
	}

	public static WebElement getAdditionalInfoForROF(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='Additional Information']"));
		return element;
	}

	public static WebElement getAdditionalInfoForRFI(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='Fed Additional Info']"));
		return element;
	}

	public static WebElement getSubmit(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[text()='Submit ']"));
		return element;
	}

	public static WebElement getCancel(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[text()='Cancel']"));
		return element;
	}
	
	public static WebElement getButtonForceComplete(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//span[contains(text(),'ForceComplete')]"));
		return element;
	}
	
	public static void selectAccount(WebDriver driver) 
	{
		element = driver.findElement(By.xpath("//select[@name='Account']"));
		Select sel = new Select(element);
		sel.selectByIndex(2);
	}
	//select[@name='Account']
	
	public static WebElement systemInteractionBtn(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(text(), 'System Interaction')]"));
		return element;
	}

	public static WebElement viewText(WebDriver driver, int i) {
		element = driver.findElement(By.xpath(".//*[@id='rawData1_" + i + "']/div/div/div[2]/pre"));
		return element;
	}

	public static WebElement viewTextgetTitle(WebDriver driver, int i) {
		element = driver.findElement(By.xpath("//*[@id='rawData1_" + i + "']/div/div/div[1]/span"));
		return element;
	}

	public static WebElement forceRelease(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'ForceRelease')]"));
		return element;
	}

	public static WebElement refresh(WebDriver driver) {
		element = driver.findElement(By.xpath("//i[@class='fa fa-refresh']"));
		return element;
	}
	
	public static WebElement refreshReceivedInstrction(WebDriver driver) {
		element = driver.findElement(By.xpath("//i[@class='fa fa-refresh fa-fs14']"));
		return element;
	}

	public static WebElement getReturnPaymentId(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//td[contains(text(),'Credit Transfer - Return of Funds')]//preceding::td[1]/a"));
		return element;
	}
	
	public static WebElement getResponseToReqForPayment(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//td[contains(text(),'Response to Request for Payment')]//preceding::td[1]/a"));
		return element;
	}
	
	public static WebElement getReconPaymentId(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//td[contains(text(),'Recon Credit Transfer')]//preceding::td[1]/a"));
		return element;
	}

	public static WebElement getStatementId(WebDriver driver) {
		element = driver.findElement(By.xpath("//td[contains(text(),'statement')]//preceding::td[1]/a"));
		return element;
	}
	
	public static WebElement getOriginalPaymentId(WebDriver driver) {
		element = driver.findElement(By.xpath("//td[contains(text(),'Credit Transfer')]//preceding::td[1]/a"));
		return element;
	}

	public static WebElement getLinkedMessageFunction(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='tab6']/tbody/tr[1]/td[2]"));
		return element;
	}

	public static WebElement acceptPayment(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Accept Payment')]"));
		return element;
	}

	public static WebElement rejectPayment(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Reject Payment')]"));
		return element;
	}

	public static WebElement matchingFileInstructionId(WebDriver driver) {
		element = driver.findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[1]"));
		return element;
	}

	public static WebElement rootPaymentId(WebDriver driver) {
		element = driver.findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[6]/span[1]"));
		return element;
	}

	public static WebElement acceptInstruction(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Accept Instruction')]"));
		return element;
	}

	public static WebElement rejectInstruction(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Reject Instruction')]"));
		return element;
	}

	public static WebElement forceStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Force Status')]"));
		return element;
	}

	public static WebElement getInstructionIdForParentStmt(WebDriver driver, String NOS) {
		element = driver.findElement(By.xpath("//li[contains(text(),'ParentStmt_" + NOS + "')]/preceding::li[1]"));
		return element;
	}

	public static WebElement viewBtn1(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@id='btn_1']"));
		return element;
	}

	public static WebElement gridBtn1(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@id='btn_2']"));
		return element;
	}

	public static WebElement acceptReturn(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Accept Return')]"));
		return element;
	}

	public static WebElement linkedMsgOrgPyID(WebDriver driver) {
		element = driver.findElement(By.xpath("//table[1]/tbody[1]/tr[1]/td[1]/a"));
		return element;
	}

	public static WebElement acceptReject(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Accept Reject')]"));
		return element;
	}

	public static void getAttachedMsgID(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Attach Message Id']"));
		Select sel = new Select(element);
		sel.selectByIndex(1);

	}

	public static void getsetAdditionalInf(WebDriver driver, String text) {
		element = driver.findElement(By.xpath("//input[@id='Additional Information']"));
		element.sendKeys(text);

	}

	public static WebElement generateRFI(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'GenerateRFI')]"));
		return element;
	}

	public static void getReceiver(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Receiver']"));
		Select sel = new Select(element);
		sel.selectByIndex(1);
		// sel.selectByVisibleText("S_BIC - CITIUS33XXX");

	}

	public static void getTemplateName(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Template Name']"));
		Select sel = new Select(element);
		sel.selectByIndex(2);

	}

	/*
	 * public static void getAcceptORrejectROFRequest(WebDriver driver, String text)
	 * { element = driver.findElement(By.xpath("//select[@id='Template Name']"));
	 * Select sel = new Select(element); sel.selectByVisibleText(text);
	 * 
	 * }
	 */
	public static void getAcceptORrejectROFRequest(WebDriver driver, String text) {
		element = driver.findElement(By.xpath("//select[@id='Accept Or Reject ROF Request']"));
		Select sel = new Select(element);
		sel.selectByVisibleText(text);

	}

	public static void getReasonOfRejection(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Reason of Rejection']"));
		Select sel = new Select(element);
		sel.deselectByIndex(2);
	}

	public static WebElement getAdditionalDescription(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Additional Description']"));
		return element;
	}

	public static WebElement getAdditionalInformation(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Additional Information']"));
		return element;
	}

	public static WebElement generateRRFI(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'GenerateRRFI')]"));
		return element;
	}

	public static WebElement generateRROF(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'GenerateRROF')]"));
		return element;
	}

	public static void getsetAdditionalInfgenerateRRFI(WebDriver driver, String text) {
		element = driver.findElement(By.xpath("//textarea[@id='Additional Info']"));
		element.sendKeys(text);

	}

	public static void getReceiver103(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='Receiver']"));
		Select sel = new Select(element);
		sel.selectByIndex(2);

	}

	public static WebElement repair(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@class='btn btnStyle pull-right']"));
		return element;
	}

	public static WebElement creditor(WebDriver driver) {
		element = driver.findElement(By.xpath("//h4[text()='Creditor']"));
		return element;
	}

	public static WebElement creditorAgent(WebDriver driver) {
		element = driver.findElement(By.xpath("//h4[text()='Creditor Agent']"));
		return element;
	}

	public static WebElement NameCreditor(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='Creditor']//input[@placeholder='Please Enter Name']"));
		return element;
	}

	public static WebElement bicCreditor(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='Creditor']//input[@placeholder='Please Enter BIC']"));
		return element;
	}

	public static WebElement clearingSchemaCodeCreditor(WebDriver driver) {
		element = driver
				.findElement(By.xpath("//div[@id='Creditor']//input[@placeholder='Please Enter Clearing Scheme ID']"));
		return element;
	}

	public static WebElement accountCreitor(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='Creditor']//input[@placeholder='Please Enter Account']"));
		return element;
	}

	public static WebElement submitCreditor(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Submit')]"));
		return element;
	}

	public static WebElement creditorAgentAccount(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='CreditorAgent']//input[@name='Account']"));
		return element;
	}

	public static WebElement creditorAgentBIC(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@id='CreditorAgent']//input[@placeholder='Please Enter BIC']"));
		return element;
	}

	public static WebElement Statementrefresh(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@class='btn-trans']"));
		return element;
	}

	public static WebElement GridButtonView(WebDriver driver) {
		element = driver.findElement(By.xpath("//i[contains(@class, 'fa fa-table')]"));
		return element;
	}

	public static WebElement searchInstructionWithId(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='searchBox']"));
		return element;
	}

	public static WebElement clickSearchInstructionButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@id='SearchFontIcon']//i[@class='fa fa-search']"));
		return element;
	}

	public static WebElement getLTERMId(WebDriver driver, String sPaymentId, String sStatus) {
		element = driver.findElement(By.xpath("//div[@id='tab_" + sPaymentId + "_31']//tr[last()]//td//p[text()='"
				+ sStatus + "']/../preceding-sibling::td[last()]//a"));
		return element;
	}

	public static WebElement getStatusWaitInRROF(WebDriver driver) {
		element = driver.findElement(By.xpath(".//tbody//tr[last()-1]//td[last()]//p"));
		return element;
	}
	
	public static WebElement selectInstrcutionByName(WebDriver driver, String sName) {
		By selectInstrcutionByName = By.xpath("(//li[contains(text(),'" + sName + "')])[1]");
		return driver.findElement(selectInstrcutionByName);
	}
	
//	public static WebElement getTopPaymentStatus(WebDriver driver) {
//		element = driver.findElement(By.xpath("//span[contains(@tooltip,'Payment Status')]/child::span"));
//		return element;
//	}
	
	//---------------------------------------Deepak--------------------------------------------------------------------
	
//	public static WebElement advancedSerachedResultPaymentId(WebDriver driver) {
//	    return driver.findElement(By.xpath("//div[@class='listView FixHead dataGroupsScroll']//tr[1]//td[1]/span[@class='ng-binding ng-scope']"));
//	}
	
	
	public static WebElement getPaymentStatus1(WebDriver driver)
	{
		
		element = driver.findElement(By.xpath("//span[@class='caption-subject bold ng-binding']"));

		return element;
	}
	
//	public static WebElement SWIFTRequestForInformationStatus_OUT(WebDriver driver) {
//		element = driver.findElement(
//				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')] /following-sibling::td[last()])[2]/p"));
//		return element;
//	}
	
//	public static WebElement SWIFTRequestForInformationStatus_IN(WebDriver driver) {
//		element = driver.findElement(
//				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')] /following-sibling::td[last()])[1]/p"));
//		return element;
//	}
	
	
//	public static WebElement SWIFTRequestForInformationRFIReference(WebDriver driver) {
//		element = driver.findElement(
//				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')] /following-sibling::td[last()])[2]/p/ancestor::tr/td[1]/a"));
//		return element;
//	}
	
	public static WebElement getPaymentStatus2(WebDriver driver) {


		element = driver.findElement(By.xpath("//span[@class='caption-subject bold ng-binding' and text()='ACCEPTED']"));

		return element;
	}
	
	public static WebElement receivedInstruction(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//*[@class='ng-binding' and text()='Received Instructions']"));
		return element;
	}
	
	
	public static WebElement WaitReturnMatchActionInstructionId(WebDriver driver)
	{
		element = driver.findElement(By.xpath("//*[@class='caption-subject bold uppercase ng-binding']"));
		return element;
	}
	
	
	public static WebElement linkedpaymentIdClick(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[@class='caption-subject bold uppercase ng-binding']"));


		return element;
	}
	
	public static WebElement waitReturnMatchActionStatus(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[@class='caption-subject bold ng-binding' and text() ='WAIT_RETURN_MATCHED_ACTION']"));


		return element;
	}
	
	
	public static WebElement acceptReturnButton(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[text()='Accept Return']"));


		return element;
	}
	
	public static WebElement receivedInstructionInstructionIdInputBox(WebDriver driver) {


		element = driver.findElement(By.xpath("//input[@class='searchBox ng-pristine ng-untouched ng-valid ng-empty']"));


		return element;
	}
	
	public static WebElement receivedInstructionInstructionIdSearch(WebDriver driver) {


		element = driver.findElement(By.xpath("(//i[@class='fa fa-search'])[1]"));


		return element;
	}
	
	public static WebElement receivedInstructionSereachMessage(WebDriver driver) {


		element = driver.findElement(By.xpath("(//a[@class='ng-binding'])[3]"));


		return element;
	}
	
	public static WebElement receivedInstructionLinkedPayment(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[@class='caption-subject bold uppercase ng-binding']"));


		return element;
	}

	public static WebElement returnPaymentStatus(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[@class='caption-subject bold ng-binding']"));


		return element;
	}
	
	public static WebElement linkedMessageTab(WebDriver driver) {


		element = driver.findElement(By.xpath("//*[text()='Linked Messages']"));

		return element;
	}
	
	public static WebElement linkMessagePaymentClick(WebDriver driver) {


		element = driver.findElement(By.xpath("(//a[@class='bold cursorPointer ng-binding'])[6]"));


		return element;
	}
	
	

	public static WebElement advancedSerachedResultPaymentId(WebDriver driver) {
		return driver.findElement(By.xpath("//div[@class='listView FixHead dataGroupsScroll']//tr[1]//td[1]/span[@class='ng-binding ng-scope']"));
	}
	
	public static WebElement getTopPaymentStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='caption-subject bold ng-binding']"));
		return element;
	}
	
	public static WebElement getAcceptReturnButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[text()='Accept Return']"));
		return element;
	}
	
	public static WebElement getAdvSearch_OrigInstrIdInput(WebDriver driver) {
		element = driver.findElement(By.xpath("//label[text()='Original Instruction ID']/parent::div//input"));
		return element;
	}
	
	public static WebElement getAdvSearch_Result(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='listView FixHead dataGroupsScroll']//tr/td[2]"));
		return element;
	}
	
	public static WebElement getOriginalPaymentStatusWithInstrId(WebDriver driver,String outputInstrId) {
		element = driver.findElement(By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'"+outputInstrId+"')]//following-sibling::td[last()]"));
		return element;
	}
	
	public static WebElement getSourceorDestinationWithInstrIdAndMsgType(WebDriver driver,String outputInstrId) {
		element = driver.findElement(By.xpath("//tr[@class='ng-scope']//td/a[contains(text(),'"+outputInstrId+"')]/parent::td/following-sibling::td[text()='Payment Instruction']//following-sibling::td[1]"));
		return element;
	}
	
	public static WebElement SWIFTRequestForInformationStatus_IN(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')]/following-sibling::td[text()='IN']//following-sibling::td[last()]"));
		return element;
	}
	
	public static WebElement SWIFTRequestForInformationStatus_OUT(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')]/following-sibling::td[text()='OUT']//following-sibling::td[last()]"));
		return element;
	}
	
	public static WebElement SWIFTRequestForInformationRFIReference(WebDriver driver) {
		element = driver.findElement(
				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSBI_INTERBANK_SWIFTManualRFI')] /following-sibling::td[last()])[2]/p/ancestor::tr/td[1]/a"));
		return element;
	}
	
	public static WebElement getReturnOfFundOriginalPaymentId(WebDriver driver)
	{
	    element= driver.findElement(By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'FEDWIRE')]/parent::tr/td[1]/span"));
	    return element;
	}
	
	public static WebElement getOriginalPayment_TransInfo(WebDriver driver)
	{
	    element= driver.findElement(By.xpath("//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'FED_IN') ] /following-sibling::td[last()]"));
	    return element;
	}
	
	public static WebElement getTopInstructionStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(@tooltip,'Instruction Status')]"));
		return element;
	}

	
	public static WebElement ExtCommStatusForWORF(WebDriver driver) {
		element = driver.findElement(
				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSFEDWIRE_INTERBANK_FedWireSVCROF_Tr')] /following-sibling::td[last()])[2]/p"));
		return element;
	}
	
	
	public static WebElement ExtCommStatusForCOMP(WebDriver driver) {
		element = driver.findElement(
				By.xpath("(//tr[@class='ng-scope']//td[@class='ng-binding'][contains(text(),'GSFEDWIRE_INTERBANK_FedWireSVCROF_Tr')] /following-sibling::td[last()])[2]/p"));
		return element;
	}

//	public static WebElement maskButton(WebDriver driver) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	public static WebElement maskButton(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Unmask')]"));
		return element;
	}
	
	
	
   //----------------------------------------End---------------------------------------------------------------------------------------

}
