package com.gs.objectRepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login1 
{
	WebDriver driver;
	
	
	public Login1 (WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")
	WebElement username;
	
	@FindBy(id="password")
	WebElement password;
	
	@FindBy(xpath="//button[contains(text(),'Login')]")
	WebElement loginButton;
	
	public WebElement getUsername()
	{
		System.out.println("username");
		return username;
	}
	
	public WebElement getPassword()
	{
		System.out.println("password");

		return password;
	}
	
	public WebElement getLoginButton()
	{
		System.out.println("loginButton");

		return loginButton;
	}
	

}
