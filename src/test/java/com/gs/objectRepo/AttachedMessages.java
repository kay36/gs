package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AttachedMessages 
{
	
	public static WebElement element=null;


	//Locating Received Instructions Tab
	public static WebElement attachedMessagesTab(WebDriver driver)
	{
		By attachedMessagesTab = By.xpath("//span[contains(text(),'Attached Messages')]");
		return driver.findElement(attachedMessagesTab);
	}
 
	public static WebElement getMessageID(WebDriver driver)
	{
		By attachedMessagesTab = By.xpath("//div[contains(@id,'tab')]//tbody[1]/tr[1]/td[2]");
		return driver.findElement(attachedMessagesTab);
	}
	
	public static WebElement fedReportRequest(WebDriver driver)
	{
		By fedReportRequest = By.xpath("//tr[2]//td[9]//p[1]");
		return driver.findElement(fedReportRequest);
	}
	
	public static WebElement fedReportResponse(WebDriver driver)
	{
		By fedReportResponse = By.xpath("//tr[last()]//td[9]//p[1]");
		return driver.findElement(fedReportResponse);
	}
	
	
	
	public static WebElement viewfeDReportResponse(WebDriver driver)
	{
		By fedReportResponse = By.xpath("//div[@class='modal fade in']//span");
		return driver.findElement(fedReportResponse);
	}
	
	public static WebElement viewfeDReportResponse_GIF100(WebDriver driver)
	{
		By viewfeDReportResponse_GIF100 = By.xpath("//pre[contains(text(),'<?xml version=\"1.0\" encoding=\"UTF-8\"?>')]");
		return driver.findElement(viewfeDReportResponse_GIF100);
	}
	
	public static WebElement viewButton(WebDriver driver)
	{
		By viewButton = By.xpath("//span[contains(text(),'View')]");
		return driver.findElement(viewButton);
	}
	
	public static WebElement viewButton1(WebDriver driver)
	{
		By viewButton1 = By.xpath("//tr[2]//td[8]//span[1]//span[1]");
		return driver.findElement(viewButton1);
	}
	
	public static WebElement gif100ViewResponseButton(WebDriver driver)
	{
		By gif100ViewResponseButton = By.xpath("//table[@id='tab2']//span[contains(text(),'View')]");
		return driver.findElement(gif100ViewResponseButton);
	}
	
	public static WebElement closeGif100ViewResponseButton(WebDriver driver)
	{
		By closeGif100ViewResponseButton = By.xpath("//div[@id='rawData1_0']//button[@class='close'][contains(text(),'�')]");
		return driver.findElement(closeGif100ViewResponseButton);
	}
	
	
	
}
