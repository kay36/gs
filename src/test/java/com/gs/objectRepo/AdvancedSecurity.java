package com.gs.objectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdvancedSecurity 
{
	static By AdvancedButton = By.id("details-button");
	static By clickonproceed = By.linkText("Proceed to ec2-13-235-44-74.ap-south-1.compute.amazonaws.com (unsafe)");

	
	public static WebElement AdvancedButton(WebDriver driver)
	{
		return driver.findElement(AdvancedButton);
	}
	
	public static WebElement clickonproceed(WebDriver driver)
	{
		return driver.findElement(clickonproceed);
	}
}
