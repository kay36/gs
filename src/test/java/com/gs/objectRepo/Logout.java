package com.gs.objectRepo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.WaitLibrary;

public class Logout 
{
	public static WebElement userprofileBtn(WebDriver driver,String userName)
	{
		By userprofileBtn = By.xpath("//span[@class='namecls ng-binding'][.='"+userName+"']");
		WaitLibrary.waitForElementPresence(driver, 30, userprofileBtn);
		return driver.findElement(userprofileBtn);
	}
	
	public static WebElement logoutBtn(WebDriver driver)
	{
		By logoutBtn = By.xpath("//span[@class='namecls ng-binding'][contains(text(),'admin')]");
		WaitLibrary.waitForElementPresence(driver, 30, logoutBtn);
		return driver.findElement(logoutBtn);
	}
	public static WebElement userlogo(WebDriver driver)
	{
		By userlogo = By.xpath("//img[@class='img-circle userImg']");
		return driver.findElement(userlogo);
	}

	public static WebElement logOut(WebDriver driver)
	{
		By logOut = By.xpath("//a[normalize-space() ='Log Out']");
		WaitLibrary.waitForElementPresence(driver, 30, logOut);
		return driver.findElement(logOut);
	}
	
}
