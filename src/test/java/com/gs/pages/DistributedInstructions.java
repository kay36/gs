package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class DistributedInstructions {
	
	public static WebElement element = null;
	public static By distInsData = ByAngular.repeater("data in datas");
	public static By outputBatchRepeater = ByAngular.repeater("batchInfo in batchInfodata");
	public static By transactionInfo = ByAngular.repeater("transInfo in transInfoAudit");
	public static By batchInfo = ByAngular.repeater("BData in Batchinfo");
	
	//Get List View path
	public static WebElement gridView(WebDriver driver) {
		By gridView = By.xpath("//*[@id='btn_2']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, gridView);
		return driver.findElement(gridView);
	}
	
	//search box Distributed Instruction ID
	public static WebElement searchWithDistInsID(WebDriver driver) {
		By searchBox = By.xpath("//input[contains(@ng-model,'uorVal')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchBox);
		return driver.findElement(searchBox);
	}
	
	//Get Distribution Table data elements
	public static  List<WebElement> distDataList(WebDriver driver) {
		return driver.findElements(distInsData);
	}
	
	// Get Distribution instruction data value
	public static WebElement distInstData(WebDriver driver, int row, int col) {
		By distInData = ((ByAngularRepeater) distInsData).row(row);
		WebElement elem = driver.findElement(distInData);
		By dat = ByAngular.repeater("val in sortMenu");
		return elem.findElements(dat).get(col);
	}
	
	// Get Output Batch information Tab
	public static WebElement outputBatchInformation(WebDriver driver) {
		element = driver.findElement(By.xpath("//b[contains(.,'Output Batch Information')]"));
		return element;
	}
	
	// get output batch Repeater List
	public static  List<WebElement> outputBatchList(WebDriver driver) {
		return driver.findElements(outputBatchRepeater);
	}
	
	// Get output batch Repeater element
	public static WebElement outputBatchData(WebDriver driver, int row, String col) {
		By outputBatchData = ((ByAngularRepeater) outputBatchRepeater).row(row).column(col);
		return driver.findElement(outputBatchData);
	}
	
	//Get Batch Reject Button
	public static WebElement batchRejectBtn(WebDriver driver) {
		By batchRej = By.xpath("//button[contains(.,'Batch Reject')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, batchRej);
		return driver.findElement(batchRej);
	}
	
	//Verifying Batch ID
	public static WebElement verifyBatchPage(WebDriver driver, String pmtId) {
		By payIDHeader = By.xpath("//span[@class='bold ng-binding'][contains(.,'"+pmtId+"')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payIDHeader);
		return driver.findElement(payIDHeader);
	}
	
	// Reject distribution btn
	public static WebElement rejectDistribution(WebDriver driver) {
		By rejectBtn = By.xpath("//input[contains(@value,'Reject')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, rejectBtn);
		return driver.findElement(rejectBtn);
	}
	
	//get status of Instruction
	public static WebElement getInsStatus(WebDriver driver) {
		By status = By.xpath("//span[contains(@data-original-title,'Instruction Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, status);
		element = driver.findElement(status);
		return element;
	}
	
	//Get Transaction Information Tab
		public static WebElement transactionInfo(WebDriver driver) {
			element = driver.findElement(By.xpath("//b[contains(.,'Transaction Information')]"));
			return element;
		}
		
		//Get Payment Id 
		public static WebElement paymentId(WebDriver driver, int row, String col) {
			By OriginalPayRef = ((ByAngularRepeater) transactionInfo).row(row).column(col);
			return driver.findElement(OriginalPayRef);
		}
		
		//Get Batch Information
		public static WebElement batchInfo(WebDriver driver) {
			element = driver.findElement(By.xpath("//b[contains(.,'Batch Information')]"));
			return element;
		}
		
		public static  List<WebElement> batchDataList(WebDriver driver) {
			return driver.findElements(batchInfo);
		}
		
		public static WebElement batchInformation(WebDriver driver, int row, String col) {
			By batchInformation = ((ByAngularRepeater) batchInfo).row(row).column(col);
			return driver.findElement(batchInformation);
		}
		
		// Click Payment with Batch Id in table data
		public static WebElement clickBatchID(WebDriver driver, String batchId) {
			By batchIDElem = By.xpath("//td[contains(.,'"+batchId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, batchIDElem);
			return driver.findElement(batchIDElem);
		}	
		
		//get batch id
		public static WebElement batchId(WebDriver driver, int row, String col) {
			By batchId = ((ByAngularRepeater) batchInfo).row(row).column(col);
			return driver.findElement(batchId);
		}
		
		//Get batch Status
		public static WebElement batchStatus(WebDriver driver) {
			By status = By.xpath("//span[@data-original-title='Batch Status']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, status);
			element = driver.findElement(status);
			return element;
		}
		
		// verify page with batch ID
		public static WebElement verifyPaymentPage(WebDriver driver, String batchId) {
			By payIDHeader = By.xpath("//span[@class='bold ng-binding'][contains(.,'"+batchId+"')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, payIDHeader);
			return driver.findElement(payIDHeader);
		}
}
