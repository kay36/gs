package com.gs.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class AdvancedSearch {
	
	public static WebElement element = null;
	
	// Show Advanced Search
	public static WebElement showAdvancedSearchButton(WebDriver driver) {
		By showAdv = By.xpath("//button[@type='button'][contains(.,'Show Advanced Search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, showAdv);
		return driver.findElement(showAdv);
	}
		
	// source Input Reference code
	public static WebElement sourceInputRef(WebDriver driver) {
		By sourceInpRef = By.xpath("//select[contains(@name,'InputReferenceCode')]//following-sibling::span");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, sourceInpRef);
		return driver.findElement(sourceInpRef);
	}
	
	//source Input Reference code list	
	public static WebElement sourceList(WebDriver driver, String source) {
		By sourceList = By.xpath("//li[text()='" + source + "']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, sourceList);
		return driver.findElement(sourceList);
	}
	
	// adv search button
	public static WebElement searchBtn(WebDriver driver) {
		//By searchBtn = By.xpath("//button[contains(@ng-click,'buildSearch()')]");
		By searchBtn = By.xpath("//button[contains(@ng-click,'submitform()')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchBtn);
		return driver.findElement(searchBtn);
	}
	
	// adv search cancel button
	public static WebElement cancelBtn(WebDriver driver) {
		By cancelBtn = By.xpath("//button[@ng-click='rstAdvancedSearchFlag();showSearchWarning = false;']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, cancelBtn);
		return driver.findElement(cancelBtn);
	}
	
	// clear search button
	public static WebElement clearBtn(WebDriver driver) {
		By clearBtn = By.xpath("//input[contains(@ng-click,'clearSearch()')]");
		//By clearBtn = By.xpath("//input[contains(@id,'searchBtn')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, clearBtn);
		return driver.findElement(clearBtn);
	}
	
	//Entry Date Start
	public static WebElement entryDate(WebDriver driver) {
		By entryDate = By.xpath("//input[@id='EntryDateStart']");
		//By clearBtn = By.xpath("//input[contains(@id,'searchBtn')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, entryDate);
		return driver.findElement(entryDate);
	}
	
	//Entry Date End
		public static WebElement entryDateend(WebDriver driver) {
			By entryDateend = By.xpath("//input[@id='EntryDateEnd']");
			//By clearBtn = By.xpath("//input[contains(@id,'searchBtn')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, entryDateend);
			return driver.findElement(entryDateend);
		}
	
}
