package com.gs.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class OnboardingData {
	
	public static WebElement element = null;
	public static By transporTableData = ByAngular.repeater("office in readData");

	//Clicking Filter Icon
	public static WebElement clickFilterIcon(WebDriver driver) {
		By filterIcon = By.xpath("//button[contains(@data-original-title,'Filter')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, filterIcon);
		return driver.findElement(filterIcon);
	}
	
	//Enter search params
	public static WebElement filterWithKeyword(WebDriver driver) {
		By searchBox = By.xpath("//input[contains(@name,'keywordSearch')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, searchBox);
		return driver.findElement(searchBox);
	}
	
	//Get Approval Table Data
	public static WebElement TransportableData(WebDriver driver, int row) {
		By appTableData = ((ByAngularRepeater) transporTableData).row(0);
		WebElement elem = driver.findElement(appTableData);
		By dat = ByAngular.repeater("field in fieldDetails.Section");
		return elem.findElements(dat).get(0);
	}
	
	//get status for transport
	public static WebElement getStatusText(WebDriver driver) {
		By getStatus1 = By.xpath("//div[@ng-if='parentInput.fieldData.Status']//div");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getStatus1);
		return driver.findElement(getStatus1);
	}
	
	//edit button for transport
	public static WebElement editBtn(WebDriver driver) {
		By editBtn = By.xpath("//a[@tooltip='Edit']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, editBtn);
		return driver.findElement(editBtn);
	}
	// Get Edit Transport Title
	public static WebElement getEditTransportTitle(WebDriver driver) {
		By editTitle = By.xpath("//div[contains(@class,'title__container')]//div[contains(text(),'Edit Transport')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, editTitle);
		return driver.findElement(editTitle);
	}
	
	// Get Status Select Box
	public static WebElement getStatusSelectBox(WebDriver driver) {
		By selectBox = By.xpath("//select[contains(@name,'Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, selectBox);
		return driver.findElement(selectBox);
	}
	
	//Select dropdown status Active for transport
	public static WebElement selectActiveDropdown(WebDriver driver) {
		By selectDropdown = By.xpath("//option[@value='ACTIVE']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, selectDropdown);
		return driver.findElement(selectDropdown);
	}
	
	//Select dropdown status Suspend for transport
	public static WebElement selectSuspendDropdown(WebDriver driver) {
		By selectDropdown = By.xpath("///option[@value='SUSPENDED']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, selectDropdown);
		return driver.findElement(selectDropdown);
	}

	// save changes
	public static WebElement submitBtn(WebDriver driver) {
		By savBtn = By.xpath("//button[contains(@tooltip,'Submit')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, savBtn);
		return driver.findElement(savBtn);
	}
	
	// Transport Clear Cache
	public static WebElement clearCache(WebDriver driver) {
		By clearCache = By.xpath("//button[contains(text(),'Clear Cache')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, clearCache);
		return driver.findElement(clearCache);
	}
	
	//Get List View path
	public static WebElement gridView(WebDriver driver) {
		By gridView = By.xpath("//*[@id=\'btn_2\']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, gridView);
		return driver.findElement(gridView);
	}
	
	
}
