package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class ReceivedStatement {
	
	public static WebElement element = null;
	
	public static By StatementDetails = ByAngular.repeater("payments in filedetailpcd");
	public static By IssueInfo = ByAngular.repeater("errorInfo in ErrorInfodata");
	public static By payID = ByAngular.repeater("payments in filedetailpcd");
	public static By systemInteraction = ByAngular.repeater("msg in cDataBID");
	
	//Clicking Accept Instruction button
	public static WebElement clickAcceptInstruction(WebDriver driver) {
		By accInstbtn = By.xpath("//button[@class='btn btn-sm ng-binding ng-scope'][contains(.,'Accept Instruction')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accInstbtn);
		return driver.findElement(accInstbtn);
	}
	
	// Get instruction ID
	public static WebElement getInstructionID(WebDriver driver) {
		By insId = By.xpath("//span[contains(@tooltip,'Instruction ID')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, insId);
		return driver.findElement(insId);
	}
	
	// Locating Statement Module
	public static WebElement statementModule(WebDriver driver) {
		By statementModule = By.xpath("//span[contains(text(),'Statement Module')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, statementModule);
		return driver.findElement(statementModule);
	}
	
	//Locate Received Statement from side bar
	public static WebElement receivedStatements(WebDriver driver) {
		By receivedStatements = By.xpath("//span[@class='ng-binding'][contains(text(),'Received Statements')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, receivedStatements);
		return driver.findElement(receivedStatements);
	}
	
	// Path of Refresh Icon 
	public static WebElement refreshIcon(WebDriver driver) {
		By refreshIcon = By.xpath("//i[contains(@class,'fa fa-refresh fa-fs14')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, refreshIcon);
		return driver.findElement(refreshIcon);
	}
	
	//Locates List View
	public static WebElement listView(WebDriver driver) {
		By listView = By.xpath("//i[contains(@class, 'fa fa-list fa-fs14')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, listView);
		return driver.findElement(listView);
	}
	
	//Locates Statement Name
	public static WebElement selectStatementByName(WebDriver driver, String sName) {
		By refreshstatement = By.xpath("//li[contains(text(),'" + sName + "')][1]");
		return driver.findElement(refreshstatement);
	}
	
	//Locates the Status of the Statement by file name
	public static WebElement getStatementStatus(WebDriver driver, String fileName) {
		By getStatementStatusName = By.xpath("//li[contains(text(),'" + fileName + "')]/following::span[@title='Instruction Status']");
		return driver.findElement(getStatementStatusName);
	}
	
	//Locates the status of statement
	public static WebElement getStmtStatus(WebDriver driver) {
		By getStmtStatus = By.xpath("//span[contains(@tooltip,'Instruction Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getStmtStatus);
		return driver.findElement(getStmtStatus);
	}
	
	//Locates Instruction Id by file name
	public static WebElement getInsIdByFileName(WebDriver driver, String fileName) {
		By getInsIdByFileName = By.xpath("//li[contains(text(),'" + fileName + "')]/preceding::li[@title='Instruction ID'][1]");
		return driver.findElement(getInsIdByFileName);
	}
	
	//Verifying statement page
	public static WebElement verifyStatementnPage(WebDriver driver, String fileName) {
		//By filNm = By.xpath("//span[@data-original-title='Transport Name'][contains(.,'"+fileName+"')]");
		By filNm = By.xpath("[contains(.,'"+fileName+"')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, filNm);
		return driver.findElement(filNm);
	}//span[@tooltip='Transport Name']
	
	// Get statement details Repeater List
	public static List<WebElement> statementDetailsList(WebDriver driver) {
		return driver.findElements(StatementDetails);
	}
	
	// Get statementDetails Repeater element
	public static WebElement statementDetailsData(WebDriver driver, int row, String col) {
		By statementData = ((ByAngularRepeater) StatementDetails).row(row).column(col);
		return driver.findElement(statementData);
	}
	
	//Gets the issue information Tab
	public static WebElement issueInformation(WebDriver driver) {
		By issueInformation = By.xpath("//i[contains(@class, 'fa fa-exchange')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, issueInformation);
		return driver.findElement(issueInformation);
	}
	
	// Get issue Information Repeater element
	public static WebElement issueInformationData(WebDriver driver, int row, String col) {
		By issueInfoData = ((ByAngularRepeater) IssueInfo).row(row).column(col);
		return driver.findElement(issueInfoData);
	}
	
	//gets original payment path
	public static WebElement OriginalPayment(WebDriver driver) {
		By OriPayment = By.xpath("//*[@id=\"tab1\"]/tbody/tr/td[19]/a");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, OriPayment);
		return driver.findElement(OriPayment);
	}
	
	//get account number
	public static WebElement getAccnumber(WebDriver driver) {
		By accountNumber = By.xpath("//div[contains(@id,'StmtDetailTable')]/div[2]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accountNumber);
		return driver.findElement(accountNumber);
	}
	
	// Get view Button
	public static WebElement viewButton(WebDriver driver, int row, String col) {
		By view = ((ByAngularRepeater) systemInteraction).row(row).column(col);
		return driver.findElement(view);
	}
	
	
	// Pushpa's xpath
	
	//click on force Complete button
		public static WebElement forceComplete(WebDriver driver) {
			By forceComplete = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'ForceComplete')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, forceComplete);
			return driver.findElement(forceComplete);
		}
		//select the force complete account data
		public static WebElement selectForceComplete(WebDriver driver) {
			//By selectforceComplete = By.xpath("//span[@id=\"select2-Account-container\"][@class=\"select2-selection__rendered\"]");
			By selectforceComplete = By.xpath("//span[@id=\"select2-{{fieldlabel}}-container\"][@class=\"select2-selection__rendered\"]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, selectforceComplete);
			return driver.findElement(selectforceComplete);
		}
				
		//click on submit for force complete
		public static WebElement SubmitForceComplete(WebDriver driver) {
			By submit = By.xpath("//div[@class='panel-footer actionWebForm actionwebDisp']//button[@type='submit']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, submit);
			return driver.findElement(submit);
		}
			
		//x-path to enter account data
		public static WebElement accountDataField(WebDriver driver) {
			By accDataField = By.xpath("//input[contains(@role,'searchbox')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accDataField);
			return driver.findElement(accDataField);
		}
				
		// Path of Refresh Icon for recon payment 
		public static WebElement reconRefreshIcon(WebDriver driver) {
			By refreshIcon = By.xpath("//button[@data-original-title='Refresh']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, refreshIcon);
			return driver.findElement(refreshIcon);
		}
				
		public static WebElement closeButton(WebDriver driver) {
			By closebtn = By.xpath("//span[contains(@data-target,'#rawData1_0')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, closebtn);
			return driver.findElement(closebtn);
		}
		
		// selva
		// click on Accept Reject button
		public static WebElement acceptReject(WebDriver driver) {
			By acceptReject = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Accept Reject')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, acceptReject);
			return driver.findElement(acceptReject);
		}

		// click on Ignore Reject button
		public static WebElement ignoreReject(WebDriver driver) {
			By ignoreReject = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Ignore Reject')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, ignoreReject);
			return driver.findElement(ignoreReject);
		}

		// click on Force Status button
		public static WebElement forceStatus(WebDriver driver) {
			By forceStatus = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'Force Status')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, forceStatus);
			return driver.findElement(forceStatus);
		}
		
		// Path of Statement Refresh Icon 
		public static WebElement statementRefreshIcon(WebDriver driver) {
			By statementRefreshIcon = By.xpath("//i[contains(@class,'fa fa-refresh')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, statementRefreshIcon);
			return driver.findElement(statementRefreshIcon);
		}
		
		// click on Force Accept button
		public static WebElement forceAccept(WebDriver driver) {
			By forceAccept = By.xpath("//span[@class='ng-binding ng-scope'][contains(.,'ForceAccept')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, forceAccept);
			return driver.findElement(forceAccept);
		}

	
}
