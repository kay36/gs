package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class AmountLimitProfile {
	
	public static By amountLimitPrifileData = ByAngular.repeater("office in readData");
	
	//Get List View path
	public static WebElement gridView(WebDriver driver) {
		By gridView = By.xpath("//*[@id='btn_2']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, gridView);
		return driver.findElement(gridView);
	}

	// Add New Amount button
	public static WebElement amtAddNew(WebDriver driver) {
		By AmtAddNew = By.xpath("//button[contains(@tooltip,'Add New')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, AmtAddNew);
		return driver.findElement(AmtAddNew);
	}
	
	//amount limit profile code
	public static WebElement amtLimitProfileCode(WebDriver driver) {
		By amtProfileCode = By.xpath("//input[contains(@id,'Amount Limit Profile Code')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileCode);
		return driver.findElement(amtProfileCode);
	}
		
	//amount limit profile currency
	public static WebElement amtLimitProfileCurr(WebDriver driver) {
		By amtProfileCurr = By.xpath("//span[(@id='select2-Currency-container')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileCurr);
		return driver.findElement(amtProfileCurr);
	}
	
	//amount limit profile currency search
	public static WebElement amtLimitProfileCurrSearch(WebDriver driver) {
		By amtProfileCurSearch = By.xpath("//input[(@type='search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileCurSearch);
		return driver.findElement(amtProfileCurSearch);
	}
	
	//Currency list	
	public static WebElement currList(WebDriver driver, String curr) {
		By currList = By.xpath("//li[text()='" + curr + "']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, currList);
		return driver.findElement(currList);
	}
	
	//amount limit profile type
	public static WebElement amtLimitProfileType(WebDriver driver) {
		By amtProfileType = By.xpath("//span[(@id='select2-Amount Limit Type-container')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileType);
		return driver.findElement(amtProfileType);
	}
	
	//amount limit profile type search
	public static WebElement amtLimitProfileTypeSearch(WebDriver driver) {
		By amtProfileTypSearch = By.xpath("//input[(@type='search')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileTypSearch);
		return driver.findElement(amtProfileTypSearch);
	}
	
	//Type list	
	public static WebElement typeList(WebDriver driver) {
		By typeList = By.xpath("//ul[@id='select2-Amount Limit Type-results']/li[text()='Individual']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, typeList);
		return driver.findElement(typeList);
	}

	//amount limit profile max amount
	public static WebElement amtLimitProfileMaxAmt(WebDriver driver) {
		By amtProfileMaxAmt = By.xpath("//input[contains(@id,'Max Amount')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileMaxAmt);
		return driver.findElement(amtProfileMaxAmt);
	}
	
	// Get Status Select Box for amount limit profile
	public static WebElement getStatusAmountProfile(WebDriver driver) {
		By selectBoxAmtProf = By.xpath("//select[contains(@name,'Status')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, selectBoxAmtProf);
		return driver.findElement(selectBoxAmtProf);
	}

	//amount limit profile effective from date
	public static WebElement amtLimitProfileEffDate(WebDriver driver) {
		By amtProfileEffDat = By.xpath("//input[contains(@id,'Effective From Date')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtProfileEffDat);
		return driver.findElement(amtProfileEffDat);
	}
	
	// Add Amount limit profile save button
	public static WebElement addAmountLimitBtn(WebDriver driver) {
		By addAmountLimitBtn = By.xpath("//button[contains(@tooltip,'Submit')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, addAmountLimitBtn);
		return driver.findElement(addAmountLimitBtn);
	}
	
	//get status for amount limit profile
	public static WebElement getStatusText(WebDriver driver) {
		By getStatus1 = By.xpath("//div[@ng-if='parentInput.fieldData.Status']//div");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, getStatus1);
		return driver.findElement(getStatus1);
	}

	//Get Mandate Table data elements
	public static  List<WebElement> amountProfileDataList(WebDriver driver) {
		return driver.findElements(amountLimitPrifileData);
	}
	
	public static WebElement amountProfileData(WebDriver driver, int row, int col) {
		By amountProfileData = ((ByAngularRepeater) amountLimitPrifileData).row(row);
		WebElement elem = driver.findElement(amountProfileData);
		By dat = ByAngular.repeater("field in fieldDetails.Section");
		return elem.findElements(dat).get(col);
	}

}
