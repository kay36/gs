package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class MethodOfPayment {
	
	public static WebElement element = null;
	
	public static By methodOfPmtRepeater = ByAngular.repeater("office in readData");
	public static By methodOfPmtTableDataRepeater = ByAngular.repeater("field in MOPdata");
	
	// Method of payment
	public static WebElement methodOfPmtHeading(WebDriver driver) {
		By methodOfPmtHeading = By.xpath("//h2[contains(.,'Method of Payment')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, methodOfPmtHeading);
		return driver.findElement(methodOfPmtHeading);
	}
	
	// Method of payment last table data
	public static WebElement methodOfPmtTableLastRow(WebDriver driver) {
		By methodOfPmtTableLastRow = By.xpath("//table[contains(@ng-show,'!changeViewFlag')]//tbody//tr[last()]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, methodOfPmtTableLastRow);
		return driver.findElement(methodOfPmtTableLastRow);
	}
	
	// Get Method of Payment Repeater data
	public static WebElement methodOfPmtTableData(WebDriver driver, int row, int col) {
		By methodOfPmtTableData = ((ByAngularRepeater) methodOfPmtRepeater).row(row);
		WebElement elem = driver.findElement(methodOfPmtTableData);
		return elem.findElements(methodOfPmtTableDataRepeater).get(col);
	}
	
	// Get Method Of Payment Repeater List
	public static List<WebElement> methodOfPmtTableList(WebDriver driver) {
		return driver.findElements(methodOfPmtRepeater);
	}
}
