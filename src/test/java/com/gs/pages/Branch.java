package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class Branch {
	
	public static WebElement element = null;
	
	public static By branchRepeater = ByAngular.repeater("office in readData");
	public static By branchTableDataRepeater = ByAngular.repeater("field in fieldDetails.Section");
	
	// Branch
	public static WebElement branchHeading(WebDriver driver) {
		By branchHeading = By.xpath("//h2[contains(.,'Branch')]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, branchHeading);
		return driver.findElement(branchHeading);
	}
	
	// Branch last table data
	public static WebElement branchTableLastRow(WebDriver driver) {
		By branchTableLastRow = By.xpath("//div[@ng-show='truetag']//table[contains(@class,'maintable')]//tbody//tr[last()]");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, branchTableLastRow);
		return driver.findElement(branchTableLastRow);
	}
	
	// Get Branch Repeater data
	public static WebElement branchTableData(WebDriver driver, int row, int col) {
		By branchTableData = ((ByAngularRepeater) branchRepeater).row(row);
		WebElement elem = driver.findElement(branchTableData);
		return elem.findElements(branchTableDataRepeater).get(col);
	}
	
	// Get Branch Repeater List
	public static List<WebElement> branchTableList(WebDriver driver) {
		return driver.findElements(branchRepeater);
	}
	
}
