package com.gs.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.gs.utilities.Constants;
import com.gs.utilities.WaitLibrary;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;

public class Mandate {
		public static By mandateTableData = ByAngular.repeater("office in readData");
	
		//Click on OnBoarding Data
		public static WebElement onboardingData(WebDriver driver) {
			By onBoardingData = By.xpath("//span[contains(text(),'Onboarding Data')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, onBoardingData);
			return driver.findElement(onBoardingData);
		}
		//Click on Mandate
		public static WebElement mandateTab(WebDriver driver) {
			By mandateTab = By.xpath("//span[contains(text(),'Mandate')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, mandateTab);
			return driver.findElement(mandateTab);
		}
		//Click on Add New Mandate
		public static WebElement addNewMandate(WebDriver driver) {
			By addNewMandate = By.xpath("//button[@tooltip='Add New']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, addNewMandate);
			return driver.findElement(addNewMandate);
		}
		//Click on Unique Mandate Reference
		public static WebElement uniqueMandateRef(WebDriver driver) {
			By uniqueMandateReference = By.xpath("//input[contains(@name,'UniqueMandateReference')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, uniqueMandateReference);
			return driver.findElement(uniqueMandateReference);
		}
		
		public static WebElement SchemeID(WebDriver driver) {
			By SchemeID = By.xpath("//span[contains(@id,'select2-Scheme ID_0-container')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SchemeID);
			return driver.findElement(SchemeID);
		}
		
		public static WebElement SchemeIDSearch(WebDriver driver) {
		By SchemeIDSearch = By.xpath("//input[@aria-controls='select2-Scheme ID_0-results']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SchemeIDSearch);
		return driver.findElement(SchemeIDSearch);
		}

		public static WebElement SchemeIDList(WebDriver driver, String schemeIDValue) {
		By SchemeIDList = By.xpath("//ul[@id = 'select2-Scheme ID_0-results']/li[text()='"+ schemeIDValue +"']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SchemeIDList);
		return driver.findElement(SchemeIDList);
		}
		
		//Select Branch Code
		public static WebElement BranchCode(WebDriver driver) {
			By BranchCode = By.xpath("//span[contains(@id,'select2-Branch Code-container')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BranchCode);
			return driver.findElement(BranchCode);
		}
		public static WebElement BranchCodeSearchField(WebDriver driver) {
			By BranchCodeSearch = By.xpath("//input[@aria-controls='select2-Branch Code-results']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BranchCodeSearch);
			return driver.findElement(BranchCodeSearch);
		}
		public static WebElement BranchCodeList(WebDriver driver, String branchCode) {
			By BranchCodeList = By.xpath("//ul[@id = 'select2-Branch Code-results']/li[text()='"+ branchCode +"']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, BranchCodeList);
			return driver.findElement(BranchCodeList);
			}
		
		//Select Account Number
		public static WebElement AccNumberSelect(WebDriver driver) {
			By AccNumberSelect = By.xpath("//span[@id='select2-Account Number-container']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, AccNumberSelect);
			return driver.findElement(AccNumberSelect);
		}
		public static WebElement AccNumberSearchField(WebDriver driver) {
			By AccNumberSearchField = By.xpath("//input[@aria-controls='select2-Account Number-results']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, AccNumberSearchField);
			return driver.findElement(AccNumberSearchField);
		}
		
		public static WebElement accNumberList(WebDriver driver, String accNum) {
			By accNumberList = By.xpath("//ul[@id = 'select2-Account Number-results']/li[text()='"+ accNum +"']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accNumberList);
			return driver.findElement(accNumberList);
			}
		
		public static WebElement accget(WebDriver driver) {
			By accNumberList = By.xpath("//ul[@id = 'select2-Account Number-results']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, accNumberList);
			return driver.findElement(accNumberList);
			}
		
		//Effective from Date
		public static WebElement EffectiveFromDate(WebDriver driver) {
			By EffectiveFromDate = By.xpath("//input[@id='Effective From Date']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, EffectiveFromDate);
			return driver.findElement(EffectiveFromDate);
		}

		//Select Mandate Type
		public static WebElement MandateType(WebDriver driver) {
			By MandateType = By.xpath("//select[contains(@name,'MandateType')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, MandateType);
			return driver.findElement(MandateType);
		}
		
		//Select Mandate Action
		public static WebElement MandateAction(WebDriver driver) {
			By MandateAction = By.xpath("//select[@name='MandateAction']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, MandateAction);
			return driver.findElement(MandateAction);
		}
		
		//Company ID
		public static WebElement CompanyId(WebDriver driver) {
			By CompanyId = By.xpath("//input[@id='CompanyID']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, CompanyId);
			return driver.findElement(CompanyId);
		}
		
		//SEC Code
		public static WebElement SECcode(WebDriver driver) {
			By SECcode = By.xpath("//input[@id='SEC_CODE']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SECcode);
			return driver.findElement(SECcode);
		}
		
		//Limit Type
		public static WebElement LimitType(WebDriver driver) {
			By LimitType = By.xpath("//select[@name='LimitType']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, LimitType);
			return driver.findElement(LimitType);
		}
		
		//Select Status
		public static WebElement StatusSelect(WebDriver driver) {
			By StatusSelect = By.xpath("//span[@id='select2-{{fieldLabel}}-container']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, StatusSelect);
			return driver.findElement(StatusSelect);
		}
		public static WebElement statusSearchField(WebDriver driver) {
			By statusSearchField = By.xpath("//input[@aria-controls='select2-{{fieldLabel}}-results']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, statusSearchField);
			return driver.findElement(statusSearchField);
		}
		
		//Select Mandate Start Date
		public static WebElement MandateStartDate(WebDriver driver) {
			By MandateStartDate = By.xpath("//input[@id='Mandate Start Date']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, MandateStartDate);
			return driver.findElement(MandateStartDate);
		}		

		//Select Mandate Expiry Date
		public static WebElement MandateExpDate(WebDriver driver) {
		By MandateExpDate = By.xpath("//input[@id='Mandate Expiry Date']");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, MandateExpDate);
		return driver.findElement(MandateExpDate);
		}
		
		//Select Amount Limit Code
		public static WebElement amtLimitCode(WebDriver driver) {
			By amtLimitCode = By.xpath("//span[@id='select2-Amount Limit Code-container']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtLimitCode);
			return driver.findElement(amtLimitCode);
		}
		public static WebElement amtLimitSearch(WebDriver driver) {
			By amtLimitSearch = By.xpath("//input[@aria-controls='select2-Amount Limit Code-results']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, amtLimitSearch);
			return driver.findElement(amtLimitSearch);
		}

		//Get Mandate Table data elements
		public static  List<WebElement> mandateDataList(WebDriver driver) {
			return driver.findElements(mandateTableData);
		}
		
		public static WebElement mandateTableData(WebDriver driver, int row, int col) {
			By mandateData = ((ByAngularRepeater) mandateTableData).row(row);
			WebElement elem = driver.findElement(mandateData);
			By dat = ByAngular.repeater("field in fieldDetails.Section");
			return elem.findElements(dat).get(col);
		}
		
		public static WebElement editBtn(WebDriver driver) {
			By editBtn = By.xpath("//i[contains(@class,'fa fa-pencil-square-o Iclr1')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, editBtn);
			return driver.findElement(editBtn);
		}
		
		//Select Mandate Action
		public static WebElement DefaultMandateAction(WebDriver driver) {
			By DefaultMandateAction = By.xpath("//select[@name='DefaultMandateAction']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, DefaultMandateAction);
			return driver.findElement(DefaultMandateAction);
		}
		
		//Save Changes button
		public static WebElement SaveChangeBtn(WebDriver driver) {
			By SaveChangeBtn = By.xpath("//span[text()='Save Changes']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, SaveChangeBtn);
			return driver.findElement(SaveChangeBtn);
		}
		
		//Add Mandate
		public static WebElement addMandate(WebDriver driver) {
			By addMandate = By.xpath("//span[text()='Add Mandate']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, addMandate);
			return driver.findElement(addMandate);
		}

		public static WebElement successMsg(WebDriver driver) {
			By successMsg = By.xpath("//*[@id='statusBox'");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, successMsg);
			return driver.findElement(successMsg);			
		}
		
		//Edit Amount Limit Code
		public static WebElement editAmtLmtCode(WebDriver driver) {
		By editAmtLmtCode = By.xpath("//select[@name='AmountLimitCode']/following-sibling::span");
		WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, editAmtLmtCode);
		return driver.findElement(editAmtLmtCode);
		}
		
		//Enter Amount Limit Code
		public static WebElement searchField(WebDriver driver) {
			By enterAmtLmtCode = By.xpath("//input[(@type='search')]");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, enterAmtLmtCode);
			return driver.findElement(enterAmtLmtCode);
		}
		
		//Click on Reset
		public static WebElement reset(WebDriver driver) {
			By reset = By.xpath("//span[@id='select2-{{subfieldLabel}}-container']");
			WaitLibrary.waitForElementPresence(driver, Constants.avg_explicit, reset);
			return driver.findElement(reset);
		}
}