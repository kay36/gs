package com.gs.tc.Client_Defects.ach_Client_Defects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.PartyServiceAssociation;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ClientDefect;
import com.gs.utilities.CommonMethods_ClientDefects;

import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ClientDefects;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC032_ACH_Defects_GSAC_3258 extends LoginLogout_ClientDefects{
	@Test

	public void executeTC032() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC032_ACH_Defect_GSAC_3258");
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
				
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Click on Payment ID");
			WebElement pmtIdElement = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElement, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElement, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentId);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			
			if(PaymentStatus.equals("IN_PROGRESS")) {
				TimeUnit.MINUTES.sleep(5);
				WebElement refresh = ReceivedInstruction.Refresh(driver);
				WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				Thread.sleep(Constants_Defects.tooshort_sleep);
			}
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on InitiateNOC button");
			WebElement initiateNOC = ReceivedInstruction.InitiateNOC(driver);
			WaitLibrary.waitForElementToBeClickable(driver, initiateNOC, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Select Change Code");
			String codeValue = "C01-Incorrect DFI Account Number";
			WebElement changeCode = ReceivedInstruction.ChangeCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, changeCode, Constants_ACH.avg_explicit).click();
			
			//Get Change Code value list
			WebElement CodeValue = ReceivedInstruction.getChangeCodeValues(driver, codeValue);
			String codeValList = ReceivedInstruction.getChangeCodeValues(driver, codeValue).getText();
			WaitLibrary.waitForElementToBeClickable(driver, CodeValue, Constants_Defects.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Selected Field : "+codeValList);
			
			log.info("Enter Account Number");
			String accNumber = "12324565789";
			WebElement accountNumber = ReceivedInstruction.AccountNumber(driver);
			accountNumber.sendKeys(accNumber);
			
			log.info("Submit for Approval");
			WebElement submit = ReceivedInstruction.submitRepairBtn(driver);
			submit.click();
			
			CommonMethods_ClientDefects.approveStatement(driver, paymentId);
			WaitLibrary.waitForAngular(driver);
			
			((ExtentTest) test).log(LogStatus.INFO, "After Approval");
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView2 = ReceivedInstruction.listView(driver);
			String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass2.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_Defects.avg_explicit).click();
			}
			WaitLibrary.waitForAngular(driver);
    		
			//Search Ins ID
    		WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
    		searchInstruction.clear();
    		searchInstruction.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		CommonMethods_ClientDefects.clickStatementWithFileName(driver, paymentFile);
    		WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName2 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Click on Payment ID");
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId2 = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader2 = ReceivedInstruction.verifyPaymentPage(driver, paymentId2);
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			//Click on Linked Message		
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			List<WebElement> linkedMsgsDataList = new ArrayList<WebElement>();
			try {
			log.info("Get Linked Message Id");
			linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			} catch (Exception e) {			
				((ExtentTest) test).log(LogStatus.FAIL, "Linked Message ID not available");
				throw new Exception("Linked Message ID not available");
			}
			
			log.info("Click on Linked Message Id");
			WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, 0, "LinkedMsgID");
			WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants_Defects.avg_explicit).click();
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			//Verify Status
			WebElement StatusMatch = ReceivedInstruction.getStatus(driver);
			String Status = WaitLibrary.waitForElementToBeVisible(driver, StatusMatch, Constants_Defects.avg_explicit).getText();			
			System.out.println("Status::::"+Status);
			
			if(Status.equals("FOR_BULKING")) {
				TimeUnit.MINUTES.sleep(13);
				WebElement refresh = ReceivedInstruction.Refresh(driver);
				WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_Defects.avg_explicit).click();
			}
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			WebElement StatusLn = ReceivedInstruction.getStatus(driver);
			String status = WaitLibrary.waitForElementToBeVisible(driver, StatusLn, Constants_Defects.avg_explicit).getText();
			Assert.assertEquals(status, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + status);		
			
			log.info("Click on Output Instruction ID");
			WebElement outputInsID = ReceivedInstruction.outputInstructionId(driver);
			String outInsId = outputInsID.getText();
			Thread.sleep(Constants_Defects.tooshort_sleep);
			outputInsID.click(); //Code Updated by Ajay
			//WaitLibrary.waitForElementToBeClickable(driver, outputInsID, Constants_Defects.avg_explicit).click();
			Thread.sleep(Constants_Defects.tooshort_sleep);
			((ExtentTest) test).log(LogStatus.PASS, "OUT Instruction Id : " + outInsId);
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extComListDistribInsList(driver);
			int extDataCount = extDataLsit.size();
			int directionCount = 0;
			
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalComData(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDistributionDwnldBtn(driver).click();
					directionCount++;
					break;
				}
			}
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "OUT file not found in External Communication");
				throw new Exception("OUT file not found in External Communication");
			}
		
			// Get RDFI Value from OUT File
			String actRDFIValue = "124085082";
			String rdfiValue = CommonMethods_ClientDefects.verifyACHOutFile("RDFIValue", "Inbound");
			System.out.println("RDFI Value : " + rdfiValue);
			Assert.assertEquals(rdfiValue, actRDFIValue);
			((ExtentTest) test).log(LogStatus.INFO, "OUT File value Verified with RDFI Value : " +rdfiValue);

			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
