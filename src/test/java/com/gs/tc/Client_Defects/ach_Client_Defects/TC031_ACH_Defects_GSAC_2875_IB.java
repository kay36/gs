package com.gs.tc.Client_Defects.ach_Client_Defects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.objectRepo.SecurityPage;
import com.gs.pages.Approvals;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ClientDefect;
import com.gs.utilities.CommonMethods_ClientDefects;
import com.gs.utilities.Constants;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ClientDefects;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC031_ACH_Defects_GSAC_2875_IB extends LoginLogout_ClientDefects{
	
	public static ArrayList<String> tabs = null;
	
	
	@Test

	public void executeTC028() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "Test Case : Return code ,description not visible to user in approval screen for rejected ACH");
						
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File : " + paymentFile);
			
    		// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
    		
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_Defects.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_Defects.avg_explicit).click();
			}
			
			// Scroll to Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");

			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_Defects.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id::: " + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
//			WebElement TransportName1 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_Defects.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status Payment: " + insStatus);
			
			log.info("Click on Payment ID");
			BrowserResolution.scrollDown(driver);
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			for(int l=0; l<paymentscount; l++) {
				String paymentFun = ReceivedInstruction.originalPaymentReference(driver, l, "MethodOfPayment").getText();
				if(paymentFun.equals("ACH_IN")) {
					WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
					paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_Defects.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					((ExtentTest) test).log(LogStatus.INFO, "Payment Id: " + paymentID);
					break;
					}else if (l==paymentscount-1 && !paymentFun.equals("ACH_IN")) {
						throw new Exception("ACH_IN payment is not Available");
				}
			}

			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			BrowserResolution.scrollUp(driver);
			//Click on Payment Action button
			try {
				WebElement paymentActionBtn = ReceivedInstruction.paymentAction(driver);
				WaitLibrary.waitForElementToBeClickable(driver, paymentActionBtn, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				((ExtentTest) test).log(LogStatus.INFO, "Payment Action button Selected to Reject Payment");
			}catch (Exception e) {
	    		throw new Exception("Payment Action button not found " + e.getMessage());
	    	}
				
			//Reject - Payment action - Action
//			WebElement paymentActionActionBtn = ReceivedInstruction.paymentActionActionBtn(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, paymentActionActionBtn, Constants_Defects.avg_explicit).click();
//			WebElement paymentActionBtnSearch = ReceivedInstruction.paymentActionBtnSearch(driver);
//			paymentActionBtnSearch.sendKeys("REJECT PAYMENT");		
//			WaitLibrary.waitForAngular(driver);
//			WebElement paymentActionList = ReceivedInstruction.paymentActionList(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, paymentActionList, Constants_Defects.avg_explicit).click();

			log.info("Select the Payment Action");
			WebElement paymentAction = ReceivedInstruction.PaymentAction(driver);
			WaitLibrary.waitForElementToBeClickable(driver, paymentAction, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			WebElement selectAction = ReceivedInstruction.actionDropDown(driver,"REJECT PAYMENT");
			WaitLibrary.waitForElementToBeClickable(driver, selectAction, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			// enter the search option
//			log.info("enter search option");
//			ReceivedStatement.accountDataField(driver).sendKeys("REJECT PAYMENT");
//			WaitLibrary.waitForAngular(driver);
//			ReceivedStatement.accountDataField(driver).sendKeys(Keys.ENTER);
			
			//Reject - Payment action - Reason Code
//			WebElement paymentActionReasonBtn = ReceivedInstruction.paymentActionReasonBtn(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, paymentActionReasonBtn, Constants_Defects.avg_explicit).click();
//			WebElement paymentReasonBtnSearch = ReceivedInstruction.paymentActionBtnSearch(driver);
//			paymentReasonBtnSearch.sendKeys("R01-Insufficient Funds");
//			WaitLibrary.waitForAngular(driver);
//			WebElement paymentReasonList = ReceivedInstruction.paymentReasonList(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, paymentReasonList, Constants_Defects.avg_explicit).click();
	

			//Code Updated (GS) 174
			log.info("Click on Reason code");
			WebElement reasonCode = ReceivedInstruction.ReasonCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, reasonCode, Constants.avg_explicit).click();

			//GS- Select Tag changed to List
//			 log.info("Select Reason Code from drop down");
//			 WebElement selectingReasonCode = ReceivedInstruction.selectReasonCode(driver);
//			 WaitLibrary.waitForElementToBeClickable(driver, selectingReasonCode,
//			 Constants.avg_explicit).click(); WaitLibrary.waitForAngular(driver);
			 Thread.sleep(Constants.tooshort_sleep);
			
			 //Code Updated by Ajay - 185 to 192
			List<WebElement> reasonCodeList = ReceivedInstruction.selectReasonCodeFromList(driver);
			for (int rc = 0; rc < reasonCodeList.size(); rc++) {
				String actualReasonCode = reasonCodeList.get(rc).getText();
				if(actualReasonCode.equalsIgnoreCase("R04-Invalid Account Number Structure")) {
					reasonCodeList.get(rc).click();
					break;
				}
			}
			 
			 
			/*
			 * //Search reason code String SearchData =
			 * "AC01-Format of the account number specified is not correct."; WebElement
			 * searchReasonCode = ReceivedInstruction.SearchReasonCode(driver);
			 * searchReasonCode.sendKeys(SearchData,Keys.ENTER);
			 * WaitLibrary.waitForAngular(driver); Thread.sleep(Constants.short_sleep);
			 */
			
			//Provide additional information
			WebElement additionalInfoSearch = ReceivedInstruction.additionalInfoSearch(driver);
			additionalInfoSearch.sendKeys("Reject the payment");
			WaitLibrary.waitForAngular(driver);
			
			//click on submit
			log.info("Click on submit");
			WebElement submitButton = ReceivedInstruction.submitRepairBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submitButton, Constants_Defects.avg_explicit).click();
			log.info("submitted");
			
			try {
				
				((ExtentTest) test).log(LogStatus.INFO, "Verifying Approval Data");
				
				// Approver Payment Control Data
				tabs = new ArrayList<String>(driver.getWindowHandles());
	    		driver.switchTo().window(tabs.get(1));
	    		
	    		WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
	    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
	    		System.out.println("clicked filter icon");
	    		
	    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
	    		filterWithKeyword.sendKeys(paymentID, Keys.ENTER);
	    		System.out.println(paymentID);
	    		System.out.println("Entered search params");
	    			
//	    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_Defects.avg_explicit).click();
//	    		System.out.println("clicked filter icon");
//	    		
	    		WaitLibrary.waitForAngular(driver);
	    		Thread.sleep(Constants.tooshort_sleep);

	    		WebElement approvalData = Approvals.approvalTableData(driver, 0);
	    		approvalData.click();
	    		WaitLibrary.waitForAngular(driver);
	    		
	    		Thread.sleep(Constants.tooshort_sleep);
	    		BrowserResolution.scrollDown(driver);
	    		
	    		List<WebElement> addtDataList = Approvals.additionApprElems(driver);
	    		int addtDataCount = addtDataList.size();
	    		System.out.println(addtDataCount);
	    		
	    		HashMap<String, String> apprvalData = new HashMap<>();
	    		
	    		for(int i=1; i<=addtDataCount; i++) {
					String fieldVal = Approvals.addtApprField(driver, i).getText();
					String dataVal = Approvals.addtApprData(driver, i).getText();
										
					apprvalData.put(fieldVal, dataVal);
				}
	    		
	    		Thread.sleep(Constants_ACH.tooshort_sleep);   
	    		
	    		
	    		// Expected Approval data
	    		HashMap<String, String> expectedData = new HashMap<>();
	    		
	    		expectedData.put("PaymentId", paymentID);
	    		expectedData.put("Action", "REJECT PAYMENT");
	    		//Code Updated (GS) 266 (R01 to R04)
	    		expectedData.put("ReasonCode", "R04-Invalid Account Number Structure");
	    		expectedData.put("AdditionalInfo", "Reject the payment");
	    		expectedData.put("MOPFamily", "NACHA");
	    		
	    		// Verifying approval data
	    		for (Map.Entry<String, String> verifyData : expectedData.entrySet()) {
	    			if(apprvalData.get(verifyData.getKey()).equals(verifyData.getValue())) {
	    				((ExtentTest) test).log(LogStatus.PASS, verifyData.getKey() + " : " + apprvalData.get(verifyData.getKey()));
	    				System.out.println(apprvalData.get(verifyData.getKey()));
	    				System.out.println(verifyData.getValue());
	    			}else {
	    				((ExtentTest) test).log(LogStatus.FAIL, verifyData.getKey() + " : " + apprvalData.get(verifyData.getKey()));
	    				System.out.println(apprvalData.get(verifyData.getKey()));
	    				System.out.println(verifyData.getValue());
	    			}
				}
	    		
	    		WebElement notes = Approvals.notes(driver);
	    		WaitLibrary.waitForElementToBeClickable(driver, notes, Constants_Defects.avg_explicit).click();
	    		notes.sendKeys(paymentID);
	    		
	    		Thread.sleep(Constants.short_sleep);
	    		
	    		BrowserResolution.scrollDown(driver);
	    		WebElement ApproveBtn = Approvals.clickApproveBtn(driver);
	    		WaitLibrary.waitForElementToBeClickable(driver, ApproveBtn, Constants_Defects.avg_explicit).click();
	    		WaitLibrary.waitForAngular(driver);
	    		System.out.println("clicked approve btn");
	    		
	    		Thread.sleep(Constants_ACH.tooshort_sleep);
	    		
	    		driver.switchTo().window(tabs.get(0));
	    		WaitLibrary.waitForAngular(driver);
				
			}catch (Exception e) {
	    		js.executeScript("arguments[0].click();", SecurityPage.security(driver));
				js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));
				WaitLibrary.waitForAngular(driver);
	    		driver.switchTo().window(tabs.get(0));
	    		WaitLibrary.waitForAngular(driver);
	    		throw new Exception("Error : " + e.getMessage());
	    	}

    		Thread.sleep(Constants_ACH.short_sleep);
    					
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());

		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}