package com.gs.tc.Client_Defects.ach_Client_Defects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.DistributedInstructions;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ClientDefect;
import com.gs.utilities.CommonMethods_ClientDefects;
import com.gs.utilities.Constants;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ClientDefects;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.dataComparision;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC007_ACH_Defect_GSAC_2934 extends LoginLogout_ClientDefects{
	@Test

	public void executeTC007() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC012_ACH_Defect_GSAC_2934 with Batch NACK");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyyMMdd");					
			String locDateYr = dateYMD.format(now);
			
			DateTimeFormatter dateMDY = DateTimeFormatter.ofPattern("MM/dd/yy"); 
			String locDate = dateMDY.format(now);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File name is : " + paymentFile);
			
			// Fetch ACK File Name
			String nackFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_ACK_File);
			System.out.println(nackFile);
			((ExtentTest) test).log(LogStatus.INFO, "NACK File name is : " + nackFile);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.ACHPain001ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.ACHPain001ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "Payment File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_Defects.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Payment ID");
			BrowserResolution.scrollDown(driver);
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			for(int l=0; l<paymentscount; l++) {
				String paymentFun = ReceivedInstruction.originalPaymentReference(driver, l, "MethodOfPayment").getText();
				String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
				if(paymentFun.equals("SAMEDAY_ACH_OUT")) {
					WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
					paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
					if(status.equals("WAIT DEBIT PROCESSING BATCH")) {
						//Wait for Bulking for 18 mins
						TimeUnit.MINUTES.sleep(19);
					}else if(status.equals("TIME WAREHOUSED")) {
						WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
						WaitLibrary.waitForAngular(driver);
						Thread.sleep(Constants_Defects.tooshort_sleep);
						
						CommonMethods_ClientDefects.forceRelease(driver, paymentID, paymentFile, insId);
						WaitLibrary.waitForAngular(driver);
						Thread.sleep(Constants_Defects.tooshort_sleep);
						TimeUnit.MINUTES.sleep(19);
					}
					else if(status.equals("FOR BULKING")) {
						//Wait for Bulking for 12 mins
						TimeUnit.MINUTES.sleep(13);
					}
					WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
					WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
					break;
					}else if (l==paymentscount-1 && !paymentFun.equals("SAMEDAY_ACH_OUT")) {
						throw new Exception("SAMEDAY_ACH_OUT payment is not Available");
				}
			}

			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
				
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			//get Output Instruction Id
			WebElement outInsIdElem = ReceivedInstruction.outputInstructionId(driver);
			String outInsId = WaitLibrary.waitForElementToBeVisible(driver, outInsIdElem, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Output Instruction ID : " + outInsId);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			// Getting and Validating modifier
			char modifierIDChar = outInsId.charAt(outInsId.length()-1);
			String modifierID = String.valueOf(modifierIDChar);
			
			if (Character.isAlphabetic(modifierIDChar)) {
				((ExtentTest) test).log(LogStatus.PASS, "Modifier ID : " + modifierIDChar + " is a valid Modifier");
		    }
		    else {
		      throw new Exception("Modifier ID : " + modifierIDChar + " is a not valid Modifier");
		    }
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			((ExtentTest) test).log(LogStatus.INFO, "Updating Batch References in NACK file");
			
			//Creating new NACK Batch from reference
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ nackFile, sampleDir+nackFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ nackFile);
			System.out.println("Dest : "+sampleDir+ nackFile);
			
			// creating new values for Nack file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{mm/dd/yy}}", locDate);
			updateVal.put("{{AFIDMOD}}", modifierID);
			updateVal.put("{{MBATNO1}}", "0000001");
			updateVal.put("{{MBATNO2}}", "0000002");
			
			// Updating Nack File
			SampleFileModifier.updateNackFile(sampleDir+nackFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading ACK File via WinSCP");
			log.info("Source File is  :" + sampleDir + nackFile);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + nackFile, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "NACK File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab1 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab1, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(Constants_ACH.tooshort_sleep);
    		WebElement searchInstructionClear = ReceivedInstruction.searchWithInsID(driver);
    		searchInstructionClear.clear();
    		searchInstructionClear.sendKeys(Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		Thread.sleep(Constants_ACH.tooshort_sleep);
    		
			log.info("Click on List View");
			WebElement listView1 = ReceivedInstruction.listView(driver);
			String listViewClass1 = WaitLibrary.waitForElementToBeVisible(driver, listView1, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass1.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView1, Constants_Defects.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, nackFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid1 = ReceivedInstruction.getInsIdByTransportName(driver, nackFile);
			String insId1 = WaitLibrary.waitForElementToBeVisible(driver, instdid1, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "NACK Instruction id::: " + insId1);
			WaitLibrary.waitForElementToBeClickable(driver, instdid1, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName1 = ReceivedInstruction.verifyInstructionPage(driver, nackFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem1 = ReceivedInstruction.getInsStatus(driver);
			String insStatus1 = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem1, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "NACK Instruction Status : " + insStatus1);
				
			log.info("Get Original Instruction ID");
			WebElement origInsId = ReceivedInstruction.getOriginalInsID(driver);
			String origInstructionID = WaitLibrary.waitForElementToBeVisible(driver, origInsId, Constants_Defects.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			String originalInsID = CommonMethods_ClientDefects.filterOriginalInsId(origInstructionID);
			Assert.assertEquals(originalInsID, outInsId);
			((ExtentTest) test).log(LogStatus.PASS, "Original Instruction ID matched with Output Instruction ID in NACK Ins : " + originalInsID);
			
			log.info("Get Instruction Status");
			WebElement instrStatus = ReceivedInstruction.InsStatus(driver);
			String instructionStatus = WaitLibrary.waitForElementToBeVisible(driver, instrStatus, Constants_Defects.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			String instStatus = CommonMethods_ClientDefects.filterInstructionStatus(instructionStatus);
			Assert.assertEquals(instStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction details Status is :" + instStatus);
			
			log.info("Navigate to Distributed Instructions");
			WebElement distInstructions = SideBarMenu.distInstructions(driver);
			BrowserResolution.scrollToElement(driver, distInstructions);
			WaitLibrary.waitForElementToBeClickable(driver, distInstructions, Constants_ACH.avg_explicit).click();
			
			log.info("Click on Grid View");
			WebElement gridView = DistributedInstructions.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.contains(Constants_Defects.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_Defects.avg_explicit).click();					
			}
			Thread.sleep(Constants_ACH.short_sleep);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Search Distributed Instructions");
			WebElement searchInstructionIn = DistributedInstructions.searchWithDistInsID(driver);
    		searchInstructionIn.clear();
    		searchInstructionIn.sendKeys(outInsId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		List<WebElement> distInsData = DistributedInstructions.distDataList(driver);
			int distDataCount = distInsData.size();
    		
			String uniqueOutputRef = null;
			
    		for(int l=0; l<distDataCount; l++) {
				try {
					uniqueOutputRef = DistributedInstructions.distInstData(driver, l, 1).getText();
					System.out.println(uniqueOutputRef);
				}catch (NoSuchElementException ne) {
					throw new Exception("No Distribution instruction found");
				}
				
				if(uniqueOutputRef.equals(outInsId)) {
					((ExtentTest) test).log(LogStatus.INFO, "Unique output reference : " + uniqueOutputRef);
					
					String outInsName = DistributedInstructions.distInstData(driver, l, 3).getText();
					System.out.println("outInsName : " +outInsName);
					
					if (outInsName.startsWith("ACH026015079D"+locDateYr+"T")) {
						((ExtentTest) test).log(LogStatus.PASS, "Output Instruction Name is valid : " + outInsName);
						System.out.println("Output Instruction Name is valid");
					}else {
						throw new Exception("Output Instruction Name is not valid " + outInsName);
					}
					
					String disInsStatus = DistributedInstructions.distInstData(driver, l, 7).getText();
					System.out.println("Status : " +disInsStatus);
					Thread.sleep(Constants_Defects.short_sleep);
					WebElement uniqueOutputRefe = DistributedInstructions.distInstData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, uniqueOutputRefe, Constants_ACH.avg_explicit).click();
					System.out.println("Clicked");
					Thread.sleep(Constants_Defects.short_sleep);
					break;
				}else if (l==distDataCount-1 && !uniqueOutputRef.equals(outInsId)) {
					throw new Exception("Distribution instruction is not Available");
				}
			}
    		BrowserResolution.scrollDown(driver);
    		
    		log.info("Navigate to Output Batch information");
			js.executeScript("arguments[0].click();", DistributedInstructions.outputBatchInformation(driver));
			((ExtentTest) test).log(LogStatus.INFO, "Select the batch ID with Status");
			
			List<WebElement> outputBatchDataList = new ArrayList<WebElement>();
			try {
			log.info("Get Output Batch information");
			outputBatchDataList = DistributedInstructions.outputBatchList(driver);
			} catch (Exception e) {			
				((ExtentTest) test).log(LogStatus.FAIL, "Output Batch information not available");
				throw new Exception("Output Batch information not available");
			}
			
			int outputBatchcount = outputBatchDataList.size();
			String batchID = "";
			log.info("Click on Output Batch information id");
			for(int l=0; l<outputBatchcount; l++) {
				String batchStatus = DistributedInstructions.outputBatchData(driver, l, "BatchStatusDetails").getText(); 
				System.out.println(batchStatus);
				if(batchStatus.equals("Status changed to WAIT_BATCH_REJECT")) {
					WebElement batchIDElem = DistributedInstructions.outputBatchData(driver, l, "BatchID");
					batchID = batchIDElem.getText();
					System.out.println(batchID);
					WaitLibrary.waitForElementToBeClickable(driver, batchIDElem, Constants_Defects.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					break;
				}else if (l==outputBatchcount-1 && !batchStatus.equals("Status changed to WAIT_BATCH_REJECT")) {
					throw new Exception("WAIT_BATCH_REJECT not available");
				}
			}
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
    		// Verifying Batch page with batch ID
    		WebElement batchIdHeader = DistributedInstructions.verifyBatchPage(driver, batchID);
    		
    		// Reject the Batch
    		((ExtentTest) test).log(LogStatus.INFO, "Reject Batch : " + batchID);
    		WebElement batchRejBtn = DistributedInstructions.batchRejectBtn(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, batchRejBtn, Constants_Defects.avg_explicit).click();
    		
    		WaitLibrary.waitForAngular(driver);
    		
    		//Sent request for approval
			CommonMethods_ClientDefects.approveStatement(driver, batchID);
			((ExtentTest) test).log(LogStatus.INFO, "After Approval");
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			    		
			log.info("Navigate to Distributed Instructions");
			WebElement distInstructions1 = SideBarMenu.distInstructions(driver);
			BrowserResolution.scrollToElement(driver, distInstructions1);
			WaitLibrary.waitForElementToBeClickable(driver, distInstructions1, Constants_ACH.avg_explicit).click();
			
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Grid View");
			WebElement gridView2 = DistributedInstructions.gridView(driver);
			String gridViewClass2= WaitLibrary.waitForElementToBeVisible(driver, gridView2, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass2.contains(Constants_Defects.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView2, Constants_Defects.avg_explicit).click();					
			}
			
			Thread.sleep(Constants_ACH.short_sleep);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Search Distributed Instructions");
			WebElement searchInstruction = DistributedInstructions.searchWithDistInsID(driver);
    		searchInstruction.clear();
    		searchInstruction.sendKeys(outInsId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		List<WebElement> distInsData2 = DistributedInstructions.distDataList(driver);
			int distDataCount2 = distInsData2.size();
			String uniqueOutputRef2 = null;
			for(int l=0; l<distDataCount2; l++) {
				try {
					uniqueOutputRef2 = DistributedInstructions.distInstData(driver, l, 1).getText();
					System.out.println(uniqueOutputRef2);
				}catch (NoSuchElementException ne) {
					throw new Exception("No Distribution instruction found");
				}
				
				if(uniqueOutputRef2.equals(outInsId)) {
					WebElement uniqueOutputRefe = DistributedInstructions.distInstData(driver, l, 1);
					WaitLibrary.waitForElementToBeClickable(driver, uniqueOutputRefe, Constants_ACH.avg_explicit).click();
					System.out.println("Clicked");
					Thread.sleep(Constants_Defects.short_sleep);
					break;
				}else if (l==distDataCount2-1 && !uniqueOutputRef2.equals(outInsId)) {
					throw new Exception("Distribution instruction is not Available");
				}
			}
			
			List<WebElement> outputBatchDataList2 = new ArrayList<WebElement>();
			try {
			log.info("Get Output Batch information");
				outputBatchDataList2 = DistributedInstructions.outputBatchList(driver);
			} catch (Exception e) {
				((ExtentTest) test).log(LogStatus.FAIL, "Output Batch information not available");
				throw new Exception("Output Batch information not available");
			}
    		
    		BrowserResolution.scrollDown(driver);
    		log.info("Navigate to Output Batch information");
    		((ExtentTest) test).log(LogStatus.INFO, "Select the rejected Batch Id:: " +batchID);
			js.executeScript("arguments[0].click();", DistributedInstructions.outputBatchInformation(driver));
			WebElement clickPayID = DistributedInstructions.clickBatchID(driver, batchID);
			WaitLibrary.waitForElementToBeClickable(driver, clickPayID, Constants_ACH.avg_explicit).click();
			Thread.sleep(Constants_Defects.tooshort_sleep);
    		
    		// Verifying Batch page with batch ID
    		WebElement batchIdHeader2 = DistributedInstructions.verifyBatchPage(driver, batchID);
    		Thread.sleep(Constants_Defects.tooshort_sleep);	
    		
    		// Verifying Batch status
    		WebElement batchStatusElement = DistributedInstructions.batchStatus(driver);
			String batchStatus = WaitLibrary.waitForElementToBeVisible(driver, batchStatusElement, Constants_Defects.avg_explicit).getText();
			Assert.assertEquals(batchStatus, "REJECTED");
			((ExtentTest) test).log(LogStatus.PASS, "Batch Status : " + batchStatus);			
			BrowserResolution.scrollDown(driver);		
    					
    		Thread.sleep(Constants_Defects.tooshort_sleep);
    		log.info("Navigate to Account Posting Tab");
    		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
				
			String[] accTypeArrIn = {"SUS", "NOS", "NOS", "SUS"};
			String[] drcr = {"D", "C", "D", "C"};
			
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String getAccountIdDr = ReceivedInstruction.accountPostingData(driver, i, "DrCrInd").getText();
				if(i<posCount-2) {
					Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
					Assert.assertEquals(getAccountIdDr, drcr[i]);
					((ExtentTest) test).log(LogStatus.PASS, "1st Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);				
					}
					if(i>=posCount-2) {
						Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
						Assert.assertEquals(getAccountIdDr, drcr[i]);
						((ExtentTest) test).log(LogStatus.PASS, "2nd Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);
					}
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			
			log.info("Verifying FinacleAccountPosting Request");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			int finCount = 0 ;
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
				}
			}
			
			if(finCount == 2) {
				((ExtentTest) test).log(LogStatus.PASS, "Two Finacle Account Posting Request's are generated");
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Two Finacle Account Posting Request are not generated");
			}		
			
			log.info("Navigate to Transaction Information Tab");
			js.executeScript("arguments[0].click();", DistributedInstructions.transactionInfo(driver));
			
			WebElement pmtIdElem = DistributedInstructions.paymentId(driver, 0, "PaymentID");
			String paymentID2 = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_Defects.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID2);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_Defects.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Selected the Payment : " + paymentID2);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader2 = ReceivedInstruction.verifyPaymentPage(driver, paymentID2);
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			log.info("Navigate to Batch Information");
			js.executeScript("arguments[0].click();", DistributedInstructions.batchInfo(driver));
			Thread.sleep(Constants_Defects.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			List<WebElement> batchList = DistributedInstructions.batchDataList(driver);
			int batchCount = batchList.size();
			System.out.println(batchCount);
			String batchId="";
			BrowserResolution.scrollDown(driver);
			for(int l=0; l<batchCount; l++) {
				WebElement batchFunction = DistributedInstructions.batchInformation(driver, l, "BatchType");
				String batchFun = batchFunction.getText();
				Thread.sleep(Constants_Defects.tooshort_sleep);
				if(batchFun.equals("PROCESSING")) {
					WebElement batchIdElem = DistributedInstructions.batchId(driver, l, "BatchID");
					batchId = WaitLibrary.waitForElementToBeVisible(driver, batchIdElem, Constants_ACH.avg_explicit).getText();
					batchIdElem.click();
					break;
				}
			}
			((ExtentTest) test).log(LogStatus.INFO, "BatchID selected with Batch type \"PROCESSING\" :: " + batchId);
			WaitLibrary.waitForAngular(driver);	
			
			log.info("Verifying Page with batchId");
			WebElement paymentIdHeader3 = DistributedInstructions.verifyPaymentPage(driver, batchId);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.short_sleep);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			int drFundCount = 0;
			
			String debitFundControlFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_DrCr_File);
			System.out.println(debitFundControlFile);
			((ExtentTest) test).log(LogStatus.INFO, "DebitFundsControl Verification");
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
					drFundCount++;
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					
					// Getting Object Data
					Thread.sleep(Constants_Defects.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants_Defects.avg_explicit);
					String objDataString = objData.getText();
					
					List<String> dateFields = Arrays.asList("valueDate", "currentDate");
				    List<String> fieldNamesArr = Arrays.asList("debitCreditIndicator", "paymentCcy", "paymentCategory");
				    
					List<JSONObject> listOfValues = dataComparision.compareDebitFuncontrol(System.getProperty("user.dir") + "\\" + Constants_Defects.gs_Samples +"\\"+sampleDir+debitFundControlFile, objDataString, dateFields, fieldNamesArr);
					
					for (JSONObject headLines : listOfValues) {	
			    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
			    		
			    		while (headLinekeys.hasNext()) {
			    		    Object key = headLinekeys.next();
			    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
			    		    JSONObject value = headLines.getJSONObject((String) key);
			    		    
			    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
			    		    while( innerMapKeys.hasNext() ) {
				    		    String innerMapKey = (String) innerMapKeys.next();
				    		    if (key == "Matched") {
				    		    	((ExtentTest) test).log(LogStatus.PASS, "Debit Fund Control Value matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
			    		    	}
				    		    else {
				    		    	((ExtentTest) test).log(LogStatus.FAIL, "Debit Fund Control Value not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    }
				    		}
			    		}
			    	}
					
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
					break;
				}
			}
			if(drFundCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Debit fund control request is not generated");
			}
			
			((ExtentTest) test).log(LogStatus.INFO, "Finacle Posting Verification");
			// Fetch Finacle File Name
			String finacleFile1 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_finacle_Pos_1);
			String finacleFileName1 = System.getProperty("user.dir") + "\\" + Constants_Defects.gs_Samples +"\\"+sampleDir+finacleFile1;
			String finacleFile2 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_finacle_Pos_2);
			String finacleFileName2 = System.getProperty("user.dir") + "\\" + Constants_Defects.gs_Samples +"\\"+sampleDir+finacleFile2;
			int fincount = 0;
			
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
					if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						fincount++;
						List<JSONObject> listOfValues = null;
							if(fincount <= 2 ) {
							WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
							BrowserResolution.scrollToElement(driver, viewBtn);
							js.executeScript("arguments[0].click();", viewBtn);
							// Getting Object Data
							Thread.sleep(Constants_Defects.tooshort_sleep);
							WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
							WaitLibrary.waitForElementToBeVisible(driver, objData, Constants_Defects.avg_explicit);
							String objDataString = objData.getText();
							
							List<String> dateFields = Arrays.asList("transactionDate");
						    List<String> normalKeys = Arrays.asList("originalPaymentFunction");
						    List<String> additionalAttributesKeys = Arrays.asList();
						    List<String> movementRequestsKeys = Arrays.asList("instructionType");
							
						    if(fincount == 1) {
								listOfValues = dataComparision.compareFinaclePosting(finacleFileName1, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
							}else if(fincount == 2) {
								listOfValues = dataComparision.compareFinaclePosting(finacleFileName2, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
							}
						    
							for (JSONObject headLines : listOfValues) {	
					    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
					    		
					    		while (headLinekeys.hasNext()) {
					    		    Object key = headLinekeys.next();
					    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
					    		    JSONObject value = headLines.getJSONObject((String) key);
					    		    
					    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
					    		    while( innerMapKeys.hasNext() ) {
						    		    String innerMapKey = (String) innerMapKeys.next();
						    		    if (key == "Matched") {
						    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting " + fincount+ " matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
					    		    	}
						    		    else {
						    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting " + fincount+ " not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
						    		    }
						    		}
					    		}
					    	}
							// Clicking close modal
							WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
							js.executeScript("arguments[0].click();", modalCloseBtn);
						}
					}
				}
			
					
//				if (invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
//					becNotfCount = j;
//				}
//			}
//			if(becNotfCount != 0) {
//				WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, becNotfCount-1);
//				BrowserResolution.scrollToElement(driver, viewBtn);
//				js.executeScript("arguments[0].click();", viewBtn);
//				
//				// Clicking close modal
//				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, becNotfCount - 1);
//				js.executeScript("arguments[0].click();", modalCloseBtn);
//			} else {
//				((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not found");
//			}
			
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
