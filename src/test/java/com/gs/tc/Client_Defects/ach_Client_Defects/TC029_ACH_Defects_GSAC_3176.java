package com.gs.tc.Client_Defects.ach_Client_Defects;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ClientDefect;
import com.gs.utilities.CommonMethods_ClientDefects;

import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ClientDefects;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC029_ACH_Defects_GSAC_3176 extends LoginLogout_ClientDefects{
	@Test

	public void executeTC029() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC029_ACH_Defect_GSAC_3176");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			System.out.println(locDateYr);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseName,Constants_Defects.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			//Creating new payment File from reference
			SampleFileModifier.createNACKRef(sampleDir+"Ref1\\"+ paymentFile, sampleDir+paymentFile);
			System.out.println("REF1 : "+sampleDir+"Ref1\\"+ paymentFile);
			System.out.println("Dest1 : "+sampleDir+ paymentFile);
			
			// creating new values for Payment file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{yyyy-MM-dd}}", locDateYr);
			
			// Updating Payment File
			SampleFileModifier.updateNackFile(sampleDir+paymentFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_Defects.ACHPain001ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_Defects.ACHPain001ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_Defects.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verify Status of Payment");
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			String status = ReceivedInstruction.originalPaymentReference(driver, 0, "Status").getText();
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
			if(status.equals("WAIT DEBIT PROCESSING BATCH") || status.equals("TIME WAREHOUSED") || status.equals("FOR BULKING")) {
				if(status.equals("WAIT DEBIT PROCESSING BATCH")) {
					//Wait for Bulking for 5 mins
					TimeUnit.MINUTES.sleep(18);
				}else if(status.equals("TIME WAREHOUSED")) {
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					
					CommonMethods_ClientDefects.forceRelease(driver, paymentID, paymentFile, insId);
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_Defects.tooshort_sleep);
					TimeUnit.MINUTES.sleep(18);
				}
				else if(status.equals("FOR BULKING")) {
					//Wait for Bulking for 4 mins
					TimeUnit.MINUTES.sleep(12);
				}
				
				log.info("Click on Received Instruction Tab");
				WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				
				log.info("Click on List View");
				WebElement listView2 = ReceivedInstruction.listView(driver);
				String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_ACH.avg_explicit).getAttribute("class");
	
				if (listViewClass2.contains(Constants_Defects.listViewClassData)) {
					WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_Defects.avg_explicit).click();
				}
				
				Thread.sleep(Constants_ACH.tooshort_sleep);
	    		WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
	    		searchInstruction.clear();
	    		searchInstruction.sendKeys(insId, Keys.ENTER);
	    		WaitLibrary.waitForAngular(driver);
	    		CommonMethods_ClientDefects.clickStatementWithFileName(driver, paymentFile);
	    		
				log.info("Verifying Instruction Page with Transport name");
				WebElement TransportNameIn = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
				WaitLibrary.waitForAngular(driver);
				
				BrowserResolution.scrollDown(driver);
				WaitLibrary.waitForAngular(driver);
				Thread.sleep(Constants_Defects.tooshort_sleep);
			}
			
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Click on Payment ID");
			WebElement pmtIdElement = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElement, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElement, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentId);
				
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Get Distribution ID");
			WebElement getDistributionID = ReceivedInstruction.DistributedID(driver);
			String DistributionID = getDistributionID.getText();
			String filterDistributionID = CommonMethods_ClientDefects.filterDistributionID (DistributionID, "Distribution ID");
			System.out.println(filterDistributionID);
			((ExtentTest) test).log(LogStatus.INFO, "Distribution ID is:: "+filterDistributionID);
			
			//Upload Incoming NOC Transaction
			String paymentFile2 = ExcelUtilities.getCellDataBySheetName(Constants_Defects.File_TestData, "ach_Client_Defects" ,TestCaseName, Constants_Defects.Col_Payment_File_1);
			System.out.println(paymentFile2);
			((ExtentTest) test).log(LogStatus.INFO, "Incomeing NOC File Name is=>" + paymentFile2);
			
			LocalDateTime now2 = LocalDateTime.now();
			DateTimeFormatter dateYMD2 = DateTimeFormatter.ofPattern("yyMMdd");
			String locDate = dateYMD2.format(now2);
			int julDate = LocalDate.now().getDayOfYear();

			String pattern="000";
			DecimalFormat myFormatter = new DecimalFormat(pattern);
			String julianDate = myFormatter.format(julDate);

			String traceID = CommonMethods_ClientDefects.getDistInsIDValue(filterDistributionID,"TRACEID");
			System.out.println("Trace ID " + traceID);

			SampleFileModifier.createNACKRef(sampleDir+"Ref2\\"+ paymentFile2, sampleDir+paymentFile2);

			HashMap<String, String> updateValues = new HashMap<>();
			
			updateValues.put("{{yyMMdd}}", locDate);
			updateValues.put("{{jdt}}", julianDate);
			updateValues.put("{{TraceID}}", traceID);

			SampleFileModifier.updateNackFile(sampleDir+paymentFile2, updateValues);
			
			((ExtentTest) test).log(LogStatus.INFO, "Process NOC Transaction File");
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile2);
			log.info("Destination File is  :" + Constants_Defects.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile2, Constants_Defects.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_Defects.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);			
			WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.clear();
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile2));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid2 = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile2);
			String insId2 = WaitLibrary.waitForElementToBeVisible(driver, instdid2, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Second File Instruction Id=>" + insId2);
			WaitLibrary.waitForElementToBeClickable(driver, instdid2, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName2 = ReceivedInstruction.verifyInstructionPage(driver, paymentFile2);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusEle = ReceivedInstruction.getInsStatus(driver);
			String instStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusEle, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(instStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status of NOC : " + instStatus);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			log.info("Click on Payment ID");
			WebElement pmtIdElement2 = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId2 = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElement2, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElement2, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader2 = ReceivedInstruction.verifyPaymentPage(driver, paymentId2);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			
			// Verifying Payment status
			WebElement paymentStatusEle = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusEle, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
