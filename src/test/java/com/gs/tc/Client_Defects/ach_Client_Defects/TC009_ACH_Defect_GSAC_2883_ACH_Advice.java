package com.gs.tc.Client_Defects.ach_Client_Defects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.gs.pages.AdvancedSearch;
import com.gs.pages.DistributedInstructions;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.CaptureScreenshot_ClientDefect;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.Constants_Defects;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ClientDefects;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC009_ACH_Defect_GSAC_2883_ACH_Advice extends LoginLogout_ClientDefects{
	@Test

	public void executeTC009() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			log.info("Current date");
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			System.out.println(locDateYr);
			
			String searchParams = "GSACH_BULKPROCESSING_NACHA_Tr";
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Grid View");
			WebElement gridView = DistributedInstructions.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.contains(Constants_Defects.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_Defects.avg_explicit).click();					
			}
						
			WaitLibrary.waitForAngular(driver);
			
			log.info("Show Advanced Search");
			WebElement showAdvSearchElem = AdvancedSearch.showAdvancedSearchButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, showAdvSearchElem, Constants_Defects.avg_explicit).click();
			
			log.info("Click source input reference");
			WebElement sourceInputRefElem = AdvancedSearch.sourceInputRef(driver);
			WaitLibrary.waitForElementToBeClickable(driver, sourceInputRefElem, Constants_Defects.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_Defects.tooshort_sleep);
			((ExtentTest) test).log(LogStatus.INFO, "Select Source in Advanced Search");
			
			try {
				WebElement sourceVal = AdvancedSearch.sourceList(driver, searchParams);			
				WaitLibrary.waitForElementToBeClickable(driver, sourceVal, Constants_Defects.avg_explicit).click();
				((ExtentTest) test).log(LogStatus.INFO, "Selected Source in advanced search: " + searchParams);
			}catch (Exception e) {
				CaptureScreenshot_ClientDefect.captureScreenshot();
				((ExtentTest) test).log(LogStatus.FAIL, "Unable to find input Reference Value in Advanced search "+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
				log.info("Click on Cancel Button");
				WebElement cancelBtnElm = AdvancedSearch.cancelBtn(driver);
				WaitLibrary.waitForElementToBeClickable(driver, cancelBtnElm, Constants_Defects.avg_explicit).click();
				throw new Exception("Unable to find input Reference Value in Advanced search " + e.getMessage());
			}
			WebElement entryDateStart=AdvancedSearch.entryDate(driver);
			entryDateStart.sendKeys(Constants_Defects.entrydate);		
			entryDateStart.sendKeys(Keys.ENTER);		
			
			WebElement entryDateEnd=AdvancedSearch.entryDateend(driver);
			entryDateEnd.sendKeys(locDateYr);		
			entryDateEnd.sendKeys(Keys.ENTER);
			
			
			log.info("Click on search Button");
			WebElement searchBtnElm = AdvancedSearch.searchBtn(driver);
			js.executeScript("window.scrollBy(0,-120)");
			js.executeScript("arguments[0].click();", searchBtnElm);
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(4000);
			
			List<WebElement> insList = ReceivedInstruction.recInsList(driver);
			int dataCount = insList.size();
			System.out.println(dataCount);
			
			if(dataCount == 0) {
				CaptureScreenshot_ClientDefect.captureScreenshot();
				((ExtentTest) test).log(LogStatus.FAIL, "No Record found for the searched source : " +searchParams +LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
				// Clearing search
				WebElement clearSearch = AdvancedSearch.clearBtn(driver);
				WaitLibrary.waitForElementToBeClickable(driver, clearSearch, Constants_Defects.avg_explicit).click();
				WaitLibrary.waitForAngular(driver);
				throw new Exception("No Record found for the searched source : " +searchParams);
			}
			
			WebElement inputRefCodeElem = ReceivedInstruction.getSourceIns(driver);
			String inpRefCode = WaitLibrary.waitForElementToBeVisible(driver, inputRefCodeElem, Constants_Defects.avg_explicit).getText();
			System.out.println(inpRefCode);
			
			if(inpRefCode.equals(searchParams)){
				((ExtentTest) test).log(LogStatus.PASS, "Source matched and Found results for : " + searchParams);
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Source not matched Expected : " + searchParams + " Found : " + inpRefCode);
			}
			
			WebElement clearSearch = AdvancedSearch.clearBtn(driver);
			js.executeScript("window.scrollBy(0,-120)");
			js.executeScript("arguments[0].click();", clearSearch);
			WaitLibrary.waitForAngular(driver);
					
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ClientDefect.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ClientDefects.test.addScreenCapture(CaptureScreenshot_ClientDefect.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_Defects.File_TestCases, "TestCases", TestCaseRow, Constants_Defects.Col_comments, exceptionerror);
		}
	}
}
