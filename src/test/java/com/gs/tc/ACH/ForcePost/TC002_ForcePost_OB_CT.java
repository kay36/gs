package com.gs.tc.ACH.ForcePost;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants;
import com.gs.utilities.Constants_ACH;

import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.dataComparision;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC002_ForcePost_OB_CT extends LoginLogout_ACH{
	@Test

	public void executeTC002() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC002_ForcePost_OB_CT");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			
			DateTimeFormatter dateMDY = DateTimeFormatter.ofPattern("MM/dd/yy"); 
			String locDate = dateMDY.format(now);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_ACH_Payment);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File name is : " + paymentFile);

			System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
			System.out.println("Dest : "+sampleDir+ paymentFile);
			
			// Creating sample file from Ref
			CommonMethods_ACH.createSampleRef(Constants_ACH.gs_Samples+sampleDir+"Ref\\"+paymentFile,Constants_ACH.gs_Samples+ sampleDir+paymentFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
			System.out.println("Dest : "+sampleDir+ paymentFile);
			// creating new values for Payment file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{yyyy-MM-dd}}", locDateYr);
			
			// Updating Payment File
			CommonMethods_ACH.updateSampleFile(Constants_ACH.gs_Samples+sampleDir+paymentFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_ACH.ACHPain001ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_ACH.ACHPain001ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "Payment File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction ID: " + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Payment ID");
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			for(int l=0; l<paymentscount; l++) {
				String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
				WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
				paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
				if(status.equals("WAIT DEBIT PROCESSING BATCH")) {
					//Wait for Bulking for 5 mins
					TimeUnit.MINUTES.sleep(20);
				}else if(status.equals("TIME WAREHOUSED")) {
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					CommonMethods_ACH.forceRelease(driver, paymentID, paymentFile, insId);
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					TimeUnit.MINUTES.sleep(20);
				}
				else if(status.equals("FOR BULKING")) {
					//Wait for Bulking for 4 mins
					TimeUnit.MINUTES.sleep(12);
				}
				WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
				WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
				break;
			}
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Payment ID : " + paymentID);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			Thread.sleep(Constants_ACH.tooshort_sleep);
				
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			//get Output Instruction Id
			WebElement outInsIdElem = ReceivedInstruction.outputInstructionId(driver);
			String outInsId = WaitLibrary.waitForElementToBeVisible(driver, outInsIdElem, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Output Instruction ID : " + outInsId);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			// Getting and Validating modifier
			char modifierIDChar = outInsId.charAt(outInsId.length()-1);
			String modifierID = String.valueOf(modifierIDChar);
			
			if (Character.isAlphabetic(modifierIDChar)) {
				((ExtentTest) test).log(LogStatus.PASS, "Modifier ID : " + modifierIDChar + " is a valid Modifier");
		    }
		    else {
		      throw new Exception("Modifier ID : " + modifierIDChar + " is a not valid Modifier");
		    }
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			String ackFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_ACK_File);
			System.out.println(ackFile);
			((ExtentTest) test).log(LogStatus.INFO, "ACK File name is : " + ackFile);
			
			((ExtentTest) test).log(LogStatus.INFO, "Updating Batch References in ACK file");
			//Creating new NACK Batch from reference
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ackFile,sampleDir+ackFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ ackFile);
			System.out.println("Dest : "+sampleDir+ ackFile);
			
			// creating new values for Nack file
			HashMap<String, String> updateValue = new HashMap<>();
			
			updateValue.put("{{mm/dd/yy}}", locDate);
			updateValue.put("{{AFIDMOD}}", modifierID);
			updateValue.put("{{MBATNO1}}", "0000001");
			updateValue.put("{{MBATNO2}}", "0000002");
			
			// Updating Nack File
			SampleFileModifier.updateNackFile(sampleDir+ackFile, updateValue);
			
			// Upload Payment File
    		log.info("Uploading ACK File via WinSCP");
			log.info("Source File is  :" + sampleDir + ackFile);
			log.info("Destination File is  :" + Constants_ACH.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + ackFile, Constants_ACH.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "ACK File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on refresh icon");
			WebElement refresh = ReceivedInstruction.refreshIcon(driver);
			WaitLibrary.waitForElementToBeClickable(driver, refresh, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
		
			log.info("Verify Status");
			WebElement PaymentStatusEle = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusEle, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(paymentStatus, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			//Upload Advice File
			String adviceFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_Advice_File);
			System.out.println(adviceFile);
			((ExtentTest) test).log(LogStatus.INFO, "Advice File is : " + adviceFile);
			
			LocalDateTime now2 = LocalDateTime.now();
			DateTimeFormatter dateYMD2 = DateTimeFormatter.ofPattern("yyMMdd");
			String locDate2 = dateYMD2.format(now2);
			int julDate = LocalDate.now().getDayOfYear();
			((ExtentTest) test).log(LogStatus.INFO, "Current Date " + locDate2);
			
			String pattern="000";
			DecimalFormat myFormatter = new DecimalFormat(pattern);
			String julianDate = myFormatter.format(julDate);
			((ExtentTest) test).log(LogStatus.INFO, "Julian Date " + julDate);
			
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ adviceFile, sampleDir+adviceFile);

			HashMap<String, String> updateValues = new HashMap<>();
			((ExtentTest) test).log(LogStatus.INFO, "Modifier ID: " + modifierID);
			
			updateValues.put("{{yyMMdd}}", locDate2);
			updateValues.put("{{jdt}}", julianDate);
			updateValues.put("{{AFIDMOD}}", modifierID);
			
			SampleFileModifier.updateNackFile(sampleDir+adviceFile, updateValues);
			((ExtentTest) test).log(LogStatus.INFO, "Process Advice File");
			
    		log.info("Uploading Advice File via WinSCP");
			log.info("Source File is  :" + sampleDir + adviceFile);
			log.info("Destination File is  :" + Constants_ACH.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + adviceFile, Constants_ACH.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "Advice File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on refresh icon");
			WebElement refreshIcon = ReceivedInstruction.refreshIcon(driver);
			WaitLibrary.waitForElementToBeClickable(driver, refreshIcon, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on batch information tab");
			WebElement batchInformation = ReceivedInstruction.batchInfoTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, batchInformation, Constants_ACH.avg_explicit).click();
			
			List<WebElement> batchList = ReceivedInstruction.batchDataList(driver);
			int batchCount = batchList.size();
			int batchIDCount = 0;
			String batchID="";
			for(int b = 0; b < batchCount; b++) {
				String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
				String status = ReceivedInstruction.batchInformation(driver, b, "BatchStatus").getText();
				if(batchType.equals("OUTPUT") && status.equals("ACCEPTED_CREDIT_SETTLEMENT_MISMATCH")) {
					((ExtentTest) test).log(LogStatus.PASS, "Batch Status is: "+status);
					WebElement batchId = ReceivedInstruction.batchInformation(driver, b, "BatchID");
					batchID = batchId.getText();
					System.out.println(batchID);
					WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_ACH.avg_explicit).click();
					batchIDCount++;
					break;
				}
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			WaitLibrary.waitForAngular(driver);
			System.out.println(batchCount);
			if(batchIDCount != 1) {
				throw new Exception("Output Batch ID not found with status ACCEPTED_CREDIT_SETTLEMENT_MISMATCH");
			}
			
			ReceivedInstruction.verifyBatchPage(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Click on ForcePost Batch Button");
			WaitLibrary.waitForElementToBeClickable(driver, ReceivedInstruction.ForcePost(driver), Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "ForcePost Batch sent for Approval");
			
			log.info("Send ForcePost Batch for Approval");
			CommonMethods_ACH.approveStatement(driver, batchID);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "After ForcePost Batch Approval");
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView2 = ReceivedInstruction.listView(driver);
			String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass2.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_ACH.avg_explicit).click();
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
    		WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
    		searchInstruction.clear();
    		searchInstruction.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		CommonMethods_ACH.clickStatementWithFileName(driver, paymentFile);
    		WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			WebElement pmtId = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId = WaitLibrary.waitForElementToBeVisible(driver, pmtId, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtId, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentId);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Payment Status");
			WebElement PaymentStatus2 = ReceivedInstruction.getStatus(driver);
			String paymtStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatus2, Constants_ACH.avg_explicit).getText();			
			System.out.println("Payment Status after ForcePost Batch: "+paymtStatus);
			Assert.assertEquals(paymtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status after ForcePost Batch: " + paymtStatus);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on batch information tab");
			WebElement batchInfo = ReceivedInstruction.batchInfoTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, batchInfo, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Click on batch ID with OUTPUT batch type");
			
			List<WebElement> BatchList = ReceivedInstruction.batchDataList(driver);
			int BatchCount = BatchList.size();
			int BatchIDCount = 0;
			String BatchID="";
			for(int b = 0; b < BatchCount; b++) {
				String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
				if(batchType.equals("OUTPUT")) {
					String status = ReceivedInstruction.batchInformation(driver, b, "BatchStatus").getText();
					Assert.assertEquals(status, "ACCEPTED");
					((ExtentTest) test).log(LogStatus.PASS, "Batch Status is: "+status);
					WebElement batchId = ReceivedInstruction.batchInformation(driver, b, "BatchID");
					BatchID = batchId.getText();
					WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_ACH.avg_explicit).click();
					BatchIDCount++;
					break;
				}
			}
			
			if(BatchIDCount != 1) {
				throw new Exception("Output Batch ID not found with status ACCEPTED");
			}
			
			ReceivedInstruction.verifyBatchPage(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			log.info("Navigate to Account Posting Tab");
    		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			
			String[] accTypeArr = {"SUS", "NOS"};
			String[] entryStatus = {"03", "03"};
			
			for(int i = 0; i < posCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				if(i < posCount) {
					Assert.assertEquals(getAccountTyp, accTypeArr[i]);
					Assert.assertEquals(EntryStatus, entryStatus[i]);
					((ExtentTest) test).log(LogStatus.PASS, "Account Posting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
				}
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			((ExtentTest) test).log(LogStatus.INFO, "Verify Finacle Account Posting Request and Response for OUTPUT batch");
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int finCountReq = 0;
			int finCountRes = 0;
			String finacleFile1 = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_finacle_Pos_1);
			System.out.println(finacleFile1);
			String finacleFileName1 = System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples +"\\"+sampleDir+finacleFile1;
			
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Request is Available");
					finCountReq++;
					List<JSONObject> listOfValues = null;
						if(finCountReq < 2 ) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
												
						List<String> dateFields = Arrays.asList("transactionDate","valueDate");
					    List<String> normalKeys = Arrays.asList("originalLocalInstrument", "originalPaymentFunction", "transactionDate");
					    List<String> additionalAttributesKeys = Arrays.asList();
					    List<String> movementRequestsKeys = Arrays.asList("valueDate","instructionType");
						
					    if(finCountReq == 1) {
							listOfValues = dataComparision.compareFinaclePosting(finacleFileName1, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
						}
					    
						for (JSONObject headLines : listOfValues) {	
				    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
				    		
				    		while (headLinekeys.hasNext()) {
				    		    Object key = headLinekeys.next();
				    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
				    		    JSONObject value = headLines.getJSONObject((String) key);
				    		    
				    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
				    		    while( innerMapKeys.hasNext() ) {
					    		    String innerMapKey = (String) innerMapKeys.next();
					    		    if (key == "Matched") {
					    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting request matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    	}
					    		    else {
					    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting request not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
					    		    }
					    		}
				    		}
				    	}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("RESPONSE")) {
					finCountRes++;
					if(finCountRes == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Response is Available");
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementResponses").getAsJsonArray().size(); k++) {
								String status = jsonObject.get("movementResponses").getAsJsonArray().get(k).getAsJsonObject().get("status").getAsString();
								if(status.equals("SUCCEEDED")) {
									System.out.println(status);
									Assert.assertEquals(status, "SUCCEEDED");
									((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting response Movement Responses is : " + status + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting response Movement Responses not matched : "+status);
								}
							}
						}catch(Exception finResp) {
							((ExtentTest) test).log(LogStatus.FAIL,finResp);
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
			}
		}
			
		if(finCountReq < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Request is not Available");
		}
		if(finCountRes < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Response is not Available");
		}
		
		log.info("Navigate to Transaction Information Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.transactionInfoTab(driver));
		WebElement pmtId2 = ReceivedInstruction.TransactionInformation(driver, 0, "PaymentID");
		String paymentId2 = WaitLibrary.waitForElementToBeVisible(driver, pmtId2, Constants_ACH.avg_explicit).getText();
		WaitLibrary.waitForElementToBeClickable(driver, pmtId2, Constants_ACH.avg_explicit).click();
		WaitLibrary.waitForAngular(driver);
		
		ReceivedInstruction.verifyPaymentPage(driver, paymentId2);
		Thread.sleep(Constants_ACH.tooshort_sleep);
		BrowserResolution.scrollDown(driver);
		
		log.info("Navigate to Batch Information");
		WebElement BatchInfo = ReceivedInstruction.batchInfoTab(driver);
		WaitLibrary.waitForElementToBeClickable(driver, BatchInfo, Constants_ACH.avg_explicit).click();
		
		log.info("Click on Processing batch Id");
		List<WebElement> batchList1 = ReceivedInstruction.batchDataList(driver);
		int batchCount1 = batchList1.size();
		int batchIDCount1 = 0;
		String batchID1 = "";
		for(int b = 0; b < batchCount1; b++) {
			String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
			if(batchType.equals("PROCESSING")) {
				WebElement batchId1 = ReceivedInstruction.batchInformation(driver, b, "BatchID");
				batchID1 = batchId1.getText();
				WaitLibrary.waitForElementToBeClickable(driver, batchId1, Constants_ACH.avg_explicit).click();
				batchIDCount1++;
				break;
			}
		}
		((ExtentTest) test).log(LogStatus.PASS, "Processing Batch ID is present");
		Thread.sleep(Constants_ACH.tooshort_sleep);

		if(batchIDCount1 != 1) {
			throw new Exception("PROCESSING Batch ID not found");
		}
		
		ReceivedInstruction.verifyBatchPage(driver);
		Thread.sleep(Constants_ACH.tooshort_sleep);
		
		log.info("Verify Status of Processing Batch Information");
		WebElement ProcessingBatchStatus = ReceivedInstruction.batchStatus(driver);
		String processingBatchStatus = WaitLibrary.waitForElementToBeVisible(driver, ProcessingBatchStatus, Constants_ACH.avg_explicit).getText();			
		System.out.println("Processing batch Status is: "+processingBatchStatus);
		Assert.assertEquals(processingBatchStatus, "ACCEPTED");
		((ExtentTest) test).log(LogStatus.PASS, "Processing batch Status is: " + processingBatchStatus);
		BrowserResolution.scrollDown(driver);
		
		log.info("Navigate to Account Posting Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
		((ExtentTest) test).log(LogStatus.INFO, "Verify Account Posting Entry Status for PROCESSING batch");
		
		List<WebElement> accountPostingList = ReceivedInstruction.accountPostingDetailsList(driver);
		int accPosCount = accountPostingList.size();
		WaitLibrary.waitForAngular(driver);
		
		BrowserResolution.scrollDown(driver);
		Thread.sleep(Constants_ACH.tooshort_sleep);
		Assert.assertEquals(accPosCount, 2);
		
		String[] accTypeArr1 = {"DDA", "SUS"};
		String[] entryStatus1 = {"03", "03"};
		for(int i = 0; i < accPosCount; i++) {
			String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
			String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
			if(i < accPosCount) {
				Assert.assertEquals(getAccountTyp, accTypeArr1[i]);
				Assert.assertEquals(EntryStatus, entryStatus1[i]);
				((ExtentTest) test).log(LogStatus.PASS, "Account Posting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
			}
		}
		
		log.info("Navigate to System Interaction Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
		BrowserResolution.scrollDown(driver);
		((ExtentTest) test).log(LogStatus.INFO, "Verify Finacle Account Posting Request and Response for PROCESSING batch");
		
		log.info("Verifying System Interaction");
		List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
		int sysIntDataCount = sysIntDataList.size();
		System.out.println(sysIntDataCount);
		int finCountRequest = 0;
		int finCountResponse = 0;
		String finacleFile2 = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_finacle_Pos_2);
		System.out.println(finacleFile2);
		String finacleFileName2 = System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples +"\\"+sampleDir+finacleFile2;
		
		for (int j=0; j< sysIntDataCount; j++) {
			String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
			String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
			System.out.println(invoPoint);
			System.out.println(RelationshipTxt);
			
			if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
				((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Request is Available");
				finCountRequest++;
				List<JSONObject> listOfValues = null;
					if(finCountRequest < 2 ) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
											
					List<String> dateFields = Arrays.asList("transactionDate","valueDate");
				    List<String> normalKeys = Arrays.asList("originalLocalInstrument", "originalPaymentFunction", "transactionDate");
				    List<String> additionalAttributesKeys = Arrays.asList();
				    List<String> movementRequestsKeys = Arrays.asList("valueDate","instructionType");
					
				    if(finCountRequest == 1) {
						listOfValues = dataComparision.compareFinaclePosting(finacleFileName2, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
					}
				    
					for (JSONObject headLines : listOfValues) {	
			    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
			    		
			    		while (headLinekeys.hasNext()) {
			    		    Object key = headLinekeys.next();
			    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
			    		    JSONObject value = headLines.getJSONObject((String) key);
			    		    
			    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
			    		    while( innerMapKeys.hasNext() ) {
				    		    String innerMapKey = (String) innerMapKeys.next();
				    		    if (key == "Matched") {
				    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting request matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
			    		    	}
				    		    else {
				    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting request not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    }
				    		}
			    		}
			    	}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
			}
			
			if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("RESPONSE")) {
				finCountResponse++;
				if(finCountResponse == 1) {
					((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Response is Available");
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementResponses").getAsJsonArray().size(); k++) {
							String status = jsonObject.get("movementResponses").getAsJsonArray().get(k).getAsJsonObject().get("status").getAsString();
							if(status.equals("SUCCEEDED")) {
								System.out.println(status);
								Assert.assertEquals(status, "SUCCEEDED");
								((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting response Movement Responses is : " + status + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting response Movement Responses not matched : "+status);
							}
						}
					}catch(Exception finResp) {
						((ExtentTest) test).log(LogStatus.FAIL,finResp);
				}
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
				js.executeScript("arguments[0].click();", modalCloseBtn);
				}
			}
		}
		if(finCountRequest < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Response is not Available");
		}
		if(finCountResponse < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Request is not Available");
		}
		
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());
		
	}
	catch(AssertionError ae){
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
		assertionerror=ae.getMessage();
		CaptureScreenshot_ACH.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
	}
	catch(Exception et){
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
		exceptionerror=et.getMessage();			
		CaptureScreenshot_ACH.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
	}
}
}
