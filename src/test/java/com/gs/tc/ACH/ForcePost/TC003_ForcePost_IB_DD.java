package com.gs.tc.ACH.ForcePost;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants;
import com.gs.utilities.Constants_ACH;

import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.dataComparision;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC003_ForcePost_IB_DD extends LoginLogout_ACH{
	@Test

	public void executeTC003() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC003_ForcePost_IB_DD");
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_ACH_Payment);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "Payment File name is : " + paymentFile);

			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyMMdd");
			String locDate = dateYMD.format(now);
			int julDate = LocalDate.now().getDayOfYear();
			((ExtentTest) test).log(LogStatus.INFO, "Current Date " + locDate);
			
			String pattern="000";
			DecimalFormat myFormatter = new DecimalFormat(pattern);
			String julianDate = myFormatter.format(julDate);
			((ExtentTest) test).log(LogStatus.INFO, "Julian Date " + julDate);
			
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ paymentFile, sampleDir+paymentFile);
			HashMap<String, String> updateValues = new HashMap<>();
			
			updateValues.put("{{yyMMdd}}", locDate);
			updateValues.put("{{jdt}}", julianDate);
			
			SampleFileModifier.updateNackFile(sampleDir+paymentFile, updateValues);
			((ExtentTest) test).log(LogStatus.INFO, "Process Payment File");
			
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_ACH.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_ACH.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "Payment File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id: " + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Payment ID");
			BrowserResolution.scrollDown(driver);
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Payment ID: " + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			Thread.sleep(Constants_ACH.tooshort_sleep);
				
			log.info("Verify Payment Status");
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "WAIT_MATCHED_MANDATE_ACTION");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			log.info("Click on Accept Mandate Button");
			WebElement acceptMandate = ReceivedInstruction.acceptMandateBtn(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Accept Mandate Button is Visible");
			WaitLibrary.waitForElementToBeClickable(driver, acceptMandate, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Accept Mandate sent for Approval");
			WaitLibrary.waitForAngular(driver);
			
			CommonMethods_ACH.approveStatement(driver, paymentID);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "After Approval");
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab2 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab2, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView2 = ReceivedInstruction.listView(driver);
			String listViewClass2 = WaitLibrary.waitForElementToBeVisible(driver, listView2, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass2.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView2, Constants_ACH.avg_explicit).click();
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
    		WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
    		searchInstruction.clear();
    		searchInstruction.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		CommonMethods_ACH.clickStatementWithFileName(driver, paymentFile);
    		WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on Payment ID");
			WebElement pmtId = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId = WaitLibrary.waitForElementToBeVisible(driver, pmtId, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtId, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentId);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Payment Status");
			WebElement PaymentStatus2 = ReceivedInstruction.getStatus(driver);
			String paymtStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatus2, Constants_ACH.avg_explicit).getText();			
			System.out.println("Payment Status after ForcePost Batch: "+paymtStatus);
			Assert.assertEquals(paymtStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status after ForcePost Batch: " + paymtStatus);
			
			log.info("Upload Advice File");
			String adviceFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_Advice_File);
			System.out.println(adviceFile);
			((ExtentTest) test).log(LogStatus.INFO, "Advice File is : " + adviceFile);
			
			((ExtentTest) test).log(LogStatus.INFO, "Current Date " + locDate);
			
			((ExtentTest) test).log(LogStatus.INFO, "Julian Date " + julDate);
			
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ adviceFile, sampleDir+adviceFile);

			updateValues.put("{{yyMMdd}}", locDate);
			updateValues.put("{{jdt}}", julianDate);
			
			SampleFileModifier.updateNackFile(sampleDir+adviceFile, updateValues);
			((ExtentTest) test).log(LogStatus.INFO, "Process Advice File");
			
    		log.info("Uploading Advice File via WinSCP");
			log.info("Source File is  :" + sampleDir + adviceFile);
			log.info("Destination File is  :" + Constants_ACH.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + adviceFile, Constants_ACH.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "Advice File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on refresh icon");
			WebElement refreshIcon = ReceivedInstruction.refreshIcon(driver);
			WaitLibrary.waitForElementToBeClickable(driver, refreshIcon, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on batch information tab");
			WebElement batchInformation = ReceivedInstruction.batchInfoTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, batchInformation, Constants_ACH.avg_explicit).click();
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			
			List<WebElement> batchList = ReceivedInstruction.batchDataList(driver);
			int batchCount = batchList.size();
			int batchIDCount = 0;
			String batchID="";
			for(int b = 0; b < batchCount; b++) {
				String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
				String status = ReceivedInstruction.batchInformation(driver, b, "BatchStatus").getText();
				if(batchType.equals("INPUT") && status.equals("ACCEPTED_DEBIT_SETTLEMENT_MISMATCH")) {
					((ExtentTest) test).log(LogStatus.PASS, "Batch Status is: "+status);
					WebElement batchId = ReceivedInstruction.batchInformation(driver, b, "BatchID");
					batchID = batchId.getText();
					System.out.println(batchID);
					WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_ACH.avg_explicit).click();
					batchIDCount++;
					break;
				}
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			WaitLibrary.waitForAngular(driver);
			
			if(batchIDCount != 1) {
				throw new Exception("Input Batch ID not found with status ACCEPTED_DEBIT_SETTLEMENT_MISMATCH");
			}
			
			ReceivedInstruction.verifyBatchPage(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Click on ForcePost Batch Button");
			WaitLibrary.waitForElementToBeClickable(driver, ReceivedInstruction.ForcePost(driver), Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "ForcePost Batch sent for Approval");
			
			log.info("Send ForcePost Batch for Approval");
			CommonMethods_ACH.approveStatement(driver, batchID);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "After ForcePost Batch Approval");
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab3 = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab3, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView3 = ReceivedInstruction.listView(driver);
			String listViewClass3 = WaitLibrary.waitForElementToBeVisible(driver, listView3, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass3.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView3, Constants_ACH.avg_explicit).click();
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
    		WebElement searchInstruction2 = ReceivedInstruction.searchWithInsID(driver);
    		searchInstruction2.clear();
    		searchInstruction2.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		CommonMethods_ACH.clickStatementWithFileName(driver, paymentFile);
    		WaitLibrary.waitForAngular(driver);
    		Thread.sleep(Constants_ACH.short_sleep);
//			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Instruction Page with Transport name");
///			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			WebElement pmtId2 = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentId2 = WaitLibrary.waitForElementToBeVisible(driver, pmtId2, Constants_ACH.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtId2, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentId2);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Payment Status");
			WebElement PaymentStatus3 = ReceivedInstruction.getStatus(driver);
			String paymtStatus2 = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatus3, Constants_ACH.avg_explicit).getText();			
			System.out.println("Payment Status after ForcePost Batch: "+paymtStatus2);
			Assert.assertEquals(paymtStatus2, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status after ForcePost Batch: " + paymtStatus2);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on batch information tab");
			WebElement batchInfo = ReceivedInstruction.batchInfoTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, batchInfo, Constants_ACH.avg_explicit).click();
			BrowserResolution.scrollDown(driver);
			
			List<WebElement> batchListData = ReceivedInstruction.batchDataList(driver);
			int BatchCount = batchListData.size();
			int BatchIDCount = 0;
			String BatchID="";
			for(int b = 0; b < BatchCount; b++) {
				String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
				if(batchType.equals("INPUT")) {
					((ExtentTest) test).log(LogStatus.INFO, "Open Input Batch information");
					WebElement batchId = ReceivedInstruction.batchInformation(driver, b, "BatchID");
					BatchID = batchId.getText();
					System.out.println(BatchID);
					WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_ACH.avg_explicit).click();
					BatchIDCount++;
					break;
				}
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			if(BatchIDCount != 1) {
				throw new Exception("Input Batch ID not found");
			}
			
			ReceivedInstruction.verifyBatchPage(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Input Batch Status");
			WebElement inputBatchStatus = ReceivedInstruction.batchStatus(driver);
			String batchStatus = WaitLibrary.waitForElementToBeVisible(driver, inputBatchStatus, Constants_ACH.avg_explicit).getText();			
			System.out.println("Input batch Status is: "+batchStatus);
			Assert.assertEquals(batchStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Input batch Status is: " + batchStatus);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Navigate to Account Posting Tab");
    		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
    		((ExtentTest) test).log(LogStatus.INFO, "Verify Account Posting for Input Batch");
    		
			List<WebElement> accountPostingList = ReceivedInstruction.accountPostingDetailsList(driver);
			int accPosCount = accountPostingList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			Assert.assertEquals(accPosCount, 2);
			((ExtentTest) test).log(LogStatus.PASS, "Account Posting verified with "+accPosCount+" Entries");
			
			String[] entryStatus1 = {"03", "03"};
			for(int i = 0; i < accPosCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				if(i < accPosCount) {
					Assert.assertEquals(EntryStatus, entryStatus1[i]);
					((ExtentTest) test).log(LogStatus.PASS, "Account Posting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
				}
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			((ExtentTest) test).log(LogStatus.INFO, "Verify Finacle Account Posting Request and response for Input Batch");
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int finCountRequest = 0;
			int finCountResponse = 0;
			String finacleFile1 = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_finacle_Pos_1);
			System.out.println(finacleFile1);
			String finacleFileName1 = System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples +"\\"+sampleDir+finacleFile1;
			
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				System.out.println(invoPoint);
				System.out.println(RelationshipTxt);
				
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Request is Available");
					finCountRequest++;
					List<JSONObject> listOfValues = null;
						if(finCountRequest < 2 ) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
												
						List<String> dateFields = Arrays.asList("transactionDate","valueDate");
					    List<String> normalKeys = Arrays.asList("originalLocalInstrument", "originalPaymentFunction", "transactionDate");
					    List<String> additionalAttributesKeys = Arrays.asList();
					    List<String> movementRequestsKeys = Arrays.asList("valueDate","instructionType");
						
					    if(finCountRequest == 1) {
							listOfValues = dataComparision.compareFinaclePosting(finacleFileName1, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
						}
					    
						for (JSONObject headLines : listOfValues) {	
				    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
				    		
				    		while (headLinekeys.hasNext()) {
				    		    Object key = headLinekeys.next();
				    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
				    		    JSONObject value = headLines.getJSONObject((String) key);
				    		    
				    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
				    		    while( innerMapKeys.hasNext() ) {
					    		    String innerMapKey = (String) innerMapKeys.next();
					    		    if (key == "Matched") {
					    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting request matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    	}
					    		    else {
					    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting request not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
					    		    }
					    		}
				    		}
				    	}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("RESPONSE")) {
					finCountResponse++;
					if(finCountResponse == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Response is Available");
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {	
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementResponses").getAsJsonArray().size(); k++) {
								String status = jsonObject.get("movementResponses").getAsJsonArray().get(k).getAsJsonObject().get("status").getAsString();
								
								if(status.equals("SUCCEEDED")) {
									System.out.println(status);
									Assert.assertEquals(status, "SUCCEEDED");
									((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting response Movement Responses is : " + status + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting response Movement Responses not matched : "+status);
								}
								
							}
						}catch(Exception finResp) {
							((ExtentTest) test).log(LogStatus.FAIL,finResp);
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
			}
		}
			
		if(finCountRequest < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Request is not Available");
		}
		if(finCountResponse < 1) {
			((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Response is not Available");
		}
			
		log.info("Navigate to Transaction Information Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.transactionInfoTab(driver));
		WebElement payID = ReceivedInstruction.TransactionInformation(driver, 0, "PaymentID");
		String PaymentID = WaitLibrary.waitForElementToBeVisible(driver, payID, Constants_ACH.avg_explicit).getText();
		WaitLibrary.waitForElementToBeClickable(driver, payID, Constants_ACH.avg_explicit).click();
		WaitLibrary.waitForAngular(driver);
		
		ReceivedInstruction.verifyPaymentPage(driver, PaymentID);
		Thread.sleep(Constants_ACH.tooshort_sleep);
		BrowserResolution.scrollDown(driver);
		
		log.info("Navigate to Account Posting Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
		((ExtentTest) test).log(LogStatus.INFO, "Verify Account Posting for Transaction Information");
		
		List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
		int posCount = accountPostingDataList.size();
		WaitLibrary.waitForAngular(driver);
		BrowserResolution.scrollDown(driver);
		Thread.sleep(Constants_ACH.tooshort_sleep);
		System.out.println(posCount);
		Assert.assertEquals(posCount, 2);
		((ExtentTest) test).log(LogStatus.PASS, "Account Posting verified with "+posCount+" Entries");
				
		String[] entryStatus = {"03", "03"};
		for(int i = 0; i < posCount; i++) {
			String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
			String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
			if(i < posCount) {
				Assert.assertEquals(EntryStatus, entryStatus[i]);
				((ExtentTest) test).log(LogStatus.PASS, "Account Posting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
			}
		}
		
		log.info("Navigate to System Interaction Tab");
		js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
		((ExtentTest) test).log(LogStatus.PASS, "Verify Finacle Account Posting request and response for Transaction Information");
		BrowserResolution.scrollDown(driver);
		
		log.info("Verifying System Interaction");
		List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
		int sysIntDataCountIn = sysIntDataListIn.size();
		System.out.println(sysIntDataCountIn);
		int finCountReq = 0;
		int finCountRes = 0;
		String finacleFile2 = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_finacle_Pos_2);
		System.out.println(finacleFile2);
		String finacleFileName2 = System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples +"\\"+sampleDir+finacleFile2;
		
		for (int j=0; j< sysIntDataCountIn; j++) {
			String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
			String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
			
			if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
				((ExtentTest) test).log(LogStatus.INFO, "Finacle Account Posting Request is Available");
				finCountReq++;
				List<JSONObject> listOfValues = null;
					if(finCountReq < 2 ) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
											
					List<String> dateFields = Arrays.asList("transactionDate","valueDate");
				    List<String> normalKeys = Arrays.asList("originalLocalInstrument", "originalPaymentFunction", "transactionDate");
				    List<String> additionalAttributesKeys = Arrays.asList();
				    List<String> movementRequestsKeys = Arrays.asList("valueDate","instructionType");
					
				    if(finCountReq == 1) {
						listOfValues = dataComparision.compareFinaclePosting(finacleFileName2, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
					}
				    
					for (JSONObject headLines : listOfValues) {	
			    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
			    		
			    		while (headLinekeys.hasNext()) {
			    		    Object key = headLinekeys.next();
			    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
			    		    JSONObject value = headLines.getJSONObject((String) key);
			    		    
			    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
			    		    while( innerMapKeys.hasNext() ) {
				    		    String innerMapKey = (String) innerMapKeys.next();
				    		    if (key == "Matched") {
				    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting request matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
			    		    	}
				    		    else {
				    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting request not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    }
				    		}
			    		}
			    	}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
			}
			
			if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("RESPONSE")) {
				finCountRes++;
				if(finCountRes == 1) {
					((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Response is Available");
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {	
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementResponses").getAsJsonArray().size(); k++) {
							String status = jsonObject.get("movementResponses").getAsJsonArray().get(k).getAsJsonObject().get("status").getAsString();
							
							if(status.equals("SUCCEEDED")) {
								System.out.println(status);
								Assert.assertEquals(status, "SUCCEEDED");
								((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting response Movement Responses is : " + status + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting response Movement Responses not matched : "+status);
							}
							
						}
					}catch(Exception jse) {
					throw new Exception(jse.getMessage());
				}
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
				js.executeScript("arguments[0].click();", modalCloseBtn);
			}
		}
	}
		
	if(finCountReq < 1) {
		((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Request is not Available");
	}
	if(finCountRes < 1) {
		((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Response is not Available");
	}
	
	ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
	ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
	ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());
		
	}
	catch(AssertionError ae){
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
		assertionerror=ae.getMessage();
		CaptureScreenshot_ACH.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
	}
	catch(Exception et){
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
		exceptionerror=et.getMessage();			
		CaptureScreenshot_ACH.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
	}
}
}
