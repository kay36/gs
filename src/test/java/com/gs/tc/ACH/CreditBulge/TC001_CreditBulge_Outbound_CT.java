package com.gs.tc.ACH.CreditBulge;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import junit.framework.Assert;

public class TC001_CreditBulge_Outbound_CT extends LoginLogout_ACH {
	
	@Test
	
	public void executeTC01() throws Exception {
		try {
			
            Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC001_CreditBulge_Outbound_CT");
			
			LocalDateTime now = LocalDateTime.now();			
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			
			// Fetch sample Directory
				 String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
						System.out.println(sampleDir);
						 
			// Fetch Payment File Name
				 String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Credit_Bulge" ,TestCaseName, Constants_ACH.Col_ACH_Payment);
						System.out.println(paymentFile);
						((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
						
			// Creating sample file from Ref
				 CommonMethods.createSampleRef(Constants_ACH.gs_Samples+sampleDir+"Ref\\"+paymentFile,Constants_ACH.gs_Samples+ sampleDir+paymentFile);
						System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
						System.out.println("Dest : "+sampleDir+ paymentFile);
						// creating new values for Payment file
						HashMap<String, String> updateVal = new HashMap<>();
						
						updateVal.put("{{yyyy-MM-dd}}", locDateYr);
						
			// Updating Payment File
						CommonMethods.updateSampleFile(Constants_ACH.gs_Samples+sampleDir+paymentFile, updateVal);
						
			// Upload Payment File
			     log.info("Uploading Payment File via WinSCP");
				 log.info("Source File is  :" + sampleDir + paymentFile);
				 log.info("Destination File is  :" + Constants_ACH.ACHPain001ChannelIn);
				 FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_ACH.ACHPain001ChannelIn);
				 ((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
				 Thread.sleep(Constants_ACH.short_sleep);
				 
				 log.info("Click on Received Instruction Tab");
					WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
					WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
				 
				 log.info("Click on List View");
					WebElement listView = ReceivedInstruction.listView(driver);
					String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");			
					
					if (listViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {
						WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
					}
					
			 // Verify Status of Uploaded Payment File
				    BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
					js.executeScript("window.scrollBy(0,-120)");
					
			 // Get and click Instruction ID
				    WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
					String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
					((ExtentTest) test).log(LogStatus.INFO, "Instruction id: " + insId);
					WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
					
					log.info("Verifying Instruction Page with Transport name");
					ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					log.info("Verifying Instruction Status");
					WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
					String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
					Assert.assertEquals(insStatus, "DEBULKED");
					((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
					Thread.sleep(Constants_ACH.short_sleep);
					BrowserResolution.scrollDown(driver);
					
					log.info("Click on Payment ID");
					Thread.sleep(Constants_ACH.tooshort_sleep);
				    java.util.List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
					int paymentscount = paymentsList.size();
					String paymentID="";
					for(int l=0; l < paymentscount; l++) {
						String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
						WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
						paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
						if(status.equals("WAIT DEBIT PROCESSING BATCH")) {
							TimeUnit.MINUTES.sleep(4);
						}else if(status.equals("TIME WAREHOUSED")) {
							WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
							WaitLibrary.waitForAngular(driver);
							Thread.sleep(Constants_ACH.tooshort_sleep);
							
							CommonMethods.forceRelease(driver, paymentID, paymentFile, insId);
							WaitLibrary.waitForAngular(driver);
							Thread.sleep(Constants_ACH.tooshort_sleep);
							TimeUnit.MINUTES.sleep(4);
						}
						WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
						WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
						break;
					}
					Thread.sleep(Constants_ACH.tooshort_sleep);
					((ExtentTest) test).log(LogStatus.INFO, "Payment ID is : "+paymentID);
					
					log.info("Verifying Payment Page with PaymentId");
					ReceivedInstruction.verifyPaymentPage(driver, paymentID);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					log.info("Click on refresh icon");
					WebElement refreshIcon = ReceivedInstruction.refreshIcon(driver);
					WaitLibrary.waitForElementToBeClickable(driver, refreshIcon, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					
					
					log.info("Verify Payment Status");
					WebElement PaymentStatusEle = ReceivedInstruction.getStatus(driver);
					String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusEle, Constants_ACH.avg_explicit).getText();
					Assert.assertEquals(PaymentStatus, "WAIT_DEBIT_PROCESSING_BATCH");
					((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
					BrowserResolution.scrollDown(driver);
					
					log.info("Click on Batch Information Tab");
					WebElement batchInfoTab = ReceivedInstruction.batchInfoTab(driver);
					WaitLibrary.waitForElementToBeClickable(driver, batchInfoTab, Constants_ACH.avg_explicit).click();
					
					List<WebElement> batchList = ReceivedInstruction.batchDataList(driver);
					int batchCount = batchList.size();
					int batchIDCount = 0;
					String batchID="";
					for(int b = 0; b < batchCount; b++) {
						String batchType = ReceivedInstruction.batchInformation(driver, b, "BatchType").getText();
						if(batchType.equals("PROCESSING")) {
							WebElement batchId = ReceivedInstruction.batchInformation(driver, b, "BatchID");
							batchID = batchId.getText();
							System.out.println(batchID);
							WaitLibrary.waitForElementToBeClickable(driver, batchId, Constants_ACH.avg_explicit).click();
							batchIDCount++;
							break;
							
						}
					}
					Thread.sleep(Constants_ACH.tooshort_sleep);
					WaitLibrary.waitForAngular(driver);
					System.out.println(batchCount);
					if(batchIDCount != 1) {
						throw new Exception("Output Batch ID not found with status WAITING_DEBITFUNDCONTROLRESPONSE");
					}
					
					ReceivedInstruction.verifyPaymentPage(driver, batchID);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					log.info("Verify Batch Status");
					WebElement BatchStatusEle = ReceivedInstruction.batchStatus(driver);
					String BatchStatus = WaitLibrary.waitForElementToBeVisible(driver, BatchStatusEle, Constants_ACH.avg_explicit).getText();
					Assert.assertEquals(BatchStatus, "WAIT_DEBIT_PROCESSING_BATCH");
					((ExtentTest) test).log(LogStatus.PASS, "Batch Status : " + BatchStatus);
					BrowserResolution.scrollDown(driver);
					
					
					log.info("Navigate to System Interaction Tab");
					js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
					BrowserResolution.scrollDown(driver);
					((ExtentTest) test).log(LogStatus.INFO, "Verify Values in System Interaction");
		    		
					List<WebElement> systemInteractionData = ReceivedInstruction.sysInteractionList(driver);
					int sysIntDataCountIn = systemInteractionData.size();
					System.out.println(sysIntDataCountIn);
					
					for (int j=0; j< sysIntDataCountIn; j++) {
						String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();				
						String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
						
						
						if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
							log.info("Click on Action Dropdown");
							WebElement actiondropdown = ReceivedInstruction.actionBtn(driver);
							WaitLibrary.waitForElementToBeClickable(driver, actiondropdown, Constants_ACH.avg_explicit).click();
							
//							log.info("Click on resendButton");
//							WebElement resendButton = ReceivedInstruction.resendButton1(driver);
//							WaitLibrary.waitForElementToBeClickable(driver, resendButton, Constants_ACH.avg_explicit).click();
						}
						
						
					}
					
			
		
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
		 }
		 catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
		  }
			
		
	}
	
	

}
