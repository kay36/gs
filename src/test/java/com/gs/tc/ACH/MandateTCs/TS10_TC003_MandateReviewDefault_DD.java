package com.gs.tc.ACH.MandateTCs;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.gs.pages.AmountLimitProfile;
import com.gs.pages.Approvals;
import com.gs.pages.Mandate;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.dataComparision;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TS10_TC003_MandateReviewDefault_DD extends LoginLogout_ACH{
	@Test

	public void executeTC03() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);
            String mandateProfile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Mandate_TC", TestCaseName,Constants_ACH.Col_Mandate_Profile);
			
			log.info("Navigate to Onboarding Data");
			WebElement onboardingData = Mandate.onboardingData(driver);
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
    		Thread.sleep(Constants_ACH.short_sleep);

			log.info("Click on Mandate");
			WebElement mandateTab = Mandate.mandateTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, mandateTab, Constants_ACH.avg_explicit).click();

			log.info("Click on Grid View");
			WebElement gridView = AmountLimitProfile.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.contains(Constants_ACH.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_ACH.avg_explicit).click();					
			}
			Thread.sleep(Constants_ACH.short_sleep);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
        	
    		log.info("Enter Unique Mandate Reference to filter");
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(mandateProfile, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
//    		log.info("Click on filter icon");
//    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();    		
//    		WaitLibrary.waitForAngular(driver);

    		log.info("Click on Unique Mandate Reference");
			List<WebElement> mandateTableData = Mandate.mandateDataList(driver);
			int mandateDataCount = mandateTableData.size();
			
			String uniqueMandateRef = null;
			for(int l=0; l<mandateDataCount; l++) {
				try {
					uniqueMandateRef = Mandate.mandateTableData(driver, l, 0).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Mandate Profile found");
				}
				
				if(uniqueMandateRef.equals(mandateProfile)) {
					WebElement uniqueMandateRefe = Mandate.mandateTableData(driver, l, 0);
					WaitLibrary.waitForElementToBeClickable(driver, uniqueMandateRefe, Constants_ACH.avg_explicit).click();
					break;
				}else if (l==mandateDataCount-1 && !uniqueMandateRef.equals(mandateProfile)) {
					throw new Exception("Mandate Profile is not Available");
				}
			}

    		log.info("Click on Edit");
    		WaitLibrary.waitForAngular(driver);
    		js.executeScript("arguments[0].click();",  Mandate.editBtn(driver));
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Select mandate action");
			WebElement mandateAction = Mandate.MandateAction(driver);
			BrowserResolution.scrollToElement(driver, mandateAction);
			js.executeScript("window.scrollBy(0,-120)");
			Select MandateAction = new Select(mandateAction);
			MandateAction.selectByVisibleText("REVIEW ALL");
			((ExtentTest) test).log(LogStatus.INFO, "Review Mandate Action Selected");
			WaitLibrary.waitForAngular(driver);
			
			log.info("Select Default mandate action");
			WebElement defaultMandateAction = Mandate.DefaultMandateAction(driver);
			BrowserResolution.scrollToElement(driver, defaultMandateAction);
			js.executeScript("window.scrollBy(0,-120)");
			Select DefaultMandateAction = new Select(defaultMandateAction);
			DefaultMandateAction.selectByVisibleText("REJECT");
			((ExtentTest) test).log(LogStatus.INFO, "Reject Mandate Action Selected");
			WaitLibrary.waitForAngular(driver);			
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on Save Changes");
			WebElement saveChangeBtn = Mandate.SaveChangeBtn(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, saveChangeBtn, Constants_ACH.avg_explicit).click();
    		((ExtentTest) test).log(LogStatus.INFO, "Saved Mandate Action");
    		WaitLibrary.fluentWait(driver);
    		Thread.sleep(Constants_ACH.long_sleep);
    		
    		log.info("Approve Unique reference code");
			CommonMethods_ACH.approveStatement(driver, mandateProfile);
			WaitLibrary.waitForAngular(driver);
			

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Mandate_TC" ,TestCaseName, Constants_ACH.Col_ACH_Payment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
			System.out.println("Dest : "+sampleDir+ paymentFile);
			
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyMMdd");
			String locDate = dateYMD.format(now);
			int julDate = LocalDate.now().getDayOfYear();
			((ExtentTest) test).log(LogStatus.INFO, "Current Date " + "<b>"+locDate);
	
			String pattern="000";
			DecimalFormat myFormatter = new DecimalFormat(pattern);
			String julianDate = myFormatter.format(julDate);
			((ExtentTest) test).log(LogStatus.INFO, "Julian Date " +"<b>"+ julDate);
			
			SampleFileModifier.createNACKRef(sampleDir+"Ref\\"+ paymentFile, sampleDir+paymentFile);
			HashMap<String, String> updateValues = new HashMap<>();
			
			updateValues.put("{{yyMMdd}}", locDate);
			updateValues.put("{{jdt}}", julianDate);
			
			SampleFileModifier.updateNackFile(sampleDir+paymentFile, updateValues);
			((ExtentTest) test).log(LogStatus.INFO, "Process Payment File");
			
    		// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_ACH.FEDACH_IN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_ACH.FEDACH_IN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Navigate to Payment Module");
			WebElement paymentModule = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, paymentModule);
			WaitLibrary.waitForElementToBeClickable(driver, paymentModule, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);

			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}

			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");

			String rec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
            Thread.sleep(Constants_ACH.short_sleep);
			
            log.info("Click on Refresh Icon");
            String status = ReceivedInstruction.originalPaymentReference(driver, 0, "Status").getText();
            if(!status.equals("TOBEPROCESSED")) {
            	String mopVerification = ReceivedInstruction.originalPaymentReference(driver, 0, "MethodOfPayment").getText();
                String verifiedText = CommonMethods_ACH.originalPaymentReferenceStatus(driver, mopVerification, "ACH_IN","MethodOfPayment");
            }
            WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Payment ID");
			List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID = "";
			for(int l=0; l<paymentscount; l++) {
				String paymentFun = ReceivedInstruction.originalPaymentReference(driver, l, "MethodOfPayment").getText();
				if(paymentFun.equals("ACH_IN")) {
					WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
					paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
					break;
				}else if (l==paymentscount-1 && !paymentFun.equals("ACH_IN")) {
					throw new Exception("ACH_IN payment is not Available");
				}
			}
			
			WaitLibrary.waitForAngular(driver);			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);						
			WaitLibrary.waitForAngular(driver);	
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "WAIT_MATCHED_MANDATE_ACTION");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);			
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Payment Event Log");
			Thread.sleep(Constants_ACH.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			List<WebElement> paymentEventLog = ReceivedInstruction.eventLogList(driver);
			int eventLogCount = paymentEventLog.size();
			int eventCount=0;
			int eventDescription1=0;
//			String expectedDes1 = "No Matching Mandate profile found for SchemeID :ACH_IN|DEBIT\", Hence processing payment with Scheme Default ACTION - 'REVIEW' set by picklistmanager for given scheme.";
			String expectedDes1 = "Matching Mandate profile found with UniqueMandateReference-DD_Mandate_VB for SchemeID :ACH_IN\", Hence processing payment with given ACTION - 'REVIEW ALL' set on Mandate";
			for (int j=0; j< eventLogCount; j++) {				
				String event = ReceivedInstruction.eventLogData(driver, j, "Event").getText();
				String description = ReceivedInstruction.eventLogData(driver, j, "Description").getText();
				if(event.equals("ValidateAndProcessMandate")) {
					eventCount++;
					BrowserResolution.scrollToElement(driver, ReceivedInstruction.eventLogData(driver, j, "Event"));
					if(eventCount == 1 && description.equals(expectedDes1)) {
						eventDescription1++;
						Assert.assertEquals(description, expectedDes1);
						((ExtentTest) test).log(LogStatus.PASS,"Payment Event Log verified::" +expectedDes1);
					}
				}				
			}
			if(eventDescription1 != 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"Payment Event Log is not matched::" + expectedDes1);
			}
 		
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying System Interaction");
			BrowserResolution.scrollDown(driver);
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();			
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {					
					becNotfCount++;
					if(becNotfCount == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						
						// Getting Object Data						
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants_ACH.avg_explicit);
						String actualBECString = objDataXml.getText();
						
						// Create lists of "FieldNames" & "FieldValues" to be check
					    List<String> fieldNames = Arrays.asList("MandateCategory", "MandateReference", "MandateSchemeName", "MandateType", "MandateAction", "MandateDefaultAction", "MandateDescription");
//					    List<String> doMatch = Arrays.asList("SCHEME-DEFAULT", "ACH_IN_SCHEME-DEFAULT", "ACH_IN", "DEBIT", "REVIEW", "REJECT",  "Default ACH_IN DEBIT Action");
					    List<String> doMatch = Arrays.asList("BULK", "DD_Mandate_VB", "ACH_IN", "DEBIT", "REVIEW ALL", "REJECT",  "Default ACH_IN DEBIT Action");
					    
					    // call "compareBECNotification()"
						List<JSONObject> listOfValues = dataComparision.compareBECMandateFields(actualBECString, fieldNames, doMatch);
						
						// Get comparison status from "compareBECNotification()"
						for (JSONObject headLines : listOfValues) {	
							// create Iterator to iterate keys
				    		Iterator<?> headLinekeys = headLines.keys();
				    		while (headLinekeys.hasNext()) {
				    		    Object key = headLinekeys.next();
				    		    
				    		    // print headLines (Matched / Unmatched)
				    		    System.out.println(key.toString());						
				    		    JSONObject value = headLines.getJSONObject((String) key);
				    		    
				    		    // create Iterator to iterate within a headLines
				    		    Iterator<?> innerMapKeys = value.keys();
				    		    while( innerMapKeys.hasNext() ) {
					    		    String innerMapKey = (String) innerMapKeys.next();
					    		    if (key == "Matched") {
					    		    	// print both keys and values which is in list
					    		    	((ExtentTest) test).log(LogStatus.PASS, innerMapKey + " : " + value.get(innerMapKey));
				    		    	}
					    		    else {
					    		    	// print both keys and values which is in list
					    		    	((ExtentTest) test).log(LogStatus.FAIL, innerMapKey + " : " + value.get(innerMapKey));
					    		    }
					    		}
				    		}
				    	}
						
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
				}
			}
			
			BrowserResolution.scrollUp(driver);
			//Click on Default Action Mandate button
			WebElement defaultActionMandateBtn = ReceivedInstruction.defaultMandateBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, defaultActionMandateBtn, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Default Mandate button Selected");
			
    		// Approver Payment Control Data
    		CommonMethods_ACH.approveStatement(driver, paymentID);
    		WaitLibrary.waitForAngular(driver);
    		
    		Thread.sleep(Constants_ACH.tooshort_sleep);
    		log.info("Navigate to Received Instructions");
    		WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
    		BrowserResolution.scrollToElement(driver, recInsTabInIn);
    		WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.short_sleep);
			
    		log.info("Click on List View");
    		WebElement listViewIn = ReceivedInstruction.listView(driver);
    		String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants_ACH.avg_explicit).getAttribute("class");

    		if (listViewClassIn.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants_ACH.avg_explicit).click();
			}

    		Thread.sleep(Constants_ACH.tooshort_sleep);
    		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
    		searchInstructionIn.clear();
    		searchInstructionIn.sendKeys(insId, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);

    		CommonMethods_ACH.clickStatementWithFileName(driver, paymentFile);
			Thread.sleep(Constants_ACH.short_sleep);
    		//BrowserResolution.scrollDown(driver);
    		
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportNamee = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
            Thread.sleep(Constants_ACH.short_sleep);
            
			WebElement getPaymentId = ReceivedInstruction.searchPaymentID(driver, paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants_ACH.avg_explicit).click();			
			WaitLibrary.waitForAngular(driver);
    		
			log.info("Verifying Payment Page with PaymentId");
			WebElement PaymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);			
			WaitLibrary.waitForAngular(driver);
    		
			// Verifying Payment status
			WebElement PaymentStatusEle = ReceivedInstruction.getStatus(driver);
			String PaymentStatuss = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusEle, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatuss, "REJECT_INPROGRESS");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatuss);
			
			BrowserResolution.scrollDown(driver);
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			List<WebElement> linkedMsgsDataList = new ArrayList<WebElement>();
			try {
			log.info("Get Linked Message Id");
			linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			} catch (Exception e) {			
				((ExtentTest) test).log(LogStatus.FAIL, "Linked Message ID not available");
				throw new Exception("Linked Message ID not available");
			}			

			log.info("Verify Presence of Linked Message Id");
			int linkMsgscount = linkedMsgsDataList.size();		
			for(int l=0; l<linkMsgscount; l++) {
			String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText();
			if(linkMsgFun.equals("Request For Payment - Return of Funds")) {
				((ExtentTest) test).log(LogStatus.PASS, "Outgoing spawned message created");
			}
			else
				{
				((ExtentTest) test).log(LogStatus.FAIL, "Outgoing spawned message is not created");
				}
			}	

			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());

		}
				
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
		}
	}
}
