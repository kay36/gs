package com.gs.tc.ACH.MandateTCs;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.gs.pages.AmountLimitProfile;
import com.gs.pages.Approvals;
import com.gs.pages.Mandate;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TS01_TC002_MandateProfile extends LoginLogout_ACH{
	@Test

	public void executeTC02() throws Exception {
		try {
			Log.startTestCase(log, TestCaseName);
			
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			
			String formDataJson = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Mandate_TC" ,TestCaseName, Constants_ACH.Col_Profile_Form);
			
			System.out.println(System.getProperty("user.dir") + Constants_ACH.gs_Samples + sampleDir + formDataJson);
			
			// call function to get variables
			JsonObject profileData = CommonMethods.getFormDataJson(System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples + sampleDir + formDataJson);
			
			//get Current Date
			String currentDate = GenericFunctions.getCurrentDateOnly("yyyy-MM-dd");

			log.info("Navigate to Onboarding Data");
			WebElement onboardingData = Mandate.onboardingData(driver);
			BrowserResolution.scrollToElement(driver, onboardingData);
			WaitLibrary.waitForElementToBeClickable(driver, onboardingData, Constants_ACH.avg_explicit).click();

			log.info("Click on Mandate");
			WebElement mandateTab = Mandate.mandateTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, mandateTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Grid View");
			WebElement gridView = AmountLimitProfile.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_ACH.avg_explicit).click();					
			}
			
			WaitLibrary.waitForAngular(driver);
			log.info("Click on add new mandate");
			WebElement addNewMandate = Mandate.addNewMandate(driver);
			WaitLibrary.waitForElementToBeClickable(driver, addNewMandate, Constants_ACH.avg_explicit).click();
			
			log.info("Enter Unique Mandate Reference");
			String uniqueMandateReferenceVal = profileData.get("Unique Mandate Reference").getAsString();
			WebElement uniqueMandateReference = Mandate.uniqueMandateRef(driver);
			WaitLibrary.waitForElementToBeClickable(driver, uniqueMandateReference, Constants_ACH.avg_explicit).click();
			uniqueMandateReference.sendKeys(uniqueMandateReferenceVal);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Unique mandate reference : "+uniqueMandateReferenceVal);
			
			//Select scheme ID
			log.info("Click on Scheme ID");
			String schemeIDValue = profileData.get("Scheme ID").getAsString();
			WebElement schemeID = Mandate.SchemeID(driver);
			WaitLibrary.waitForElementToBeClickable(driver, schemeID, Constants_ACH.avg_explicit).click();

//			log.info("Send values to Scheme ID Search box");
//			WebElement SchemeIDSearch = Mandate.SchemeIDSearch(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, SchemeIDSearch, Constants_ACH.avg_explicit).click();
//			SchemeIDSearch.sendKeys(schemeIDValue);

			WebElement schemeIDList = Mandate.SchemeIDList(driver, schemeIDValue);
			WaitLibrary.waitForElementToBeClickable(driver, schemeIDList, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Selected Scheme ID : "+schemeIDValue);
			
			//Select branch
			log.info("Select Branch code from Dropdown");
			String branchCodeValue = profileData.get("Branch Code").getAsString();
			WebElement branchCode = Mandate.BranchCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, branchCode, Constants_ACH.avg_explicit).click();
			
			WebElement branchCodeSearch = Mandate.BranchCodeSearchField(driver);
			WaitLibrary.waitForElementToBeClickable(driver, branchCodeSearch, Constants_ACH.avg_explicit).click();
			branchCodeSearch.sendKeys(branchCodeValue);
			
			WebElement branchCodeList = Mandate.BranchCodeList(driver, branchCodeValue);
			WaitLibrary.waitForElementToBeClickable(driver, branchCodeList, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Selected Branch code : "+branchCodeValue);
			
			//Select Account Number
			log.info("Select Account Number from Dropdown");
			String accNumberValue = profileData.get("Account Number").getAsString();
			WebElement accNumber = Mandate.AccNumberSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, accNumber, Constants_ACH.avg_explicit).click();
			
			WebElement accNumberSearch = Mandate.AccNumberSearchField(driver);
			WaitLibrary.waitForElementToBeClickable(driver, accNumberSearch, Constants_ACH.avg_explicit).click();
			accNumberSearch.sendKeys(accNumberValue, Keys.ENTER);
			
			WebElement getACC = Mandate.accget(driver);
			Thread.sleep(2000);
			WaitLibrary.waitForElementToBeVisible(driver, getACC, Constants_ACH.avg_explicit);
			String getAccNum=Mandate.accget(driver).getText();
			
			System.out.println(getAccNum);
			WebElement accountNumberList = Mandate.accNumberList(driver, getAccNum);
			WaitLibrary.waitForElementToBeClickable(driver, accountNumberList, Constants_ACH.avg_explicit).click();
			
			((ExtentTest) test).log(LogStatus.INFO, "Selected Account Number : "+accNumberValue);
			
			//Mandate start date
			log.info("Enter Mandate Start Date");
			String mandateStartVal = profileData.get("Mandate Start Date").getAsString();
			WebElement mandateStart = Mandate.MandateStartDate(driver);
			WaitLibrary.waitForElementToBeClickable(driver, mandateStart, Constants_ACH.avg_explicit).click();
			mandateStart.sendKeys(mandateStartVal, Keys.ENTER );
			
			((ExtentTest) test).log(LogStatus.INFO, "Entered Mandate Start Date : "+mandateStartVal);
			
			//Select mandate type
			log.info("Select mandate type");
			String mandateTypeVal = profileData.get("Mandate Type").getAsString();
			WebElement mandateType = Mandate.MandateType(driver);
			BrowserResolution.scrollToElement(driver, mandateType);
			js.executeScript("window.scrollBy(0,-120)");
			Select MandateType = new Select(mandateType);
			MandateType.selectByVisibleText(mandateTypeVal);
			((ExtentTest) test).log(LogStatus.INFO, "Selected Mandate type : "+mandateTypeVal);
			
			//Select Mandate Action
			log.info("Select mandate action");
			String mandateActionVal = profileData.get("Mandate Action").getAsString();
			WebElement mandateAction = Mandate.MandateAction(driver);
			BrowserResolution.scrollToElement(driver, mandateAction);
			js.executeScript("window.scrollBy(0,-120)");
			Select MandateAction = new Select(mandateAction);
			MandateAction.selectByVisibleText(mandateActionVal);
			((ExtentTest) test).log(LogStatus.INFO, "Selected Mandate Action : "+mandateActionVal);
			
			//Select Status
			log.info("Select Status from Dropdown");
			String statusVal = profileData.get("Status").getAsString();
			WebElement status = Mandate.StatusSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, status, Constants_ACH.avg_explicit).click();
			
			WebElement statusSearch = Mandate.statusSearchField(driver);
			WaitLibrary.waitForElementToBeClickable(driver, statusSearch, Constants_ACH.avg_explicit).click();
			statusSearch.sendKeys(statusVal);
			statusSearch.sendKeys(Keys.ENTER);
			((ExtentTest) test).log(LogStatus.INFO, "Status entered : "+statusVal);

			//Effective From Date
			log.info("Enter Effective From Date");
			WebElement effectiveFromDate = Mandate.EffectiveFromDate(driver);
			WaitLibrary.waitForElementToBeClickable(driver, effectiveFromDate, Constants_ACH.avg_explicit).click();
			effectiveFromDate.sendKeys(currentDate, Keys.ENTER );
			((ExtentTest) test).log(LogStatus.INFO, "Entered Effective From Date : "+currentDate);
			
			log.info("Enter CompanyID");
			String companyIDVal = profileData.get("Company ID").getAsString();
			WebElement companyID = Mandate.CompanyId(driver);
			WaitLibrary.waitForElementToBeClickable(driver, companyID, Constants_ACH.avg_explicit).click();
			companyID.sendKeys(companyIDVal);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Company ID : "+companyIDVal);
			
//			log.info("Enter SEC Code");
//			WebElement secCode = Mandate.SECcode(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, secCode, Constants_ACH.avg_explicit).click();
//			secCode.sendKeys("WEB");
//			((ExtentTest) test).log(LogStatus.INFO, "Entered SEC code");
			
			//Select Limit Type
			log.info("Select limit type");
			String amountLimitTypeVal = profileData.get("Amount Limit").getAsJsonArray().get(0).getAsJsonObject().get("Limit Type").getAsString();
			WebElement limitType = Mandate.LimitType(driver);
			BrowserResolution.scrollToElement(driver, limitType);
			Select LimitType = new Select(limitType);
			LimitType.selectByVisibleText(amountLimitTypeVal);
			((ExtentTest) test).log(LogStatus.INFO, "Limit type selected : "+amountLimitTypeVal);
			
			//Select Amount Limit Code
			log.info("Select Amount Limit code");
			String amountLimitCodeVal = profileData.get("Amount Limit").getAsJsonArray().get(0).getAsJsonObject().get("Amount Limit Code").getAsString();
			WebElement amountLimitCode = Mandate.amtLimitCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amountLimitCode, Constants_ACH.avg_explicit).click();
			
			WebElement amtLimitCodeSearch = Mandate.amtLimitSearch(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amtLimitCodeSearch, Constants_ACH.avg_explicit).click();
			amtLimitCodeSearch.sendKeys(amountLimitCodeVal);
			WaitLibrary.fluentWait(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			amtLimitCodeSearch.sendKeys(Keys.ENTER);
			((ExtentTest) test).log(LogStatus.INFO, "Amount Limit code selected : "+amountLimitCodeVal);
			
			WebElement addMandateBtn = Mandate.addMandate(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, addMandateBtn, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		((ExtentTest) test).log(LogStatus.INFO, "Mandate Added : "+uniqueMandateReferenceVal);
    		Thread.sleep(Constants_ACH.tooshort_sleep);
    		
    		log.info("Click on Grid View");
			WebElement gridView1 = AmountLimitProfile.gridView(driver);
			String gridViewClass1= WaitLibrary.waitForElementToBeVisible(driver, gridView1, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView1, Constants_ACH.avg_explicit).click();					
			}
			
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    	
    		log.info("Enter Unique Mandate Reference to filter");
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(uniqueMandateReferenceVal, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on Unique Mandate Reference");
    					
    		List<WebElement> mandateTableData = Mandate.mandateDataList(driver);
			int mandateDataCount = mandateTableData.size();
			String uniqueMandateRef = null;
			for(int l=0; l<mandateDataCount; l++) {
				try {
					uniqueMandateRef = Mandate.mandateTableData(driver, l, 0).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("No Mandate Profile found");
				}
				
				if(uniqueMandateRef.equals(uniqueMandateReferenceVal)) {
					((ExtentTest) test).log(LogStatus.PASS, "Mandate Profile is Created Successfully.");
					break;
				}else if (l==mandateDataCount-1 && !uniqueMandateRef.equals(uniqueMandateReferenceVal)) {
					throw new Exception("Mandate Profile not found");
				}
			}
    				
    		ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());
			
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
		}
}
}


	