package com.gs.tc.ACH.MandateTCs;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.google.gson.JsonObject;
import com.gs.pages.AmountLimitProfile;
import com.gs.pages.Approvals;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TS01_TC001_AmountLimitProfile extends LoginLogout_ACH {
	@Test
	
	public void executeTC01() throws Exception {
		try {
			Log.startTestCase(log,TestCaseName);
			
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			
			String formDataJson = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Mandate_TC" ,TestCaseName, Constants_ACH.Col_Profile_Form);
			
			System.out.println(System.getProperty("user.dir") + Constants_ACH.gs_Samples + sampleDir + formDataJson);
			
			// call function to get variables
			JsonObject profileData = CommonMethods.getFormDataJson(System.getProperty("user.dir") + "\\" + Constants_ACH.gs_Samples + sampleDir + formDataJson);
			
			log.info("Navigate to Add on");
			WebElement addOn = SideBarMenu.addOn(driver);
			BrowserResolution.scrollToElement(driver, addOn);
			WaitLibrary.waitForElementToBeClickable(driver, addOn, Constants_ACH.avg_explicit).click();
			
			log.info("Navigate to Amount Limit Profile ");
			WaitLibrary.waitForAngular(driver);
			WebElement amtLimitProfile = SideBarMenu.amtLimitProfile(driver);
			BrowserResolution.scrollToElement(driver, amtLimitProfile);
			js.executeScript("arguments[0].click();" , amtLimitProfile);
			
			log.info("Click on Grid View");
			WebElement gridView = AmountLimitProfile.gridView(driver);
			String gridViewClass= WaitLibrary.waitForElementToBeVisible(driver, gridView, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView, Constants_ACH.avg_explicit).click();					
			}
			Thread.sleep(Constants_ACH.short_sleep);
			WaitLibrary.waitForAngular(driver);
		
			log.info("Click on Add New Button");
			WebElement amountAddNew = AmountLimitProfile.amtAddNew(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amountAddNew, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
					
			log.info("Amount Limit Profile Code");
			String amtLimitProfileCodeValue = profileData.get("Amount Limit Profile Code").getAsString();  
			WebElement amtLimitProfileCode = AmountLimitProfile.amtLimitProfileCode(driver);
			amtLimitProfileCode.sendKeys(amtLimitProfileCodeValue);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Amount Limit Profile Code : "+ amtLimitProfileCodeValue);
			
			log.info("Amount Limit Profile Currency");
			String amtLimitProfileCurrSearchValue = profileData.get("Currency").getAsString();
			WebElement amtLimitProfileCurr = AmountLimitProfile.amtLimitProfileCurr(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amtLimitProfileCurr, Constants_ACH.avg_explicit).click();	
			WaitLibrary.waitForAngular(driver);		
			WebElement currencyList = AmountLimitProfile.currList(driver, amtLimitProfileCurrSearchValue);
			WaitLibrary.waitForElementToBeClickable(driver, currencyList, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Entered Amount Limit Profile Currency : "+ amtLimitProfileCurrSearchValue);
			
			log.info("Amount Limit Profile Type");
			String amtLimitProfileTypSearchValue = profileData.get("Amount Limit Type").getAsString();
			WebElement amtLimitProfileType = AmountLimitProfile.amtLimitProfileType(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amtLimitProfileType, Constants_ACH.avg_explicit).click();
			WebElement amtLimitProfileTypSearch = AmountLimitProfile.amtLimitProfileTypeSearch(driver);
			amtLimitProfileTypSearch.sendKeys(amtLimitProfileTypSearchValue, Keys.ENTER);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Amount Limit Profile Type : "+ amtLimitProfileTypSearchValue);
			
			log.info("Amount Limit Profile Max Amount");
			String amtLimitProfileMaxAmtValue = profileData.get("Max Amount").getAsString();
			WebElement amtLimitProfileMaxAmt = AmountLimitProfile.amtLimitProfileMaxAmt(driver);
			amtLimitProfileMaxAmt.sendKeys(amtLimitProfileMaxAmtValue);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Amount Limit Profile Max Amount : "+amtLimitProfileMaxAmtValue);
			
			log.info("Select Amount Limit Profile Status");
			String profStatusValue = profileData.get("Status").getAsString();
			WebElement amountStatusSelectBox = AmountLimitProfile.getStatusAmountProfile(driver);
			BrowserResolution.scrollToElement(driver, amountStatusSelectBox);
			js.executeScript("window.scrollBy(0,-120)");
			Select profStatus = new Select(amountStatusSelectBox);
			profStatus.selectByVisibleText(profStatusValue);
			BrowserResolution.scrollDown(driver);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Selected Amount Limit Profile Status : "+profStatusValue);
	
			log.info("Amount Limit Profile Effective date");
			String amtLimitProfileEffDateValue = profileData.get("Effective From Date").getAsString();
			WebElement amtLimitProfileEffDate = AmountLimitProfile.amtLimitProfileEffDate(driver);
			amtLimitProfileEffDate.sendKeys(amtLimitProfileEffDateValue, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Entered Amount Limit Profile Effective date : "+amtLimitProfileEffDateValue);
			
			log.info("Submit Button");
			WebElement addAmountLimitButton = AmountLimitProfile.addAmountLimitBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, addAmountLimitButton, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
	
			log.info("Approver Amount Limit Profile");
			CommonMethods.approveStatement(driver, amtLimitProfileCodeValue);
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Grid View");
			WebElement gridView1 = AmountLimitProfile.gridView(driver);
			String gridViewClass1= WaitLibrary.waitForElementToBeVisible(driver, gridView1, Constants_ACH.avg_explicit).getAttribute("class");
						
			if (gridViewClass1.equalsIgnoreCase("btn-trans cmmonBtnColors")) {			
				WaitLibrary.waitForElementToBeClickable(driver, gridView1, Constants_ACH.avg_explicit).click();					
			}
		
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on Filter Icon");
			WaitLibrary.waitForAngular(driver);
			WebElement clickFilterIcon = Approvals.clickFilterIcon(driver);
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
    	
    		log.info("Enter Amount profile code to filter");
    		WebElement filterWithKeyword = Approvals.filterWithKeyword(driver);
    		filterWithKeyword.sendKeys(amtLimitProfileCodeValue, Keys.ENTER);
    		WaitLibrary.waitForAngular(driver);
    		
    		log.info("Click on filter Icon");
    		WaitLibrary.waitForElementToBeClickable(driver, clickFilterIcon, Constants_ACH.avg_explicit).click();
    		WaitLibrary.waitForAngular(driver);
			
			//Verify Created Amount Profile
			List<WebElement> amtLimitProf  = AmountLimitProfile.amountProfileDataList(driver);
			int amountProfileDataCount = amtLimitProf.size();
			String amountLmtProfile = null;
			for(int l=0; l<amountProfileDataCount; l++) {
				try {
					amountLmtProfile = AmountLimitProfile.amountProfileData(driver, l, 0).getText();
				}catch (NoSuchElementException ne) {
					throw new Exception("Amount Limit Profile not found");
				}
				
				if(amountLmtProfile.equals(amtLimitProfileCodeValue)) {
					((ExtentTest) test).log(LogStatus.PASS, "Amount Limit Profile is Created Successfully.");
					break;
				}else if (l==amountProfileDataCount-1 && !amountLmtProfile.equals(amtLimitProfileCodeValue)) {
					throw new Exception("Amount Limit Profile not Created");
				}
			}
				
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
			catch(AssertionError ae){
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
				assertionerror=ae.getMessage();			
				CaptureScreenshot_ACH.captureScreenshot();
				((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, assertionerror);
			}
			catch(Exception et){
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
				exceptionerror=et.getMessage();			
				CaptureScreenshot_ACH.captureScreenshot();
				((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
				ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, exceptionerror);
			}
			
		}


}
