package com.gs.tc.ACH.BulkReject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_ACH;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants_ACH;

import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ACH;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC002_BulkReject_OB_DD extends LoginLogout_ACH {
	
	@Test

	public void executeTC02() throws Exception {
		try {

			Log.startTestCase(log, TestCaseName);
			
			((ExtentTest) test).log(LogStatus.INFO, "TC002_BulkReject_OB_DD");
			
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dateYMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");					
			String locDateYr = dateYMD.format(now);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseName,Constants_ACH.Col_sampleDirectory);
			System.out.println(sampleDir);
			 
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "Bulk_Reject" ,TestCaseName, Constants_ACH.Col_ACH_Payment);
			System.out.println(paymentFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Creating sample file from Ref
			CommonMethods_ACH.createSampleRef(Constants_ACH.gs_Samples+sampleDir+"Ref\\"+paymentFile,Constants_ACH.gs_Samples+ sampleDir+paymentFile);
			System.out.println("REF : "+sampleDir+"Ref\\"+ paymentFile);
			System.out.println("Dest : "+sampleDir+ paymentFile);
			// creating new values for Payment file
			HashMap<String, String> updateVal = new HashMap<>();
			
			updateVal.put("{{yyyy-MM-dd}}", locDateYr);
			
			// Updating Payment File
			CommonMethods_ACH.updateSampleFile(Constants_ACH.gs_Samples+sampleDir+paymentFile, updateVal);
			
			// Upload Payment File
    		log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants_ACH.ACHPain008ChannelIn);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile, Constants_ACH.ACHPain008ChannelIn);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants_ACH.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants_ACH.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants_ACH.avg_explicit).getAttribute("class");			
			
			if (listViewClass.contains(Constants_ACH.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants_ACH.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants_ACH.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id: " + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
//			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.getInsStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants_ACH.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			Thread.sleep(Constants_ACH.short_sleep);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on Payment ID");
			Thread.sleep(Constants_ACH.tooshort_sleep);
		    java.util.List<WebElement> paymentsList = ReceivedInstruction.paymentList(driver);
			int paymentscount = paymentsList.size();
			String paymentID="";
			for(int l=0; l < paymentscount; l++) {
				String status = ReceivedInstruction.originalPaymentReference(driver, l, "Status").getText();
				WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, l, "PaymentID");
				paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants_ACH.avg_explicit).getText();
				if(status.equals("WAIT CREDIT PROCESSING BATCH")) {
					TimeUnit.MINUTES.sleep(4);
				}else if(status.equals("TIME WAREHOUSED")) {
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants_ACH.avg_explicit).click();
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					
					CommonMethods_ACH.forceRelease(driver, paymentID, paymentFile, insId);
					WaitLibrary.waitForAngular(driver);
					Thread.sleep(Constants_ACH.tooshort_sleep);
					TimeUnit.MINUTES.sleep(4);
				}
				
				WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
				WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
				break;
			}
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Payment Status");
			WebElement PaymentStatusEle = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusEle, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatus, "FOR_BULKING");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			log.info("Click Reject Payment Button");
			WebElement rejectPayment = ReceivedInstruction.RejectPayment(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Reject Payment Button is visible");
			WaitLibrary.waitForElementToBeClickable(driver, rejectPayment, Constants_ACH.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Reject Payment sent for Approval");
			
			CommonMethods_ACH.approveStatement(driver, paymentID);
     		WaitLibrary.waitForAngular(driver);
     		((ExtentTest) test).log(LogStatus.INFO, "Approved Reject payment");
     		
     		log.info("Navigate to Received Instructions");
     		WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
     		BrowserResolution.scrollToElement(driver, recInsTabInIn);
     		WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants_ACH.avg_explicit).click();
     		
     		log.info("Click on List View");
     		WebElement listViewIn = ReceivedInstruction.listView(driver);
     		String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants_ACH.avg_explicit).getAttribute("class");

     		if (listViewClassIn.contains(Constants_ACH.listViewClassData)) {
     			WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants_ACH.avg_explicit).click();
     		}
     		Thread.sleep(Constants_ACH.tooshort_sleep);
     		
     		WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
     		searchInstructionIn.clear();
     		searchInstructionIn.sendKeys(insId, Keys.ENTER);
     		WaitLibrary.waitForAngular(driver);
     		
     		CommonMethods_ACH.clickStatementWithFileName(driver, paymentFile);
     		WaitLibrary.waitForAngular(driver);
 			BrowserResolution.scrollDown(driver);
 			
 			log.info("Click on PaymentId");
 			WebElement clickPayment = ReceivedInstruction.clickPaymentID(driver, paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, clickPayment, Constants_ACH.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			Thread.sleep(Constants_ACH.tooshort_sleep);
			
			log.info("Verify Payment Status");
			WebElement PaymentStatusElem = ReceivedInstruction.getStatus(driver);
			String PaymentStatusEleme = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElem, Constants_ACH.avg_explicit).getText();
			Assert.assertEquals(PaymentStatusEleme, "REJECTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatusEleme);
			BrowserResolution.scrollDown(driver);
			
			log.info("Navigate to Account Posting Tab");
    		js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
    		((ExtentTest) test).log(LogStatus.INFO, "Verify Account Posting Entry Status");
    		
    		List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			BrowserResolution.scrollDown(driver);
			
			Thread.sleep(Constants_ACH.tooshort_sleep);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			((ExtentTest) test).log(LogStatus.PASS, "Account Posting Verified with two records");
    		
			String[] entryStatus = {"01", "01"};
			for(int i = 0; i < posCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				if(i < posCount) {
					Assert.assertEquals(EntryStatus, entryStatus[i]);
					((ExtentTest) test).log(LogStatus.PASS, "Account Posting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
				}
			}
			
			log.info("Navigate to Payment Event Log");
			js.executeScript("arguments[0].click();", ReceivedInstruction.payEventLog(driver));
    		((ExtentTest) test).log(LogStatus.INFO, "Verify Payment Event Log");
    		BrowserResolution.scrollDown(driver);
    		String approver = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestCases, "Approver", "Application_URL", 5);
    		
			List<WebElement> paymentEventLog = ReceivedInstruction.eventLogList(driver);
			int eventLogCount = paymentEventLog.size();
			int processPayCount = 0;
			int actionHandlerCount = 0;
			int approvalCount = 0;
			String processPaymentDesc = "Payment Status moved to FOR_BULKING";
			String actionHandlerDesc = "Payment has been rejected from the User Actions";
			String approvalDesc = "Payment waiting at REJECT-FOR_BULKING-WAITFORAPPROVAL has been approved by : "+approver+". Comments : "+paymentID+"";
			
			for (int j = 0; j < eventLogCount; j++) {				
				String event = ReceivedInstruction.eventLogData(driver, j, "Event").getText();
				String description = ReceivedInstruction.eventLogData(driver, j, "Description").getText();
				if(event.equals("PROCESS_PAYMENT")) {
					if(description.equals(processPaymentDesc)) {
						BrowserResolution.scrollToElement(driver, ReceivedInstruction.eventLogData(driver, j, "Event"));
						processPayCount++;
						((ExtentTest) test).log(LogStatus.PASS,"Process Payment Verified with description: "+processPaymentDesc);
					}
				}
				if(event.equals("ACTION_HANDLER")) {
					if(description.equals(actionHandlerDesc)){
						BrowserResolution.scrollToElement(driver, ReceivedInstruction.eventLogData(driver, j, "Event"));
						actionHandlerCount++;
						((ExtentTest) test).log(LogStatus.PASS,"Action Handler Verified with description: "+actionHandlerDesc);
					}
				}
				if(event.equals("APPROVAL")) {
					if(description.equals(approvalDesc)){
						BrowserResolution.scrollToElement(driver, ReceivedInstruction.eventLogData(driver, j, "Event"));
						approvalCount++;
						((ExtentTest) test).log(LogStatus.PASS,"Approval Verified with description: "+approvalDesc);
					}
				}
			}
			if(processPayCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"Process Payment not Found");
			}
			if(actionHandlerCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"Action Handler not Found");
			}
			if(approvalCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"Approval not Found");
			}
			
			log.info("Navigate to Issue Information Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.issueInfoTab(driver));
			BrowserResolution.scrollDown(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Verify Issue Information");
			String pmtForBulkDesc = "Payment Status moved to FOR_BULKING";
			String pmtRejByUser = "Payment Status moved to REJECTED by manual action";
			int payBulkCount = 0;
			int payRejUserCount = 0;
			
			List<WebElement> issueInfoData = ReceivedInstruction.issueInfoDataList(driver);
			int inssueInfoCount = issueInfoData.size();
			for (int j = 0; j < inssueInfoCount; j++) {				
				String issueCode = ReceivedInstruction.issueInformation(driver, j, "IncidenceCode").getText();
				String description = ReceivedInstruction.issueInformation(driver, j, "OriginalDescription").getText();
				if(issueCode.equals("PMNTFORBULKING")) {
					payBulkCount++;
					Assert.assertEquals(description, pmtForBulkDesc);
					((ExtentTest) test).log(LogStatus.PASS,"Issue Code Verified for: "+issueCode);
				}
				if(issueCode.equals("PMNTRJCTBYUSER")) {
					payRejUserCount++;
					Assert.assertEquals(description, pmtRejByUser);
					((ExtentTest) test).log(LogStatus.PASS,"Issue Code Verified for: "+issueCode);
				}
			}
			if(payBulkCount != 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"PMNTFORBULKING not Found");
			}
			if(payRejUserCount != 1) {
				((ExtentTest) test).log(LogStatus.FAIL,"PMNTRJCTBYUSER not Found");
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			((ExtentTest) test).log(LogStatus.INFO, "Verify Finacle Account Posting");
    		
			List<WebElement> systemInteractionData = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = systemInteractionData.size();
			System.out.println(sysIntDataCountIn);
			int becCount = 0;
			String finacleFile1 = ExcelUtilities.getCellDataBySheetName(Constants_ACH.File_TestData, "ForcePost" ,TestCaseName, Constants_ACH.Col_finacle_Pos_1);
			System.out.println(finacleFile1);
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();				
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becCount++;
				}
			}
			if(becCount != 0) {
				((ExtentTest) test).log(LogStatus.PASS, "BEC Notification is Available");
				WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, becCount-1);
				BrowserResolution.scrollToElement(driver, viewBtn);
				js.executeScript("arguments[0].click();", viewBtn);
				
				// Getting Object Data
				Thread.sleep(Constants_ACH.tooshort_sleep);
				WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, becCount-1);
				WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants_ACH.avg_explicit);
				String objDataXmlString = objDataXml.getText();
				String objDataString = U.xmlToJson(objDataXmlString);
				try {
					JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
					JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
					String status = temp.get("Status").getAsString();
					if(status.equals("REJECTED")) {
						log.info("BECNOTIFICATION status"+status);
						((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status+ " is verified");
					}
				}
				catch(Exception bec) {
					((ExtentTest) test).log(LogStatus.FAIL,bec);
				}
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, becCount-1);
				js.executeScript("arguments[0].click();", modalCloseBtn);
		
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
			
		 catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_AssertionComments, assertionerror);
		 }
		 catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot_ACH.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ACH.test.addScreenCapture(CaptureScreenshot_ACH.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants_ACH.File_TestCases, "TestCases", TestCaseRow, Constants_ACH.Col_comments, exceptionerror);
		  }
	}
}
