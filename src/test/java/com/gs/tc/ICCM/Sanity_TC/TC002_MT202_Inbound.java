package com.gs.tc.ICCM.Sanity_TC;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.dataComparision;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC002_MT202_Inbound extends LoginLogout_ICCM{
@Test
	
	public void executeTC002() throws Exception {
		try {
			System.out.println("TC002_MT202_Inbound");
			Log.startTestCase(log, TestCaseName);
			
			// get Current Date
			LocalDate date = LocalDate.now();
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_IncomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+paymentFile, "inbound", "202");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+paymentFile, "payment");
			
			// Get F20 of Sample file
			String paymentRef = SampleFileModifier.getSampleFileFieldValue(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+paymentFile, ":20:","IN"); 
			System.out.println(paymentRef);
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Sanity_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Sanity_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Sanity_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			} 				
				
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
	 
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			// Fetch Payment details File Name
			String paymentDetailsFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_paymentDetails);
						
			// Get Payment data actual
            String paymntData = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+paymentDetailsFile)));
			
            // Get Payment details data
            WebElement payDetailsCol1 = ReceivedInstruction.getPaymentDetailsCol1(driver);
            String payDetailsData1 = payDetailsCol1.getText();
            
            WebElement payDetailsCol2 = ReceivedInstruction.getPaymentDetailsCol2(driver);
            String payDetailsData2 = payDetailsCol2.getText();
            
            WebElement payDetailsCol3 = ReceivedInstruction.getPaymentDetailsCol3(driver);
            String payDetailsData3 = payDetailsCol3.getText();
            
            // Get Payment data obtained
            String payDetails = payDetailsData1 + "\n" + payDetailsData2 + "\n" + payDetailsData3;
            
            // Verifying Payment details
            List<String> fieldNames = Arrays.asList("Original Payment Reference", "Original File Reference", "Original Payment Function", "Amount", "Currency", "Method Of Payment", "Status","Original Value Date", "Value Date");
			 
			 for (String fieldTemp : fieldNames){
				 String actualData = CommonMethods.getPaymentDetailVal(paymntData, fieldTemp);
				 String obtainData = CommonMethods.getPaymentDetailVal(payDetails, fieldTemp);
				 if(fieldTemp.equals("Value Date") || fieldTemp.equals("Original Value Date")) {
					 if(obtainData.equalsIgnoreCase(date.toString())) {
						 ((ExtentTest) test).log(LogStatus.PASS, "Payment Details " + fieldTemp + " value matched : " + obtainData);
					 }else {
						 ((ExtentTest) test).log(LogStatus.FAIL, "Payment Details " + fieldTemp + " value not matched : " + obtainData);
					 };
				 }else if(fieldTemp.equals("Original Payment Reference") || fieldTemp.equals("Original File Reference")){
					 if(obtainData.equalsIgnoreCase(paymentRef)) {
						 ((ExtentTest) test).log(LogStatus.PASS, "Payment Details " + fieldTemp + " value matched with orignal F20 value : " + obtainData);
					 }else {
						 ((ExtentTest) test).log(LogStatus.FAIL, "Payment Details " + fieldTemp + " value not matched with orignal F20 value : " + obtainData);
					 };
				 }
				 else {
					 if(actualData.equalsIgnoreCase(obtainData)) {
						 ((ExtentTest) test).log(LogStatus.PASS, "Payment Details " + fieldTemp + " value matched : " + obtainData);
					 }else {
						 ((ExtentTest) test).log(LogStatus.FAIL, "Payment Details " + fieldTemp + " value not matched : " + obtainData);
					 };
				 }
				 
			 }
            
			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
					
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"NOS", "SUS", "SUS", "DDA"};
			String[] entryStatus = {"03", "03", "03", "03"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
				Assert.assertEquals(EntryStatus, entryStatus[i]);
				
				((ExtentTest) test).log(LogStatus.PASS, "Account POsting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
			}
			((ExtentTest) test).log(LogStatus.PASS, "Two Step of Posting : " + posCount + " steps verified");
			
			//Navigate to System interaction to verify OFAC and BEC notification
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			
			//Verfiying Debit Funds Control
			int drFundCount = 0;
						
			// Fetch credit Control File Name
			String creditFundControlFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_debitFundControl);
			System.out.println(creditFundControlFile);
			
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
					drFundCount++;
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					
					List<String> dateFields = Arrays.asList("valueDate", "currentDate", "eventDateTime");
				    List<String> fieldNamesArr = Arrays.asList("requestType", "clientId", "accountId", "crossAccountId", "paymentAmount", "debitCreditIndicator", "paymentCcy", "paymentCategory", "deploymentId");
				    
					List<JSONObject> listOfValues = dataComparision.compareDebitFuncontrol(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+creditFundControlFile, objDataString, dateFields, fieldNamesArr);
					
					for (JSONObject headLines : listOfValues) {	
			    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
			    		
			    		while (headLinekeys.hasNext()) {
			    		    Object key = headLinekeys.next();
			    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
			    		    JSONObject value = headLines.getJSONObject((String) key);
			    		    
			    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
			    		    while( innerMapKeys.hasNext() ) {
				    		    String innerMapKey = (String) innerMapKeys.next();
				    		    if (key == "Matched") {
				    		    	((ExtentTest) test).log(LogStatus.PASS, "Credit Fund Control Value matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
			    		    	}
				    		    else {
				    		    	((ExtentTest) test).log(LogStatus.FAIL, "Credit Fund Control Value not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    }
				    		}
			    		}
			    	}
					
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
					break;
				}
			}
			
			if(drFundCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Credit fund control request is not generated");
			}
			
			//Verifying Finacle Posting Request
			int finCount = 0;
			
			// Fetch Finacle File Name
			String finacleFile1 = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_finacle_Pos_1);
			String finacleFileName1 = System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+finacleFile1;
			
			String finacleFile2 = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_finacle_Pos_2);
			String finacleFileName2 = System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+finacleFile2;
			
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					List<JSONObject> listOfValues = null;
						if(finCount <= 2 ) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
												
						List<String> dateFields = Arrays.asList("transactionDate", "valueDate", "bankSettlementDate", "fundAvailableDate");
					    List<String> normalKeys = Arrays.asList("nostroAccount", "originalPaymentFunction", "deploymentId", "transactionDate");
					    List<String> movementRequestsKeys = Arrays.asList("valueDate", "bankSettlementDate", "fundAvailableDate","instructionType","debitAccount","creditAccount","amount","currency");
						
					    if(finCount == 1) {
					    	List<String> additionalAttributesKeys = Arrays.asList("isBatchPosting", "serviceCode", "linkReferenceAmount");
							listOfValues = dataComparision.compareFinaclePosting(finacleFileName1, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
						}else if(finCount == 2) {
							List<String> additionalAttributesKeys = Arrays.asList("isBatchPosting", "serviceCode","methodOfPayment", "linkReferenceAmount");
							listOfValues = dataComparision.compareFinaclePosting(finacleFileName2, objDataString, normalKeys, dateFields, additionalAttributesKeys, movementRequestsKeys);
						}
					    
						for (JSONObject headLines : listOfValues) {	
				    		Iterator headLinekeys = headLines.keys();						// create Iterator to iterate keys
				    		
				    		while (headLinekeys.hasNext()) {
				    		    Object key = headLinekeys.next();
				    		    System.out.println(key.toString());						// print headLines (Matched / Unmatched)
				    		    JSONObject value = headLines.getJSONObject((String) key);
				    		    
				    		    Iterator innerMapKeys = value.keys();						// create Iterator to iterate within a headLines
				    		    while( innerMapKeys.hasNext() ) {
					    		    String innerMapKey = (String) innerMapKeys.next();
					    		    if (key == "Matched") {
					    		    	((ExtentTest) test).log(LogStatus.PASS, "Finacle Posting " + finCount+ " matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
				    		    	}
					    		    else {
					    		    	((ExtentTest) test).log(LogStatus.FAIL, "Finacle Posting " + finCount+ " not matched : " + innerMapKey + " : " + value.get(innerMapKey));	// print both keys and values which is in list
					    		    }
					    		}
				    		}
				    	}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
			}
			
			
			// Verifying BEC Notification
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				String[] becNotificationStatus = {"TOBEPROCESSED","WAITING_OFACRESPONSE", "COMPLETED", "ACCEPTED"};
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					
					becNotfCount++;
					if(becNotfCount <= 4) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						String objDataString = U.xmlToJson(objDataXmlString);
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
							String status = temp.get("Status").getAsString();
							if(status.equals(becNotificationStatus[becNotfCount-1])) {
								log.info("BECNOTIFICATION status"+status);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION status "+status);
							}
						}

					catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
				 }
			}
			
			//Navigate to external communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					directionCount++;
					Thread.sleep(Constants.tooshort_sleep);
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication OUT file not verified");
			}else {
				// Verifying downloaded OUT File
				String actualFileName = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_out_File);
				String actualFile =  System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+ actualFileName;
				
				String obtainedFile = CommonMethods.getDownloadedFile().toString();
				
				List<String> fieldKey = Arrays.asList(":21:",":52A:",":58A:",":72:");
				
				for (String fieldTemp : fieldKey){
					 
					if(fieldTemp.equals(":21:")) {
						String obtainedVal = SampleFileModifier.getSampleFileFieldValue(obtainedFile, fieldTemp,"IN");
						if(paymentRef.equals(obtainedVal)) {
							((ExtentTest) test).log(LogStatus.PASS, "Out File "+ fieldTemp +" Value matched with Original F20 : " + obtainedVal);
						}else {
							((ExtentTest) test).log(LogStatus.PASS, "Out File "+ fieldTemp +" Value not matched with Original F20 : " + obtainedVal);
						}
						
					}else {
						 String actualVal = SampleFileModifier.getSampleFileFieldValue(actualFile, fieldTemp,"IN");
						 System.out.println(actualVal);
						 String obtainedVal = SampleFileModifier.getSampleFileFieldValue(obtainedFile, fieldTemp,"IN");
						 System.out.println(obtainedVal);
						 if(actualVal.equals(obtainedVal)) {
							 ((ExtentTest) test).log(LogStatus.PASS, "Out File "+ fieldTemp +" Value matched : " + obtainedVal);
						 }else {
							 ((ExtentTest) test).log(LogStatus.FAIL, "Out File "+ fieldTemp +" Value not matched : " + obtainedVal);
						 } 	
					}
					 
				 }
			}
			
			
						
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}
// End of Class
}
