package com.gs.tc.ICCM.Sanity_TC;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC003_MT910_GS_OUT extends LoginLogout_ICCM{
	
	@Test

	public void executeTC003() throws Exception {
		try {
			
			System.out.println("TC003_MT910_GS_OUT");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_IncomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity + "\\" + sampleDir + paymentFile,"inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+ sampleDir + paymentFile,"payment");
			
			// Get F72 value from Orig Sample file
			String[] fieldDataArr = SampleFileModifier.getFieldData(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+ sampleDir + paymentFile, "MT910");
			String fieldData = String.join("", fieldDataArr);
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_Sanity_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Sanity_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");

			String rec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			Thread.sleep(Constants.short_sleep);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "REPAIR");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			BrowserResolution.scrollDown(driver);
			
			//click on return button
			WebElement repairButton = ReceivedInstruction.repairBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, repairButton, Constants.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Clicked on repair button");

			//click on creditor
			WebElement creditor = ReceivedInstruction.Creditor(driver);
			String credit = ReceivedInstruction.Creditor(driver).getText();
			BrowserResolution.scrollToElement(driver, creditor);
			js.executeScript("arguments[0].click();", creditor);
			System.out.println("Creditor clicked");
			
			//click on creditor Account Number text field
			WebElement creditorText = ReceivedInstruction.creditorTxt(driver);
			BrowserResolution.scrollToElement(driver, creditor);
			js.executeScript("arguments[0].click();", creditorText);
	//		WaitLibrary.waitForElementToBeClickable(driver, creditorText, Constants.avg_explicit).click();
			
			//get data from excel
			log.info("get Account from excel");
			String accNumber1 = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Sanity_TC",TestCaseName,Constants.Col_AccountNumber);
			System.out.println(accNumber1);
			ReceivedInstruction.creditorTxt(driver).sendKeys(accNumber1);
			((ExtentTest) test).log(LogStatus.INFO, "Account number "+accNumber1+" for Creditor entered");
			
			//click on creditor Agent
			WebElement creditorAgent = ReceivedInstruction.CreditorAgent(driver);
			String creditAgent = ReceivedInstruction.CreditorAgent(driver).getText();
			BrowserResolution.scrollToElement(driver, creditorAgent);
			js.executeScript("arguments[0].click();", creditorAgent);
			System.out.println("Creditor Agent clicked");
			
			//get BEC ID from Excel
			log.info("get BEC ID from excel");
			String becID = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Sanity_TC",TestCaseName,Constants.Col_BECID);
			System.out.println(becID);
			ReceivedInstruction.creditorAgentBIC(driver).sendKeys(becID);
			((ExtentTest) test).log(LogStatus.INFO, "BIC "+becID+" entered");
			
			//get data from excel
			log.info("get Account from excel");
			String accNumber2 = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Sanity_TC",TestCaseName,Constants.Col_AccountNumber);
			System.out.println(accNumber2);
			ReceivedInstruction.creditorAgentTxt(driver).sendKeys(accNumber2);
			((ExtentTest) test).log(LogStatus.INFO, "Account number "+accNumber2+" for Creditor Agent entered");
			
			//click on submit
			WebElement submit = ReceivedInstruction.Submit(driver);
			BrowserResolution.scrollToElement(driver, submit);
			js.executeScript("arguments[0].click();", submit);
			((ExtentTest) test).log(LogStatus.INFO, "Submitted request for Approval");
			
			//Sent request for approval
			CommonMethods.approveStatement(driver, paymentID);
			
			//************************After Approval****************************//
			
			((ExtentTest) test).log(LogStatus.INFO, "After Approval");
			//Navigate to Received Instructions
			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtId = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);
			
			//Verify payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			log.info("get mop");
			WebElement MOfP = ReceivedInstruction.paymentMethod(driver);
			String MofP = WaitLibrary.waitForElementToBeVisible(driver, MOfP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MofP);

			String methodOfPayment = CommonMethods.filterPayMethodDiv(MofP);
			Assert.assertEquals(methodOfPayment, "GS_OUT");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPayment);
			
			BrowserResolution.scrollDown(driver);

			//Navigate to System Interaction
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));

			// Goto System Interaction and verify Finaclerequest
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIN = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIN = sysIntDataListIN.size();
			System.out.println(sysIntDataCountIN);
			int finCountIN = 0 ;
				for (int j=0; j< sysIntDataCountIN; j++) {
					String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
					String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
					if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						finCountIN++;
					System.out.println(finCountIN);
					if(finCountIN == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						//Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								
								if(insType.equals("WIRE_IN_NOS")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_IN_NOS");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched"+ insType);
									throw new Exception("1st Finacle Account instruction Type not matched");
								}
								
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(finCountIN == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						//Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
							
								if(insType.equals("WIRE_IN_CLI")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_IN_CLI");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
									throw new Exception("2nd Finacle Account instruction Type not matched");
								}
								
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				 
				if(j==sysIntDataCountIN-1) {
					if(finCountIN !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
				}
			}
			
			//Navigate to Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			
			String[] accTypeArrIn = {"NOS", "SUS", "SUS", "DDA"};
			
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				
				if(i<posCount-2) {
					Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
					((ExtentTest) test).log(LogStatus.PASS, "1st Leg of posting verified with - Account type :: "+accTypeVal);				
					}
					if(i>=posCount-2) {
						Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
						((ExtentTest) test).log(LogStatus.PASS, "2nd Leg of posting verified with - Account type :: "+accTypeVal);
					}
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList2 = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount2 = sysIntDataList2.size();
			System.out.println(sysIntDataCount2);
			int creditCount = 0;
			for (int j=0; j< sysIntDataCount2; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
					creditCount++;
					if(creditCount == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "CreditFundsControl Notification is Generated");
					}
				}
			}
			if(creditCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "CreditFundControl Request is Not Generated");
			}
			
			//Navigate to external communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			//Get download File name
			Thread.sleep(Constants.tooshort_sleep);
			String obtainedFile = CommonMethods.getDownloadedFile().toString();
			
			//Get F72 value of download file
			String[] obtainedFieldDataArr = SampleFileModifier.getFieldData(obtainedFile, "MT910");
			
			System.out.println(fieldData);
			String obtainedFieldData = String.join("", obtainedFieldDataArr);
			
			System.out.println(obtainedFieldData);
			// Comparing Original F72 value with Obtained Value
			String[] obtainedValues = null;
			if (obtainedFieldData.contains("NOCD")) {
				obtainedValues = obtainedFieldData.split("NOCD");
				if(!obtainedValues[0].isEmpty() && obtainedValues[0].equals("INSCITIUS33XXX")) {
					((ExtentTest) test).log(LogStatus.PASS, "INS/CITIUS33XXX and NOCD values are mapped with F72 out File ");
					if(obtainedValues.length>1 && fieldData.startsWith(obtainedValues[1])) {
						((ExtentTest) test).log(LogStatus.PASS, "Out file F72 is mapped from the original file");
					}
					else {
						((ExtentTest) test).log(LogStatus.FAIL, "Out file F72 is not mapped from the original file");
					}
				}
				else {
					if(obtainedValues[0].isEmpty()) {
						((ExtentTest) test).log(LogStatus.FAIL, "INS/CITIUS33XXX value not mapped with F72 out File ");
					}else {
						((ExtentTest) test).log(LogStatus.FAIL, "Expected INS/CITIUS33XXX value not mapped with F72 out File , Found : " + obtainedValues[0]);
					}
				}
			}
			else {
				((ExtentTest) test).log(LogStatus.FAIL, "NOCD value not mapped with F72 out File ");
			}
			
			//Navigate to System interaction to verify OFAC and BEC notification
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int ofacCount = 0;
			int BEC1 = 0;
			int BEC2 =0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					ofacCount++;
					if(ofacCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Data
						Thread.sleep(Constants.tooshort_sleep);
					
						WebElement OFACData = ReceivedInstruction.OFAC(driver, j);
						String getOFACData = OFACData.getText();
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						
						// Verifying MBBK Info
						String MBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "MBBKINFO");
						if(MBBKinfo.startsWith("NOCD")) {
							((ExtentTest) test).log(LogStatus.PASS, "NOCD value mapped with MBBK Info OFAC Request");
							if(MBBKinfo.replace("NOCD", "").equalsIgnoreCase(fieldData)) {
								((ExtentTest) test).log(LogStatus.PASS, "MBBK Info value matched with original F72 value");
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "MBBK Info value not matched with original F72 value");
							}
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "NOCD value not mapped with MBBK Info OFAC Request");
						}
						
						// Verifying SBBK Info
						String SBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "SBBKINFO");
						if(SBBKinfo.equalsIgnoreCase(fieldData)) {
							((ExtentTest) test).log(LogStatus.PASS, "SBBK Info value matched with original F72 value");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "SBBK Info value not matched with original F72 value");
						}
					}
				}else if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					//Thread.sleep(Constants.tooshort_sleep);
					WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
					String objDataXmlString = objDataXml.getText();
					String objDataString = U.xmlToJson(objDataXmlString);
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
						String status = temp.get("Status").getAsString();
			
						if(status.equals("ACCEPTED")) {
							log.info("BECNOTIFICATION status"+status);
							((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION is generated for "+status+" Status");
							BEC1++;
						}
						if(status.equals("COMPLETED")) {
							log.info("BECNOTIFICATION status"+status);
							((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION is generated for "+status+" Status");
							BEC2++;
						}
						
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
			}
				if(BEC1 != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not generated for Accepted Status");
				}
				if(BEC2 != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not generated for Completed Status");
				}

			
			//Navigate to System interaction
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList3 = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount3 = sysIntDataList3.size();
			System.out.println(sysIntDataCount3);

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

