package com.gs.tc.ICCM.Sanity_TC;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC001_MT940_Statement extends LoginLogout_ICCM{
	
	@Test
	
	public void executeTC200() throws Exception {
	try {
		System.out.println("TC200");
		Log.startTestCase(log,TestCaseName);

		// Fetch sample Directory
		String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
		System.out.println(sampleDir);
		
		// Fetch statement File Name 
		String stmtFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Sanity_TC",TestCaseName,Constants.Col_Stmt_File);
		((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
		
		// Updating Transaction reference in Sample file
		SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+stmtFile, "statement", "940");
		
		// Updating Value Date in sample file
		SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+sampleDir+stmtFile, "statement");
		
		//Upload Statement File	
		log.info("Uploading Statement File via WinSCP");
		log.info("Source File is  :"+Constants.gs_Sanity_Dir+"\\"+sampleDir+stmtFile);
		log.info("Destination File is  :"+Constants.SWIFTIN);
		FilesUpload.uploadFileByWinSCP(Constants.gs_Sanity_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
		System.out.println(Constants.gs_Sanity_Dir+"\\"+sampleDir+stmtFile);
		System.out.println(Constants.SWIFTIN);
		((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
		Thread.sleep(Constants.short_sleep);

		log.info("Navigate to Received Statement");
		WebElement statementModule = ReceivedStatement.statementModule(driver);
		BrowserResolution.scrollToElement(driver, statementModule);
		WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

		log.info("Clicking on Received Statements");
		WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
		WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
		
		log.info("Click on List View");
		WebElement listView = ReceivedInstruction.listView(driver);
		String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
					
		if (listViewClass.contains(Constants.listViewClassData)) {			
			WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
		} 				
		
		CommonMethods.clickStatementWithFileName(driver, stmtFile);
		Thread.sleep(Constants.long_sleep);
		
		//Get Instruction ID
		String insId = ReceivedStatement.getInstructionID(driver).getText();
		((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
		
		//Verify Status of Uploaded Payment File			
		String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
		((ExtentTest) test).log(LogStatus.PASS, "Statement Status :" + recStatementStatus);
		
		switch (recStatementStatus) {
		  case "DEBULKED":
		    System.out.println(recStatementStatus);
		    break;
		  case "STATEMENT_REVIVE":
			BrowserResolution.scrollDown(driver);
		    System.out.println(recStatementStatus);
		    ReceivedStatement.clickAcceptInstruction(driver).click();
		    System.out.println("clicked accept");
		    WaitLibrary.waitForAngular(driver);
		    CommonMethods.approveStatement(driver, insId);
		    WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
     		searchInstruction.clear();
     		searchInstruction.sendKeys(insId, Keys.ENTER);
     		WaitLibrary.waitForAngular(driver);
     		
     		CommonMethods.clickStatementWithFileName(driver, stmtFile);
     		WaitLibrary.waitForAngular(driver);
		    break;
		  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
		    System.out.println(recStatementStatus);
		    break;
		}
		
		String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
		
		log.info("Verifying the status of uploaded file");
		Assert.assertEquals(recStmtStatus, "DEBULKED");
		((ExtentTest) test).log(LogStatus.PASS, "Statement Status :" + recStmtStatus);
				
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
	}
	catch(AssertionError ae){
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
		assertionerror=ae.getMessage();			
		CaptureScreenshot.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
	}
	catch(Exception et){
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
		exceptionerror=et.getMessage();			
		CaptureScreenshot.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
	}
	
}

}
