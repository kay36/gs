package com.gs.tc.ICCM.Sanity_TC;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC001_Book_MT103 extends LoginLogout_ICCM{
	
	@Test

	public void executeTC001() throws Exception {
		try {
			
			System.out.println("TC001_Book_MT103");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Sanity_TC" ,TestCaseName, Constants.Col_OutgoingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity + "\\" + sampleDir + paymentFile,"outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+ sampleDir + paymentFile,"payment");
			
			// Get F72 value from Orig Sample file
			String[] fieldDataArr = SampleFileModifier.getFieldData(System.getProperty("user.dir") + "\\" + Constants.gs_Sanity +"\\"+ sampleDir + paymentFile, "MT910");
			String fieldData = String.join("", fieldDataArr);
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_Sanity_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Sanity_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");

			String rec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			log.info("get mop");
			WebElement MOfP = ReceivedInstruction.paymentMethod(driver);
			String MofP = WaitLibrary.waitForElementToBeVisible(driver, MOfP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MofP);

			String methodOfPayment = CommonMethods.filterPayMethodDiv(MofP);
			Assert.assertEquals(methodOfPayment, "BOOK");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPayment);
			
			BrowserResolution.scrollDown(driver);

			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int fraudCountReq = 0;
			int fraudCountResp = 0;
			int finCountReq =0;
			int finCountResp =0;
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("FRAUD") && RelationshipTxt.equals("REQUEST")) {
					fraudCountReq++;
					if(fraudCountReq == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "Fraud Request is Available");
						
					}
				}
				if(invoPoint.equals("FRAUD") && RelationshipTxt.equals("RESPONSE")) {
					fraudCountResp++;
					if(fraudCountResp == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "Fraud Response is Available");
					}
				}
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("RESPONSE")) {
					finCountReq++;
					if(finCountReq == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "Finacle Account Posting Response is Available");
					}
				}
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCountResp++;
					if(finCountResp == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						//Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("TRANSFER")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "TRANSFER");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "1st Finacle Posting request Instruction Type not matched"+insType);
									throw new Exception("1st Finacle Posting request Instruction Type not matched");
								}
								
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				
			}
			if(fraudCountReq < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Fraud Request is not Available");
			}
			if(fraudCountResp < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Fraud Response is not Available");
			}
			if(finCountReq < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Response is not Available");
			}
			if(finCountResp < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "Finacle Account Posting Request is not Available");
			}
			//Navigate to Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			
			String[] accTypeArrIn = {"DDA", "DDA"};
			
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				
				if(i<posCount) {
					Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
					((ExtentTest) test).log(LogStatus.PASS, "1st Leg of posting verified with - Account type :: "+accTypeVal);				
					}
			}

			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList2 = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount2 = sysIntDataList2.size();
			System.out.println(sysIntDataCount2);
			int creditCount = 0;
			int debitCountReq =0;
			int debitCountRes =0;
			for (int j=0; j< sysIntDataCount2; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
					creditCount++;
					if(creditCount == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "CreditFundsControl Notification is Available");
					}
				}
				if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
					debitCountReq++;
					if(debitCountReq == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "DebitFundsControl REQUEST is Available");
					}
				}
				if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("RESPONSE")) {
					debitCountRes++;
					if(debitCountRes == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "DebitFundsControl RESPONSE is Available");
					}
				}
			}
			if(creditCount < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "CreditFundControl Request is Not Available");
			}
			if(debitCountReq < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "DebitFundsControl Request is not Available");
			}
			if(debitCountRes < 1) {
				((ExtentTest) test).log(LogStatus.FAIL, "DebitFundsControl Response is not Available");
			}
			//Navigate to External communication Tab
			
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount1 = 0;
			int directionCount2 = 0;
			int messageTypeCount1 = 0;
			int messageTypeCount2 = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getMsgType = ReceivedInstruction.externalCommunication(driver, j, "MessageType").getText();
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				
				if (getMsgType.equals("MT900") && getDirection.equals("OUT")) {
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getMsgType + " with " +"'"+ getDirection+"'"+" Direction");
					directionCount1++;
					messageTypeCount1++;
				}
				if (getMsgType.equals("MT103") && getDirection.equals("OUT")) {
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getMsgType + " with " +"'"+ getDirection+"'"+" Direction");
					directionCount2++;
					messageTypeCount2++;
				}
			}
			if (directionCount1 != 1) {
				System.out.println("Direction of MT900 not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified with MT900 Direction");
			}
			if (directionCount2 != 1) {
				System.out.println("Direction of MT103 not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified with MT103 Direction");
			}
			if (messageTypeCount1 != 1) {
				System.out.println("Message type of MT900 not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified with MT900 Message Type");
			}
			if (messageTypeCount2 != 1) {
				System.out.println("Message type of MT103 not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified with MT103 Message Type");
			}
			
			//Navigate to System interaction
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList3 = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount3 = sysIntDataList3.size();
			System.out.println(sysIntDataCount3);
			int BEC1 = 0;
			int BEC2 =0;
			int BEC3 =0;
			for (int j=0; j< sysIntDataCount2; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					//Thread.sleep(Constants.tooshort_sleep);
					WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
					String objDataXmlString = objDataXml.getText();
					String objDataString = U.xmlToJson(objDataXmlString);
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
						String status = temp.get("Status").getAsString();
						if(status.equals("TOBEPROCESSED")) {
							log.info("BECNOTIFICATION status"+status);
							((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION is generated for "+status+" Status");
							BEC1++;
						}
						if(status.equals("ACCEPTED")) {
							log.info("BECNOTIFICATION status"+status);
							((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION is generated for "+status+" Status");
							BEC2++;
						}
						if(status.equals("COMPLETED")) {
							log.info("BECNOTIFICATION status"+status);
							((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION is generated for "+status+" Status");
							BEC3++;
						}
						
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
				}
				if(BEC1 != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not generated for TOBEPROCESSED Status");
				}
				if(BEC2 != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not generated for ACCEPTED Status");
				}
				if(BEC3 != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not generated for COMPLETED Status");
				}
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

