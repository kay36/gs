package com.gs.tc.ICCM.Other_TCs;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC001_ForceAccept_MT103 extends LoginLogout_ICCM{
	
@Test
	
	public void executeTC181() throws Exception {
		try {
			System.out.println("TC001_ForceAccept_MT103");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Other_TCs",TestCaseName,Constants.Col_OutgoingPaymentOthers);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Others +"\\"+sampleDir+paymentFile, "outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Others +"\\"+ sampleDir + paymentFile,"payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Others_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Others_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_Others_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
					}			
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String insId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			log.info("Click on instruction id"); 
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();

			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			
			BrowserResolution.scrollDown(driver);
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			
			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			WebElement getPaymentId = ReceivedInstruction.getPaymentId(driver);
			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtId);
			
			WebElement outputInsElem = ReceivedInstruction.outputInstructionId(driver);
			String outputInsId = outputInsElem.getText();
			((ExtentTest) test).log(LogStatus.INFO, "Output Instructions ID :" + outputInsId);
						
			//Process AMH response file				
			// Fetch Payment File Name 
			String AWHFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Other_TCs",TestCaseName,Constants.Col_AMH_Response);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + AWHFile);
			
			//Modifying AMH file with Message Reference
			SampleFileModifier.UpdateXML(System.getProperty("user.dir") + "\\" + Constants.gs_Others +"\\"+sampleDir+AWHFile , outputInsId, "MessageReference");
		
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Others_Dir+"\\"+sampleDir+AWHFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Others_Dir+"\\"+sampleDir+AWHFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Others_Dir+"\\"+sampleDir+AWHFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement AWHrecInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, AWHrecInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, AWHrecInsTab, 30).click();
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, AWHFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			String AWHrec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, AWHFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(AWHrec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "AMH Instruction Status :" + AWHrec_fileStatus);
			
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
	
			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtId1 = ReceivedInstruction.getPaymentId(driver); 
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId1, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId1, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderr = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);			
			
			//Verify the payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			//click on force Accept			
			WebElement forceAccept = ReceivedStatement.forceAccept(driver);
			WaitLibrary.waitForElementToBeClickable(driver, forceAccept, Constants.avg_explicit).click();

			//Sent request for approval
			CommonMethods.approveStatement(driver, pmtIdTxt);
			log.info("After Approval");
			
			//Navigate to Received Instructions
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
					}
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement payId = ReceivedInstruction.getPaymentId(driver);
			String pmtIdText = WaitLibrary.waitForElementToBeVisible(driver, payId, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdText);
			WaitLibrary.waitForElementToBeClickable(driver, payId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdText);
			
			BrowserResolution.scrollDown(driver);
			//Verify payment status
			WebElement paymentStatusElmnt = ReceivedInstruction.getStatus(driver);
			String payStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElmnt, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+payStatus);
			Assert.assertEquals(payStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + payStatus);
			BrowserResolution.scrollDown(driver);
			
			//Navigate to External Communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Verify Status");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getMsgType = ReceivedInstruction.externalCommunication(driver, j, "MessageType").getText();
				String getStatus = ReceivedInstruction.externalCommunication(driver, j, "Status").getText();
				
				if (getMsgType.equals("Confirmation 2") && getStatus.equals("MATCHED")) {
					// Download the file
					ReceivedInstruction.externalCommDwnldBbtncon2(driver, j).click();
					Thread.sleep(Constants.short_sleep);
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getMsgType + " with " + getStatus+" Status");
					directionCount++;
					break;
				}
			}			

			if (directionCount != 1) {
				System.out.println("Message type not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}				
		
			//get the downloaded file
			Thread.sleep(Constants.tooshort_sleep);
			String obtainedFile = CommonMethods.getDownloadedFile().toString();
			System.out.println(obtainedFile);

			String[] fieldArray = {"ConfirmationStatus", "ConfirmationRawStatus"};
			for (String fieldArrayVal : fieldArray){
				String obtainedFieldDataArray = SampleFileModifier.forceAcceptComp(obtainedFile,fieldArrayVal);
				System.out.println(obtainedFieldDataArray);
				if(!obtainedFieldDataArray.isEmpty()) {
					((ExtentTest) test).log(LogStatus.PASS, "Confirmation2: " + fieldArrayVal + " " + obtainedFieldDataArray);
				}else {
						((ExtentTest) test).log(LogStatus.FAIL, "Confirmation2: " + fieldArrayVal +" not found" );
					}	
			}
			CommonMethods.cleanDownloadDir();

			log.info("Verify Status");
			List<WebElement> extDataList = ReceivedInstruction.extCommunicationList(driver);
			int extDataCountt = extDataList.size();
			System.out.println(extDataCountt);
			int directionCountt = 0;
			for (int j = 0; j < extDataCountt; j++) {
				String getMsgType = ReceivedInstruction.externalCommunication(driver, j, "MessageType").getText();
				String getStatus = ReceivedInstruction.externalCommunication(driver, j, "Status").getText();
				BrowserResolution.scrollDown(driver);
				if (getMsgType.equals("MT900") && getStatus.equals("COMPLETED")) {
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn(driver, j).click();
					Thread.sleep(Constants.short_sleep);
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getMsgType + " with " + getStatus+" Status");
					directionCountt++;
					break;
				}
			}
			if (directionCountt != 1) {
				System.out.println("Message type not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}

			//get the downloaded file
			Thread.sleep(Constants.short_sleep);
			String obtainedFile1 = CommonMethods.getDownloadedFile().toString();
			System.out.println(obtainedFile1);
			
			String[] fieldArr = {":20:", ":21:", ":25P:", ":32A:"};
			
			for (int i=0; i<fieldArr.length; i++) {
				String obtainedFieldDataArry = SampleFileModifier.getFieldValue(obtainedFile1, fieldArr[i]);
				System.out.println("Field value "+ fieldArr[i] + obtainedFieldDataArry);
				if(!obtainedFieldDataArry.isEmpty()) {
					((ExtentTest) test).log(LogStatus.PASS, "MT900 Field value " + fieldArr[i] + obtainedFieldDataArry);
				}else {
					((ExtentTest) test).log(LogStatus.FAIL, "MT900 Field value " + fieldArr[i] + "not found" );
				}			
			}		
						
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
