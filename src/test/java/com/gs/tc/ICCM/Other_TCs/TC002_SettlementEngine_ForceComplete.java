package com.gs.tc.ICCM.Other_TCs;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.restAPI;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.ResponseBody;

public class TC002_SettlementEngine_ForceComplete extends LoginLogout_ICCM{

	@Test

	public void executeTC002() throws Exception {
		try {
			
			System.out.println("TC002_SettlementEngine_ForceComplete");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch API request File
			String requestFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Other_TCs" ,TestCaseName, Constants.Col_requestFile);
			System.out.println(requestFile);
			
			String data = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "\\" + Constants.gs_Others + "\\" + sampleDir + requestFile)));
			System.out.println(data);
			
			//Request Header
			Header contentType1 = new Header("Content-Type", "application/json");
//			Header sourceIndicator1 = new Header("source-indicator", "API");
			Header Authorization1 = new Header("Authorization", "SessionToken:"+tokenFromStorage);
			
			Headers headers1 = new Headers(contentType1,Authorization1);
			
			ResponseBody response1 = restAPI.getpostAPIResponse("instructions/upload", data, headers1);
			
			// get Instruction value from Response
			String instId1 = response1.jsonPath().getString("BusinessPrimaryKey[0].Value");
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id from API Response:::" + instId1);
			
			String jSONString = "{\"InstructionID\":\""+instId1+"\",\"Account\":\"DDAGSILUSD13\"}";
			String newString = jSONString.toString();
			System.out.println(newString);

			//Request Header
			Header contentType2 = new Header("Content-Type", "application/json");
			Header Authorization2 = new Header("Authorization", "SessionToken:"+tokenFromStorage);
			
			Headers headers2 = new Headers(contentType2, Authorization2);
			Thread.sleep(Constants.tooshort_sleep);
			ResponseBody response2 = restAPI.getpostAPIResponse("/statements/settlementEngine/forcestop/response", newString, headers2);
			
			// get Instruction value from Response
			String Status = response2.jsonPath().getString("Status");
			System.out.println(Status);
			
			if(Status.equals("SUCCESS")) {
				
			((ExtentTest) test).log(LogStatus.PASS, Status);
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			Thread.sleep(Constants.tooshort_sleep);
			
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(instId1, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
			
			// Get and click Instruction ID
			
			WebElement instdid = ReceivedInstruction.findInsID(driver,instId1);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Clicked Instruction id:::" + insId);
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollToElement(driver, instdid);
			js.executeScript("arguments[0].click();", instdid);
			
			Thread.sleep(Constants.short_sleep);
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			System.out.println(pmtIdElem);
			BrowserResolution.scrollToElement(driver, pmtIdElem);
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Transaction Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);

			BrowserResolution.scrollDown(driver);
			
			//Verify Account posting with two legs of posting
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with 2nd leg of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			BrowserResolution.scrollDown(driver);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"NOS", "SUS", "SUS", "DDA"};
			for(int i=2; i<posCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
				
				((ExtentTest) test).log(LogStatus.PASS, "Account POsting - Account type :: "+getAccountTyp+"");
			}
			String creditor = ReceivedInstruction.accountPostingData(driver, 3, "AccountID").getText();
			
			//Navigate to System interaction to verify OFAC and BEC notification
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int creditCount = 0;
			int finCount = 0;
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				String[] becNotificationStatus = {"TOBEPROCESSED", "WAITING_FRAUDRESPONSE", "WAITING_OFACRESPONSE", "RECON_HOLD","ACCEPTED"};
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					
					becNotfCount++;
					if(becNotfCount < 6) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						String objDataString = U.xmlToJson(objDataXmlString);
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
							String status = temp.get("Status").getAsString();
							if(status.equals(becNotificationStatus[becNotfCount-1])) {
								log.info("BECNOTIFICATION status"+status);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION status "+status);

							}
						}

					catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
					}else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						finCount++;
						if(finCount == 2) {
							WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
							BrowserResolution.scrollToElement(driver, viewBtn);
							js.executeScript("arguments[0].click();", viewBtn);
							// Getting Object Data
							Thread.sleep(Constants.tooshort_sleep);
							WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
							WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
							String objDataString = objData.getText();
							try {
								JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
								
								for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
									String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
									
									if(insType.equals("BREAK_ADJUSTMENT")) {
										System.out.println(insType);
										Assert.assertEquals(insType, "BREAK_ADJUSTMENT");
										((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									}else {
										((ExtentTest) test).log(LogStatus.FAIL, "2nd Finacle Posting request Instruction Type not matched::"+insType);
									}
									
								}
							}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					}else if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
							creditCount++;
							if(creditCount == 1) {
								WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
								BrowserResolution.scrollToElement(driver, viewBtn);
								js.executeScript("arguments[0].click();", viewBtn);
								// Getting Object Data
								Thread.sleep(Constants.tooshort_sleep);
								WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
								WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
								String objDataString = objData.getText();
								try {
									
									JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
									
									String accountID = jsonObject.get("accountId").getAsString();
									if(accountID.equals("FIN"+creditor)) {
										System.out.println(accountID);
										Assert.assertEquals(accountID, "FIN"+creditor);
										((ExtentTest) test).log(LogStatus.PASS, "CreditFundControlt Notification Account ID : " + accountID + " verified");
									}else {
										((ExtentTest) test).log(LogStatus.FAIL, "CreditFundControlt Notification Account ID not matched::"+accountID);
									}
										
								}catch(Exception jse) {
								throw new Exception(jse.getMessage());
							}
							// Clicking close modal
							WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
							js.executeScript("arguments[0].click();", modalCloseBtn);
						}
							}
						}
					
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Status Failed");
			}
				
	
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
			}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

