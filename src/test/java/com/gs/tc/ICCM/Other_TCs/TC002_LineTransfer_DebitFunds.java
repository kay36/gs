package com.gs.tc.ICCM.Other_TCs;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.InitiateLineTransfer;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC002_LineTransfer_DebitFunds extends LoginLogout_ICCM{
	
	@Test

	public void executeTC002() throws Exception {
		try {
			
			System.out.println("TC002_LineTransfer_DebitFunds");
			Log.startTestCase(log, TestCaseName);

			log.info("Navigate to Initiate Line Transfer");
			WebElement initiateLineTransferTab = InitiateLineTransfer.initiateLineTransferTab(driver);
			BrowserResolution.scrollToElement(driver, initiateLineTransferTab);
			WaitLibrary.waitForElementToBeClickable(driver, initiateLineTransferTab, Constants.avg_explicit).click();
			
			log.info("Enter Amount");
			WebElement amount = InitiateLineTransfer.amountIniLineTrans(driver);
			WaitLibrary.waitForElementToBeClickable(driver, amount, Constants.avg_explicit).click();
			amount.sendKeys("5000");
			
			Actions actions = new Actions(driver);

			WebElement currency = InitiateLineTransfer.currencyIniLineTransfer(driver);
			actions.moveToElement(currency).build().perform();
			WaitLibrary.waitForElementToBeClickable(driver, currency, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);

			// Send Acc number
			WebElement currencySearch = InitiateLineTransfer.currencySearchField(driver);
			WaitLibrary.waitForElementToBeClickable(driver, currencySearch, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			currencySearch.sendKeys("USD");
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			currencySearch.sendKeys(Keys.ENTER);

			//Enter current date
			String currentDate = GenericFunctions.getCurrentDateOnly("yyyy-MM-dd");
			System.out.println(currentDate);
			log.info("Enter Current Date");
			WebElement date = InitiateLineTransfer.date(driver);
			WaitLibrary.waitForElementToBeClickable(driver, date, Constants.avg_explicit).click();
			date.sendKeys(currentDate, Keys.ENTER);
			
			
			log.info("Enter Source Account Number");
		    Thread.sleep(3000);
	        WebElement sourceAccNumber = InitiateLineTransfer.sourceAccountNumberdropdown(driver);
		    actions.moveToElement(sourceAccNumber).build().perform();   
			WaitLibrary.waitForAngular(driver);
			WaitLibrary.waitForElementToBeClickable(driver, sourceAccNumber, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver); 
		
			
			// Send Acc number
			WebElement sourceAccNumberSearch = InitiateLineTransfer.sourceAccountNumberSearch(driver);
			WaitLibrary.waitForElementToBeClickable(driver, sourceAccNumberSearch, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			sourceAccNumberSearch.sendKeys("NOSCITI01");
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			sourceAccNumberSearch.sendKeys(Keys.ENTER);
			
			log.info("Enter Destination Account Number");
			WebElement destinationAccNumber = InitiateLineTransfer.destinationAccountNumber(driver);
			BrowserResolution.scrollToElement(driver, destinationAccNumber);
			js.executeScript("window.scrollBy(0,-120)");
			actions.moveToElement(destinationAccNumber).build().perform();
			WaitLibrary.waitForElementToBeClickable(driver, destinationAccNumber, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);

			// Send Account number
			WebElement destinationAccNumbersearch = InitiateLineTransfer.destinationAccountNumberSearch(driver);
			WaitLibrary.waitForElementToBeClickable(driver, destinationAccNumbersearch, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			destinationAccNumbersearch.sendKeys("998888888");
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			destinationAccNumbersearch.sendKeys(Keys.ENTER);

			log.info("Submit");
			WebElement submit = InitiateLineTransfer.submitLineTransfer(driver);
			BrowserResolution.scrollToElement(driver, submit);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			WaitLibrary.waitForElementToBeClickable(driver, submit, Constants.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Initiate Line Transfer Details Submitted");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			WaitLibrary.waitForAngular(driver);
	        Thread.sleep(Constants.tooshort_sleep);
			
			String sourceData = "GSBI_LINETRANSFER_LineMovement_API";
			System.out.println(sourceData);
			log.info("Click on Payment file");
			WebElement source = ReceivedInstruction.getSource(driver,sourceData);
			String sourceText = WaitLibrary.waitForElementToBeVisible(driver, source, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			WaitLibrary.waitForElementToBeClickable(driver, source, Constants.avg_explicit).click();
			System.out.println(sourceText);
			Thread.sleep(Constants.short_sleep);
			
			log.info("get instruction id");
			WebElement getInsID = ReceivedInstruction.getInsId(driver);
			String insID = WaitLibrary.waitForElementToBeVisible(driver, getInsID, Constants.avg_explicit).getText();
			System.out.println(insID);
			((ExtentTest) test).log(LogStatus.INFO, "Instruction ID :: " + insID);
			Thread.sleep(Constants.short_sleep);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			System.out.println(pmtIdElem);
			BrowserResolution.scrollToElement(driver, pmtIdElem);
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Transaction Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "RISK_FILTER_HOLD");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			//*****************************Click on Accept Return****************************************//
			((ExtentTest) test).log(LogStatus.INFO, "Click on Accept Return");
			WebElement actionButton = ReceivedInstruction.acceptPayment(driver);
			
			WaitLibrary.waitForElementToBeClickable(driver, actionButton, Constants.avg_explicit).click();
			((ExtentTest) test).log(LogStatus.INFO, "Sent Request for Approval");
			
			//Send request for approval
			CommonMethods.approveStatement(driver, paymentID);
			
			//******************AFTER APPROVEL***************************//
			((ExtentTest) test).log(LogStatus.INFO, "After Approval");
			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabIn, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listViewIN = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIN, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIN, Constants.avg_explicit).click();
			}
			
			WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.clear();
			searchInstruction.sendKeys(insID, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
					
			// Get and click Instruction ID
			
			WebElement instdidIn = ReceivedInstruction.findInsID(driver,insID);
			String insIdIN = WaitLibrary.waitForElementToBeVisible(driver, instdidIn, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insIdIN);
			Thread.sleep(Constants.tooshort_sleep);
			
			BrowserResolution.scrollToElement(driver, instdidIn);
			js.executeScript("arguments[0].click();", instdidIn);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.tooshort_sleep);

			// Click on Payment ID
			WebElement pmtIdElemIn = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			System.out.println(pmtIdElemIn);
			BrowserResolution.scrollToElement(driver, pmtIdElemIn);
			String paymentIDIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElemIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentIDIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElemIn, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, paymentIDIn);
			
			Thread.sleep(Constants.tooshort_sleep);
			// Verifying Payment status
			WebElement PaymentStatusEle = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusEle, Constants.avg_explicit).getText();
			System.out.println("Transaction Status:::: "+paymentStatus);
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			BrowserResolution.scrollDown(driver);
			
			//Verify Account posting with two legs of posting
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"SUS", "SUS", "SUS", "NOS"};
			String[] entryStatus = {"03", "03", "03", "03"};
			for(int i=0; i<posCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
				Assert.assertEquals(EntryStatus, entryStatus[i]);
			
				((ExtentTest) test).log(LogStatus.PASS, "Account POsting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
			}
			String debitor = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			
			//Navigate to external communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			//get the downloaded file
			Thread.sleep(Constants.short_sleep);
			String obtainedFile1 = CommonMethods.getDownloadedFile().toString();
			System.out.println(obtainedFile1);
			
			String[] fieldArr = {":32A:", ":52A:", ":53B:", ":57A:", ":58D:", ":72:"};
			
			for (int i=0; i<fieldArr.length; i++) {
				String obtainedFieldDataArry = SampleFileModifier.getFieldValue(obtainedFile1, fieldArr[i]);
				System.out.println("Field value "+ fieldArr[i] + obtainedFieldDataArry);
				if(!obtainedFieldDataArry.isEmpty()) {
					((ExtentTest) test).log(LogStatus.INFO, "Field value of " + fieldArr[i] +"is:: "+ obtainedFieldDataArry);
				}else {
					((ExtentTest) test).log(LogStatus.FAIL, "Field value " + fieldArr[i] + "not found" );
				}			
			}
			//Navigate to System interaction to verify OFAC and BEC notification
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int drFundCount = 0;
		
			int finCountOUT = 0;
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				String[] becNotificationStatus = {"TOBEPROCESSED", "WAITING_FRAUDRESPONSE", "RISK_FILTER_HOLD","WAITING_DEBITFUNDSCONTROLRESPONSE", "ACCEPTED"};
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					
					becNotfCount++;
					if(becNotfCount < 5) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						String objDataString = U.xmlToJson(objDataXmlString);
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
							String status = temp.get("Status").getAsString();
							if(status.equals(becNotificationStatus[becNotfCount-1])) {
								log.info("BECNOTIFICATION status"+status);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION status "+status);

							}
						}

					catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
				}else if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
						drFundCount++;
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							
							String accountID = jsonObject.get("accountId").getAsString();
							String paymentCategory = jsonObject.get("paymentCategory").getAsString();
							String DorC = jsonObject.get("debitCreditIndicator").getAsString();
							if(accountID.equals(debitor)) {
							((ExtentTest) test).log(LogStatus.PASS, "Debitor account ID is :: "+accountID);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Debitor account ID not matched");
							}
							if(paymentCategory.equals("SYSTEM")) {
							((ExtentTest) test).log(LogStatus.PASS, "Payment Category is :: "+paymentCategory);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Payment Category is not matched");
							}
							if(DorC.equals("DEBIT")) {
							((ExtentTest) test).log(LogStatus.PASS, "debitCreditIndicator is:: "+DorC);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "DebitCreditIndicator is not matched");
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
					
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						finCountOUT++;
						if(finCountOUT == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						
								String serviceCode = jsonObject.get("additionalAttributes").getAsJsonObject().get("serviceCode").getAsString();
								String originalPaymentSubfunction = jsonObject.get("additionalAttributes").getAsJsonObject().get("originalPaymentSubFunction").getAsString();
								
								
								((ExtentTest) test).log(LogStatus.PASS, "Service Code in finacleAccountPosting Request is :: "+serviceCode);
								((ExtentTest) test).log(LogStatus.PASS, "Original Payment sub function in finacleAccountPosting Request is :: "+originalPaymentSubfunction);
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
			}
			}
				if(drFundCount != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "Debit fund control request is not generated");
				}
				
				
				
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
			}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

