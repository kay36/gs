package com.gs.tc.ICCM.Other_TCs;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.gs.utilities.restAPI;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.ResponseBody;

public class TC001_Funding_Movement_Transfer extends LoginLogout_ICCM{
	
	@Test

	public void executeTC001() throws Exception {
		try {
			
			System.out.println("TC001_Funding_Movement_Transfer");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch API request File
			String requestFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Other_TCs" ,TestCaseName, Constants.Col_requestFile);
			System.out.println(requestFile);
			
			String data = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "\\" + Constants.gs_Others + "\\" + sampleDir + requestFile)));
			System.out.println(data);
			
			//Request Header
			Header contentType = new Header("Content-Type", "application/json");
			Header Authorization = new Header("Authorization", "SessionToken:"+tokenFromStorage);
			
			Headers headers = new Headers(contentType, Authorization);
			
			ResponseBody response = restAPI.getpostAPIResponse("/payments/fundingmovement", data, headers);
			System.out.println(response);
			
			// get Instruction value from Response
			String Status = response.jsonPath().getString("Status");
			if(Status.equals("SUCCESS")) {
				((ExtentTest) test).log(LogStatus.PASS, "Status : " + Status);
				
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Status Failed");
			}
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			Thread.sleep(Constants.tooshort_sleep);
			
			String sourceData = "TBS_LINETRANSFER_FundingMovementRaw_API";
			System.out.println(sourceData);
			log.info("Click on Payment file");
			WebElement source = ReceivedInstruction.getSource(driver,sourceData);
			String sourceText = WaitLibrary.waitForElementToBeVisible(driver, source, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			WaitLibrary.waitForElementToBeClickable(driver, source, Constants.avg_explicit).click();
			System.out.println(sourceText);

			Thread.sleep(Constants.short_sleep);
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			System.out.println(pmtIdElem);
			BrowserResolution.scrollToElement(driver, pmtIdElem);
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Transaction Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);

			BrowserResolution.scrollDown(driver);
			
			//Verify Account posting with two legs of posting
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			BrowserResolution.scrollDown(driver);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"SUS", "SUS", "SUS", "NOS"};
			String[] entryStatus = {"03", "03", "03", "03"};
			for(int i=0; i<posCount; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
				Assert.assertEquals(EntryStatus, entryStatus[i]);
			
				((ExtentTest) test).log(LogStatus.PASS, "Account POsting - Account type :: "+getAccountTyp+ " with entry status "+EntryStatus+" is verified");
			}
			String debitor = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			
			//Navigate to external communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			//get the downloaded file
			Thread.sleep(Constants.short_sleep);
			String obtainedFile1 = CommonMethods.getDownloadedFile().toString();
			System.out.println(obtainedFile1);
			
			String[] fieldArr = {":32A:", ":52A:", ":53B:", ":57A:", ":58A:", ":72:"};
			
			for (int i=0; i<fieldArr.length; i++) {
				String obtainedFieldDataArry = SampleFileModifier.getFieldValueFunding(obtainedFile1, fieldArr[i]);
				System.out.println("Field value "+ fieldArr[i] + obtainedFieldDataArry);
				if(!obtainedFieldDataArry.isEmpty()) {
					((ExtentTest) test).log(LogStatus.INFO, "Field value of " + fieldArr[i] +"is:: "+ obtainedFieldDataArry);
				}else {
					((ExtentTest) test).log(LogStatus.FAIL, "Field value " + fieldArr[i] + "not found" );
				}			
			}
			//Navigate to System interaction to verify OFAC and BEC notification
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int drFundCount = 0;
		
			int finCountOUT = 0;
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				String[] becNotificationStatus = {"TOBEPROCESSED", "WAITING_FRAUDRESPONSE","WAITING_DEBITFUNDSCONTROLRESPONSE","WAITING_OFACRESPONSE", "DELIVERED", "ACCEPTED"};
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					
					becNotfCount++;
					if(becNotfCount < 5) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						String objDataString = U.xmlToJson(objDataXmlString);
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
							String status = temp.get("Status").getAsString();
							if(status.equals(becNotificationStatus[becNotfCount-1])) {
								log.info("BECNOTIFICATION status"+status);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION status "+status);

							}
						}

					catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				
					}
				}else if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
						drFundCount++;
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							
							String accountID = jsonObject.get("accountId").getAsString();
							String paymentCategory = jsonObject.get("paymentCategory").getAsString();
							String DorC = jsonObject.get("debitCreditIndicator").getAsString();
							if(accountID.equals(debitor)) {
							((ExtentTest) test).log(LogStatus.PASS, "Debitor account ID is :: "+accountID);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Debitor account ID not matched");
							}
							if(paymentCategory.equals("CHANNEL-SWIFT")) {
							((ExtentTest) test).log(LogStatus.PASS, "Payment Category is :: "+paymentCategory);
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "Payment Category is not matched");
							}
							
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
					
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						finCountOUT++;
						if(finCountOUT == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						
								String originalPaymentSubfunction = jsonObject.get("additionalAttributes").getAsJsonObject().get("originalPaymentSubFunction").getAsString();
								if(originalPaymentSubfunction.equals("FUNDING")) {
								((ExtentTest) test).log(LogStatus.PASS, "Original Payment sub function in finacleAccountPosting Request is :: "+originalPaymentSubfunction);
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "Original Payment sub function not matched ");
								}
								}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
			}
			}
				if(drFundCount != 1) {
					((ExtentTest) test).log(LogStatus.FAIL, "Debit fund control request is not generated");
				}
	

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
			}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

