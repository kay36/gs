package com.gs.tc.ICCM.F50;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot_F50;
import com.gs.utilities.CommonMethods_F50;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_F50;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC102_MT910_50K_1238 extends LoginLogout_F50{
	
	@Test

	public void executeTC102() throws Exception {
		try {
			
			System.out.println("TC102_MT910_50K_1238");
			Log.startTestCase(log, TestCaseName);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "F50" ,TestCaseName, Constants.Col_IncomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Samples + "\\" + sampleDir + paymentFile,"inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Samples +"\\"+ sampleDir + paymentFile,"payment");
			((ExtentTest) test).log(LogStatus.PASS, "Updated Payment Sample File");

			// Get F50 value from Sample file
			String[] fieldDataArr = SampleFileModifier.getFieldData(System.getProperty("user.dir")+ "\\" + Constants.gs_Samples +"\\"+ sampleDir + paymentFile, "50F");
			String fieldData = String.join("", fieldDataArr);
			System.out.println(fieldData);
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(sampleDir + paymentFile,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Click on Received Instruction Tab");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			log.info("Verifying Instruction Status");
			WebElement insStatusElem = ReceivedInstruction.insStatus(driver);
			String insStatus = WaitLibrary.waitForElementToBeVisible(driver, insStatusElem, Constants.avg_explicit).getText();			
			Assert.assertEquals(insStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status : " + insStatus);
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "REPAIR");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			BrowserResolution.scrollDown(driver);
			
			//click on repair button
			log.info("Click on repair button");
			WebElement repairButton = ReceivedInstruction.repairBtn(driver);
			BrowserResolution.scrollToElement(driver, repairButton);
			js.executeScript("arguments[0].click();", ReceivedInstruction.repairBtn(driver));
			((ExtentTest) test).log(LogStatus.INFO, "Sent for repair");

			//click on creditor
			log.info("Click on Creditor");
			WebElement creditor = ReceivedInstruction.Creditor(driver);
			BrowserResolution.scrollToElement(driver, creditor);
			js.executeScript("arguments[0].click();", creditor);
			System.out.println("Creditor clicked");
			
			//click on creditor Account number
			log.info("Enter Account Number");
			WebElement creditorText = ReceivedInstruction.creditorTxt(driver);
			//WaitLibrary.waitForElementToBeClickable(driver, creditorText, Constants.avg_explicit).click();
			BrowserResolution.scrollToElement(driver, creditorText);
			js.executeScript("arguments[0].click();", creditorText);
			
			//get Account number from excel
			log.info("get Account number from excel");
			String accNumber = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"F50",TestCaseName,Constants.Col_AccountNumber);
			System.out.println(accNumber);
			ReceivedInstruction.creditorTxt(driver).sendKeys(accNumber);
			log.info("Account number "+accNumber+" entered");
			((ExtentTest) test).log(LogStatus.INFO, "Account number "+accNumber+" entered");
			
			//click on submit
			WebElement submit = ReceivedInstruction.Submit(driver);
			BrowserResolution.scrollToElement(driver, submit);
			js.executeScript("arguments[0].click();", submit);
			log.info("Submitted for Approval");
			((ExtentTest) test).log(LogStatus.INFO, "Submitted for Approval");

			//Sent request for approval
			CommonMethods_F50.approveStatement(driver, paymentID);
			
			//************************After Approval****************************//
			log.info("After Approval");
			
			//Navigate to Received Instructions
			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods_F50.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtId = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with PaymentId");
			ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);
			
			//Verify payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			BrowserResolution.scrollDown(driver);
			
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					((ExtentTest) test).log(LogStatus.INFO, "Downloaded OUT file");
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "OUT file is not found");
			}
			Thread.sleep(Constants.short_sleep);
			
			// Getting the downloaded out file
			String obtainedFile = CommonMethods_F50.getDownloadedFile().toString();

			//Get F50 value of downloaded out file
			String[] obtainedFieldDataArr = SampleFileModifier.getFieldData(obtainedFile, "50F");
			String obtainedFieldData = String.join("", obtainedFieldDataArr);
			System.out.println(obtainedFieldData);

			if(fieldData.equals(obtainedFieldData)){
				((ExtentTest) test).log(LogStatus.PASS, "Out file F50 value is matched with the original file");
			}else{
				((ExtentTest) test).log(LogStatus.FAIL, "Out file F50 value is not matched with the original file"+obtainedFieldData);
			}

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot_F50.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_F50.test.addScreenCapture(CaptureScreenshot_F50.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot_F50.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_F50.test.addScreenCapture(CaptureScreenshot_F50.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

		// End of catch	block
		
	}	// End of Test Method
		
} 		// End of Class

