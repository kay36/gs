package com.gs.tc.ICCM.Reject_Return;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.CommonMethods_ACH;
import com.gs.utilities.Constants;
import com.gs.utilities.Constants_ACH;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC004_Inbound_Return_MT299 extends LoginLogout_ICCM{

	@Test
	
	public void executeTC04() throws Exception {
		try {
			System.out.println("TC04");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_outgoingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+paymentFile, "outbound", "202");
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + paymentFile,"payment");

			//Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
						
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}

			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			// Verify Received instruction status
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String payInsId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+payInsId);
						
			log.info("Click on instruction id"); 
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("verifiying status");
			WebElement pmtStatusElem = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusElem, Constants.avg_explicit).getText();	
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusElem, Constants_ACH.avg_explicit).getText();
			System.out.println("Payment Status::::"+pmtStatus);
			
//			if(pmtStatus.equals("TIME WAREHOUSED")) {
//				WaitLibrary.waitForElementToBeClickable(driver, pmtStatusElem, Constants_ACH.avg_explicit).click();
//				WaitLibrary.waitForAngular(driver);
//				Thread.sleep(Constants.tooshort_sleep);
//				
//				CommonMethods_ACH.forceRelease(driver, paymentID, paymentFile, payInsId);
//				WaitLibrary.waitForAngular(driver);
//				Thread.sleep(tooshort_sleep);
//				TimeUnit.MINUTES.sleep(5);
//			}
			
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			//get Orig Payment Reference
			WebElement origPayRefElem = ReceivedInstruction.originalPaymentReference(driver, 0, "OriginalPaymentReference");
			String origPayRef = origPayRefElem.getText();
			System.out.println(origPayRef);
			
			// Click on payment id
			log.info("Get Payment id"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdid, Constants.avg_explicit).click();
			
			log.info("get output instruction ID");
			WebElement outputInsElem = ReceivedInstruction.outputInstructionId(driver);
			String outputInsId = outputInsElem.getText();
			System.out.println(outputInsId);
			
			//Select the SWIFT Payment Action
			log.info("get mop");
			WebElement MOP = ReceivedInstruction.paymentMethod(driver);
			WaitLibrary.waitForAngular(driver);
			String MoP = WaitLibrary.waitForElementToBeVisible(driver, MOP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MoP);
			
			String methodOfPay = CommonMethods.filterPayMethodDiv(MoP);
			Assert.assertEquals(methodOfPay, "SWIFT_OUT");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPay);
			
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			//Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"DDA", "SUS", "SUS", "NOS"};
			String[] entryStatus = {"03", "03", "03", "03"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
				System.out.println(EntryStatus);
				Assert.assertEquals(EntryStatus, entryStatus[i]);

				((ExtentTest) test).log(LogStatus.PASS, "Two step posting - Account type :: "+getAccountTyp+ " Entry status verified :: "+EntryStatus);				
			}
			String getAccountIdDr1 = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			String getAccountIdCr1 = ReceivedInstruction.accountPostingData(driver, 1, "AccountID").getText();
			String getAccountIdDr2 = ReceivedInstruction.accountPostingData(driver, 2, "AccountID").getText();
			String getAccountIdCr2 = ReceivedInstruction.accountPostingData(driver, 3, "AccountID").getText();
			System.out.println(getAccountIdDr1+" "+getAccountIdCr1+" "+getAccountIdDr2+" "+getAccountIdCr2);
			
			BrowserResolution.scrollDown(driver);
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));

			// Goto System Interaction and verify BEC,OFAc and  Finacle
			log.info("Verifying System Interaction");

			List<WebElement> sysIntDataListOUT = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountOUT = sysIntDataListOUT.size();
			Thread.sleep(5000);
			int finCountOUT = 0 ;
			int drFundCount = 0;
			for (int j=0; j< sysIntDataCountOUT; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();

				if(invoPoint.equals("DEBITFUNDSCONTROL") && RelationshipTxt.equals("REQUEST")) {
					drFundCount++;
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						
						String reqType = jsonObject.get("requestType").getAsString();
						String clientID = jsonObject.get("clientId").getAsString();
						String accountID = jsonObject.get("accountId").getAsString();
						String drcIndicator = jsonObject.get("debitCreditIndicator").getAsString();
						
						((ExtentTest) test).log(LogStatus.INFO, "Debit control request, request type :: "+reqType);
						((ExtentTest) test).log(LogStatus.INFO, "Debit control request client ID :: "+clientID);
						((ExtentTest) test).log(LogStatus.INFO, "Debit control request accountID :: "+accountID);
						((ExtentTest) test).log(LogStatus.INFO, "Debit control request  Debit or Credit indicator :: "+drcIndicator);
						
//						if(reqType.equals("CONTROL")) {
//							
//							log.info("Request Type is : "+reqType);
//							((ExtentTest) test).log(LogStatus.PASS, "Debit control request, request type is :: "+reqType);
//						}else {
//							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request, request type not matched:: "+reqType);
//							
//						}
//					
//						if(clientID.equals("GSIL")) {
//							
//							log.info("Request Type is : "+clientID);
//							((ExtentTest) test).log(LogStatus.PASS, "Debit control Client ID is :: "+clientID);
//						}else {
//							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request client ID not matched:: "+clientID);
//							
//						}
//						if(accountID.equals("FINDDAGSILUSD13")) {
//							
//							log.info("Request Type is : "+accountID);
//							((ExtentTest) test).log(LogStatus.PASS, "Debit control request accountID is :: "+accountID);
//						}else {
//							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request accountID is not matched:: "+accountID);
//							
//						}
//						if(drcIndicator.equals("DEBIT")) {
//							
//							log.info("Request Type is : "+drcIndicator);
//							((ExtentTest) test).log(LogStatus.PASS, "Debit control request Debit or Credit is :: "+drcIndicator);
//						}else {
//							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request  Debit or Credit is not matched:: "+drcIndicator);
//							
//						}
						
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
				
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCountOUT++;
					if(finCountOUT == 1) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
							String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
							String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
							String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
							
							if(insType.equals("WIRE_OUT_CLI")) {
								System.out.println(insType);
								Assert.assertEquals(insType, "WIRE_OUT_CLI");
								((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
				
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched : "+ insType);
								throw new Exception("1st Finacle Account instruction Type not matched");
							}
							if(drAccount.contains("FIN"+getAccountIdDr1)) {
								System.out.println(drAccount);
								Assert.assertEquals(drAccount,"FIN"+getAccountIdDr1);
								((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Debit Account : " + drAccount + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account Debit Account not matched"+ drAccount);
								throw new Exception("2nd Finacle Account Debit Account not matched");
							}
							if(crAccount.contains("EXT"+getAccountIdCr1)) {
								System.out.println(crAccount);
								Assert.assertEquals(crAccount, "EXT"+getAccountIdCr1);
								((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Credit Account : " + crAccount + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account Credit Account not matched"+ crAccount);
								throw new Exception("2nd Finacle Account Credit Account not matched");
							}
						}
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				
				else if(finCountOUT == 2) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
							String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
							String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
							String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
							
							if(insType.equals("WIRE_OUT_NOS")) {
								System.out.println(insType);
								Assert.assertEquals(insType, "WIRE_OUT_NOS");
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
								throw new Exception("2nd Finacle Account instruction Type not matched");
							}
							if(drAccount.contains("EXT"+getAccountIdDr2)) {
								System.out.println(drAccount);
								Assert.assertEquals(drAccount,"EXT"+getAccountIdDr2);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Debit Account : " + drAccount + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Debit Account not matched"+ drAccount);
								throw new Exception("2nd Finacle Account Debit Account not matched");
							}
							if(crAccount.contains("EXT"+getAccountIdCr2)) {
								System.out.println(crAccount);
								Assert.assertEquals(crAccount, "EXT"+getAccountIdCr2);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Credit Account : " + crAccount + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Credit Account not matched"+ crAccount);
								throw new Exception("2nd Finacle Account Credit Account not matched");
							}
						}
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				}
				if(j==sysIntDataCountOUT-1) {
					if(finCountOUT !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
					if(drFundCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"Credit fund control notification not found");
						throw new Exception("Credit fund control notification not found");
					}
				}
			}
			
			
			//**********************************//Incoming Payment//********************************************************//
			
			// Fetch Payment File Name 
			String incomPaymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomPaymentFile);
			
			//updating output instructions id in MREF
			SampleFileModifier.updateStatementFile(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+incomPaymentFile, outputInsId, "f21");
			Thread.sleep(1000);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+incomPaymentFile, "inbound", "202");
				
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + paymentFile,"payment");

			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTabIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabIn, 30).click();
			
			WaitLibrary.waitForAngular(driver);
			
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionInIn =  ReceivedInstruction.searchWithInsID(driver);
			searchInstructionInIn.clear();
			//searchInstructionInIn.sendKeys(Keys.CLEAR);
			searchInstructionInIn.sendKeys(payInsId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
	
			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdInIn = ReceivedInstruction.getPaymentId(driver); 
			String pmtIdTxtInIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdInIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtIdTxtInIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdInIn, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrInIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtInIn);
			ReceivedStatement.reconRefreshIcon(driver).click();
			BrowserResolution.scrollDown(driver);

			//Navigate to external communication tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			log.info("Verify with the payment file status");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int Count=0;
			for (int j=0; j< extDataCount; j++) {
			String getmessageType = ReceivedInstruction.externalCommunication(driver, j, "MessageType").getText();
			String getStatus = ReceivedInstruction.externalCommunication(driver, j, "Status").getText();
			
			if(getmessageType.equals("Confirmation 2") && getStatus.equals("MATCHED")) {
					System.out.println(getmessageType+"and"+getStatus);
					Assert.assertEquals(getmessageType, "Confirmation 2");
					Assert.assertEquals(getStatus, "MATCHED");
					Count++;
					((ExtentTest) test).log(LogStatus.PASS, "MT299 is mapped with Matched status.");
				}
			
			}
			if(Count!=1) {
				System.out.println("Out file not with the status");
			}
			
			BrowserResolution.scrollUp(driver);
			
			log.info("Click on Force Status Button");
			WebElement forceStatus = ReceivedStatement.forceStatus(driver);
			WaitLibrary.waitForElementToBeVisible(driver, forceStatus, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			//Select values for the Force button message
			// Select the Force status Action
			log.info("Select the Force status Action");
			WebElement paymentAction = ReceivedInstruction.actionSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, paymentAction, Constants.avg_explicit).click();
			ReceivedInstruction.actionSelectSearch(driver).sendKeys("RETURN PAYMENT", Keys.ENTER);
			Thread.sleep(Constants.tooshort_sleep);
			
			// Select the Field Causing REJT/RETN
			log.info("Select the Field Causing REJT/RETN");
			WebElement fieldREJTRETN = ReceivedInstruction.FieldCausingREJTRETN(driver);
			WaitLibrary.waitForElementToBeClickable(driver, fieldREJTRETN, Constants.avg_explicit).click();
			ReceivedInstruction.fieldCausingREJTRETNSearch(driver).sendKeys("20", Keys.ENTER);
			Thread.sleep(Constants.tooshort_sleep);
			
//			// Click Reason code
       		log.info("Click on Reason code");
		    WebElement reasonCode = ReceivedInstruction.ReasonCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, reasonCode, Constants.avg_explicit).click();
			ReceivedInstruction.ReasonCode(driver).sendKeys(Keys.ARROW_DOWN , Keys.ENTER);
       		Thread.sleep(Constants.tooshort_sleep);	
			
			// Select the Attach Message ID
			log.info("Select the Attach Message ID");
			WebElement attachMsg = ReceivedInstruction.ChangeCode(driver);
			WaitLibrary.waitForElementToBeClickable(driver, attachMsg, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			ReceivedInstruction.SearchReasonCode(driver).sendKeys("GI",Keys.ENTER);
			Thread.sleep(Constants.tooshort_sleep);	
			
			// Enter the Additional Information
			log.info("Enter the Additional Information");
			WebElement addInfo = ReceivedInstruction.additionalInfoSearch(driver);
			WaitLibrary.waitForElementToBeClickable(driver, addInfo, Constants.avg_explicit).click();
			addInfo.sendKeys("Force statuss for Inbound return");
			Thread.sleep(Constants.tooshort_sleep);

			// click on submit
			log.info("Click on submit");
			WebElement submitPaymentAction = ReceivedStatement.SubmitForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submitPaymentAction, Constants.avg_explicit).click();
			
			Thread.sleep(Constants.tooshort_sleep);
						
			//Send request for approval
			CommonMethods.approveStatement(driver, pmtIdTxtInIn);
			
			ReceivedStatement.reconRefreshIcon(driver).click();
			Thread.sleep(Constants.short_sleep);
			ReceivedStatement.reconRefreshIcon(driver).click();
			BrowserResolution.scrollDown(driver);
						
			//Click on Linked Message Tab	
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));

			//Click Linked Message id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Get Linked Message Id");
			List<WebElement> linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			int linkMsgscount = linkedMsgsDataList.size();
			log.info("Click on Linked Message Id");
			String linkdMsgTxt = "";
			for(int l=0; l<linkMsgscount; l++) {
				String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText(); 
				if(linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, l, "LinkedMsgID");
					linkdMsgTxt = linkedMessageId.getText();
					WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
					break;
				}else if (l==linkMsgscount-1 && !linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					throw new Exception("Credit Transfer - Return of Funds not available in Linked Messages");
				}
			}
			
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Linked Payment Page with PaymentId");
			WebElement linkedPayHeader = ReceivedInstruction.verifyPaymentPage(driver, linkdMsgTxt);
			
			//Verify the linked payment status
			WebElement linkedPaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String linkedPaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, linkedPaymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Linked Payment Status::::"+linkedPaymentStatus);
			Assert.assertEquals(linkedPaymentStatus, "WAIT_RETURN_MATCHED_ACTION");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + linkedPaymentStatus);
			
			log.info("click on Acccept Return");
			WebElement actionButton = ReceivedInstruction.acceptReturn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, actionButton, Constants.avg_explicit).click();

			//Send request for approval
			CommonMethods.approveStatement(driver, linkdMsgTxt);
					
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(payInsId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			List<WebElement> payList = ReceivedInstruction.paymentList(driver);
			int payListCount = payList.size();
			String pmtIdTxtIn = "";
			for(int p=0;p<payListCount;p++) {
				String payMOP = ReceivedInstruction.originalPaymentReference(driver, p, "MethodOfPayment").getText();
				if(payMOP.equals("SWIFT_OUT")) {
					WebElement pmtIdIn = ReceivedInstruction.originalPaymentReference(driver, p, "PaymentID");
					pmtIdTxtIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdIn, Constants.avg_explicit).getText();
					System.out.println("Payment id:::" + pmtIdTxtIn);
					WaitLibrary.waitForElementToBeClickable(driver, pmtIdIn, Constants.avg_explicit).click();
					break;
				}
			}

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtIn);
			
			// click on refresh button
			ReceivedInstruction.Refresh(driver).click();
			Thread.sleep(Constants.short_sleep);
			BrowserResolution.scrollDown(driver);
			
			//Verify the original payment status
			Thread.sleep(1000);
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Linked Payment Status::::"+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "RETURNED");
			((ExtentTest) test).log(LogStatus.PASS, "Original Payment Status : " + PaymentStatus);
			
			//Click on Linked Message Tab	
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));

			//Click Linked Message id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Get Linked Message Id");
			List<WebElement> linkedMsgsDataList1 = ReceivedInstruction.linkedMsgsList(driver);
			int linkMsgscount1 = linkedMsgsDataList1.size();
			String linkdMsgTxt1 = "";
			log.info("Click on Linked Message Id");
			for(int l=0; l<linkMsgscount1; l++) {
				String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText(); 
				if(linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, l, "LinkedMsgID");
					linkdMsgTxt1 = linkedMessageId.getText();
					WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
					break;
				}else if (l==linkMsgscount1-1 && !linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					throw new Exception("Credit Transfer - Return of Funds not available in Linked Messages");
				}
			}
			
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Linked Payment Page with PaymentId");
			WebElement linkedPayHeader1 = ReceivedInstruction.verifyPaymentPage(driver, linkdMsgTxt1);
			
			//Verify the original payment status
			WebElement linkedPaymentStsElement = ReceivedInstruction.getStatus(driver);
			String linkedPaymentsts = WaitLibrary.waitForElementToBeVisible(driver, linkedPaymentStsElement, Constants.avg_explicit).getText();			
			System.out.println("Linked Payment Status::::"+linkedPaymentsts);
			Assert.assertEquals(linkedPaymentsts, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Linked Payment Status : " + linkedPaymentsts);
	
			//Select the SWIFT Payment Action
			log.info("get mop");
			WebElement MOP1 = ReceivedInstruction.paymentMethod(driver);
			WaitLibrary.waitForAngular(driver);
			String MoP1 = WaitLibrary.waitForElementToBeVisible(driver, MOP1, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MoP1);
			
			String methodOfPay1 = CommonMethods.filterPayMethodDiv(MoP1);
			Assert.assertEquals(methodOfPay1, "GS_OUT");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPay1);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus1 = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn1 = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus1, 60).getText();
			System.out.println("Button status :" +checkMaskBtn1);
			if(checkMaskBtn1.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus1);
			}
			Thread.sleep(2000);
			
			
			BrowserResolution.scrollDown(driver);
			
			//**************************************************************//
			
			//Verify Account posting with 1st and 2nd leg of posting
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with 1st leg of posting");
			List<WebElement> accountPostingDataList1LOP = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCountIn1LOP = accountPostingDataList1LOP.size();
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCountIn1LOP);
			Assert.assertEquals(posCountIn1LOP, 4);
			String[] accTypeArr1LOP = {"NOS", "SUS", "SUS", "DDA"};
			String[] accStatusArr = {"03", "03", "03", "03"};
			
			for(int i=0;i<(posCountIn1LOP-2);i++) {
				String accTypeValIn = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String accStatusIn = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(accTypeValIn, accTypeArr1LOP[i]);
				Assert.assertEquals(accStatusIn, accStatusArr[i]);
				
				((ExtentTest) test).log(LogStatus.PASS, "First Leg - Account type :: "+accTypeValIn+ " Entry status verified :: "+accStatusIn);
			}
			String getAccountIdDrIN1 = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			String getAccountIdCrIN1 = ReceivedInstruction.accountPostingData(driver, 1, "AccountID").getText();
			System.out.println(getAccountIdDrIN1+" "+getAccountIdCrIN1);
			
			log.info("Verify Account posting with 2nd leg of posting");
			List<WebElement> accountPostingDataList2LOP = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount2LOP = accountPostingDataList2LOP.size();
			WaitLibrary.waitForAngular(driver);
				
			BrowserResolution.scrollDown(driver);
			System.out.println(posCount2LOP);
			Assert.assertEquals(posCount2LOP, 4);
			String[] accTypeArrIn2LOP = {"NOS", "SUS", "SUS", "DDA"};
			String[] entryStatus2LOP = {"03", "03", "03", "03"};
			for(int i=2; i<posCount2LOP; i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String EntryStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(getAccountTyp, accTypeArrIn2LOP[i]);
				Assert.assertEquals(EntryStatus, entryStatus2LOP[i]);
			
				((ExtentTest) test).log(LogStatus.PASS, "Second Leg - Account type :: "+getAccountTyp+ " Entry status verified :: "+EntryStatus);
			}
			String getAccountIdDrIN2 = ReceivedInstruction.accountPostingData(driver, 2, "AccountID").getText();
			String getAccountIdCrIN2 = ReceivedInstruction.accountPostingData(driver, 3, "AccountID").getText();
			System.out.println(getAccountIdDrIN2+" "+getAccountIdCrIN2);
			
			// Navigate to external communication tab
			
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Verify Direction");
			List<WebElement> extDataLsitIN = ReceivedInstruction.extCommunicationList(driver);
			int extDataCountIN = extDataLsitIN.size();
			System.out.println(extDataCountIN);
			int directionCount = 0;
			for (int j = 0; j < extDataCountIN; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					Thread.sleep(Constants.short_sleep);
					String MREFVal = CommonMethods.readMREFDownloadFile();
					Assert.assertEquals(MREFVal, origPayRef);
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getDirection + " Output Ins Id : " + MREFVal);
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			
			// Goto System Interaction and verify Finacle request
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIN = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIN = sysIntDataListIN.size();
			System.out.println(sysIntDataCountIN);
			int finCountIN = 0 ;
			int crFundCount = 0;
				for (int j=0; j< sysIntDataCountIN; j++) {
					String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
					
					String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
					if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
						crFundCount++;
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							
							String reqType = jsonObject.get("requestType").getAsString();
							String clientID = jsonObject.get("clientId").getAsString();
							String accountID = jsonObject.get("accountId").getAsString();
							String drcIndicator = jsonObject.get("debitCreditIndicator").getAsString();
							
							((ExtentTest) test).log(LogStatus.INFO, "Credit control request, request type :: "+reqType);
							((ExtentTest) test).log(LogStatus.INFO, "Credit control request client ID :: "+clientID);
							((ExtentTest) test).log(LogStatus.INFO, "Credit control request accountID :: "+accountID);
							((ExtentTest) test).log(LogStatus.INFO, "Credit control request  Debit or Credit indicator :: "+drcIndicator);
							
//							if(reqType.equals("CONTROL")) {
//								log.info("Request Type is : "+reqType);
//								((ExtentTest) test).log(LogStatus.PASS, "Credit control request, request type is :: "+reqType);
//								
//							}else {
//								((ExtentTest) test).log(LogStatus.FAIL, "Credit control request, request type not matched, "+reqType);
//								
//							}
//							if(clientID.equals("GSIL")) {
//								
//								log.info("Request Type is : "+clientID);
//								((ExtentTest) test).log(LogStatus.PASS, "Debit control Client ID is :: "+clientID);
//							}else {
//								((ExtentTest) test).log(LogStatus.FAIL, "Debit control request client ID not matched"+clientID);
//								
//							}
//							if(accountID.equals("FINDDAGSILUSD13")) {
//								
//								log.info("Request Type is : "+accountID);
//								((ExtentTest) test).log(LogStatus.PASS, "Debit control request accountID is :: "+accountID);
//							}else {
//								((ExtentTest) test).log(LogStatus.FAIL, "Debit control request accountID is not matched"+accountID);
//								
//							}
//							if(drcIndicator.equals("CREDIT")) {
//								
//								log.info("Request Type is : "+drcIndicator);
//								((ExtentTest) test).log(LogStatus.PASS, "Credit control request Debit or Credit is :: "+drcIndicator);
//							}else {
//								((ExtentTest) test).log(LogStatus.FAIL, "Credit control request  Debit or Credit is not matched, "+drcIndicator);
//								
//							}
							
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
					
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
						finCountIN++;
					System.out.println(finCountIN);
					if(finCountIN == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
								System.out.println(getAccountIdDrIN1);
								System.out.println(getAccountIdCr1);
								if(insType.equals("WIRE_IN_NOS")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_IN_NOS");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched"+ insType);
									throw new Exception("1st Finacle Account instruction Type not matched");
								}
								if(drAccount.equals("EXT"+getAccountIdDrIN1)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount,"EXT"+getAccountIdDrIN1);
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Debit Account : " + drAccount + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account Debit Account not matched"+ drAccount);
									
								}
								if(crAccount.equals("EXT"+getAccountIdCrIN1)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "EXT"+getAccountIdCrIN1);
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Credit Account : " + crAccount + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account Credit Account not matched"+ crAccount);
									
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(finCountIN == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
								System.out.println(getAccountIdDr2);
								System.out.println(getAccountIdCr2);
								if(insType.equals("WIRE_IN_CLI")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_IN_CLI");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
									throw new Exception("2nd Finacle Account instruction Type not matched");
								}
								if(drAccount.equals("EXT"+getAccountIdDrIN2)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount,"EXT"+getAccountIdDrIN2);
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Debit Account : " + drAccount + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Debit Account not matched"+ drAccount);
									
								}
								if(crAccount.equals("FIN"+getAccountIdCrIN2)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "FIN"+getAccountIdCrIN2);
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Credit Account : " + crAccount + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Credit Account not matched"+ crAccount);
									
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				 
				if(j==sysIntDataCountIN-1) {
					if(finCountIN !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
					if(crFundCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"Credit fund control notification not found");
						throw new Exception("Credit fund control notification not found");
					}
				}
			}
				
			log.info("BEC Notification");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int becNotfCount = 0;
			for (int j = 1; j <= sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j - 1, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j - 1, "Relationship").getText();

				if (invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becNotfCount = j;
					System.out.println(becNotfCount);
				}
			}

			if (becNotfCount != 0) {
				WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, becNotfCount - 1);
				BrowserResolution.scrollToElement(driver, viewBtn);
				js.executeScript("arguments[0].click();", viewBtn);
				// Getting Object Data
				Thread.sleep(Constants.tooshort_sleep);
				WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, becNotfCount - 1);
				WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
				String objDataXmlString = objDataXml.getText();

				String objDataString = U.xmlToJson(objDataXmlString);

				try {
					JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
					JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1");
					String eventID = temp.get("EventID").getAsString();
					System.out.println("BEC notification ID " + eventID);
					if (eventID.equals("BE1CI")) {
						log.info("BECNOTIFICATION status : " + eventID);
						((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION Event ID is :: " + eventID);
					} else {
						System.out.println("BECNOTIFICATION Event ID is not matched");
						((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION Event ID is not matched :: " + eventID);
					}
				} catch (Exception jse) {
					throw new Exception(jse.getMessage());
				}
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, becNotfCount - 1);
				js.executeScript("arguments[0].click();", modalCloseBtn);
			} else {
				((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not found");      
			}  
			 
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		} 
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
