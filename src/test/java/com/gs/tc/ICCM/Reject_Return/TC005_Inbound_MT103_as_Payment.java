package com.gs.tc.ICCM.Reject_Return;

import java.util.List;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC005_Inbound_MT103_as_Payment extends LoginLogout_ICCM{

	@Test
	
	public void executeTC05() throws Exception {
		try {
			System.out.println("TC005_Inbound_MT103_as_Payment");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+paymentFile, "inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + paymentFile,"payment");

			//Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
						
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			// Verify Received instruction status
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String payInsId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+payInsId);
						
			log.info("Click on instruction id"); 
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on payment id
			log.info("Get Payment id"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdid, Constants.avg_explicit).click();
			
			Thread.sleep(2000);
			//click on process as payment
			log.info("click on process as payment");
			WebElement actionButton = ReceivedInstruction.processAsPayment(driver);
			WaitLibrary.waitForElementToBeClickable(driver, actionButton, Constants.avg_explicit).click();

			//Send request for approval
			CommonMethods.approveStatement(driver, pmtId);
			
			//******************AFTER APPROVEL***************************//
			
			// Send request for approval

			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(payInsId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtIdIn = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxtIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxtIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdIn, Constants.avg_explicit).click();

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtIn);

			BrowserResolution.scrollDown(driver);
			
			//Verify the linked payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Linked Payment Status::::"+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
	
			//Select the SWIFT Payment Action
			log.info("get mop");
			WebElement MOP = ReceivedInstruction.paymentMethod(driver);
			WaitLibrary.waitForAngular(driver);
			String MoP = WaitLibrary.waitForElementToBeVisible(driver, MOP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MoP);
			
			String methodOfPay = CommonMethods.filterPayMethodDiv(MoP);
			Assert.assertEquals(methodOfPay, "SWIFT_IN");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPay);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			//Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with 2nd leg of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] accTypeArrIn = {"NOS", "SUS", "SUS", "DDA"};
			String[] accStatusArrIn = {"03", "03", "03", "03"};
			for(int i=2;i<posCount;i++) {
				String accTypeValIn = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String accStatusIn = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				
				Assert.assertEquals(accTypeValIn, accTypeArrIn[i]);
				Assert.assertEquals(accStatusIn, accStatusArrIn[i]);
				
				((ExtentTest) test).log(LogStatus.PASS, "Second Leg - Account type :: "+accTypeValIn+ " Entry status verified :: "+accStatusIn);				
			}
			String getAccountIdDr1 = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			String getAccountIdCr1 = ReceivedInstruction.accountPostingData(driver, 1, "AccountID").getText();
			String getAccountIdDr2 = ReceivedInstruction.accountPostingData(driver, 2, "AccountID").getText();
			String getAccountIdCr2 = ReceivedInstruction.accountPostingData(driver, 3, "AccountID").getText();
			System.out.println(getAccountIdDr1+" "+getAccountIdCr1+" "+getAccountIdDr2+" "+getAccountIdCr2);
			
			
			BrowserResolution.scrollDown(driver);
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			// Goto System Interaction and verify BEC,OFAc and  Finacle
			log.info("Verifying System Interaction");
			
			List<WebElement> sysIntDataListOUT = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountOUT = sysIntDataListOUT.size();
			
			int finCountOUT = 0 ;
			int drFundCount = 0;
			for (int j=0; j< sysIntDataCountOUT; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();

				if(invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
					drFundCount++;
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						
						String reqType = jsonObject.get("requestType").getAsString();
						String clientID = jsonObject.get("clientId").getAsString();
						String accountID = jsonObject.get("accountId").getAsString();
						String drcIndicator = jsonObject.get("debitCreditIndicator").getAsString();
						
						if(reqType.equals("POSTING")) {
							
							log.info("Request Type is : "+reqType);
							((ExtentTest) test).log(LogStatus.PASS, "Debit control request, request type is :: "+reqType);
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request, request type not matched"+reqType);
							
						}
					
						if(clientID.equals("GSIL")) {
							
							log.info("Request Type is : "+clientID);
							((ExtentTest) test).log(LogStatus.PASS, "Debit control Client ID is :: "+clientID);
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request client ID not matched"+clientID);
							
						}
						if(accountID.equals("FINDDAGSILUSD13")) {
							
							log.info("Request Type is : "+accountID);
							((ExtentTest) test).log(LogStatus.PASS, "Debit control request accountID is :: "+accountID);
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request accountID is not matched"+accountID);
							
						}
						if(drcIndicator.equals("CREDIT")) {
							
							log.info("Request Type is : "+drcIndicator);
							((ExtentTest) test).log(LogStatus.PASS, "Debit control request Debit or Credit is :: "+drcIndicator);
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "Debit control request  Debit or Credit is not matched"+drcIndicator);
							
						}
						
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
				
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCountOUT++;
					if(finCountOUT == 1) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
							String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
							String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
							String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
							
							if(insType.equals("WIRE_IN_NOS")) {
								System.out.println(insType);
								Assert.assertEquals(insType, "WIRE_IN_NOS");
								((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
				
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched : "+ insType);
								throw new Exception("1st Finacle Account instruction Type not matched");
							}
//							removed EXT infront of getaccountidDr1
							if(drAccount.contains(getAccountIdDr1)) {
								System.out.println(drAccount);
								Assert.assertEquals(drAccount,getAccountIdDr1);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Debit Account : " + drAccount + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Debit Account not matched"+ drAccount);
								
							}
							if(crAccount.contains("EXT"+getAccountIdCr1)) {
								System.out.println(crAccount);
								Assert.assertEquals(crAccount, "EXT"+getAccountIdCr1);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Credit Account : " + crAccount + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Credit Account not matched"+ crAccount);
								
							}
						}
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				
				else if(finCountOUT == 2) {
					WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
					BrowserResolution.scrollToElement(driver, viewBtn);
					js.executeScript("arguments[0].click();", viewBtn);
					// Getting Object Data
					Thread.sleep(Constants.tooshort_sleep);
					WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
					WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
					String objDataString = objData.getText();
					try {
						JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
						for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
							String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
							String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
							String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
							
							if(insType.equals("WIRE_IN_CLI")) {
								System.out.println(insType);
								Assert.assertEquals(insType, "WIRE_IN_CLI");
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
								throw new Exception("2nd Finacle Account instruction Type not matched");
							}
							if(drAccount.contains("EXT"+getAccountIdDr2)) {
								System.out.println(drAccount);
								Assert.assertEquals(drAccount,"EXT"+getAccountIdDr2);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Debit Account : " + drAccount + " verified");
								
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Debit Account not matched"+ drAccount);
								
							}
							if(crAccount.contains("FIN"+getAccountIdCr2)) {
								System.out.println(crAccount);
								Assert.assertEquals(crAccount, "FIN"+getAccountIdCr2);
								((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Credit Account : " + crAccount + " verified");
							
							}else {
								((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Credit Account not matched"+ crAccount);
							
							}
						}
					}catch(Exception jse) {
						throw new Exception(jse.getMessage());
					}
					// Clicking close modal
					WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
					js.executeScript("arguments[0].click();", modalCloseBtn);
				}
				}
				if(j==sysIntDataCountOUT-1) {
					if(finCountOUT !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
					if(drFundCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"Credit fund control notification not found");
						throw new Exception("Credit fund control notification not found");
					}
				}
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
