package com.gs.tc.ICCM.Reject_Return;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC001_Inbound_Reject_MT103 extends LoginLogout_ICCM{
	
	@Test

	public void executeTC001() throws Exception {
		try {
			System.out.println("TC001_Inbound_Reject_MT103");
			Log.startTestCase(log, TestCaseName);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);

			// Fetch Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Return_Reject",TestCaseName, Constants.Col_outgoingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);

			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + paymentFile,"outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+ sampleDir + paymentFile,"payment");

			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_REJ_RET_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + paymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}

			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");

			String rec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			// Get Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);

			log.info("Click on instruction id");
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();

			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);

			BrowserResolution.scrollDown(driver);
			log.info("Get Payment id & Verify Status");
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver);
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtId);

			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();
			System.out.println("Payment Status::::" + pmtStatus);
			Assert.assertEquals(pmtStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);

			WebElement getPaymentId = ReceivedInstruction.getPaymentId(driver);
			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants.avg_explicit).click();

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtId);

			WebElement outputInsElem = ReceivedInstruction.outputInstructionId(driver);
			String outputInsId = outputInsElem.getText();
			System.out.println(outputInsId);

			// Incoming Payment- AWH response
			// Fetch Payment File Name
			String incomAWHFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Return_Reject",	TestCaseName, Constants.Col_statementFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomAWHFile);

			// Modifying AWH file with Message Reference
			SampleFileModifier.UpdateXML(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + incomAWHFile,outputInsId, "MessageReference");

			// Modifying statement sample file with Incoming Payments
			log.info("Modifying sample file with payment reference");
			SampleFileModifier.modifyFileInboundOutbound(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + incomAWHFile,outputInsId, "inbound");

			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomAWHFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomAWHFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomAWHFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement AWHrecInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, AWHrecInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, AWHrecInsTab, 30).click();

			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver,ReceivedInstruction.getTransportNameStatusByFileName(driver, incomAWHFile));
			System.out.println(incomAWHFile);
			js.executeScript("window.scrollBy(0,-120)");

			String AWHrec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, incomAWHFile).getText();
			System.out.println(AWHrec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(AWHrec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + AWHrec_fileStatus);

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstruction = ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtId1 = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId1, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId1, Constants.avg_explicit).click();

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderr = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);

			// Verify the payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status::::" + paymentStatus);
			Assert.assertEquals(paymentStatus, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			BrowserResolution.scrollDown(driver);
			// Navigate to external communication tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			// Message Type confirmation1 and status checking
			String extCommExpectedMsgType = "Confirmation 1";
			String extCommExpectedStatus = "MATCHED";
			String extCommMsgType = ReceivedInstruction.externalCommunication(driver, 2, "MessageType").getText();
			String extCommStatus = ReceivedInstruction.externalCommunication(driver, 2, "Status").getText();
			if (extCommMsgType.equals(extCommExpectedMsgType) && (extCommStatus.equals(extCommExpectedStatus))) {
				((ExtentTest) test).log(LogStatus.PASS, "External Communication Status : " + extCommStatus);
				System.out.println(extCommStatus);
			} else {
				System.out.println("External Communication status not matched");
			}

			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));

			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount1 = accountPostingDataList.size();
			System.out.println(posCount1);
			Assert.assertEquals(posCount1, 4);
			String[] accTypeArr = { "DDA", "SUS", "SUS", "NOS" };
			String[] entryStatusArr = { "03", "03", "01", "01" };
			for (int i = 0; i < posCount1; i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String entStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				Assert.assertEquals(accTypeVal, accTypeArr[i]);
				Assert.assertEquals(entStatus, entryStatusArr[i]);
				((ExtentTest) test).log(LogStatus.PASS, "Two Step of Posting Status : " + entStatus);
				System.out.println(entStatus);
			}

			// Incoming Payment- Reject
			// Fetch Payment File Name
			String incomPaymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "Return_Reject", TestCaseName, Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomPaymentFile);

			// updating output instructions id in MREF
			SampleFileModifier.updateStatementFile(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + incomPaymentFile,outputInsId, "mref");
			Thread.sleep(1000);

			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + incomPaymentFile,"inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+ sampleDir + incomPaymentFile,"payment");

			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomPaymentFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomPaymentFile,Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir + "\\" + sampleDir + incomPaymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTabIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabIn, 30).click();

			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtIdIn = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxtIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxtIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdIn, Constants.avg_explicit).click();

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtIn);

			BrowserResolution.scrollDown(driver);
			// Verify the payment status
			WebElement pmtStatusidIn = ReceivedInstruction.getStatus(driver);
			String pmtStatusIn = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusidIn, Constants.avg_explicit).getText();
			System.out.println("Payment Status::::" + pmtStatusIn);
			Assert.assertEquals(pmtStatusIn, "WAIT_REJECT_MATCHED_ACTION");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatusIn);

			Thread.sleep(Constants.tooshort_sleep);
			log.info("Click on Accept Reject Button");
			WebElement acceptRejectt = ReceivedStatement.acceptReject(driver);
			WaitLibrary.waitForElementToBeVisible(driver, acceptRejectt, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.long_sleep);
			
			// Send request for approval
			CommonMethods.approveStatement(driver, pmtIdTxtIn);

			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionInIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionInIn.clear();
			searchInstructionInIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtIdInIn = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxtInIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdInIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxtInIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdInIn, Constants.avg_explicit).click();

			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrInIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtInIn);

			BrowserResolution.scrollDown(driver);
			// Check Original file in rejected status
			WebElement rejpmtStatusid = ReceivedInstruction.getStatus(driver);
			String rejpmtStatus = WaitLibrary.waitForElementToBeVisible(driver, rejpmtStatusid, Constants.avg_explicit).getText();
			System.out.println("Payment Status::::" + rejpmtStatus);
			Assert.assertEquals(rejpmtStatus, "REJECTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + rejpmtStatus);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);

			BrowserResolution.scrollDown(driver);
			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));

			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataListIn = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCountIn = accountPostingDataListIn.size();
			System.out.println(posCountIn);
			Assert.assertEquals(posCountIn, 6);
			String[] accTypeArrIn = { "DDA", "SUS", "SUS", "NOS", "DDA", "SUS" };
			String[] accCrDrArrIn = { "D", "C", "D", "C", "C", "D" };
			String[] accStatusArrIn = { "03", "03", "09", "09", "03", "03" };
			for (int i = 0; i < posCountIn; i++) {
				String accTypeValIn = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String accStatusIn = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				String accDebitCreditIn = ReceivedInstruction.accountPostingData(driver, i, "DrCrInd").getText();
				
				Assert.assertEquals(accTypeValIn, accTypeArrIn[i]);
				Assert.assertEquals(accStatusIn, accStatusArrIn[i]);
				Assert.assertEquals(accDebitCreditIn, accCrDrArrIn[i]);
			}
			String getAccountIdDr1 = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			String getAccountIdCr1 = ReceivedInstruction.accountPostingData(driver, 1, "AccountID").getText();
			String getAccountIdCr2 = ReceivedInstruction.accountPostingData(driver, 4, "AccountID").getText();
			String getAccountIdDr2 = ReceivedInstruction.accountPostingData(driver, 5, "AccountID").getText();

			WaitLibrary.waitForAngular(driver);
			// Navigate to external communication tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			String extCommExpectedMsgTypeIn = "MT199";
			String extCommExpectedStatusIn = "COMPLETED";
			String extCommMsgTypeIn = ReceivedInstruction.externalCommunication(driver, 4, "MessageType").getText();
			String extCommStatusIn = ReceivedInstruction.externalCommunication(driver, 4, "Status").getText();

			if (extCommMsgTypeIn.equals(extCommExpectedMsgTypeIn) && (extCommStatusIn.equals(extCommExpectedStatusIn))) {
				System.out.println(extCommStatusIn);
				((ExtentTest) test).log(LogStatus.PASS, "External Communication Status : " + extCommStatusIn);
			} 
			else {
				System.out.println("Message Type not available");
			}

			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			Thread.sleep(2000);
			// Goto System Interaction and verify Finacle request
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int finCount = 0;
			for (int j = 0; j < sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();

				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();

				if (invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					if (finCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for (int k = 0; k < jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
								System.out.println("Debit account " + getAccountIdDr1);
								System.out.println("Credit account " + getAccountIdCr1);
								System.out.println(drAccount);
								System.out.println(crAccount);
								if (insType.equals("WIRE_OUT_CLI")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_OUT_CLI");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
								} else {
									System.out.println("WIRE_OUT_CLI not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"1st Finacle Account instruction Type not matched" + insType);
								}
								if (drAccount.equals("FIN" + getAccountIdDr1)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount, "FIN" + getAccountIdDr1);
									((ExtentTest) test).log(LogStatus.PASS,	"1st Finacle Posting request Debit Account : " + drAccount + " verified");
								} else {
									System.out.println("Debit Account not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"1st Finacle Debit Account not matched" + drAccount);
								}
								if (crAccount.equals("EXT" + getAccountIdCr1)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "EXT" + getAccountIdCr1);
									((ExtentTest) test).log(LogStatus.PASS,	"1st Finacle Posting request Credit Account : " + crAccount + " verified");

								} 
								else {
									System.out.println("Credit Account not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"1st Finacle Credit Account not matched" + crAccount);
								}
							}
						} catch (Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					} 
					else if (finCount == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for (int k = 0; k < jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();

								if (insType.equals("WIRE_OUT_CLI_REV")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_OUT_CLI_REV");
									((ExtentTest) test).log(LogStatus.PASS,	"2nd Finacle Posting request Instruction Type : " + insType + " verified");

								} else {
									System.out.println("WIRE_OUT_CLI_REV not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"2nd Finacle Account instruction Type not matched" + insType);
								}
								if (drAccount.equals("EXT" + getAccountIdDr2)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount, "EXT" + getAccountIdDr2);
									((ExtentTest) test).log(LogStatus.PASS,	"2nd Finacle Posting request Debit Account : " + drAccount + " verified");
								} else {
									System.out.println("Debit Account not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"2nd Finacle Debit Account not matched" + drAccount);
								}
								if (crAccount.equals("FIN" + getAccountIdCr2)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "FIN" + getAccountIdCr2);
									((ExtentTest) test).log(LogStatus.PASS,	"2nd Finacle Posting request Credit Account : " + crAccount + " verified");

								} else {
									System.out.println("Credit Account not matched");
									((ExtentTest) test).log(LogStatus.FAIL,	"2nd Finacle Credit Account not matched" + crAccount);
								}
							}
						} catch (Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						break;
					}
				}

				if (j == sysIntDataCount - 1) {
					if (finCount != 2) {
						((ExtentTest) test).log(LogStatus.FAIL, "2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
				}
			}

			log.info("BEC Notification");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int becNotfCount = 0;
			for (int j = 1; j <= sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j - 1, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j - 1, "Relationship").getText();

				// String[] becNotificationEvent = {"BE1REJ"};
				if (invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becNotfCount = j;
					System.out.println(becNotfCount);
				}
			}

			if (becNotfCount != 0) {
				WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, becNotfCount - 1);
				BrowserResolution.scrollToElement(driver, viewBtn);
				js.executeScript("arguments[0].click();", viewBtn);
				// Getting Object Data
				Thread.sleep(Constants.tooshort_sleep);
				WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, becNotfCount - 1);
				WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
				String objDataXmlString = objDataXml.getText();

				String objDataString = U.xmlToJson(objDataXmlString);

				try {
					JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
					JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1");
					String eventID = temp.get("EventID").getAsString();
					System.out.println("BEC notification ID " + eventID);
					if (eventID.equals("BE1REJ")) {
						log.info("BECNOTIFICATION status : " + eventID);
						((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION Event ID is :: " + eventID);
					} else {
						System.out.println("BECNOTIFICATION Event ID is not matched");
					}
				} catch (Exception jse) {
					throw new Exception(jse.getMessage());
				}
				// Clicking close modal
				WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, becNotfCount - 1);
				js.executeScript("arguments[0].click();", modalCloseBtn);
			} else {
				((ExtentTest) test).log(LogStatus.FAIL, "BECNOTIFICATION is not found");
			}

			// Credit Funds Control
			log.info("Credit Funds Control");
			List<WebElement> sysIntDataListCr = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountCr = sysIntDataListCr.size();
			System.out.println(sysIntDataCountCr);
			int crNotfCount = 0;
			for (int j = 0; j < sysIntDataCountCr; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				if (invoPoint.equals("CREDITFUNDSCONTROL") && RelationshipTxt.equals("NOTIFICATION")) {
					crNotfCount++;
					System.out.println(crNotfCount);
					if (crNotfCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							String clientID = jsonObject.get("clientId").getAsString();
							String acctID = jsonObject.get("accountId").getAsString();
							String drCrIndicator = jsonObject.get("debitCreditIndicator").getAsString();
							
							System.out.println("Client ID " + clientID);
							System.out.println("Account ID " + acctID);
							System.out.println("Debit / Credit Indicator " + drCrIndicator);
							
							if (clientID.equals("GSIL")) {
								System.out.println(clientID);
								Assert.assertEquals(clientID, "GSIL");
								((ExtentTest) test).log(LogStatus.PASS, "Client ID : " + clientID);
							} else {
								System.out.println("Credit ID not matched");
								((ExtentTest) test).log(LogStatus.FAIL, "Credit ID not matched" + clientID);
							}

							if (acctID.equals("FIN" + getAccountIdDr1)) {
								System.out.println(acctID);
								Assert.assertEquals(acctID, "FIN" + getAccountIdDr1);
								((ExtentTest) test).log(LogStatus.PASS, "Account ID : " + acctID);

							} else {
								System.out.println("Account ID not matched");
								((ExtentTest) test).log(LogStatus.FAIL, "Account ID not matched" + acctID);
							}
							if (drCrIndicator.equals("CREDIT")) {
								System.out.println(drCrIndicator);
								Assert.assertEquals(drCrIndicator, "CREDIT");
								((ExtentTest) test).log(LogStatus.PASS, "Debit / Credit Indicator : " + drCrIndicator);
							} else {
								System.out.println("Credit Indicator not matched");
								((ExtentTest) test).log(LogStatus.FAIL, "Credit Indicator not matched" + drCrIndicator);
							}
						} catch (Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
			}

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

	}

}
