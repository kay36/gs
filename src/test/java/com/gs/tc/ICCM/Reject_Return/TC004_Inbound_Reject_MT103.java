package com.gs.tc.ICCM.Reject_Return;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC004_Inbound_Reject_MT103 extends LoginLogout_ICCM{
	
@Test
	
	public void executeTC004() throws Exception {
		try {
			System.out.println("TC004_Inbound_Reject_MT103");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_outgoingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+paymentFile, "outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+ sampleDir + paymentFile,"payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}				
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String insId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			log.info("Click on instruction id"); 
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();

			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			
			BrowserResolution.scrollDown(driver);
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			
			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			WebElement getPaymentId = ReceivedInstruction.getPaymentId(driver);
			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtId);
			
			WebElement outputInsElem = ReceivedInstruction.outputInstructionId(driver);
			String outputInsId = outputInsElem.getText();
			System.out.println(outputInsId);
            
			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataList1 = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList1.size();
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			String[] entryStatusArr1 = {"01", "01","01", "01"};
			for(int i=0;i<posCount;i++) {
				String entStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				Assert.assertEquals(entStatus, entryStatusArr1[i]);
				((ExtentTest) test).log(LogStatus.PASS, "Two Leg of Posting Status : " +entStatus);
				System.out.println(entStatus);
			}
			
			//Incoming Payment- AWH response				
			// Fetch Payment File Name 
			String incomAWHFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_statementFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomAWHFile);
			
			//Modifying AWH file with Message Reference
			SampleFileModifier.UpdateXML(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+incomAWHFile , outputInsId, "MessageReference");
		
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomAWHFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomAWHFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomAWHFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement AWHrecInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, AWHrecInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, AWHrecInsTab, 30).click();
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, incomAWHFile));
			System.out.println(incomAWHFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String AWHrec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, incomAWHFile).getText();
			System.out.println(AWHrec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(AWHrec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + AWHrec_fileStatus);
			
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
	
			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtId1 = ReceivedInstruction.getPaymentId(driver); 
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId1, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId1, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderr = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);			
			
			//Verify the payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			BrowserResolution.scrollDown(driver);
				      
			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount1 = accountPostingDataList.size();
			System.out.println(posCount1);
			Assert.assertEquals(posCount1, 4);
			String[] accTypeArr = {"DDA", "SUS", "SUS", "NOS"};
			String[] entryStatusArr = {"03", "03", "01", "01"};
			for(int i=0;i<posCount1;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String entStatus = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				Assert.assertEquals(accTypeVal, accTypeArr[i]);
				Assert.assertEquals(entStatus, entryStatusArr[i]);
				(( ExtentTest) test).log(LogStatus.PASS, "Two leg of Posting Status : " +entStatus);
				System.out.println(entStatus);
			}

			//Incoming Payment- Reject				
			// Fetch Payment File Name 
			String incomPaymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomPaymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+incomPaymentFile, "inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+ sampleDir + incomPaymentFile,"payment");
						
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+incomPaymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTabIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabIn, 30).click();
			
			WaitLibrary.waitForAngular(driver);			
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn =  ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
	
			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdIn = ReceivedInstruction.getPaymentId(driver); 
			String pmtIdTxtIn = WaitLibrary.waitForElementToBeVisible(driver, pmtIdIn, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtIdTxtIn);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdIn, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxtIn);
			
			BrowserResolution.scrollDown(driver);
			//Verify the payment status
			WebElement pmtStatusidIn = ReceivedInstruction.getStatus(driver);
			String pmtStatusIn = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusidIn, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatusIn);
			Assert.assertEquals(pmtStatusIn, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatusIn);
	
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
