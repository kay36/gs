package com.gs.tc.ICCM.Reject_Return;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.objectRepo.RecInspage;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC001_Outbound_Return_SWIFT_MT103 extends LoginLogout_ICCM{

	@Test
	
	public void executeTC01() throws Exception {
		try {
			System.out.println("TC01");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"Return_Reject",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET +"\\"+sampleDir+paymentFile, "outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_REJ_RET + "\\" + sampleDir + paymentFile,"payment");

			//Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_REJ_RET_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}

			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			// Verify Received instruction status
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String payInsId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+payInsId);
						
			log.info("Click on instruction id"); 
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			WebElement pmtStatusElem = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusElem, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			//get Orig Payment Reference
			WebElement origPayRefElem = ReceivedInstruction.originalPaymentReference(driver, 0, "OriginalPaymentReference");
			String origPayRef = origPayRefElem.getText();
			System.out.println(origPayRef);
			
			// Click on payment id
			log.info("Get Payment id"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdid, Constants.avg_explicit).click();
			
			Thread.sleep(2000);
			
			//Select the SWIFT Payment Action
			log.info("get mop");
			WebElement MOP = ReceivedInstruction.paymentMethod(driver);
			WaitLibrary.waitForAngular(driver);
			String MoP = WaitLibrary.waitForElementToBeVisible(driver, MOP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MoP);
			
			String methodOfPay = CommonMethods.filterPayMethodDiv(MoP);
			Assert.assertEquals(methodOfPay, "SWIFT_IN");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPay);
						
			//Click on payment action button
			WebElement actionButton = ReceivedInstruction.actionBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, actionButton, Constants.avg_explicit).click();

			//Select the SWIFT Payment Action
			log.info("Select the SWIFT Payment Action");
			WebElement paymentAction = ReceivedInstruction.swiftPaymentAction(driver);
			WaitLibrary.waitForElementToBeClickable(driver, paymentAction, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			//enter the search option
			log.info("enter search option");
			ReceivedStatement.accountDataField(driver).sendKeys("RETURN PAYMENT",Keys.ENTER);
			
			// Click Field Causing REJT/RETN
			log.info("Click on Field Causing REJT/RETN");
			WebElement fieldCausingRejRet = ReceivedInstruction.retrejSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, fieldCausingRejRet, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Select Field Causing REJT/RETN from drop down");
			WebElement selectingField = ReceivedInstruction.retrejOptions(driver);
			WaitLibrary.waitForElementToBeClickable(driver, selectingField, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			// Click Reason code
//			WebElement ReasonCode = ReceivedInstruction.ReasonSelect2(driver);
//			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			executor.executeScript("arguments[0].click();", ReasonCode);
			
			log.info("Click on Reason code");
			WebElement reasonCode = ReceivedInstruction.ReasonSelect2(driver);
			WaitLibrary.waitForElementToBeClickable(driver, reasonCode, Constants.avg_explicit).click();		
			Thread.sleep(Constants.short_sleep);	
			
			//Search reason code
			Thread.sleep(Constants.short_sleep);
			String SearchData = "AC01-Format of the account number specified is not correct.";
			WebElement searchReasonCode = ReceivedInstruction.SearchReasonCode(driver);
			searchReasonCode.sendKeys(SearchData,Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			//click on submit
			log.info("Click on submit");
			WebElement submitPaymentAction = ReceivedStatement.SubmitForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submitPaymentAction, Constants.avg_explicit).click();
			log.info("submitted");
			
			//Send request for approval
			CommonMethods.approveStatement(driver, pmtId);
			WaitLibrary.waitForAngular(driver);
			
			ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			Thread.sleep(3000);
			
			//Click on Linked Message Tab	
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			
			//Verify linked message function and Direction and Click Linked Message id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Get Linked Message Id");
			List<WebElement> linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			int linkMsgscount = linkedMsgsDataList.size();
			log.info("Click on Linked Message Id");
			for(int l=0; l<linkMsgscount; l++) {
				String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText(); 
				if(linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, l, "LinkedMsgID");
					WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
					break;
				}else if (l==linkMsgscount-1 && !linkMsgFun.equals("Credit Transfer - Return of Funds")) {
					throw new Exception("Credit Transfer - Return of Funds not available in Linked Messages");
				}
			}
			
			ReceivedStatement.reconRefreshIcon(driver).click();
			Thread.sleep(5000);
			ReceivedStatement.reconRefreshIcon(driver).click();
			Thread.sleep(5000);
			ReceivedStatement.reconRefreshIcon(driver).click();
			//Verify the payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);

			//Get method of payment
			log.info("get mop");
			WebElement MOfP = ReceivedInstruction.paymentMethod(driver);
			String MofP = WaitLibrary.waitForElementToBeVisible(driver, MOfP, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(MofP);
			
			String methodOfPayment = CommonMethods.filterPayMethodDiv(MofP);
			Assert.assertEquals(methodOfPayment, "SWIFT_OUT");
			((ExtentTest) test).log(LogStatus.PASS, "Method Of Payment :" + methodOfPayment);
			
			//get original msg sub function
			log.info("get original msg sub function");
			WebElement origMsgSubFun = ReceivedInstruction.originalMsgfn(driver);
			String origMsgSubFunction = WaitLibrary.waitForElementToBeVisible(driver, origMsgSubFun, Constants.avg_explicit).getText();
			WaitLibrary.waitForAngular(driver);
			System.out.println(origMsgSubFunction);
			
			String origMsg = CommonMethods.orimsgDiv(origMsgSubFunction);
			Assert.assertEquals(origMsg, "ReturnOutbound");
			((ExtentTest) test).log(LogStatus.PASS, "Original Payment Reference :" + origMsg);
			
			BrowserResolution.scrollDown(driver);
			
			Thread.sleep(1000);	
			WebElement maskBtnStatus = RecInspage.maskButton(driver);
			Thread.sleep(1000);
			
			String checkMaskBtn = WaitLibrary.waitForElementToBeVisible(driver, maskBtnStatus, 60).getText();
			System.out.println("Button status :" +checkMaskBtn);
			if(checkMaskBtn.equals("Unmask")) {
				Utils.clickByJs(maskBtnStatus);
			}
			Thread.sleep(2000);
			
			//Verify Account posting with reversal of two legs of posting
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with reversal of two legs of posting");
			List<WebElement> accountPostingDataListOut = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCountOut = accountPostingDataListOut.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			System.out.println(posCountOut);
			Assert.assertEquals(posCountOut, 4);
			String[] accTypeArrOut = {"DDA", "SUS", "SUS", "NOS"};
			for(int i=0;i<posCountOut;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArrOut[i]);
			}

			String getAccountIdDr1 = ReceivedInstruction.accountPostingData(driver, 0, "AccountID").getText();
			String getAccountIdCr1 = ReceivedInstruction.accountPostingData(driver, 1, "AccountID").getText();
			String getAccountIdDr2 = ReceivedInstruction.accountPostingData(driver, 2, "AccountID").getText();
			String getAccountIdCr2 = ReceivedInstruction.accountPostingData(driver, 3, "AccountID").getText();
			System.out.println(getAccountIdDr1+" "+getAccountIdCr1+" "+getAccountIdDr2+" "+getAccountIdCr2);
			
			((ExtentTest) test).log(LogStatus.PASS, "Verified with reversal of two legs of posting");
			
			//Navigate to external communication tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					Thread.sleep(Constants.short_sleep);
					String MREFVal = CommonMethods.readMREFDownloadFile();
					Assert.assertEquals(MREFVal, origPayRef);
					((ExtentTest) test).log(LogStatus.PASS, "External Communication verified : " + getDirection + " Output Ins Id : " + MREFVal);
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));

			// Go to System Interaction and verify Finaclerequest
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int finCount = 0 ;
				for (int j=0; j< sysIntDataCount; j++) {
					String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
					
					String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
					
					if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					if(finCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
								
								if(insType.equals("WIRE_OUT_CLI")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_OUT_CLI");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "1st Finacle Posting request Instruction Type not matched"+insType);
									throw new Exception("1st Finacle Posting request Instruction Type not matched");
								}
								
								if(drAccount.equals("FIN"+getAccountIdDr1)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount, "FIN"+getAccountIdDr1);
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Debit Account : " + drAccount + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL, "1st Finacle Posting request Debit Account not matched"+drAccount);
									throw new Exception("1st Finacle Posting request Debit Account not matched");
								}
								if(crAccount.equals("EXT"+getAccountIdCr1)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "EXT"+getAccountIdCr1);
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Credit Account : " + crAccount + " verified");
								
								}
								else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account Credit Account not matched"+ crAccount);
									throw new Exception("1st Finacle Account Credit Account not matched");
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(finCount == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								String drAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("debitAccount").getAsString();
								String crAccount = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("creditAccount").getAsString();
								
								if(insType.equals("WIRE_OUT_NOS")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "WIRE_OUT_NOS");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
									throw new Exception("2nd Finacle Account instruction Type not matched");
								}
								
								if(drAccount.contains("EXT"+getAccountIdDr2)) {
									System.out.println(drAccount);
									Assert.assertEquals(drAccount,"EXT"+getAccountIdDr2);
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Debit Account : " + drAccount + " verified");
									
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Debit Account not matched"+ drAccount);
									throw new Exception("2nd Finacle Account Debit Account not matched");
								}
								if(crAccount.contains("EXT"+getAccountIdCr2)) {
									System.out.println(crAccount);
									Assert.assertEquals(crAccount, "EXT"+getAccountIdCr2);
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Credit Account : " + crAccount + " verified");
								
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Credit Account not matched"+ crAccount);
									throw new Exception("2nd Finacle Account Credit Account not matched");
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				 
				if(j==sysIntDataCount-1) {
					if(finCount !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
				}
			}
			
			//Navigate to linked message	
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));

			//Click Linked Message id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Click on Linked Message Id");
			WebElement linkedMessageIdIn = ReceivedInstruction.linkedMessageId(driver, 0, "LinkedMsgID");
			WaitLibrary.waitForElementToBeClickable(driver, linkedMessageIdIn, Constants.avg_explicit).click();
			
			WaitLibrary.waitForAngular(driver);
			
			//verify status
			WebElement statusMatch = ReceivedInstruction.getStatus(driver);
			String StatusMatch = WaitLibrary.waitForElementToBeVisible(driver, statusMatch, Constants.avg_explicit).getText();			
			System.out.println("Verify Status::::"+StatusMatch);
			Assert.assertEquals(StatusMatch, "RETURNED");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + StatusMatch);
			
			BrowserResolution.scrollDown(driver);
			
			//go to system interaction tab
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);

			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int becNotfCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				String[] becNotificationEvent = {"BE1CI"};
				if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becNotfCount++;
					System.out.println(becNotfCount);
					if(becNotfCount == 4) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						
						String objDataString = U.xmlToJson(objDataXmlString);
						
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1");
							String eventID = temp.get("EventID").getAsString();
							System.out.println(eventID);
							if(eventID.equals("BE1CI")) {
								log.info("BECNOTIFICATION status : "+eventID);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION Event ID is :: "+eventID);
							}else {
								throw new Exception("BECNOTIFICATION event ID not matched");
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
			}
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
