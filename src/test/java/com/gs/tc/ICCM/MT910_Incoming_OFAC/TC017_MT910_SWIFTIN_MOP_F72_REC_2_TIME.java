package com.gs.tc.ICCM.MT910_Incoming_OFAC;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC017_MT910_SWIFTIN_MOP_F72_REC_2_TIME extends LoginLogout_ICCM{
	
	@Test

	public void executeTC017() throws Exception {
		try {
			
			System.out.println("TC017_MT910_SWIFTIN_MOP_F72_REC_2_TIME");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT910_Incoming_OFAC" ,TestCaseName, Constants.Col_incomingPayment_MT910);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 + "\\" + sampleDir + paymentFile,"inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 +"\\"+ sampleDir + paymentFile,"payment");
			
			// Get F72 value from Orig Sample file
			String[] fieldDataArr = SampleFileModifier.getFieldData(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 +"\\"+ sampleDir + paymentFile, "MT910");
			String fieldData = String.join("", fieldDataArr);
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_MT910_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_MT910_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			System.out.println(paymentFile);
			WebElement transportStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile);
			String rec_fileStatus = WaitLibrary.waitForElementToBeVisible(driver, transportStatus, Constants.avg_explicit).getText();
			BrowserResolution.scrollToElement(driver, transportStatus);
			js.executeScript("window.scrollBy(0,-120)");

			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "REPAIR");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
						
			//click on repair button
			log.info("Click on repair button");
			WebElement repairButton = ReceivedInstruction.repairBtn(driver);
			BrowserResolution.scrollToElement(driver, repairButton);
			js.executeScript("arguments[0].click();", ReceivedInstruction.repairBtn(driver));
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			((ExtentTest) test).log(LogStatus.INFO, "Sent for repair");
			
			//click on creditor
			log.info("Click on Creditor");
			WebElement creditor = ReceivedInstruction.Creditor(driver);
			BrowserResolution.scrollToElement(driver, creditor);
			js.executeScript("arguments[0].click();", creditor);
			System.out.println("Creditor clicked");
			
			//click on creditor text
			log.info("Enter Account Number");
			WebElement creditorText = ReceivedInstruction.creditorTxt(driver);
			js.executeScript("arguments[0].click();", creditorText);
			Thread.sleep(Constants.short_sleep);
			
			//get data from excel
			log.info("get Account from excel");
			String accNumber = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT910_Incoming_OFAC",TestCaseName,Constants.Col_accountNumber);
			System.out.println(accNumber);
			ReceivedInstruction.creditorTxt(driver).sendKeys(accNumber);
			log.info("Account number "+accNumber+" entered");
			((ExtentTest) test).log(LogStatus.INFO, "Account number "+accNumber+" entered");
			
			//click on submit
			WebElement submit = ReceivedInstruction.Submit(driver);
			BrowserResolution.scrollToElement(driver, submit);
			js.executeScript("arguments[0].click();", submit);
			log.info("Submitted for Approval");
			((ExtentTest) test).log(LogStatus.INFO, "Submitted for Approval");

			//Sent request for approval
			CommonMethods.approveStatement(driver, paymentID);
			
			//************************After Approval****************************//
			log.info("After Approval");
			
			//Navigate to Received Instructions
			Thread.sleep(Constants.tooshort_sleep);
			log.info("Navigate to Received Instructions");
			WebElement recInsTabInIn = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTabInIn);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTabInIn, Constants.avg_explicit).click();

			log.info("Click on List View");
			WebElement listViewIn = ReceivedInstruction.listView(driver);
			String listViewClassIn = WaitLibrary.waitForElementToBeVisible(driver, listViewIn, Constants.avg_explicit).getAttribute("class");

			if (listViewClassIn.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listViewIn, Constants.avg_explicit).click();
			}

			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
			searchInstructionIn.clear();
			searchInstructionIn.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);

			log.info("Get Payment id & Verify Status");
			WebElement pmtId = ReceivedInstruction.getPaymentId(driver);
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeaderrIn = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);
			
			//Verify payment status
			WebElement paymentStatusElement = ReceivedInstruction.getStatus(driver);
			String paymentStatus = WaitLibrary.waitForElementToBeVisible(driver, paymentStatusElement, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+paymentStatus);
			Assert.assertEquals(paymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + paymentStatus);
			
			//Navigate to Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			
			String[] accTypeArrIn = {"NOS", "SUS", "SUS", "DDA"};
			String[] drcr = {"D", "C", "D", "C"};
			
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String getAccountIdDr = ReceivedInstruction.accountPostingData(driver, i, "DrCrInd").getText();
				if(i<posCount-2) {
					Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
					Assert.assertEquals(getAccountIdDr, drcr[i]);
					((ExtentTest) test).log(LogStatus.PASS, "1st Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);				
					}
					if(i>=posCount-2) {
						Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
						Assert.assertEquals(getAccountIdDr, drcr[i]);
						((ExtentTest) test).log(LogStatus.PASS, "2nd Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);
					}
			}
			
			//Navigate to external communication Tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			//Download the out file and make verifications
			log.info("Verify Direction");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			System.out.println(extDataCount);
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "Direction").getText();
				if (getDirection.equals("OUT")) {
					System.out.println(j);
					Assert.assertEquals(getDirection, "OUT");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn1(driver, j).click();
					directionCount++;
					break;
				}
			}
			if (directionCount != 1) {
				System.out.println("Direction not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}

			//get the downloaded file
			Thread.sleep(Constants.tooshort_sleep);
			String obtainedFile = CommonMethods.getDownloadedFile().toString();
			
			//Get F72 value of download file
			String[] obtainedFieldDataArr = SampleFileModifier.getFieldData(obtainedFile, "MT910");
			
			System.out.println(fieldData);
			String obtainedFieldData = String.join("", obtainedFieldDataArr);
			
			System.out.println(obtainedFieldData);
			// Comparing Original F72 value with Obtained Value
			String[] obtainedValues = null;
			if (obtainedFieldData.contains("NOCD")) {
				obtainedValues = obtainedFieldData.split("NOCD");
				if(!obtainedValues[0].isEmpty() && obtainedValues[0].equals("INSCITIUS33XXX")) {
					((ExtentTest) test).log(LogStatus.PASS, "INS/CITIUS33XXX and NOCD values are mapped with F72 out File ");
					if(obtainedValues.length>1 && fieldData.startsWith(obtainedValues[1])) {
						((ExtentTest) test).log(LogStatus.PASS, "Out file F72 is mapped from the original file");
					}
					else {
						((ExtentTest) test).log(LogStatus.FAIL, "Out file F72 is not mapped from the original file");
					}
				}
				else {
					if(obtainedValues[0].isEmpty()) {
						((ExtentTest) test).log(LogStatus.FAIL, "INS/CITIUS33XXX value not mapped with F72 out File ");
					}else {
						((ExtentTest) test).log(LogStatus.FAIL, "Expected INS/CITIUS33XXX value not mapped with F72 out File , Found : " + obtainedValues[0]);
					}
				}
			}
			else {
				((ExtentTest) test).log(LogStatus.FAIL, "NOCD value not mapped with F72 out File ");
			}
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int ofacCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					ofacCount++;
					if(ofacCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Data
						Thread.sleep(Constants.tooshort_sleep);
						
						WebElement OFACData = ReceivedInstruction.OFAC(driver, j);
						String getOFACData = OFACData.getText();
						System.out.println(getOFACData);
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						
						// Verifying MBBK Info
						String MBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "MBBKINFO");
						if(MBBKinfo.startsWith("NOCD")) {
							((ExtentTest) test).log(LogStatus.PASS, "NOCD value mapped with MBBK Info OFAC Request");
							if(MBBKinfo.replace("NOCD", "").equalsIgnoreCase(fieldData)) {
								((ExtentTest) test).log(LogStatus.PASS, "MBBK Info value matched with original F72 value");
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "MBBK Info value not matched with original F72 value");
							}
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "NOCD value not mapped with MBBK Info OFAC Request");
						}
						
						// Verifying SBBK Info
						String SBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "SBBKINFO");
						if(SBBKinfo.equalsIgnoreCase(fieldData)) {
							((ExtentTest) test).log(LogStatus.PASS, "SBBK Info value matched with original F72 value");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "SBBK Info value not matched with original F72 value");
						}
					}
				}
			}
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

