package com.gs.tc.ICCM.MT910_Incoming_OFAC;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC004_MT910_Funding_F72_BNF_BOOK_funding extends LoginLogout_ICCM{
	
	@Test

	public void executeTC004() throws Exception {
		try {
			
			System.out.println("TC004_MT910_Funding_F72_BNF_BOOK_funding");
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Incoming Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT910_Incoming_OFAC" ,TestCaseName, Constants.Col_incomingPayment_MT910);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 + "\\" + sampleDir + paymentFile,"inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 +"\\"+ sampleDir + paymentFile,"payment");

			// Get F72 value from Orig Sample file
			String[] fieldDataArr = SampleFileModifier.getFieldData(System.getProperty("user.dir") + "\\" + Constants.gs_MT910 +"\\"+ sampleDir + paymentFile, "MT910");
			String fieldData = String.join("", fieldDataArr);

			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_MT910_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_MT910_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			System.out.println(paymentFile);
			WebElement transportStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile);
			String rec_fileStatus = WaitLibrary.waitForElementToBeVisible(driver, transportStatus, Constants.avg_explicit).getText();
			BrowserResolution.scrollToElement(driver, transportStatus);
			js.executeScript("window.scrollBy(0,-120)");

			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			BrowserResolution.scrollDown(driver);

			//Navigate to Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with two legs of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 4);
			
			String[] accTypeArrIn = {"NOS", "SUS", "SUS", "SUS"};
			String[] drcr = {"D", "C", "D", "C"};
			
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				String getAccountIdDr = ReceivedInstruction.accountPostingData(driver, i, "DrCrInd").getText();
				if(i<posCount-2) {
					Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
					Assert.assertEquals(getAccountIdDr, drcr[i]);
					((ExtentTest) test).log(LogStatus.PASS, "1st Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);				
					}
					if(i>=posCount-2) {
						Assert.assertEquals(accTypeVal, accTypeArrIn[i]);
						Assert.assertEquals(getAccountIdDr, drcr[i]);
						((ExtentTest) test).log(LogStatus.PASS, "2nd Leg of posting verified with - Account type :: "+accTypeVal+" and C/D account is::  "+getAccountIdDr);
					}
			}
		
			//Navigate to System Interaction and Verify OFAC
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			BrowserResolution.scrollDown(driver);
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataListIn = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCountIn = sysIntDataListIn.size();
			System.out.println(sysIntDataCountIn);
			int ofacCount = 0;
			for (int j=0; j< sysIntDataCountIn; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				
				if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					ofacCount++;
					if(ofacCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Data
						Thread.sleep(Constants.tooshort_sleep);
						
						WebElement OFACData = ReceivedInstruction.OFAC(driver, j);
						String getOFACData = OFACData.getText();
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						
						// Verifying MBBK Info
						String MBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "MBBKINFO");
						if(MBBKinfo.startsWith("NOCD")) {
							((ExtentTest) test).log(LogStatus.PASS, "NOCD value mapped with MBBK Info OFAC Request");
							if(MBBKinfo.replace("NOCD", "").equalsIgnoreCase(fieldData)) {
								((ExtentTest) test).log(LogStatus.PASS, "MBBK Info value matched with original F72 value");
							}else {
								((ExtentTest) test).log(LogStatus.FAIL, "MBBK Info value not matched with original F72 value");
							}
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "NOCD value not mapped with MBBK Info OFAC Request");
						}
						
						// Verifying SBBK Info
						String SBBKinfo = CommonMethods.getOFACDataVal(getOFACData, "SBBKINFO");
						if(SBBKinfo.equalsIgnoreCase(fieldData)) {
							((ExtentTest) test).log(LogStatus.PASS, "SBBK Info value matched with original F72 value");
						}else {
							((ExtentTest) test).log(LogStatus.FAIL, "SBBK Info value not matched with original F72 value");
						}
					}
				}
			}

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class

