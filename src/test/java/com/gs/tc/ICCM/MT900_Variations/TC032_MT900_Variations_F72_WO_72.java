package com.gs.tc.ICCM.MT900_Variations;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC032_MT900_Variations_F72_WO_72 extends LoginLogout_ICCM{
	@Test

	public void executeTC032() throws Exception {
		try {
			
			Log.startTestCase(log, TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Outbound Payment File Name
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT900_Variations",TestCaseName, Constants.Col_outboundPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 + "\\" + sampleDir + paymentFile,"outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 +"\\"+ sampleDir + paymentFile,"payment");
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_MT900_Dir + "\\" + sampleDir + paymentFile);
			log.info("Destination File is  :" + Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_MT900_Dir + "\\" + sampleDir + paymentFile,Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass = WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");

			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}
			
			// Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");

			String rec_fileStatus = ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			// Get and click Instruction ID
			WebElement instdid = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			String insId = WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::" + insId);
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			// Click on Payment ID
			WebElement pmtIdElem = ReceivedInstruction.originalPaymentReference(driver, 0, "PaymentID");
			String paymentID = WaitLibrary.waitForElementToBeVisible(driver, pmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::" + paymentID);
			WaitLibrary.waitForElementToBeClickable(driver, pmtIdElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, paymentID);
			
			// Verifying Payment status
			WebElement PaymentStatusElement = ReceivedInstruction.getStatus(driver);
			String PaymentStatus = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatus);
			Assert.assertEquals(PaymentStatus, "COMPLETED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + PaymentStatus);
			
			log.info("get output instruction ID");
			WebElement outputInsElem = ReceivedInstruction.outputInstructionId(driver);
			String outputInsId = outputInsElem.getText();
			System.out.println(outputInsId);
			((ExtentTest) test).log(LogStatus.INFO, "Output Instruction Id : " + outputInsId);
			
			// Fetch AMH Response File Name
			String amhResponseFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT900_Variations",	TestCaseName, Constants.Col_amhResponse);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + amhResponseFile);
			
			// Modifying AMH file with Message Reference
			SampleFileModifier.UpdateXML(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 + "\\" + sampleDir + amhResponseFile, outputInsId, "MessageReference");
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_MT900_Dir + "\\" + sampleDir + amhResponseFile);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_MT900_Dir + "\\" + sampleDir + amhResponseFile,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			
			// Reverifying payment status
			String PaymentStatusC1 = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatusC1);
			Assert.assertEquals(PaymentStatusC1, "DELIVERED");
			((ExtentTest) test).log(LogStatus.PASS, "Original Payment Status : " + PaymentStatusC1);
			
			// Fetch MT900 File Name
			String mt900File = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT900_Variations",	TestCaseName, Constants.Col_MT900);
			((ExtentTest) test).log(LogStatus.INFO, "File name is : " + mt900File);
			
			// updating output instructions id in MREF
			SampleFileModifier.updateStatementFile(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 + "\\" + sampleDir + mt900File, outputInsId, "f21");
			Thread.sleep(Constants.tooshort_sleep);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 + "\\" + sampleDir + mt900File,"outbound", "202");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_MT900 +"\\"+ sampleDir + mt900File,"payment");
			
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_MT900_Dir + "\\" + sampleDir + mt900File);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_MT900_Dir + "\\" + sampleDir + mt900File,Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			
			// Reverifying payment status
			String PaymentStatusC2 = WaitLibrary.waitForElementToBeVisible(driver, PaymentStatusElement, Constants.avg_explicit).getText();
			System.out.println("Payment Status:::: "+PaymentStatusC2);
			Assert.assertEquals(PaymentStatusC2, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Original Payment Status : " + PaymentStatusC2);
			BrowserResolution.scrollDown(driver);
			
			// Navigate to external communication tab
			log.info("Navigate to External Communication Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getExtCommunicationTab(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Message Type");
			List<WebElement> extDataLsit = ReceivedInstruction.extCommunicationList(driver);
			int extDataCount = extDataLsit.size();
			int directionCount = 0;
			for (int j = 0; j < extDataCount; j++) {
				String getDirection = ReceivedInstruction.externalCommunication(driver, j, "MessageType").getText();
				if (getDirection.equals("MT900")) {
					Assert.assertEquals(getDirection, "MT900");
					// Download the file
					ReceivedInstruction.externalCommDwnldBtn(driver, j).click();
					directionCount++;
					break;
				}
			}
			
			if (directionCount != 1) {
				System.out.println("Message Type not matched");
				((ExtentTest) test).log(LogStatus.FAIL, "External Communication not verified");
			}
			
			//Get download File name
			Thread.sleep(Constants.tooshort_sleep);
			String obtainedFile = CommonMethods.getDownloadedFile().toString();
			
			//Get F72 value of download file
			String[] obtainedFieldData = SampleFileModifier.getFieldData(obtainedFile, "MT900");
			
			// No value in sample file, checking 1st line of obtained
			if(obtainedFieldData[0].equals("INSCITIUS33XXX")) {
				((ExtentTest) test).log(LogStatus.PASS, "Line no 1 : of F72 attached with : /INS/CITIUS33XXX");
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Line no 1 : of F72 not matched with : /INS/CITIUS33XXX");
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());	
		}
		
// End of Try block
		
		catch (AssertionError ae) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror = ae.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Assertion Error</b>" + "<br>" + ae.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		} 
		catch (Exception et) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror = et.getMessage();
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>" + "<b>Exception</b>" + "<br>" + et.getMessage() + LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath, TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}

// End of catch	block
		
	}	// End of Test Method
	
} 		// End of Class
