package com.gs.tc.ICCM.MT940Statement;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ManualMatching;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FileUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.Utils;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC085_Manual_Matching extends LoginLogout_ICCM{

	@Test
	
	public void executeTC085() throws Exception {
		try {
			System.out.println("TC085");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_outgoingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			}

			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			// Verify Received instruction status
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String payInsId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+payInsId);
						
			log.info("Click on instruction id"); 
			WaitLibrary.waitForElementToBeClickable(driver, instdid, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			BrowserResolution.scrollDown(driver);  
			
			// Get payment id and verify status
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			
			WebElement pmtStatusElem = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusElem, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
		
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
	
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String stmtInsId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Statement Ins id:::"+stmtInsId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, stmtInsId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	     		searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(stmtInsId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
			    
				CommonMethods.clickStatementWithFileName(driver, stmtFile);
				WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
		
			log.info("Verifying Statement Matching Status");
			String statusText = ReceivedStatement.statementDetailsData(driver, 0, "MatchingStatus").getText();
			String verifiedText = CommonMethods.statementDetailsDataStatus(driver, statusText, "UNMATCHED", "MatchingStatus");
			System.out.println(verifiedText);
			
			if(verifiedText.equals("UNMATCHED")) {
				((ExtentTest) test).log(LogStatus.PASS, "Status: " + verifiedText);
			}else {
				((ExtentTest) test).log(LogStatus.FAIL, "Status: " + verifiedText);
				throw new Exception("Statament Matching Status not matched");
			}
			
//=========================================MANUAL_MATCH======================================
			((ExtentTest) test).log(LogStatus.INFO, "Manually matching Payment and statement");
			
			// Copying payment id to Clip board
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Clipboard clipboard = toolkit.getSystemClipboard();
			StringSelection copiedPmt = new StringSelection(pmtId);
			clipboard.setContents(copiedPmt, null);
			
			log.info("Navigate to Manual Match");
			WebElement matchingModule = ManualMatching.matchingModule(driver);
			BrowserResolution.scrollToElement(driver, matchingModule);
			WaitLibrary.waitForElementToBeClickable(driver, matchingModule, Constants.avg_explicit).click();
			
			log.info("Clicking on Manual Matching");
			WebElement manualMatching = ManualMatching.manualMatching(driver);
			WaitLibrary.waitForElementToBeClickable(driver, manualMatching, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			// Click Payment-Statement as Matching Type
			log.info("Click on Matching Type Select box");
			WebElement matchingType = ManualMatching.matchingType(driver);
			WaitLibrary.waitForElementToBeClickable(driver, matchingType, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Select matching type from drop down");
			WebElement matchingTypeOptions = ManualMatching.matchingTypeOptions(driver);
			WaitLibrary.waitForElementToBeClickable(driver, matchingTypeOptions, Constants.avg_explicit).click();
			
			Thread.sleep(Constants.tooshort_sleep);
			
			// Select a Match Parameter value
			log.info("Click on Match select box");
			WebElement matchSelectElem = ManualMatching.matchSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, matchSelectElem, Constants.avg_explicit).click();
			
			log.info("Select match option from drop down");
			WebElement matchOptions = ManualMatching.matchOptions(driver);
			WaitLibrary.waitForElementToBeClickable(driver, matchOptions, Constants.avg_explicit).click();
			Thread.sleep(Constants.tooshort_sleep);
			
			// Select a With Parameter value
			log.info("Click on Match select box");
			WebElement withSelectElem = ManualMatching.withSelect(driver);
			WaitLibrary.waitForElementToBeClickable(driver, withSelectElem, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Select match option from drop down");
			WebElement withOptions = ManualMatching.withOptions(driver);
			WaitLibrary.waitForElementToBeClickable(driver, withOptions, Constants.avg_explicit).click();
			Thread.sleep(Constants.tooshort_sleep);
			
			// Click Submit Button
			log.info("Click on Submit");
			WebElement submit = ManualMatching.submitMatchingType(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submit, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			Actions actions = new Actions(driver);
			
			// Search Match payment ID
			log.info("Search Match using payment ID");
			System.out.println(pmtId);
			ManualMatching.matchSearchBox(driver).click();
			actions.keyDown( Keys.CONTROL ).sendKeys( "v" ).keyUp( Keys.CONTROL ).build().perform();
			
			WebElement matchsearch = ManualMatching.matchSearchBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, matchsearch, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Click anywhere on the payments");
			WebElement clickMatchData = ManualMatching.clickPmts(driver, 0 ,"PaymentID");
			WaitLibrary.waitForElementToBeClickable(driver, clickMatchData, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			BrowserResolution.scrollDown(driver);
			
			// Search With payment ID
			log.info("Search With using statement ID");
			System.out.println(stmtInsId);
			ManualMatching.withSearchBox(driver).sendKeys(stmtInsId);
			Thread.sleep(Constants.tooshort_sleep);
			
			WebElement withSearchBtn = ManualMatching.withSearchBtn(driver);
			WaitLibrary.waitForElementToBeClickable(driver, withSearchBtn, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			// Click Match checkbox
			js.executeScript("arguments[0].click();",  ManualMatching.matchCheckBox(driver));
			
			// Click With checkbox
			js.executeScript("arguments[0].click();",  ManualMatching.withCheckBox(driver));
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollUp(driver);
			
			//Click on force match
			WebElement ForceMatch = ManualMatching.forceMatch(driver);
			WaitLibrary.waitForElementToBeClickable(driver, ForceMatch, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			//Send request for approval
			CommonMethods.approveStatement(driver, pmtId);
			WaitLibrary.waitForAngular(driver);
			
//***************==============RECEIVED INSTRUCTIONS================*************************//
			log.info("Navigate to Received Instructions");
			WebElement payModule = SideBarMenu.paymentModule(driver);
			BrowserResolution.scrollToElement(driver, payModule);
			js.executeScript("arguments[0].click();", SideBarMenu.paymentModule(driver));
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
						
			Thread.sleep(Constants.tooshort_sleep);
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.sendKeys(payInsId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, paymentFile);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtId1 = ReceivedInstruction.getPaymentId(driver); 
			String pmtIdTxt = WaitLibrary.waitForElementToBeVisible(driver, pmtId1, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtIdTxt);
			WaitLibrary.waitForElementToBeClickable(driver, pmtId1, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtIdTxt);			
			BrowserResolution.scrollDown(driver);

//			//Account Posting Tab
//            log.info("Navigate to Account Posting");
//			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
//			WaitLibrary.waitForAngular(driver);	
			
			log.info("Verifying Statement Matching Status");
			String stmtMatchingStatus = ReceivedInstruction.accountPostingData(driver, 0, "StatementMatchingStatus").getText();
			System.out.println(stmtMatchingStatus);
			Assert.assertEquals(stmtMatchingStatus, "MANUALLY_MATCHED");
			((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status : " + stmtMatchingStatus);
			
			//Click on Linked Message		
			BrowserResolution.scrollDown(driver);
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			
			// Verify Bank statement and click Linked Msg id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Get Linked Message Id");
			List<WebElement> linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			int linkMsgscount = linkedMsgsDataList.size();
			log.info("Click on Linked Message Id");
			for(int l=0; l<linkMsgscount; l++) {
				String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText(); 
				if(linkMsgFun.equals("Bank Statement")) {
					WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, l, "LinkedMsgID");
					WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
					break;
				}else if (l==linkMsgscount-1 && !linkMsgFun.equals("Bank Statement")) {
					throw new Exception("Bank Statement not available in Linked Messages");
				}
			}
			
			log.info("Verifying Statement Page with Transport name");
    		WebElement stmtTransportName = ReceivedStatement.verifyStatementnPage(driver, stmtFile);
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Statement Matching Status");
			String stmtMatching = ReceivedStatement.statementDetailsData(driver, 0, "MatchingStatus").getText();
			System.out.println(stmtMatching);
			Assert.assertEquals(stmtMatching, "MANUALLY_MATCHED");
			((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status: " + stmtMatching);
			
			log.info("Click on Recon Payment Id");
			WebElement reconPayment = ReceivedStatement.statementDetailsData(driver, 0, "ReconPaymentMsgID");
			String reconPaymentId = WaitLibrary.waitForElementToBeVisible(driver, reconPayment, Constants.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, reconPayment, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Recon Payment Page with PaymentId");
			WebElement reconPaymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, reconPaymentId);			
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			//go to account posting tab and verify with two leg of posting
			log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			log.info("Verify Account posting with two leg of posting");
			List<WebElement> reconAccountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int reconPosCount = reconAccountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			System.out.println(reconPosCount);
			Assert.assertEquals(reconPosCount, 4);
			String[] reconAccTypeArr = {"NOS", "SUS", "SUS", "NOS"};
			for(int i=0; i<reconPosCount ;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				Assert.assertEquals(getAccountTyp, reconAccTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + reconPosCount);
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			
			// Goto System Interaction and verify BEC,OFAc and  Finacle
			log.info("Verifying System Interaction");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int ofacCount = 0;
			int becNotfCount = 0;
			int finCount = 0 ;
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				String[] becNotificationStatus = {"TOBEPROCESSED", "WAITING_FRAUDRESPONSE", "WAITING_OFACRESPONSE", "RECON_HOLD", "ACCEPTED"};
				if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					ofacCount++;
					if(ofacCount == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "OFAC interface verified");
					}
				}
				else if(invoPoint.equals("BECNOTIFICATION") && RelationshipTxt.equals("NOTIFICATION")) {
					becNotfCount++;
					if(becNotfCount < 6) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objDataXml = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objDataXml, Constants.avg_explicit);
						String objDataXmlString = objDataXml.getText();
						String objDataString = U.xmlToJson(objDataXmlString);
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							JsonObject temp = jsonObject.getAsJsonObject("VolPayHubAlertNotification1").getAsJsonObject("Contents").getAsJsonObject("PaymentControlDataNo");
							String status = temp.get("Status").getAsString();
							if(status.equals(becNotificationStatus[becNotfCount-1])) {
								log.info("BECNOTIFICATION status"+status);
								((ExtentTest) test).log(LogStatus.PASS, "BECNOTIFICATION status "+status);
							}else {
								throw new Exception("BECNOTIFICATION status not matched");
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					if(finCount == 1) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("BREAK_ADJUSTMENT")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "BREAK_ADJUSTMENT");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
									break;
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched"+ insType);
									throw new Exception("1st Finacle Account instruction Type not matched");
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(finCount == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("BREAK_ADJUSTMENT")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "BREAK_ADJUSTMENT");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									break;
								}else {
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
									throw new Exception("2nd Finacle Account instruction Type not matched");
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
				}
				 
				if(j==sysIntDataCount-1) {
					if(ofacCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"OFAC interface not found");
						throw new Exception("OFAC interface not found");
					}
					if(finCount !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
					if(becNotfCount !=5) {
						((ExtentTest) test).log(LogStatus.FAIL,"BECNOTIFICATION not found");
						throw new Exception("BECNOTIFICATION not found");
					}
				}
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
