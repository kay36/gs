package com.gs.tc.ICCM.MT940Statement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC038_Multipage_MT940 extends LoginLogout_ICCM{
	
	@Test
	
	public void executeTC038() throws Exception {
	try {
		System.out.println("TC038");
		Log.startTestCase(log,TestCaseName);
		
		// Fetch account Number 
		String accountNumber=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_accountNumber);
		System.out.println(accountNumber);		
		((ExtentTest) test).log(LogStatus.INFO, "Account number is :" + accountNumber);
		
		// Fetch sample Directory
		String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseName,Constants.Col_sampleDirectory);
		System.out.println(sampleDir);
		
		// Fetch Statement Files
		String stmtFiles=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, "MT940Statement",TestCaseName, Constants.Col_statementFile);
		System.out.println(stmtFiles);
		
		String[] stmtFile = stmtFiles.split("/");
		//Pass this value in the F28 field sample file update
		long unixTime = System.currentTimeMillis();
		String valueField=Long.toString(unixTime).substring(10);
		
		for (int s = 0; s < stmtFile.length; s++) {
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile[s], "statement", "940");
			Thread.sleep(2000);
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile[s], "statement");
			Thread.sleep(1000);
			
			// Updating F28 field in sample file
			int incrValue=s+1;
			SampleFileModifier.updateStatementF28field(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile[s], valueField, incrValue);
		}
		
		log.info("Navigate to Received Statement");
		Actions action = new Actions(driver);
		action.moveToElement(ReceivedStatement.statementModule(driver)).build().perform();	
		WebElement statementModule = ReceivedStatement.statementModule(driver);
		WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

		log.info("Clicking on Received Statements");
		WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
		WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
		WaitLibrary.waitForAngular(driver);
        Thread.sleep(Constants.short_sleep);
		
		log.info("Click on List View");
		WebElement listView = ReceivedInstruction.listView(driver);
		String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
					
		if (listViewClass.contains(Constants.listViewClassData)) {			
			WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
		} 				
		
		for (int i = 0; i < stmtFile.length; i++) {
			// Upload Payment File
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :" + Constants.gs_Mt940_Dir + "\\" + sampleDir + stmtFile[i]);
			log.info("Destination File is  :" + Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir + "\\" + sampleDir + stmtFile[i], Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir + "\\" + sampleDir + stmtFile[i]);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			ReceivedStatement.refreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollToElement(driver, ReceivedStatement.selectStatementByName(driver,stmtFile[i]));
			js.executeScript("window.scrollBy(0,-120)");
			String recStatementStatus = ReceivedStatement.getStatementStatus(driver, stmtFile[i]).getText();
			System.out.println(recStatementStatus);
			log.info("Verifying the status of uploaded file");
			if (i == stmtFile.length-1) {
				Assert.assertEquals(recStatementStatus, "INITIATE_PAGINATION");
			}else {
				Assert.assertEquals(recStatementStatus, "STATEMENT_PAGEWAIT");
			}
			BrowserResolution.scrollUp(driver);
		}
		
		CommonMethods.clickStatementWithFileName(driver, "ParentStmt_" +accountNumber);
		
		//Get Instruction ID
		String instrID = ReceivedStatement.getInstructionID(driver).getText();
		((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+instrID);
		
		//Verify Status of Uploaded Payment File			
		String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
		System.out.println(recStatementStatus);
		
		switch (recStatementStatus) {
		  case "DEBULKED":
		    System.out.println(recStatementStatus);
		    break;
		  case "STATEMENT_REVIVE":
			BrowserResolution.scrollDown(driver);
		    System.out.println(recStatementStatus);
		    ReceivedStatement.clickAcceptInstruction(driver).click();
		    System.out.println("clicked accept");
		    WaitLibrary.waitForAngular(driver);
		    CommonMethods.approveStatement(driver, instrID);
		    ReceivedStatement.refreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			CommonMethods.clickStatementWithFileName(driver, "ParentStmt_" +accountNumber);
		    break;
		  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
		    System.out.println(recStatementStatus);
		    break;
		}

		String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
		
		log.info("Verifying the status of uploaded file");
		Assert.assertEquals(recStmtStatus, "DEBULKED");
		((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
		
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
	}
	catch(AssertionError ae){
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
		assertionerror=ae.getMessage();			
		CaptureScreenshot.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
	}
	catch(Exception et){
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
		exceptionerror=et.getMessage();			
		CaptureScreenshot.captureScreenshot();
		((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
	}
	
}

}
