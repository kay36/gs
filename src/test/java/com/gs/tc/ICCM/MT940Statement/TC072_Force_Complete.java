package com.gs.tc.ICCM.MT940Statement;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC072_Force_Complete extends LoginLogout_ICCM{
	
	@Test
	
	public void executeTC072() throws Exception {
		try {
			System.out.println("TC072");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
									
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
	
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
			} 				

			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String insId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, insId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
				searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(insId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
	     		CommonMethods.clickStatementWithFileName(driver, stmtFile);
	     		WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStmtStatus);
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			
			BrowserResolution.scrollDown(driver);
		
			log.info("Click on Recon Payment Id");
			String reconPaymentId=null;
			try {
				WebElement reconPayment = ReceivedStatement.statementDetailsData(driver, 0, "ReconPaymentMsgID");
				reconPaymentId = WaitLibrary.waitForElementToBeVisible(driver, reconPayment, Constants.avg_explicit).getText();
				WaitLibrary.waitForElementToBeClickable(driver, reconPayment, Constants.avg_explicit).click();
			}catch (Exception e)
			{
				((ExtentTest) test).log(LogStatus.FAIL, "ReconPaymentMsgID not generated" );	
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with Payment ID");
			ReceivedInstruction.verifyPaymentPage(driver, reconPaymentId);
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			//Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with one leg of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			String[] accTypeArr = {"NOS", "SUS"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount);
			
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollUp(driver);
			//click on force complete
			log.info("click on force Complete");
			WebElement ForceComplete = ReceivedStatement.forceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, ForceComplete, Constants.avg_explicit).click();
			Thread.sleep(Constants.tooshort_sleep);
			
			//Select the force complete account data
			log.info("Select Account");
			WebElement SelectForceComplete = ReceivedStatement.selectForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, SelectForceComplete, Constants.avg_explicit).click();
			
			//get data from excel
			log.info("get Account from excel");
			String recAccNumber = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_accountNumber);
			System.out.println(recAccNumber);
			ReceivedStatement.accountDataField(driver).sendKeys(recAccNumber,Keys.ENTER);
			
			//click on submit
			log.info("Click on submit");
			WebElement submitForceComplete = ReceivedStatement.SubmitForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submitForceComplete, Constants.avg_explicit).click();
			log.info("submitted");
			
			//Send request for approval
			CommonMethods.approveStatement(driver, reconPaymentId);
			Thread.sleep(Constants.tooshort_sleep);
		    ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			
			//verify status
			WebElement reconStatusMatch = ReceivedInstruction.getStatus(driver);
			String reconStatus = WaitLibrary.waitForElementToBeVisible(driver, reconStatusMatch, Constants.avg_explicit).getText();			
			System.out.println("Recon Status::::"+reconStatus);
			Assert.assertEquals(reconStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + reconStatus);
			
			BrowserResolution.scrollDown(driver);
			
			//go to account posting tab and verify with two leg of posting
			log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
				
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			log.info("Verify Account posting with two leg of posting");
			List<WebElement> accountPostingDataList1 = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount1 = accountPostingDataList1.size();
			WaitLibrary.waitForAngular(driver);
			System.out.println(posCount1);
			Assert.assertEquals(posCount1, 4);
			String[] accTypeArr1 = {"NOS", "SUS", "SUS", "DDA"};
			for(int i=0; i<posCount1 ;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				//need to verify column
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr1[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount1);
			
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			
			// Get System Interaction Data count
			log.info("Verifying FinacleAccountPosting Request Action");
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			System.out.println(sysIntDataCount);
			int ofacCount = 0;
			int crFundCount = 0;
			int finCount = 0 ;
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				if(invoPoint.equals("CREDITFUNDSCONTROL")) {
					System.out.println(invoPoint);
					crFundCount++;
					if(crFundCount == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "CREDIT FUNDS CONTROL notification verified");
					}
				}else if(invoPoint.equals("OFAC") && RelationshipTxt.equals("REQUEST")) {
					System.out.println(invoPoint);
					ofacCount++;
					if(ofacCount == 1) {
						((ExtentTest) test).log(LogStatus.PASS, "OFAC interface verified");
					}
				}
				else if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					if(finCount == 1) {
						System.out.println(invoPoint);
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("BREAK_ADJUSTMENT")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "BREAK_ADJUSTMENT");
									((ExtentTest) test).log(LogStatus.PASS, "1st Finacle Posting request Instruction Type : " + insType + " verified");
									break;
								}else {
									WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
									js.executeScript("arguments[0].click();", modalCloseBtn);
									((ExtentTest) test).log(LogStatus.FAIL,"1st Finacle Account instruction Type not matched"+ insType);
									
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
					}
					else if(finCount == 2) {
						System.out.println(invoPoint);
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(Constants.tooshort_sleep);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("BREAK_ADJUSTMENT")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "BREAK_ADJUSTMENT");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									break;
								}else {
									WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
									js.executeScript("arguments[0].click();", modalCloseBtn);
									((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account instruction Type not matched"+ insType);
									
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						break;
					}
				}
				
				if(j==sysIntDataCount-1) {
					if(ofacCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"OFAC interface not found");
						
					}
					if(crFundCount !=1) {
						((ExtentTest) test).log(LogStatus.FAIL,"Credit fund control notification not found");
						
					}
					if(finCount !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						
					}
				}
			}
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
