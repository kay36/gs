package com.gs.tc.ICCM.MT940Statement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC069_Recon_Generation extends LoginLogout_ICCM{

	@Test	
	public void executeTC069() throws Exception {
		try {
			System.out.println("TC069");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "inbound", "910");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);

			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
			} 				

			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String insId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			log.info("Click on instruction id"); 
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			
			BrowserResolution.scrollDown(driver);  
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			
			WebElement origPayRefElem = ReceivedInstruction.originalPaymentReference(driver, 0, "OriginalPaymentReference");
			String origPayRef = origPayRefElem.getText();
			System.out.println(origPayRef);
			
			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
												
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			// Updating Greater Value Date for 62 field in sample file
			SampleFileModifier.updateValueDateGreater(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Modifying sample file Original payment reference
//			log.info("Modifying sample file with payment reference");
//			SampleFileModifier.updateStatementFile(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile , origPayRef, "inbound");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String stmtInsId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+stmtInsId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, stmtInsId);
			    ReceivedStatement.refreshIcon(driver).click();
				WaitLibrary.waitForAngular(driver);
				
				WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
				searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(stmtInsId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
				CommonMethods.clickStatementWithFileName(driver, stmtFile);
				WaitLibrary.waitForAngular(driver);
				break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			Thread.sleep(Constants.short_sleep);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.long_sleep);
			ReceivedStatement.statementRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			
			//Verify Recon Status of Uploaded Payment File
			//go to Payment
			log.info("Navigate to Received Instructions");
			js.executeScript("arguments[0].click();", SideBarMenu.paymentModule(driver));
			WebElement recInstTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInstTab, Constants.avg_explicit).click();
			
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.clear();
			searchInstruction.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			js.executeScript("window.scrollBy(0,-120)");
			
			String recInsStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(recInsStatus);
			
			log.info("Click on instruction id"); 
			WebElement getInstIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInstIdByTransportName, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Click on Payment Id");
			WebElement insPayment = ReceivedInstruction.getPaymentId(driver);
			String payID =  insPayment.getText();
			WaitLibrary.waitForElementToBeClickable(driver, insPayment, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement payIDHeader = ReceivedInstruction.verifyPaymentPage(driver, payID);
			
			//Click on Linked Message		
			BrowserResolution.scrollDown(driver);
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			
			// Verify Recon credit transfer and click recon id
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			List<WebElement> linkedMsgsDataList = new ArrayList<WebElement>();
			try {
			log.info("Get Linked Message Id");
			linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			} catch (Exception e) {			
				((ExtentTest) test).log(LogStatus.FAIL, "Linked Message ID not available");
				throw new Exception("Linked Message ID not available");
			}
			int linkMsgscount = linkedMsgsDataList.size();
			log.info("Click on Linked Message Id");
			for(int l=0; l<linkMsgscount; l++) {
				String linkMsgFun = ReceivedInstruction.linkedMsgsData(driver, l, "LinkedMsgFunc").getText(); 
				if(linkMsgFun.equals("Recon Credit Transfer")) {
					WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, l, "LinkedMsgID");
					WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
					break;
				}else if (l==linkMsgscount-1 && !linkMsgFun.equals("Recon Credit Transfer")) {
					throw new Exception("Recon Credit Transfer Payment not available");
				}
			}
			
			//verify status
			WebElement statusMatch = ReceivedInstruction.getStatus(driver);
			String Status = WaitLibrary.waitForElementToBeVisible(driver, statusMatch, Constants.avg_explicit).getText();			
			System.out.println("Recon Status::::"+Status);
			Assert.assertEquals(Status, "RECON_HOLD");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + Status);		
			
			//Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			WaitLibrary.waitForAngular(driver);			
			BrowserResolution.scrollDown(driver);	
			
			log.info("Get Account Id");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);			
			BrowserResolution.scrollDown(driver);			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			String[] accTypeArr = {"SUS", "NOS"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				//need to verify column
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount);
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());		
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
