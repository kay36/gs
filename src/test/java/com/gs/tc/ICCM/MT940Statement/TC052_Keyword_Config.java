package com.gs.tc.ICCM.MT940Statement;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC052_Keyword_Config extends LoginLogout_ICCM{
	
	@Test
	
	public void executeTC052() throws Exception {
		try {
			System.out.println("TC052");
			Log.startTestCase(log,TestCaseName);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
						
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
            Thread.sleep(Constants.long_sleep);
			 
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
			WaitLibrary.waitForAngular(driver);
	        Thread.sleep(Constants.short_sleep);			
			
	        if (listViewClass.contains(Constants.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
			} 				
			Thread.sleep(Constants.short_sleep);
			// To click the Statement with File name
			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String insId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			//Verify Status of Uploaded Statement File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, insId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	     		searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(insId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
	     		
	     		CommonMethods.clickStatementWithFileName(driver, stmtFile);
	     		WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);

			log.info("Verifying Status");
			List<WebElement> stmtSummaryDataList = ReceivedStatement.statementDetailsList(driver);
			int stmtCount = stmtSummaryDataList.size();
			for(int j = 0; j < 10; j++) {
				for(int l = 0; l < stmtCount; l++) {
					String Status =  ReceivedStatement.statementDetailsData(driver, l, "Status" ).getText();
					String stmtMatchingStatus = ReceivedStatement.statementDetailsData(driver, l, "MatchingStatus" ).getText();
					if(!Status.equals("COMPLETED") || !stmtMatchingStatus.equals("NOT_FOR_MATCH")) {
						Thread.sleep(Constants.long_sleep);
						ReceivedStatement.statementRefreshIcon(driver).click();
						WaitLibrary.waitForAngular(driver);
						Thread.sleep(Constants.tooshort_sleep);
					}
				}
			}
			for(int i=0;i<stmtCount;i++) {
				String statusText = ReceivedStatement.statementDetailsData(driver, i, "Status" ).getText();
				BrowserResolution.scrollDown(driver);
				System.out.println(statusText);
				Assert.assertEquals(statusText, "COMPLETED");
				((ExtentTest) test).log(LogStatus.PASS, "Status: " + statusText);
				
				ReceivedStatement.statementRefreshIcon(driver).click();
				WaitLibrary.waitForAngular(driver);
				Thread.sleep(Constants.long_sleep);
				ReceivedStatement.statementRefreshIcon(driver).click();
				WaitLibrary.waitForAngular(driver);
			
				log.info("Verifying Statement Matching Status");
				String stmtMatchingStatus = ReceivedStatement.statementDetailsData(driver, i, "MatchingStatus" ).getText();
				BrowserResolution.scrollDown(driver);
				System.out.println(stmtMatchingStatus);
				Assert.assertEquals(stmtMatchingStatus, "NOT_FOR_MATCH");
				((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status: " + stmtMatchingStatus);
			
				if(i == stmtCount-1) {
					log.info("Get Recon Payment ID");
					WebElement reconPayment = ReceivedStatement.statementDetailsData(driver, i, "ReconPaymentMsgID");
					String reconPaymentId = ReceivedStatement.statementDetailsData(driver, i, "ReconPaymentMsgID" ).getText();
					BrowserResolution.scrollDown(driver);
					System.out.println(reconPaymentId);
	
					log.info("Click on Recon Payment Id");
					WaitLibrary.waitForElementToBeClickable(driver, reconPayment, Constants.avg_explicit).click();
	
					log.info("Verifying Payment Page with Recon Payment ID");
					WebElement reconPaymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, reconPaymentId);
				}
			}
	
			BrowserResolution.scrollDown(driver);	
			log.info("Navigate to Account Posting & Capture Snapshot");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Two step of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			Assert.assertEquals(posCount, 4);
			String[] accTypeArr = {"NOS", "SUS", "SUS", "SUS"};
			for(int i=0;i<posCount;i++) {
				String accTypeVal = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				Assert.assertEquals(accTypeVal, accTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Two Step of Posting : " + posCount + " steps verified");
			
			log.info("Verifying Status");
			String[] status= {"03", "03", "03", "03"};
			for(int i=0;i<posCount;i++) {
				String statusTxt = ReceivedInstruction.accountPostingData(driver, i, "EntryStatus").getText();
				Assert.assertEquals(statusTxt, status[i]);
			}
			log.info("Navigate to System Interaction Tab");
			js.executeScript("arguments[0].click();", ReceivedInstruction.systemInteraction(driver));
			
			log.info("Verifying FinacleAccountPosting Request Action");
			// Get System Interaction Data count
			List<WebElement> sysIntDataList = ReceivedInstruction.sysInteractionList(driver);
			int sysIntDataCount = sysIntDataList.size();
			int finCount = 0 ;
			for (int j=0; j< sysIntDataCount; j++) {
				String invoPoint = ReceivedInstruction.sysInteractionData(driver, j, "InvocationPoint").getText();
				String RelationshipTxt = ReceivedInstruction.sysInteractionData(driver, j, "Relationship").getText();
				if(invoPoint.equals("FINACLEACCOUNTPOSTING") && RelationshipTxt.equals("REQUEST")) {
					finCount++;
					if(finCount == 2) {
						WebElement viewBtn = ReceivedInstruction.sysInterViewBtn(driver, j);
						BrowserResolution.scrollToElement(driver, viewBtn);
						js.executeScript("arguments[0].click();", viewBtn);
						// Getting Object Data
						Thread.sleep(2000);
						WebElement objData = ReceivedInstruction.sysInterObjData(driver, j);
						WaitLibrary.waitForElementToBeVisible(driver, objData, Constants.avg_explicit);
						String objDataString = objData.getText();
						try {
							JsonObject jsonObject = new JsonParser().parse(objDataString).getAsJsonObject();
							for(int k=0; k<jsonObject.get("movementRequests").getAsJsonArray().size(); k++) {
								String insType = jsonObject.get("movementRequests").getAsJsonArray().get(k).getAsJsonObject().get("instructionType").getAsString();
								if(insType.equals("FEE")) {
									System.out.println(insType);
									Assert.assertEquals(insType, "FEE");
									((ExtentTest) test).log(LogStatus.PASS, "2nd Finacle Posting request Instruction Type : " + insType + " verified");
									break;
								}
							}
						}catch(Exception jse) {
							throw new Exception(jse.getMessage());
						}
						// Clicking close modal
						WebElement modalCloseBtn = ReceivedInstruction.sysModalClose(driver, j);
						js.executeScript("arguments[0].click();", modalCloseBtn);
						break;
					}
				}
				if(j==sysIntDataCount-1) {
					if(finCount !=2) {
						((ExtentTest) test).log(LogStatus.FAIL,"2nd Finacle Account Posting not found");
						throw new Exception("2nd Finacle Account Posting not found");
					}
				}
			}

			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
