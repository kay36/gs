package com.gs.tc.ICCM.MT940Statement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.taskdefs.Sleep;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC080_Force_Complete extends LoginLogout_ICCM{
	
	@Test
	
	public void executeTC080() throws Exception {
		try {
			System.out.println("TC080");
			Log.startTestCase(log,TestCaseName);
			
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
									
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();
	
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();
			} 				

			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String insId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, insId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
				searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(insId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
				CommonMethods.clickStatementWithFileName(driver, stmtFile);
				WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStmtStatus);
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			
			BrowserResolution.scrollDown(driver);
		
			log.info("Click on Recon Payment Id");
			String reconPaymentId=null;
			try {
			WebElement reconPayment = ReceivedStatement.statementDetailsData(driver, 0, "ReconPaymentMsgID");
			reconPaymentId = WaitLibrary.waitForElementToBeVisible(driver, reconPayment, Constants.avg_explicit).getText();
			WaitLibrary.waitForElementToBeClickable(driver, reconPayment, Constants.avg_explicit).click();
			}catch (Exception e)
			{
				((ExtentTest) test).log(LogStatus.FAIL, "ReconPaymentMsgID not generated" );	
			}
			WaitLibrary.waitForAngular(driver);
			
			log.info("Verifying Payment Page with Payment ID");
			ReceivedInstruction.verifyPaymentPage(driver, reconPaymentId);
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			
			BrowserResolution.scrollDown(driver);
			
			//Account Posting Tab
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			log.info("Verify Account posting with one leg of posting");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			
			BrowserResolution.scrollDown(driver);
			
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			String[] accTypeArr = {"NOS", "SUS"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount);
			
			BrowserResolution.scrollUp(driver);
			//click on force complete
			log.info("click on force Complete");
			WebElement ForceComplete = ReceivedStatement.forceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, ForceComplete, Constants.avg_explicit).click();
			
			//Select the force complete account data
			log.info("Select Account");
			WebElement SelectForceComplete = ReceivedStatement.selectForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, SelectForceComplete, Constants.avg_explicit).click();
			
			//get data from excel
			log.info("get text");
			String reconData = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_accountNumber);
			System.out.println(reconData);
			Thread.sleep(Constants.tooshort_sleep);
			ReceivedStatement.accountDataField(driver).sendKeys(reconData,Keys.ENTER);
						
			//click on submit
			log.info("Click on submit");
			WebElement submitForceComplete = ReceivedStatement.SubmitForceComplete(driver);
			WaitLibrary.waitForElementToBeClickable(driver, submitForceComplete, Constants.avg_explicit).click();
			log.info("submitted");
			
			//Send request for approval
			CommonMethods.approveStatement(driver, reconPaymentId);
			Thread.sleep(Constants.tooshort_sleep);
		    ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.short_sleep);
			TimeUnit.MINUTES.sleep(5);
		    ReceivedStatement.reconRefreshIcon(driver).click();
			WaitLibrary.waitForAngular(driver);

			//verify status
			WebElement reconStatusMatch = ReceivedInstruction.getStatus(driver);
			String reconStatus = WaitLibrary.waitForElementToBeVisible(driver, reconStatusMatch, Constants.avg_explicit).getText();			
			System.out.println("Recon Status::::"+reconStatus);
			Assert.assertEquals(reconStatus, "RECON_HOLD");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + reconStatus);
			
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}


	}
