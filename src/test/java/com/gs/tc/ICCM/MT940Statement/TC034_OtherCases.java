package com.gs.tc.ICCM.MT940Statement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.pages.SideBarMenu;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC034_OtherCases extends LoginLogout_ICCM{

	@Test
	
	public void executeTC034() throws Exception {
		try {
			System.out.println("TC034");
			Log.startTestCase(log,TestCaseName);
								
			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			//Incoming Payment					
			// Fetch Payment File Name 
			String paymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + paymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "inbound", "202");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+paymentFile, "payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+paymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, Constants.avg_explicit).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
			} 				
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));
			System.out.println(paymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);

			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, paymentFile); 
			String insId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			log.info("Click on instruction id"); 
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, paymentFile);
			
			BrowserResolution.scrollDown(driver);
			log.info("Click on Payment ID");
			WebElement PaymentId = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = PaymentId.getText();
			WaitLibrary.waitForElementToBeClickable(driver, PaymentId, Constants.avg_explicit).click();
			
			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			WebElement origPayRefElem = ReceivedInstruction.originalPaymentReference(driver, 0, "OriginalPaymentReference");
			String origPayRef = origPayRefElem.getText();
			System.out.println(origPayRef);
			
//			WebElement getPaymentId = ReceivedInstruction.getPaymentId(driver);
//			String pmtId = getPaymentId.getText();
//			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, pmtId);
            
			BrowserResolution.scrollDown(driver);
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Verifying Statement Matching Status");
			String statusTxt = ReceivedInstruction.accountPostingData(driver, 0, "StatementMatchingStatus").getText();
			System.out.println(statusTxt);
			Assert.assertEquals(statusTxt, "WAIT_FOR_MATCH");
			((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status: " + statusTxt);

//EMPTY MT940 Payment
			// Fetch Payment File Name 
			String emptyStmtFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_emptyPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + emptyStmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+emptyStmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+emptyStmtFile, "statement");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+emptyStmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+emptyStmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+emptyStmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();

			CommonMethods.clickStatementWithFileName(driver, emptyStmtFile);
			
			//Get Instruction ID
			String empStmInsId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+empStmInsId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
			    BrowserResolution.scrollDown(driver);
				System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, empStmInsId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	     		searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(empStmInsId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
	     		
	     		CommonMethods.clickStatementWithFileName(driver, emptyStmtFile);
	     		WaitLibrary.waitForAngular(driver);
			    break;
			    
			    
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);

			log.info("Navigate to Received Instructions");
			js.executeScript("arguments[0].click();", SideBarMenu.paymentModule(driver));
			WebElement recInstTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInstTab, Constants.avg_explicit).click();
			
			//Verify Status of Uploaded Payment File
			//go to MT202Incoming
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.clear();
			searchInstruction.sendKeys(insId, Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile));

			js.executeScript("window.scrollBy(0,-120)");
			String recInsStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, paymentFile).getText();
			System.out.println(recInsStatus);
			
			log.info("Click on instruction id"); 
			WebElement getInstIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, paymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInstIdByTransportName, Constants.avg_explicit).click();
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.tooshort_sleep);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.short_sleep);
			BrowserResolution.scrollDown(driver);
			
			log.info("Click on Payment ID");
			WebElement inPaymtId = ReceivedInstruction.paymentId(driver);
			String payID =  inPaymtId.getText();
			WaitLibrary.waitForElementToBeClickable(driver, inPaymtId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement payIDHeader = ReceivedInstruction.verifyPaymentPage(driver, payID);
			
			BrowserResolution.scrollDown(driver);
			
			//Click on Linked Message		
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			
			List<WebElement> linkedMsgsDataList = new ArrayList<WebElement>();
			try {
			log.info("Get Linked Message Id");
			linkedMsgsDataList = ReceivedInstruction.linkedMsgsList(driver);
			} catch (Exception e) {			
				((ExtentTest) test).log(LogStatus.FAIL, "Linked Message ID not available");
				throw new Exception("Linked Message ID not available");
			}
			
			log.info("Click on Linked Message Id");
			WebElement linkedMessageId = ReceivedInstruction.linkedMessageId(driver, 0, "LinkedMsgID");
			WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId, Constants.avg_explicit).click();
			
			Thread.sleep(2000);
			WebElement reconStatusMatch = ReceivedInstruction.getStatus(driver);
			String reconStatus = WaitLibrary.waitForElementToBeVisible(driver, reconStatusMatch, Constants.avg_explicit).getText();			
			System.out.println("Recon Status::::"+reconStatus);
			Assert.assertEquals(reconStatus, "RECON_HOLD");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + reconStatus);		
			
			BrowserResolution.scrollDown(driver);
            log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Get Account Id");
			List<WebElement> accountPostingDataList = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount = accountPostingDataList.size();
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			System.out.println(posCount);
			Assert.assertEquals(posCount, 2);
			String[] accTypeArr = {"SUS", "NOS"};
			for(int i=0;i<posCount;i++) {
				String getAccountTyp = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				//need to verify column
				System.out.println(getAccountTyp);
				Assert.assertEquals(getAccountTyp, accTypeArr[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount);
			
			
//Process Matched MT940 Statement
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Modifying sample file Original payment reference
			log.info("Modifying sample file with payment reference");
			SampleFileModifier.updateStatementFile(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile , origPayRef, "inbound");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule1 = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule1);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule1, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements1 = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements1, Constants.avg_explicit).click();
			
			Thread.sleep(2000);
			WebElement searchStmt =  ReceivedInstruction.searchWithInsID(driver);
			searchStmt.clear();
			searchStmt.sendKeys(Keys.ENTER);
			WaitLibrary.waitForAngular(driver);

			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String stmInsId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+stmInsId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus1 = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus1);
			
			switch (recStatementStatus1) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus1);
			    break;
			  case "STATEMENT_REVIVE":
			    BrowserResolution.scrollDown(driver);
				System.out.println(recStatementStatus1);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, stmInsId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	     		searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(empStmInsId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
	     		
	     		CommonMethods.clickStatementWithFileName(driver, emptyStmtFile);
	     		WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus1);
			    break;
			}
			
			String recStmtStatus1 = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus1);
			
			BrowserResolution.scrollDown(driver);
			
			log.info("Verifying Statement Matching Status");
			String statementMatchingStatus = ReceivedStatement.statementDetailsData(driver, 0, "MatchingStatus").getText();
			System.out.println(statementMatchingStatus);
			Assert.assertEquals(statementMatchingStatus, "MATCHED");
			((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status: " + statementMatchingStatus);
			
			log.info("Click on Original Payment ID");
			WebElement originalPaymentId = ReceivedStatement.OriginalPayment(driver);
			WaitLibrary.waitForElementToBeClickable(driver, originalPaymentId, Constants.avg_explicit).click();
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			//Click on Linked Message		
			log.info("Navigate to Linked Message");
			js.executeScript("arguments[0].click();", ReceivedInstruction.linkedMessage(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Click on Linked Message Id");
			WebElement linkedMessageId2 = ReceivedInstruction.linkedMessageId(driver, 0, "LinkedMsgID");
			WaitLibrary.waitForElementToBeClickable(driver, linkedMessageId2, Constants.avg_explicit).click();
			
			WebElement reconStatusMatch1 = ReceivedInstruction.getStatus(driver);
			String reconStatus1 = WaitLibrary.waitForElementToBeVisible(driver, reconStatusMatch1, Constants.avg_explicit).getText();			
			System.out.println("Recon Status::::"+reconStatus1);
			Assert.assertEquals(reconStatus1, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Status : " + reconStatus1);
			
			BrowserResolution.scrollDown(driver);
            
			log.info("Navigate to Account Posting");
			js.executeScript("arguments[0].click();", ReceivedInstruction.getAccountPostingTab(driver));
			
			WaitLibrary.waitForAngular(driver);
			BrowserResolution.scrollDown(driver);
			log.info("Get Account Id");
			List<WebElement> accountPostingDataList1 = ReceivedInstruction.accountPostingDetailsList(driver);
			int posCount1 = accountPostingDataList1.size();
			System.out.println(posCount1);
			Assert.assertEquals(posCount1, 4);
			String[] accTypeArr1 = {"NOS", "SUS", "SUS", "NOS"};
			for(int i=0;i<posCount1;i++) {
				String getAccountType = ReceivedInstruction.accountPostingData(driver, i, "AccountType").getText();
				//need to verify column
				System.out.println(getAccountType);
				Assert.assertEquals(getAccountType, accTypeArr1[i]);
			}
			((ExtentTest) test).log(LogStatus.PASS, "Posting count: " + posCount1);
		
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
