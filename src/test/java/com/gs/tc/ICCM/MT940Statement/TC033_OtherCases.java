package com.gs.tc.ICCM.MT940Statement;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gs.pages.ReceivedInstruction;
import com.gs.pages.ReceivedStatement;
import com.gs.utilities.BrowserResolution;
import com.gs.utilities.CaptureScreenshot;
import com.gs.utilities.CommonMethods;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.FilesUpload;
import com.gs.utilities.GenericFunctions;
import com.gs.utilities.Log;
import com.gs.utilities.LoginLogout_ICCM;
import com.gs.utilities.SampleFileModifier;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC033_OtherCases extends LoginLogout_ICCM{

	@Test
	
	public void executeTC033() throws Exception {
		try {
			System.out.println("TC033");
			Log.startTestCase(log,TestCaseName);

			// Fetch sample Directory
			String sampleDir = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData,"TestCases",TestCaseName,Constants.Col_sampleDirectory);
			System.out.println(sampleDir);
			
			//Incoming Payment					
			// Fetch Payment File Name 
			String incomPaymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_incomingPayment);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + incomPaymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+incomPaymentFile, "inbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+incomPaymentFile, "payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+incomPaymentFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+incomPaymentFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+incomPaymentFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInsTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInsTab, 30).click();
			
			log.info("Click on List View");
			WebElement listView = ReceivedInstruction.listView(driver);
			String listViewClass= WaitLibrary.waitForElementToBeVisible(driver, listView, Constants.avg_explicit).getAttribute("class");
						
			if (listViewClass.contains(Constants.listViewClassData)) {			
				WaitLibrary.waitForElementToBeClickable(driver, listView, Constants.avg_explicit).click();					
			} 				
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, incomPaymentFile));
			System.out.println(incomPaymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus=ReceivedInstruction.getTransportNameStatusByFileName(driver, incomPaymentFile).getText();
			System.out.println(rec_fileStatus);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus);
			
			//Get Instruction ID 
			WebElement instdid =  ReceivedInstruction.getInsIdByTransportName(driver, incomPaymentFile); 
			String insId =  WaitLibrary.waitForElementToBeVisible(driver, instdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+insId);
			
			log.info("Click on instruction id"); 
			WebElement getInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, incomPaymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getInsIdByTransportName, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement TransportName = ReceivedInstruction.verifyInstructionPage(driver, incomPaymentFile);

			BrowserResolution.scrollDown(driver);
			
			log.info("Get Payment id & Verify Status"); 
			WebElement pmtIdid = ReceivedInstruction.getPaymentId(driver); 
			String pmtId = WaitLibrary.waitForElementToBeVisible(driver, pmtIdid, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+pmtId);
			
			WebElement pmtStatusid = ReceivedInstruction.getPaymentStatus(driver);
			String pmtStatus = WaitLibrary.waitForElementToBeVisible(driver, pmtStatusid, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+pmtStatus);
			Assert.assertEquals(pmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + pmtStatus);
			
			log.info("Get Original Payment Reference");
			String statusTxt = ReceivedInstruction.originalPaymentReference(driver, 0, "OriginalPaymentReference" ).getText();
			System.out.println(statusTxt);
			
			//Outgoing Payment
			// Fetch Payment File Name 
			String outPaymentFile = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_outgoingPayment);
			Log.startTestCase(log,TestCaseName);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + outPaymentFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+outPaymentFile, "outbound", "103");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+outPaymentFile, "payment");
			
			//Upload Payment File	
			log.info("Uploading Payment File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+outPaymentFile);
			log.info("Destination File is  :"+Constants.SWIFTCHANNELIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+outPaymentFile, Constants.SWIFTCHANNELIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+outPaymentFile);
			System.out.println(Constants.SWIFTCHANNELIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Instructions");
			WebElement recInstTab = ReceivedInstruction.recInsTab(driver);
			BrowserResolution.scrollToElement(driver, recInsTab);
			WaitLibrary.waitForElementToBeClickable(driver, recInstTab, Constants.avg_explicit).click();
			
			//Verify Status of Uploaded Payment File
			BrowserResolution.scrollToElement(driver, ReceivedInstruction.getTransportNameStatusByFileName(driver, outPaymentFile));
			System.out.println(outPaymentFile);
			js.executeScript("window.scrollBy(0,-120)");
			
			String rec_fileStatus1=ReceivedInstruction.getTransportNameStatusByFileName(driver, outPaymentFile).getText();
			System.out.println(rec_fileStatus1);
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(rec_fileStatus1, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + rec_fileStatus1);
			
			//Get Instruction ID 
			WebElement outInstdid =  ReceivedInstruction.getInsIdByTransportName(driver, outPaymentFile); 
			String outInsId =  WaitLibrary.waitForElementToBeVisible(driver, outInstdid, Constants.avg_explicit).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+outInsId);
			
			log.info("Click on instruction id"); 
			WebElement getOutInsIdByTransportName = ReceivedInstruction.getInsIdByTransportName(driver, outPaymentFile);
			WaitLibrary.waitForElementToBeClickable(driver, getOutInsIdByTransportName, Constants.avg_explicit).click();
			
			log.info("Verifying Instruction Page with Transport name");
			WebElement outTransportName = ReceivedInstruction.verifyInstructionPage(driver, outPaymentFile);
			
			BrowserResolution.scrollDown(driver);
			log.info("Get Payment id & Verify Status"); 
			WebElement outPmtIdElem = ReceivedInstruction.getPaymentId(driver); 
			String outPmtId = WaitLibrary.waitForElementToBeVisible(driver, outPmtIdElem, Constants.avg_explicit).getText();
			System.out.println("Payment id:::"+outPmtId);
			
			WebElement outPmtStatusElem = ReceivedInstruction.getPaymentStatus(driver);
			String outPmtStatus = WaitLibrary.waitForElementToBeVisible(driver, outPmtStatusElem, Constants.avg_explicit).getText();			
			System.out.println("Payment Status::::"+outPmtStatus);
			Assert.assertEquals(outPmtStatus, "ACCEPTED");
			((ExtentTest) test).log(LogStatus.PASS, "Payment Status : " + outPmtStatus);
			
			WebElement getPaymentId = ReceivedInstruction.getPaymentId(driver);
			WaitLibrary.waitForElementToBeClickable(driver, getPaymentId, Constants.avg_explicit).click();
			
			log.info("Verifying Payment Page with PaymentId");
			WebElement paymentIdHeader = ReceivedInstruction.verifyPaymentPage(driver, outPmtId);
			
			//Get Output Instruction ID 
			WebElement outputInstId =  ReceivedInstruction.outputInstructionId(driver); 
			String instId =  WaitLibrary.waitForElementToBeVisible(driver, outputInstId, Constants.avg_explicit).getText();
			System.out.println("Output Instruction id:::"+instId);
			
			//Statement Processing
			// Get Statement File name
			String stmtFile=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,"MT940Statement",TestCaseName,Constants.Col_statementFile);
			System.out.println(stmtFile);
			((ExtentTest) test).log(LogStatus.INFO, "File name is :" + stmtFile);
			
			// Updating Transaction reference in Sample file
			SampleFileModifier.updateTransactionReference(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement", "940");
			
			// Updating Value Date in sample file
			SampleFileModifier.updateValueDate(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile, "statement");
			
			//Modifying statement sample file with Incoming and Outgoing Payments
			log.info("Modifying sample file with payment reference");
			SampleFileModifier.modifyFileInboundOutbound(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile , instId, "outbound");
			Thread.sleep(Constants.short_sleep);
			SampleFileModifier.modifyFileInboundOutbound(System.getProperty("user.dir") + "\\" + Constants.gs_Mt940 +"\\"+sampleDir+stmtFile , statusTxt, "inbound");
			
			//Upload statement File	
			log.info("Uploading Statement File via WinSCP");
			log.info("Source File is  :"+Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			log.info("Destination File is  :"+Constants.SWIFTIN);
			FilesUpload.uploadFileByWinSCP(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile, Constants.SWIFTIN);
			System.out.println(Constants.gs_Mt940_Dir+"\\"+sampleDir+stmtFile);
			System.out.println(Constants.SWIFTIN);
			((ExtentTest) test).log(LogStatus.PASS, "File Uploaded");
			Thread.sleep(Constants.short_sleep);
			
			log.info("Navigate to Received Statement");
			WebElement statementModule = ReceivedStatement.statementModule(driver);
			BrowserResolution.scrollToElement(driver, statementModule);
			WaitLibrary.waitForElementToBeClickable(driver, statementModule, Constants.avg_explicit).click();

			log.info("Clicking on Received Statements");
			WebElement receivedStatements = ReceivedStatement.receivedStatements(driver);
			WaitLibrary.waitForElementToBeClickable(driver, receivedStatements, Constants.avg_explicit).click();

			CommonMethods.clickStatementWithFileName(driver, stmtFile);
			
			//Get Instruction ID
			String stmtInsId = ReceivedStatement.getInstructionID(driver).getText();
			((ExtentTest) test).log(LogStatus.INFO, "Instruction id:::"+stmtInsId);
			
			//Verify Status of Uploaded Payment File			
			String recStatementStatus = ReceivedStatement.getStmtStatus(driver).getText();
			System.out.println(recStatementStatus);
			
			switch (recStatementStatus) {
			  case "DEBULKED":
			    System.out.println(recStatementStatus);
			    break;
			  case "STATEMENT_REVIVE":
				BrowserResolution.scrollDown(driver);
			    System.out.println(recStatementStatus);
			    ReceivedStatement.clickAcceptInstruction(driver).click();
			    System.out.println("clicked accept");
			    WaitLibrary.waitForAngular(driver);
			    CommonMethods.approveStatement(driver, stmtInsId);
			    WebElement searchInstructionIn = ReceivedInstruction.searchWithInsID(driver);
	     		searchInstructionIn.clear();
	     		searchInstructionIn.sendKeys(stmtInsId, Keys.ENTER);
	     		WaitLibrary.waitForAngular(driver);
	     		
	     		CommonMethods.clickStatementWithFileName(driver, stmtFile);
	     		WaitLibrary.waitForAngular(driver);
			    break;
			  case "ACCEPT-STATEMENT_REVIVE-WAITFORAPPROVAL":
			    System.out.println(recStatementStatus);
			    break;
			}
			
			String recStmtStatus = ReceivedStatement.getStmtStatus(driver).getText();
			
			log.info("Verifying the status of uploaded file");
			Assert.assertEquals(recStmtStatus, "DEBULKED");
			((ExtentTest) test).log(LogStatus.PASS, "Instruction Status :" + recStmtStatus);
			
			WaitLibrary.waitForAngular(driver);
			Thread.sleep(Constants.long_sleep);
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.long_sleep);
			ReceivedStatement.statementRefreshIcon(driver).click();
			Thread.sleep(Constants.short_sleep);
			
			BrowserResolution.scrollDown(driver);
			Thread.sleep(Constants.short_sleep);
			
			log.info("Wait for the status to be loaded");
			String MatchingStatus = ReceivedStatement.statementDetailsData(driver, 0, "MatchingStatus" ).getText();
            String verifiedText = CommonMethods.statementDetailsDataStatus(driver, MatchingStatus, "MATCHED","MatchingStatus");
			
			log.info("Verifying Statement Matching Status");
			List<WebElement> statementMatchingDataList = ReceivedStatement.statementDetailsList(driver);
			int stmtCount = statementMatchingDataList.size();
			
			System.out.println(stmtCount);
			Assert.assertEquals(stmtCount, 6);
			String[] stmtTypeArr = {"MATCHED", "MATCHED", "UNMATCHED", "UNMATCHED", "NOT_FOR_MATCH", "NOT_FOR_MATCH"};
			for(int i=0;i<stmtCount;i++) {
				MatchingStatus = ReceivedStatement.statementDetailsData(driver, i, "MatchingStatus").getText();
				System.out.println(MatchingStatus);
				Assert.assertEquals(MatchingStatus, stmtTypeArr[i]);				
			}
			((ExtentTest) test).log(LogStatus.PASS, "Statement Matching Status: " + stmtCount);
			
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Pass");
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		catch(AssertionError ae){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			assertionerror=ae.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Assertion Error</b>"+"<br>"+ae.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, assertionerror);
		}
		catch(Exception et){
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "Failed");
			exceptionerror=et.getMessage();			
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Exception</b>"+"<br>"+et.getMessage()+LoginLogout_ICCM.test.addScreenCapture(CaptureScreenshot.screenshotName));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_outputsPath,TC_Outputs);
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, exceptionerror);
		}
		
	}

}
