package com.gs.utilities;

import java.io.File;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.gs.objectRepo.Login;
import com.gs.objectRepo.Logout;
import com.gs.objectRepo.SecurityPage;
import com.paulhammant.ngwebdriver.NgWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class LoginLogoutTest {
	
	public static String uName = null;
	public static String uNameForUser = null;
	public static String pwdForUser = null;
	public static Logger log;
	public static WebDriver driver = null;
	public static JavascriptExecutor js;
	public static ArrayList<String> tabs2 = null;
	public static String testcaseName = null;
	public static int TestCaseRow;
	public static ExtentReports report;
	public static ExtentReports report1;
	public static ExtentTest test;
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static NgWebDriver ngWebDriver = null;

	public static boolean isWindows() {
		
		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isLinux() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

	}

	public static boolean isSolaris() {

		return (OS.indexOf("sunos") >= 0);

	}

	public static String returnEnv() {
		String env = null;
		System.out.println(OS);

		if (isWindows()) {
			env = "windows";
		} else if (isMac()) {
			env = "mac";
		} else if (isLinux()) {
			env = "linux";
		} else if (isSolaris()) {
			env = "solaris";
		} else {
			env = "Unsupported";
		}
		return env;

	}

	@BeforeSuite
	public static void login() throws Exception {
		System.out.println("Before suite");
		try {
			report = new ExtentReports(Constants.extendedreport);
			report.loadConfig(new File(System.getProperty("user.dir")+"\\ReportsConfig.xml"));
			System.out.println("extendedreport path:" + Constants.extendedreport);
			String envDetails = returnEnv();
			System.out.println("envDetails::::" + envDetails);
			System.out.println("Inside Before suite login");
			log = Logger.getLogger("LoginlogoutTest");

			if (envDetails.equals("windows")) {
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver.exe");
				ChromeOptions capability = new ChromeOptions();
				capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
				capability.addArguments("--allow-running-insecure-content");
				driver = new ChromeDriver(capability);
			} else if (envDetails.equals("linux")) {
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver");
				driver = new ChromeDriver();
			}

			js = (JavascriptExecutor) driver;

			// Launch Application
			String baseURL = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "LoginDetails",
					"Application_URL", 4);

			String baseURLApprover = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "User",
					"Application_URL", 4);

			uName = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "LoginDetails", "Application_URL",
					5);

			String pwd = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "LoginDetails",
					"Application_URL", 6);

			uNameForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "User", "Application_URL", 5);

			pwdForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "User", "Application_URL", 6);

			System.out.println("URL is :" + baseURL);
			System.out.println("UN is :" + uName);
			System.out.println("PWD is :" + pwd);
			Login.launch(driver, baseURL);
			log.info("Browser launched");
			System.out.println("Browser Launched");
			Login.usrName(driver).sendKeys(uName);
			Login.password(driver).sendKeys(pwd);
			WebElement lgnButton = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnButton, 30).click();
			System.out.println("User Logged In");
			
			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;
			js.executeScript("window.open()");

			log.info("Switch to new tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			Login.launch(driver, baseURLApprover);
			
			System.out.println("UN is :" + uNameForUser);
			System.out.println("PWD is :" + pwdForUser);
			WebElement userNm = Login.usrName(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userNm, 10).sendKeys(uNameForUser);
			WebElement userPwd = Login.password(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userPwd, 10).sendKeys(pwdForUser);
			WebElement lgnBtnApprover = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnBtnApprover, 30).click();
			System.out.println("Approver Logged In");
			Thread.sleep(2000);
			js.executeScript("arguments[0].click();", SecurityPage.security(driver));
			js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));
			driver.switchTo().window(tabs2.get(0));
			
//			Thread.sleep(10000);
			
//			js.executeScript("arguments[0].click();", SecurityPage.security(driver));
//
//			Thread.sleep(2000);
//
//			js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));
//
//			Thread.sleep(3000);
//			driver.switchTo().window(tabs2.get(0));

		} catch (Exception ej) {
			ej.printStackTrace();
		}

	}

	@AfterSuite
	public void Userlogout() throws Exception {
		System.out.println("Inside Before suite logout");
		
		try {
			System.out.println("Writing into Extent reports");
			report.endTest(null);
			report.flush();
			System.out.println("Extent report generated");
			
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ESCAPE);
			act.moveToElement(Logout.userprofileBtn(driver, uName)).build().perform();
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));
			System.out.println("Logout successfully");
//			WaitLibrary.waitForAngular(driver);
			//Thread.sleep(2000);
			
			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;
			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			act.sendKeys(Keys.ESCAPE);
			act.moveToElement(Logout.userprofileBtn(driver, uNameForUser)).build().perform();
			js.executeScript("arguments[0].click();", Logout.logOut(driver));
			System.out.println("Logout successfully");
//			WaitLibrary.waitForAngular(driver);
			//Thread.sleep(2000);

			driver.close();
			driver.quit();

		} catch (Exception en) {
			en.printStackTrace();

		}
	}

}
