package com.gs.utilities;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;
import com.google.common.base.Function;
import com.gs.objectRepo.AllPaymentsPage;
import com.gs.objectRepo.RecInspage;
import com.gs.objectRepo.SecurityPage;

public class BrowserResolution extends LoginLogout_ICCM{

//	public static JavascriptExecutor js;
	public static ArrayList<String> tabs2 = null;

	public static void zoomIn(WebDriver driver) {
		// To zoom In page 4 time using CTRL and + keys.
		for (int i = 0; i < 4; i++) {
			driver.findElement(By.tagName("html")).sendKeys(Keys.chord(Keys.CONTROL, Keys.ADD));
		}
	}

	/*
	 * public static void zoomOut(WebDriver driver){ //To zoom out page 4 time using
	 * CTRL and - keys. for(int i=0; i<4; i++){
	 * driver.findElement(By.tagName("html")).sendKeys(Keys.chord(Keys.CONTROL,
	 * Keys.SUBTRACT)); } }
	 */

	/*
	 * public static void set100(WebDriver driver){ //To set browser to default zoom
	 * level 100%
	 * driver.findElement(By.tagName("html")).sendKeys(Keys.chord(Keys.CONTROL,
	 * "0")); }
	 */

	public static void defaultResol(WebDriver driver) {
		// To set browser to default zoom level 100%
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='100%'");
	}

	public static void scrollDown(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public static void scrollUp(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, 0)");
	}

	public static void scrollDownByPixel(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,750)");
	}

	public static void zoomOut(WebDriver driver) {
		// To zoom out page 4 time using CTRL and - keys.
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='50%'");
	}

	public static void zoomOutWithPer(WebDriver driver, int per) {
		// To zoom out page 4 time using CTRL and - keys.
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='" + per + "" + "%" + "'");
	}

	public static void zoomOutBy70(WebDriver driver) {
		// To zoom out page 4 time using CTRL and - keys.
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.body.style.zoom='80%'");
	}

	public static void slider(WebDriver driver) {
		// int x=10;
		WebElement slider = AllPaymentsPage.sidebar(driver);
		int width = slider.getSize().getWidth();
		System.out.println("slider width:::::::" + width);
		Actions move = new Actions(driver);
		move.clickAndHold().build().perform();
		move.dragAndDropBy(slider, 100, 200);
		// move.moveToElement(slider, ((width*x)/200), 0).click();
		// move.build().perform();
		System.out.println("Slider moved");
	}

	public static void multiBrowsers(WebDriver driver, Logger log, String baseURL, String uNameForUser,
			String pwdForUser, String Screenshots, String uName, String pmtId, String TestCaseName, int TestCaseRow,
			Object test) throws Exception {

		try {
			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;

			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			Thread.sleep(5000);

			log.info("Click on list view");
			WebElement stmtrefresh = RecInspage.Statementrefresh(driver);
			WaitLibrary.waitForElementToBeClickable(driver, stmtrefresh, 40).click();
			Thread.sleep(5000);

			String btnValue = RecInspage.viewBtn1(driver).getAttribute("class");
			System.out.print(btnValue);
			Thread.sleep(1000);
			if (btnValue.equals("btn-trans disabledBtnColor")) {
				WebElement grdBtnView = RecInspage.GridButtonView(driver);
				WaitLibrary.waitForElementToBeClickable(driver, grdBtnView, 40).click();

			}

			log.info("Click in filter ");
			WebElement fltrIcon = SecurityPage.filterIcon(driver);
			WaitLibrary.waitForElementToBeClickable(driver, fltrIcon, 40).click();
			Thread.sleep(1000);

			WebElement keywordSearch = SecurityPage.keywordSearch(driver);
			keywordSearch.sendKeys(pmtId);
			Thread.sleep(2000);

			WebElement searchSymbl = SecurityPage.searchSymbol(driver);
			WaitLibrary.waitForElementToBeClickable(driver, searchSymbl, 40).click();
//<<<<<<< HEAD
//			Thread.sleep(1000);
//
////			SecurityPage.closeSearchBox(driver);
////			Thread.sleep(2000);
//
//			WebElement secPageClk = SecurityPage.clickele(driver, pmtId);
//			WaitLibrary.waitForElementToBeClickable(driver, secPageClk, 40).click();
//=======
			//SecurityPage.closeSearchBox(driver);
			Thread.sleep(1000);
			//LoginLogout_ICCM.js.executeScript("arguments[0].click();", SecurityPage.closeSearchBox(driver));
			
			 try { 
				
					Wait<WebDriver> wait = new FluentWait<WebDriver>(LoginLogout_ICCM.driver) 
						       
							   . withTimeout(Duration.ofSeconds(10))
							       . pollingEvery(Duration.ofMillis(300))
							       .ignoring(NoSuchElementException.class)
							           .ignoring(NullPointerException.class)
							           .withMessage("User defined message");

						WebElement rs = wait.until(new Function<WebDriver, WebElement>() 
						{
						    public WebElement apply(WebDriver driver) {
						    return SecurityPage.clickele(driver, pmtId);
						}
						});
				 
				 LoginLogout_ICCM.js.executeScript("arguments[0].click();", rs); 
			 } catch (NoSuchElementException e) {
			LoginLogout_ICCM.js.executeScript("arguments[0].click();",SecurityPage.clickele(driver, pmtId));
			 e.printStackTrace();
			 }
			//WaitLibrary.waitForElementToBeClickable(driver, secPageClk, 40).click();

			Thread.sleep(3000);

			BrowserResolution.scrollDown(driver);
			WebElement secPageApprover = SecurityPage.approve(driver);
			WaitLibrary.waitForElementToBeClickable(driver, secPageApprover, 50).click();
			Thread.sleep(3000);

			BrowserResolution.zoomOut(driver);
			Thread.sleep(2000);

			CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "AfterApprovals");
			/*
			 * BrowserResolution.defaultResol(driver); Thread.sleep(3000); // Switch to main
			 * window driver.switchTo().window(tabs2.get(0)); Thread.sleep(2000);
			 */
		} catch (Exception ev) {
			ev.printStackTrace();
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_status, "Failed");
			// ((ExtentTest) test).log(LogStatus.FAIL, TestCaseName,"Test Failed:Actual
			// Result: " + actualresult + "Expected Result: " + expectedresult);
			BrowserResolution.zoomOut(driver);
			Thread.sleep(1000);
			CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "ApproverSideException");
			Thread.sleep(1000);
			BrowserResolution.defaultResol(driver);
			String extentScreenshot = ExtentReportSS.ExtentReportSSPath(Screenshots);

//			((ExtentTest) test).log(LogStatus.FAIL,((ExtentTest) test).addScreenCaptureFromPath(extentScreenshot + "//ApproverSideException.png"));
			((ExtentTest) test).log(Status.FAIL, (Markup) ((ExtentTest) test))
					.addScreenCaptureFromPath(extentScreenshot + "//ApproverSideException.png");

		} finally {
			BrowserResolution.defaultResol(driver);
			Thread.sleep(3000);
			// Switch to main window
			driver.switchTo().window(tabs2.get(0));
			Thread.sleep(3000);
		}
	}
	
	public static void multiBrowsersForReject(WebDriver driver, Logger log, String baseURL, String uNameForUser,
			String pwdForUser, String Screenshots, String uName, String pmtId, String TestCaseName, int TestCaseRow,
			Object test) throws Exception {

		try {
			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;

			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			Thread.sleep(5000);

			log.info("Click on list view");
			WebElement stmtrefresh = RecInspage.Statementrefresh(driver);
			WaitLibrary.waitForElementToBeClickable(driver, stmtrefresh, 40).click();
			Thread.sleep(5000);

			String btnValue = RecInspage.viewBtn1(driver).getAttribute("class");
			System.out.print(btnValue);
			Thread.sleep(1000);
			if (btnValue.equals("btn-trans disabledBtnColor")) {
				WebElement grdBtnView = RecInspage.GridButtonView(driver);
				WaitLibrary.waitForElementToBeClickable(driver, grdBtnView, 40).click();

			}

			log.info("Click in filter ");
			WebElement fltrIcon = SecurityPage.filterIcon(driver);
			WaitLibrary.waitForElementToBeClickable(driver, fltrIcon, 40).click();
			Thread.sleep(1000);

			WebElement keywordSearch = SecurityPage.keywordSearch(driver);
			keywordSearch.sendKeys(pmtId);
			Thread.sleep(2000);

			WebElement searchSymbl = SecurityPage.searchSymbol(driver);
			WaitLibrary.waitForElementToBeClickable(driver, searchSymbl, 40).click();
//<<<<<<< HEAD
//			Thread.sleep(1000);
//
////			SecurityPage.closeSearchBox(driver);
////			Thread.sleep(2000);
//
//			WebElement secPageClk = SecurityPage.clickele(driver, pmtId);
//			WaitLibrary.waitForElementToBeClickable(driver, secPageClk, 40).click();
//=======
			//SecurityPage.closeSearchBox(driver);
			Thread.sleep(1000);
			//LoginLogout_ICCM.js.executeScript("arguments[0].click();", SecurityPage.closeSearchBox(driver));
			
			 try { 
				
					Wait<WebDriver> wait = new FluentWait<WebDriver>(LoginLogout_ICCM.driver) 
						       
							   . withTimeout(Duration.ofSeconds(10))
							       . pollingEvery(Duration.ofMillis(300))
							       .ignoring(NoSuchElementException.class)
							           .ignoring(NullPointerException.class)
							           .withMessage("User defined message");

						WebElement rs = wait.until(new Function<WebDriver, WebElement>() 
						{
						    public WebElement apply(WebDriver driver) {
						    return SecurityPage.clickele(driver, pmtId);
						}
						});
				 
				 LoginLogout_ICCM.js.executeScript("arguments[0].click();", rs); 
			 } catch (NoSuchElementException e) {
			LoginLogout_ICCM.js.executeScript("arguments[0].click();",SecurityPage.clickele(driver, pmtId));
			 e.printStackTrace();
			 }
			//WaitLibrary.waitForElementToBeClickable(driver, secPageClk, 40).click();

			Thread.sleep(3000);
		
			SecurityPage.notes(driver).sendKeys("No Info");
			Thread.sleep(2000);
			
			BrowserResolution.scrollDown(driver);
			WebElement secPageApprover = SecurityPage.reject(driver);
			WaitLibrary.waitForElementToBeClickable(driver, secPageApprover, 50).click();
			Thread.sleep(3000);

			BrowserResolution.zoomOut(driver);
			Thread.sleep(2000);

			CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "AfterApprovals");
			/*
			 * BrowserResolution.defaultResol(driver); Thread.sleep(3000); // Switch to main
			 * window driver.switchTo().window(tabs2.get(0)); Thread.sleep(2000);
			 */
		} catch (Exception ev) {
			ev.printStackTrace();
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_status, "Failed");
			// ((ExtentTest) test).log(LogStatus.FAIL, TestCaseName,"Test Failed:Actual
			// Result: " + actualresult + "Expected Result: " + expectedresult);
			BrowserResolution.zoomOut(driver);
			Thread.sleep(1000);
			CaptureScreenshot.getScreenshotInPath(driver, Screenshots, "ApproverSideException");
			Thread.sleep(1000);
			BrowserResolution.defaultResol(driver);
			String extentScreenshot = ExtentReportSS.ExtentReportSSPath(Screenshots);

//			((ExtentTest) test).log(LogStatus.FAIL,((ExtentTest) test).addScreenCaptureFromPath(extentScreenshot + "//ApproverSideException.png"));
			((ExtentTest) test).log(Status.FAIL, (Markup) ((ExtentTest) test))
					.addScreenCaptureFromPath(extentScreenshot + "//ApproverSideException.png");

		} finally {
			BrowserResolution.defaultResol(driver);
			Thread.sleep(3000);
			// Switch to main window
			driver.switchTo().window(tabs2.get(0));
			Thread.sleep(3000);
		}
	}

	public static void scrollToElement(WebDriver driver, WebElement element) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(2000);
	}

	
	
	
	
	
}