package com.gs.utilities;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.gs.objectRepo.Login;
import com.gs.objectRepo.Logout;
import com.gs.objectRepo.SecurityPage;
import com.gs.utilities.Constants;
import com.gs.utilities.ExcelUtilities;
import com.gs.utilities.WaitLibrary;
import com.relevantcodes.extentreports.ExtentReports;

public class LoginLogout_Report 
{

	public static String uName = null;
	public static String uNameForUser = null;
	public static String pwdForUser = null;
	public static Logger log ;
	public static WebDriver driver=null;
	public static JavascriptExecutor js;
	public static ArrayList<String> tabs2 = null;
	public static String testcaseName = null;
	public static int TestCaseRow ;
	public static ExtentReports report;
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static boolean isWindows() 
	{

		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() 
	{
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isLinux() 
	{
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}

	public static boolean isSolaris() 
	{
		return (OS.indexOf("sunos") >= 0);
	}

	public static String returnEnv()
	{
		String env=null;

		if (isWindows()) 
		{
			env="windows";
		} 
		else if (isMac()) 
		{
			env="mac";
		} 
		else if (isLinux()) 
		{
			env="linux";
		} 
		else if (isSolaris()) 
		{
			env="solaris";
		} 
		else 
		{
			env="Unsupported";
		}
		return env;

	}
	
	@BeforeSuite
	public static void login() throws Exception 
	{
		try 
		{

			
			report = new ExtentReports(Constants.extendedreport);
			System.out.println("extendedreport path:"+Constants.extendedreport);
			String envDetails = returnEnv();
			System.out.println("envDetails::::" + envDetails);
			System.out.println("Inside Before suite login");
			log = Logger.getLogger("Loginlogout");

			if (envDetails.equals("windows")) 
			{
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver.exe");
				driver = new ChromeDriver();
			} 
			else if (envDetails.equals("linux"))
			{
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver");
				driver = new ChromeDriver();
			}

//			//Synchronization - Iimplicit wait 		
//			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			js = (JavascriptExecutor) driver;

			// Launch Application
			String baseURL = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "PaymentCreater","Application_URL", 4);
			
			String baseURLApprover = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "PaymentCreater","Application_URL", 4);
			
			uName = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "PaymentCreater", "Application_URL",5);
			
			String pwd = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "PaymentCreater","Application_URL", 6);
			
			uNameForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "User", "UserApprovals",5);
			
			pwdForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "User", "UserApprovals",6);
			
			System.out.println("URL is :" + baseURL);
			System.out.println("UN is :" + uName);
			System.out.println("PWD is :" + pwd);
			Login.launch(driver, baseURL);
			Thread.sleep(3000);
			log.info("Browser launched");

			System.out.println("Browser Launched");
			Login.usrName(driver).sendKeys(uName);
			Login.password(driver).sendKeys(pwd);

			WebElement lgnButton = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnButton, 30).click();
			Thread.sleep(10000);
			System.out.println("User Logged In");

			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;
			js.executeScript("window.open()");
			Thread.sleep(2000);

			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			Login.launch(driver, baseURLApprover);
			Thread.sleep(3000);
			WebElement userNm = Login.usrName(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userNm, 10).sendKeys(uNameForUser);
			WebElement userPwd = Login.password(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userPwd, 10).sendKeys(pwdForUser);
			WebElement lgnBtnApprover = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnBtnApprover, 30).click();
			Thread.sleep(20000);
			js.executeScript("arguments[0].click();", SecurityPage.security(driver));

			Thread.sleep(2000);

			js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));

			Thread.sleep(4000);
			driver.switchTo().window(tabs2.get(0));
			Thread.sleep(2000);

		} catch (Exception ej) 
		{
			ej.printStackTrace();
		}

	}

	@AfterSuite
	public void Userlogout() throws Exception
	{
		System.out.println("Inside Before suite logout");

		try
		{
			System.out.println("Writing into Extent reports");
			report.endTest(null);
			report.flush();
			System.out.println("Extent report generated");
			
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ESCAPE);
			Thread.sleep(3000);

			act.moveToElement(Logout.userprofileBtn(driver,uName)).build().perform();
			Thread.sleep(2000);
			
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));
			Thread.sleep(5000);
			
			System.out.println("Logout successfully");
			log.info("Open new tab and enter volpay hub url");

			js = (JavascriptExecutor) driver;

			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			act.sendKeys(Keys.ESCAPE);
			Thread.sleep(3000);

			act.moveToElement(Logout.userprofileBtn(driver, uNameForUser)).build().perform();
			Thread.sleep(2000);
			
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));
			Thread.sleep(5000);

			System.out.println("Logout successfully");
//			driver.close();
//			driver.quit();
			
		}
		catch(Exception en)
		{
			en.printStackTrace();
		}

	}
}
