package com.gs.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

	public static WebDriver driver = null;

	public static String getTestCaseName(String sTestCase) throws Exception {
		String value = sTestCase;
		try {
			int posi = value.indexOf("@");
			value = value.substring(0, posi);
			posi = value.lastIndexOf(".");
			value = value.substring(posi + 1);
			return value;
		} catch (Exception e) {
			// Log.error("Class Utils | Method getTestCaseName | Exception desc :
			// "+e.getMessage());
			throw (e);
		}
	}

	public static WebDriver initialiseBrowser(String browser) {

		if (browser.equalsIgnoreCase("Google Chrome")) {
			System.setProperty("webdriver.chrome.driver", "Resources//chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browser.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "Resources//geckodriver.exe");
			driver = new FirefoxDriver();
		}

		else if (browser.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", "Resources//IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}

		else {
			System.out.println("Driver is " + driver);
		}

		return driver;

	}

	public static void waitForElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static String[] getFields(String path) throws IOException

	{
		// System.out.println("File path::::"+path);
		File file = new File(path);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String[] array = new String[10];
		// String currency = null;
		// String Application_Code = null;
		// String immidiate_Destination = null;
		int count = 1;
		String line = null;
		while ((line = br.readLine()) != null) {
			if (count == 1) {
				// Schedule
				// String schedule = line.substring(3, 6);
				// System.out.println("Schedule:::"+schedule);
				array[0] = line.substring(3, 6);
			} else if (count == 2) {
				// Transaction type
				array[1] = line.substring(66, 70);
			} else if (count == 3) {
				// Destination Bank account number
				array[2] = line.substring(13, 33);
				array[4] = line.substring(185, 200);

			} else if (count == 4) {
				// originating bank account number
				array[3] = line.substring(162, 182);
			}
			count++;

		}
		br.close();
		return array;
	}

	public static String[] getTransactionId(File file) throws IOException

	{
		// System.out.println("File path::::"+path);
		// File file = new File(path);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String[] array = new String[2];
		// String currency = null;
		// String Application_Code = null;
		// String immidiate_Destination = null;
		int count = 1;
		String modifiedThirdLine = "";
		String finalLine = "";
		String line = null;
		while ((line = br.readLine()) != null) {
			if (count == 4) {
				// originating bank account number
				array[0] = line.substring(185, 200);
			}
			if (line.startsWith("6")) {
				String thirdLine = line;
				modifiedThirdLine = thirdLine.replaceAll(" ", "");
			}
			if (line.startsWith("7")) {
				String fourthLine = line;
				finalLine = modifiedThirdLine + fourthLine.replaceAll(" ", "");
				array[1] = finalLine;
			}
			/*
			 * else if(count==3) { String thirdLine = line; modifiedThirdLine =
			 * thirdLine.replaceAll(" ", ""); } else if(count == 4) { String fourthLine =
			 * line; modifiedThirdLine = modifiedThirdLine+fourthLine.replaceAll(" ", "");
			 * array[1] = modifiedThirdLine; }
			 */
			count++;

		}
		br.close();
		return array;
	}

	public static String getParticipentid(File file) throws IOException

	{
		/*
		 * System.out.println("File path::::"+path); File file = new File(path);
		 */
		BufferedReader br = new BufferedReader(new FileReader(file));
		int count = 1;
		String participentid = null;
		String line = null;
		while ((line = br.readLine()) != null) {
			if (count == 1) {
				// Schedule
				participentid = line.substring(14, 18);
				System.out.println("Schedule:::" + participentid);
			}
			break;

		}
		br.close();
		return participentid;
	}

	public static void updateSample_With_CurrentDate(File file, int refStartPosition, int refEndPosition,
			int dateStartPosition, int dateEndPosition) throws IOException {
		String date = null;
		String ref = null;
		// File inputFile = new File(path);
		BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		BufferedWriter bw = null;
		String line1 = null;
		String line2 = null;
		int count = 1;
		String line = null;
		int totalLinesCount = 1;
		while ((line = br1.readLine()) != null) {
			totalLinesCount++;
		}
		while ((line = br.readLine()) != null) {
			if (count == 1) {
				bw = new BufferedWriter(new FileWriter(file));
				bw.write(line + "\r\n");
			}
			if (count != 1 && count != 2 && count != 4 && count != totalLinesCount - 1) {

				// System.out.println("linessss:"+line);
				bw.write(line + "\r\n");
			} else if (count == 2) {
				ref = line.substring(refStartPosition, refEndPosition);
				// System.out.println("ref::::"+ref);
				line1 = line.replaceAll(ref, randomNumber(4));
				// System.out.println("line2222::::"+line1);
				bw.write(line1 + "\r\n");
			} else if (count == 4) {
				date = line.substring(dateStartPosition, dateEndPosition);
				System.out.println("date:::" + date);
				line2 = line.replace(date, systemdateyy());
				// System.out.println("line2222::::"+line2);
				bw.write(line2 + "\r\n");
			} else if (count == totalLinesCount - 1) {
				bw.write(line);
			}
			count++;
		}
		br.close();
		br1.close();
		bw.flush();
		bw.close();

	}

	public static String randomNumber(int length) {
		String s = "1234567890";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			int index = random.nextInt();
			index = Math.abs(index % s.length());
			sb.append(s.charAt(index));
		}
		return sb.toString().toLowerCase();
	}

	public static String systemdate() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		// bw.write("Time:::"+date);
		return date;

	}

	public static String systemdateyy() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyMMdd");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		// bw.write("Time:::"+date);
		return date;

	}

	public static String systemdateWithHypen() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		// bw.write("Time:::"+date);
		return date;

	}

	public static String executeRESTCommand(String command) {
		String s = null;

		try {
			Runtime rt = Runtime.getRuntime();

			Process proc = rt.exec(command);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			// System.out.println("Here is the standard output of the command:\n");

			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			return s;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	public static String getRandomString(int length) {
		String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			int index = random.nextInt();
			index = Math.abs(index % s.length());
			sb.append(s.charAt(index));
		}
		return sb.toString().toLowerCase();
	}

	public static String getSubString(String str, int beginIndex, int endIndex) {
		String subString = str.substring(beginIndex, endIndex);
		return subString;
	}
	
	public static void clickByJs(WebElement webElement) {
	((JavascriptExecutor)LoginLogout_ICCM.driver).executeScript("arguments[0].click();",webElement);	
	}

}
