package com.gs.utilities;

public class Constants {

	public static final String Path_TestData = "TestData";
	
	public static String DownloadsPath = "Downloads";
	
	//public static final String baseRESTURL = "https://ec2-3-208-75-199.compute-1.amazonaws.com:8443/VolPayRest/rest/v2";
//	public static final String baseRESTURL ="https://10.206.1.43:8443/VolPayRest/rest/v2";
	public static final String baseRESTURL ="https://10.206.1.22:8443/VolPayRest/rest/v2"; 

	public static String extendedreport = "reports\\Report.html";
	public static String extendedreport_ACH = "reports\\ACH.html";
	public static String extendedreport_BRFP = "reports\\BRFP.html";
	public static String extendedreport_ClientDefects = "reports\\ClientDefects.html";
	public static String extendedreport_F50 = "reports\\F50.html";
	public static String extendedreport_ICCM = "reports\\ICCM.html";
	
	// wait constants
	public static int tooshort_sleep = 2000;
	public static int short_sleep = 5000;
	public static int short_implicit = 5;
	public static int short_explicit = 10;
	
	public static int avg_sleep = 10000;
	public static int avg_implicit = 10;
	public static int avg_explicit = 30;
	
	public static int long_sleep = 20000;
	public static int long_implicit = 15;
	public static int long_explicit = 50;


	public static String Deployment = "E:\\GS_Environment\\apache-tomcat\\VPH_messages\\Input";
	public static String FXBUISNESSpain001_Channel = Deployment + "\\" + "FXBUISNESSpain001";
	public static String SWIFTIN_Channel = Deployment + "\\" + "SWIFTIN";
	public static String SWIFTCHANNELIN_Channel = Deployment + "\\" + "SWIFTCHANNELIN";
	public static String SWIFTIN = "SWIFTIN/";
	public static String SWIFTCHANNELIN = "SWIFTCHANNELIN1/";
	
	public static final String gs_Samples = Path_TestData + "\\" + "Samples";
	
	public static final String gs_Mt940 = gs_Samples + "\\" + "MT940_Statement";
	public static final String gs_REJ_RET = gs_Samples + "\\" + "Reject_Return";
	public static final String gs_MT900 = gs_Samples + "\\" + "MT900_Variations_F72";
	public static final String gs_MT910 = gs_Samples + "\\" + "MT910_Incoming_OFAC";
	public static final String gs_Sanity = gs_Samples + "\\" + "Sanity_TCs";
	public static final String gs_Others = gs_Samples + "\\" + "Other_TCs";
	public static final String gs_Mt940_Dir = "MT940_Statement";
	public static final String gs_REJ_RET_Dir = "Reject_Return";
	public static final String gs_MT900_Dir = "MT900_Variations_F72";
	public static final String gs_MT910_Dir = "MT910_Incoming_OFAC";
	public static final String gs_Sanity_Dir = "Sanity_TCs";
	public static final String gs_Others_Dir = "Other_TCs";
	
	public static final String TC001_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase01\\TC_01.txt";
	public static final String TC002_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase02\\TC_02.txt";
	public static final String TC003_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase03\\TC_03.txt";
	public static final String TC004_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase04\\TC_04.txt";
	public static final String TC005_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase05\\TC_05.txt";
	public static final String TC006_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase06\\TC_06.txt";
	public static final String TC007_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase07\\TC_07.txt";
	public static final String TC008_Received_Reconcile_Statement_Flag_File = "Received_Statement_Reconcile_Statement_Flag\\TestCase08\\TC_08.txt";
	public static final String TC033_OtherCases_Incoming = "Other Cases\\TestCase33\\MT103Incoming29.txt";
	public static final String TC033_OtherCases_Outgoing = "Other Cases\\TestCase34\\MT103Outgoing29.txt";
	public static final String TC033_OtherCases_Statement = "Other Cases\\TestCase33\\AllMatchingStatus29.txt";
	public static final String TC034_OtherCases_Incoming = "Other Cases\\TestCase34\\MT202Incoming.txt";
	public static final String TC034_OtherCases_Empty = "Other Cases\\TestCase34\\Empty MT940.txt";
	public static final String TC034_OtherCases_MatchedStatement = "Other Cases\\TestCase34\\AutoMatchINMT202.txt";
	

	public static String Path_AutoIT = Path_TestData + "/" + "AutoIT/FileUpload.exe";

	public static final String File_TestData = Path_TestData + "\\" + "TestCases.xlsx";
	public static final String File_TestData1 = Path_TestData + "\\" + "TestData1.xlsx";

	// Application_Info sheet columns
	public static final int Col_ApplicationDetails = 1;
	public static final int Col_ApplicationData = 2;

	// Test Case sheet column names
	public static final int Col_testcaseName = 1;
	public static final int Col_tcDescription = 2;
	public static final int Col_browser = 3;
	public static final int Col_URL = 4;
	public static final int Col_userName = 5;
	public static final int Col_password = 6;
	public static final int Col_inputFileName = 7;
	public static final int Col_sampleDirectory = 2;
	public static final int Col_status = 3;
	public static final int Col_exeDateTime = 4;
	public static final int Col_outputsPath = 5;
	public static final int Col_comments = 6;
	public static final int Col_Elecomments = 7;
//	public static final int Col_inputFileName1=2;

	// Statements sheet column names
	public static final int Col_statement_Party = 2;
	public static final int Col_statement_Service = 3;
	public static final int Col_statement_InputFormat = 4;
	public static final int Col_statement_PSA = 5;
	public static final int Col_statement_InputFileName = 6;
	public static final int Col_statement_InputFileName2=7;
	public static final int Col_Return_InputFileName=7;

	// view details
	public static final int Col_View_sampleName = 1;
	public static final int Col_View_transactionID = 2;
	public static final int Col_View_Actual_view_details = 3;
	public static final int Col_View_Expected_view_details = 4;
	public static final int Col_View_status = 5;

	// BPSUI Entities test data
	public static final int Col_EntityNum = 1;
	public static final int Col_Session = 2;
	public static final int Col_PEN_SUBMITTED = 3;
	public static final int Col_PEN_ACK = 4;
	public static final int Col_PEN_Rej = 5;
	public static final int Col_USD_SUBMITTED = 6;
	public static final int Col_USD_ACK = 7;
	public static final int Col_USD_Rej = 8;
	public static final int Col_Submitted_Total = 9;
	public static final int Col_Ack_Total = 10;
	public static final int Col_Rej_Total = 11;
	public static final int status = 12;

	public static final int view_status = 5;
	// ActiveMQ TRIGGERS sheet of Test_Data workbook column names
	public static final int Col_QueueName = 1;
	public static final int Col_JsonMsg = 2;

	// Test data sheet column names
	public static final int roleName = 2;

	// Test Case Users column names
	public static final int Col_testcaseName_user = 1;
	public static final int Col_userId = 2;
	public static final int Col_FirstName = 3;
	public static final int Col_LastName = 4;
	public static final int Col_EmailId = 5;
	public static final int Col_Password = 6;
	public static final int Col_ConfirmPassword = 7;
	public static final int Col_RoleId = 8;
	public static final int Col_effectiveDate = 9;


	// FileUpload sheet columns

	public static final int Col_Party = 2;
	public static final int Col_Service = 3;
	public static final int Col_InputFormat = 4;
	public static final int Col_PSA = 5;
	public static final int Col_pmtConfStatus = 6;
	public static final int Col_CT_In_IncomingPaymentConfirmationStatus = 7;

	public static final int Col_Party2 = 7;
	public static final int Col_Service2 = 8;
	public static final int Col_InputFormat2 = 9;
	public static final int Col_PSA2 = 5;
	public static final int Col_InputFileName2 = 10;
	public static final int Col_InputFileName3 = 11;

	// Advanced Search Columns
	public static final int Col_SearchPSA = 5;
	public static final int Col_MOP = 7;
	public static final int Col_PaymentStatus = 14;
	public static final int Col_InstructionId = 15;
	public static final int Col_IMAD=16; 
	public static final int Col_pRefrence=10;

	//DB Output Folder Path
	public static final String Path_DBData = "DBOutput";
	
	// Test Case Roles column names
	public static final int Col_TestcaseName = 1;
	public static final int Col_RoleName = 2;

	// MT940statements Sheet columns
	public static final int Col_accountNumber = 3;
	public static final int Col_OrigInstructionId = 2;
	
	public static final int Col_incomingPayment = 5;
	public static final int Col_outgoingPayment = 6;
	public static final int Col_statementFile = 7;
	public static final int Col_emptyPayment = 8;
	
	//MT900 sheet columns
	public static final int Col_MT900 = 2;
	public static final int Col_outboundPayment = 3;
	public static final int Col_amhResponse = 4;
	
	//MT910 sheet columns
	public static final int Col_BECI = 2;
	public static final int Col_incomingPayment_MT910 = 4;
	
	//Sanity TCs
	public static final int Col_IncomingPayment = 2;
	public static final int Col_OutgoingPayment = 3;
	public static final int Col_AccountNumber = 4;
	public static final int Col_BECID = 5;
	public static final int Col_paymentDetails = 6;
	public static final int Col_debitFundControl = 7;
	public static final int Col_finacle_Pos_1 = 8;
	public static final int Col_finacle_Pos_2 = 9;
	public static final int Col_out_File = 10;
	public static final int Col_MT900_out_File = 11;
	public static final int Col_Stmt_File = 12;
	
	//Others
	public static final int Col_AMH_Response = 3;
	public static final int Col_OutgoingPaymentOthers = 4;
	public static final int Col_requestFile = 5;
	public static final int Col_requestFile2 = 6;

	//Values
	public static final String listViewClassData = "cmmonBtnColors";
}
