package com.gs.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.gs.objectRepo.Login;
import com.gs.objectRepo.Logout;
import com.gs.objectRepo.SecurityPage;
import com.gs.pages.ReceivedInstruction;
import com.gs.pages.SideBarMenu;
import com.gs.tc.ICCM.F50.TC001_MT103Outgoing50F_CUST_1238;
import com.paulhammant.ngwebdriver.NgWebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LoginLogout_ClientDefects {

	public static String uName = null;
	public static String uNameForUser = null;
	public static String pwdForUser = null;
	public static Logger log;
	public static WebDriver driver = null;
	public static JavascriptExecutor js;
	public static ArrayList<String> tabs2 = null;
	public static String testcaseName = null;
	public static ExtentReports report;
	public static ExtentReports report1;
	public static ExtentTest test;
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static NgWebDriver ngWebDriver = null;
	
	public static String TestCaseName;
	public static  int TestCaseRow;
	public static String TC_Outputs;
	public static String Screenshots=null;
	public static String exceptionerror = null;
	public static String assertionerror = null;
	public static String tokenFromStorage;


	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isLinux() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

	}

	public static boolean isSolaris() {

		return (OS.indexOf("sunos") >= 0);

	}

	public static String returnEnv() {
		String env = null;
		System.out.println(OS);

		if (isWindows()) {
			env = "windows";
		} else if (isMac()) {
			env = "mac";
		} else if (isLinux()) {
			env = "linux";
		} else if (isSolaris()) {
			env = "solaris";
		} else {
			env = "Unsupported";
		}
		return env;

	}

	@Parameters("browserConfig") 
	@BeforeSuite
	
	public static void login(@Optional String browserConfig) throws Exception {
		System.out.println("Before suite");
		if(browserConfig != null) {
			System.out.println(browserConfig);
		}else {
			browserConfig = "";
		}
		try {
			report = new ExtentReports(Constants.extendedreport_ClientDefects);
			report.loadConfig(new File(System.getProperty("user.dir")+"\\ReportsConfig.xml"));
		
			System.out.println("extendedreport path:" + Constants.extendedreport_ClientDefects);
			String envDetails = returnEnv();
			System.out.println("envDetails::::" + envDetails);
			System.out.println("Inside Before suite login");
			log = Logger.getLogger("Loginlogout");

			if (envDetails.equals("windows")) {
				System.setProperty("webdriver.chrome.silentOutput", "true");//to be verified
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver.exe");
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("download.default_directory", System.getProperty("user.dir")+"\\Downloads");
				ChromeOptions capability = new ChromeOptions();
				capability.setExperimentalOption("prefs", chromePrefs);
				capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
				capability.addArguments("--allow-running-insecure-content");
//				DesiredCapabilities resolution = new DesiredCapabilities();
//				resolution.setCapability("resolution", "1024x768");
				
				//capability.setPageLoadStrategy(PageLoadStrategy.NORMAL);
				
				if (browserConfig.equals("headless")) {
					capability.addArguments("--headless");						
				}
				else if (browserConfig.equals("incognito")) {
					capability.addArguments("--incognito");					
				}
				
				driver = new ChromeDriver(capability);
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
			} else if (envDetails.equals("linux")) {
				System.setProperty("webdriver.chrome.driver", "Resources/chromedriver");
				driver = new ChromeDriver();
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
			}

			js = (JavascriptExecutor) driver;

			// Launch Application
			String baseURL = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Operator",
					"Application_URL", 4);

			String baseURLApprover = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Approver",
					"Application_URL", 4);

			uName = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Operator", "Application_URL",
					5);

			String pwd = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Operator",
					"Application_URL", 6);

			uNameForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Approver", "Application_URL", 5);

			pwdForUser = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData, "Approver", "Application_URL", 6);

			System.out.println("URL is :" + baseURL);
			System.out.println("UN is :" + uName);
			System.out.println("PWD is :" + pwd);
			Login.launch(driver, baseURL);
			WaitLibrary.waitForAngular(driver);
			log.info("Browser launched");
			System.out.println("Browser Launched");
			Login.usrName(driver).sendKeys(uName);
			Login.password(driver).sendKeys(pwd);
			WebElement lgnButton = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnButton, 30).click();
			System.out.println("User Logged In");
			
			WebElement operatorProfile =  Logout.userprofileBtn(driver, uName);
			WaitLibrary.waitForElementToBeVisible(driver, operatorProfile, Constants.avg_explicit);

			log.info("Open new tab and enter volpay hub url");
			js = (JavascriptExecutor) driver;
			js.executeScript("window.open()");

			log.info("Open new tab and control is switch to current tab");
			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			Login.launch(driver, baseURLApprover);
			WaitLibrary.waitForAngular(driver);
			System.out.println("UN is :" + uNameForUser);
			System.out.println("PWD is :" + pwdForUser);
			WebElement userNm = Login.usrName(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userNm, Constants.avg_explicit).sendKeys(uNameForUser);
			WebElement userPwd = Login.password(driver);
			WaitLibrary.waitForElementToBeVisible(driver, userPwd, Constants.avg_explicit).sendKeys(pwdForUser);
			WebElement lgnBtnApprover = Login.loginButton(driver);
			WaitLibrary.waitForElementToBeClickable(driver, lgnBtnApprover, Constants.avg_explicit).click();
			System.out.println("Approver Logged In");
			
			WebElement approverProfile =  Logout.userprofileBtn(driver, uNameForUser);
			WaitLibrary.waitForElementToBeVisible(driver, approverProfile, Constants.avg_explicit);
			
			WaitLibrary.waitForAngular(driver);
			js.executeScript("arguments[0].click();", SecurityPage.security(driver));
			js.executeScript("arguments[0].click();", SecurityPage.approvals(driver));
			driver.switchTo().window(tabs2.get(0));
			
			tokenFromStorage = (String) js.executeScript("return sessionStorage.getItem(\"SessionToken\")");
			System.out.println("Session Token is : " + tokenFromStorage);

		} catch (Exception ej) {
			ej.printStackTrace();
//			driver.close();
//			driver.quit();
		}

	}

	@BeforeClass

	public void beforeMethod() throws Exception 
	{
		
		TestCaseName = Utils.getTestCaseName(this.toString());
		log = Logger.getLogger(TestCaseName);
		System.out.println("TestCase Name is "+TestCaseName);


//		report = new ExtentReports(Constants.extendedreport);
//		report.loadConfig(new File(System.getProperty("user.dir")+"\\ReportsConfig.xml"));
//		
////		Class TestCaseName = TestCaseName.class;  
////        Package pkg = TestCaseName.getPackage(); //get the package   
////        System.out.println(pkg.getName());
//		
//		System.out.println("extendedreport path:" + Constants.extendedreport);
		
		
		System.out.println("path::::"+Constants.File_TestData);
		ExcelUtilities.setExcelFile(Constants.File_TestData,"TestCases");

		TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.File_TestData,"TestCases", TestCaseName, Constants.Col_testcaseName);
		System.out.println("Testcase row number is "+TestCaseRow);
		String All_OUTPUTS=FileUtilities.createAllOutputs(Constants.Path_TestData);

		TC_Outputs=FileUtilities.createParentDir(All_OUTPUTS,TestCaseName);
		System.out.println(TC_Outputs);
		test =report.startTest(TestCaseName);
		((ExtentTest) test).log(LogStatus.INFO, "Test Case : " + TestCaseName);
		Screenshots=FileUtilities.createsubDir(TC_Outputs, "Screenshots");
		
		FileUtils.cleanDirectory(new File(System.getProperty("user.dir")+"\\Downloads"));
		
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, "");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_status, "");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, "");
		ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_Elecomments, "");
	}
	
	@AfterClass
	public void afterMethod() throws Exception
	{
		
		try{
			System.out.println("After class method");
			WebElement payModule = SideBarMenu.paymentModule(driver);
			Actions action = new Actions(driver);
			action.moveToElement(payModule).build().perform();
			js.executeScript("window.scrollBy(0,-120)");
			Thread.sleep(Constants.tooshort_sleep);
			js.executeScript("arguments[0].click();", payModule);
			// To clear previous searched instruction id
			WebElement recInstructions = ReceivedInstruction.recInsTab(driver);
//			WaitLibrary.waitForElementToBeClickable(driver, recInstructions, Constants.avg_explicit).click();
			js.executeScript("arguments[0].click();", recInstructions);		
			WebElement searchInstruction =  ReceivedInstruction.searchWithInsID(driver);
			searchInstruction.clear();
			searchInstruction.sendKeys(Keys.ENTER);
			WaitLibrary.waitForAngular(driver);
			
			js.executeScript("arguments[0].click();", SideBarMenu.homeModule(driver));
			js.executeScript("arguments[0].click();", SideBarMenu.myProfile(driver));
			SideBarMenu.myProfileHeadding(driver);
			Thread.sleep(Constants.tooshort_sleep);
			action.moveToElement(payModule).build().perform();
			js.executeScript("window.scrollBy(0,-120)");
			Thread.sleep(Constants.tooshort_sleep);
			js.executeScript("arguments[0].click();", payModule);
			js.executeScript("arguments[0].click();", SideBarMenu.allPayments(driver));
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_exeDateTime, GenericFunctions.getCurrentTime());
		}
		
		catch(Exception ez)
		{
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow, Constants.Col_comments, ez.getMessage());
			System.out.println(ez.getMessage());
			CaptureScreenshot.captureScreenshot();
			((ExtentTest) test).log(LogStatus.FAIL, "<br>"+"<b>Test Failed</b>"+"<br>"+TestCaseName+LoginLogout_F50.test.addScreenCapture(CaptureScreenshot.screenshotName));		
		}

	}



	@AfterSuite
	public void Userlogout() throws Exception {
		System.out.println("Inside Before suite logout");

		try {
			System.out.println("Writing into Extent reports");
			report.endTest(null);
			report.flush();
			System.out.println("Extent report generated");
			Actions act = new Actions(driver);
			act.sendKeys(Keys.ESCAPE);
			act.moveToElement(Logout.userprofileBtn(driver, uName)).build().perform();
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));
			WaitLibrary.waitForAngular(driver);
			System.out.println("Logout successfully");
			WaitLibrary.waitForElementToBeVisible(driver, Login.loginButton(driver), Constants.avg_explicit);
			Thread.sleep(Constants.tooshort_sleep);
			js = (JavascriptExecutor) driver;

			tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			act.sendKeys(Keys.ESCAPE);
			act.moveToElement(Logout.userprofileBtn(driver, uNameForUser)).build().perform();
			js.executeScript("arguments[0].click();", driver.findElement(By.linkText("Log Out")));
			WaitLibrary.waitForAngular(driver);
			System.out.println("Logout successfully");
			WaitLibrary.waitForElementToBeVisible(driver, Login.loginButton(driver), Constants.avg_explicit);
			Thread.sleep(Constants.tooshort_sleep);
			driver.close();
			driver.quit();

		} catch (Exception en) {
			en.printStackTrace();

		}

	}

}
