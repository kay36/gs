package com.gs.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DBManager {
	public static Properties prop;
	public static Connection getDbConn() throws SQLException, ClassNotFoundException, IOException {
		prop= WinSCPFIleUplaod.readPropertiesFile(System.getProperty("user.dir")+"\\TestData\\GlobalData.properties");
		Connection con=null;
		if(con==null){
				Class.forName("oracle.jdbc.driver.OracleDriver");
				con=DriverManager.getConnection(prop.getProperty("url"),prop.getProperty("DBuserName"),prop.getProperty("DBpassWord")); 
		}	
		return con;
	}

	public static void main(String[] args) throws Exception {
		String testcaseName="MT103OutgoingEUR";

		
		//updateInstructionIdInExcel(fileName, sheetName, testcaseName, instructionId);
		
		getOFACNotificationsFromDB("OFEC_"+testcaseName, "7128826");

			
		}
/*		ResultSet rs = executeQuery("SELECT PAYMENTID,ORIGINALPAYMENTREFERENCE,ORIGINALFILEREFERENCE,OUTPUTINSTRUCTIONID FROM VPHADMIN.PAYMENTCONTROLDATA WHERE INSTRUCTIONID=\'4656215\'");
		//ResultSet rs = executeQuery(prop.getProperty("GetData").replace("INSTRUCTIONID1", "4656215"));
		String paymentID = null;
		while (rs.next()) {
				paymentID = rs.getString("PAYMENTID");
			 	originalPaymentRef  = rs.getString("ORIGINALPAYMENTREFERENCE");
			 	originalFileRef = rs.getString("ORIGINALFILEREFERENCE");
			 	outputInstrId = rs.getString("OUTPUTINSTRUCTIONID");
			 	//uniqueOutputRef = rs.getString("UNIQUEOUTPUTREFERENCE");
			 	System.out.println("PAYMENTID :" + rs.getString("PAYMENTID"));
			 	System.out.println("ORIGINALPAYMENTREFERENCE :" + rs.getString("ORIGINALPAYMENTREFERENCE"));
			 	System.out.println("ORIGINALFILEREFERENCE :" + rs.getString("ORIGINALFILEREFERENCE"));
				}
		
				rs.close();
				//rs = executeQuery("SELECT * FROM VPHADMIN.ACCOUNTPOSTINGS WHERE PAYMENTID=\'"+paymentID+"\'");
				rs = executeQuery(prop.getProperty("AccountPostings").replace("PAYMENTID1", paymentID));
				 updateDBDataToCSV(testcaseName, "ACCOUNT_POSTINGS", rs);	
				 
				// rs = executeQuery("SELECT * FROM ACKDETAILS WHERE INSTRUCTIONID=\'6883373\'");
				 rs = executeQuery(prop.getProperty("AckDetails").replace("INSTRID", "6883373"));
				 getBLOBDataFromDBInTxtFile(testcaseName,"ACK","ACKFile", rs,"ACKDATA");
				 
				 
			//	 rs = executeQuery("SELECT * FROM VPHADMIN.ACCOUNTPOSTINGS WHERE PAYMENTID=\'I20010800019415\'");
			//	 getDoutputInstrIdataFromDBInTxtFile(testcaseName,"ACK_OUTPUTS","ACK_Outputs", rs,"POSTINGOVERRIDEINDICATOR");
				 
				// rs = executeQuery("SELECT * FROM VPHADMIN.BANKINTERACTIONDATA WHERE PAYMENTID=\'"+paymentID+"\'");
				 rs = executeQuery(prop.getProperty("BankInteraction").replace("PAYMENTID1", paymentID));
				 getBLOBBankInteractionDataFromDBInTxtFile(testcaseName,"BANK_INTERACTION","BECNOTIFICATION", rs,"OBJECT");
				
			//	 rs = executeQuery("SELECT * FROM VPHADMIN.ACCOUNTPOSTINGS WHERE PAYMENTID=\'I20010800019415\'");
			//	 getDataFromDBInTxtFile(testcaseName,"CONFIRMATION","confirmation", rs,"POSTINGOVERRIDEINDICATOR");
				
				 if(outputInstrId!=null) {
			     //rs = executeQuery("SELECT * FROM VPHADMIN.PAYMENTOUTPUTDATA WHERE UNIQUEOUTPUTREFERENCE=\'"+outputInstrId+"\'");
			     rs = executeQuery(prop.getProperty("UniqueOutRef").replace("OUTINSTRID", outputInstrId));
			     getDataFromDBInTxtFile(testcaseName,"OUTPUTS_POD",testcaseName, rs,"OUTPUTMESSAGE");
				 }else {
				 rs = executeQuery(prop.getProperty("UniqueOutRef").replace("OUTINSTRID", originalPaymentRef));
				// rs = executeQuery("SELECT * FROM VPHADMIN.PAYMENTOUTPUTDATA WHERE UNIQUEOUTPUTREFERENCE=\'"+originalPaymentRef+"\'");
				 getDataFromDBInTxtFile(testcaseName,"OUTPUTS_POD",testcaseName, rs,"OUTPUTMESSAGE"); 
				 }
	 */
	
    static XSSFWorkbook workbook;
	public static void updateInstructionIdInExcel(String fileName, String sheetName,String testcaseName, String instructionId) {
	
		try {
			FileInputStream fis = new FileInputStream(new File(fileName));
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet(sheetName);
			if (workbook.getNumberOfSheets() != 0) {
		        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
		           if (workbook.getSheetName(i).equals(sheetName)) {
		        	   sheet = workbook.getSheet(sheetName);
		            }
		        }
		    }
			//get active rows
			int totalRows = sheet.getLastRowNum()+1;
			for (int i = 1; i < totalRows; i++) {
				XSSFRow r = sheet.getRow(i);
				String ce = r.getCell(1).getStringCellValue();
				if (ce.contains(testcaseName)) {
					r.createCell(2).setCellValue(instructionId);
					fis.close();
					System.out.println("InstructionId updated");
					FileOutputStream fos = new FileOutputStream(new File(fileName));
					workbook.write(fos);
					fos.close();
					break;
				}
			}
			
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	public static void updateDataFromDBToExcel(String fileName, String sheetName,String testcaseName,String InstructionID, ResultSet rs) {
		
		try {
				 File file = new File(fileName);
				    // Check file existence 
				    if (file.exists() == false) {
				        // Create new file if it does not exist;
				        workbook = new XSSFWorkbook();
				    } else {
				        try ( 
				            // Make current input to exist file
				            InputStream is = new FileInputStream(file)) {
				                workbook = new XSSFWorkbook(is);
				            }
				    }
				Sheet spreadsheet = workbook.getSheet(sheetName);
				if (workbook.getNumberOfSheets() != 0) {
				        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				           if (workbook.getSheetName(i).equals(sheetName)) {
				        	    spreadsheet = workbook.getSheet(sheetName);
				            }
				        }
				    }

			//get active rows
			int totalRows = spreadsheet.getLastRowNum();
			for (int i = 1; i <= totalRows; i++) {
				Row r = spreadsheet.getRow(i);
				String ce = r.getCell(1).getStringCellValue();
				if (ce.contains(testcaseName)) {
	
					ResultSetMetaData rsmd = rs.getMetaData();
					List<String> columns = new ArrayList<String>();
							for(int a = 1; a<=rsmd.getColumnCount();a++) {
							columns.add(rsmd.getColumnLabel(a));
						}
						System.out.println(columns);	
				       // ResultSetMetaData metaData = rs.getMetaData();
				        Row headerRow = spreadsheet.getRow(0);
				 
				        // exclude the first column which is the ID field
				        for (int j = 3; j <= columns.size(); j++) {
				        
				            String columnName = columns.get(j-3);
				            headerRow.createCell(0).setCellValue("Serial Number");
				            headerRow.createCell(1).setCellValue("Test Case Name");
				            headerRow.createCell(2).setCellValue("Instructionn ID");
				            headerRow.createCell(j).setCellValue(columnName);
				        }
				        	        
						while (rs.next()) {
							//int lastRow = spreadsheet.getLastRowNum();
							 int testCaseRow = ExcelUtilities.getRowContainsBySheetName(fileName,sheetName, testcaseName, Constants.Col_testcaseName);
							for(int k=3; k< columns.size();k++) {
								//spreadsheet.getRow(testCaseRow).createCell(column)
								spreadsheet.getRow(testCaseRow).createCell(k).setCellValue(Objects.toString(rs.getObject(columns.get(k-3)),""));
								//row.
							}
					}
	
					System.out.println("Data updated");
					FileOutputStream fos = new FileOutputStream(new File(fileName));
					workbook.write(fos);
					fos.close();
					break;
		}
		
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet executeQuery(String query) throws ClassNotFoundException, SQLException, IOException {
		Connection conn = getDbConn();//("jdbc:oracle:thin:@172.31.91.67:1521:ORCL", "vphreadyonly", "vphreadonly123");
		System.out.println(conn);
		if(conn!=null) {
		 PreparedStatement ps = conn.prepareStatement(query);
		 return ps.executeQuery();
	}
		return null;
	}
	

	public static BufferedWriter fileWriter;
    public static void updateDBDataToCSV(String testScriptName, String csvFileName, ResultSet rs)
            throws SQLException, IOException {
        String testOutput = FileUtilities.createParentDir(
                FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"), testScriptName);
        String excelPath = FileUtilities.createsubDir(testOutput, "ACCOUNT_POSTING");
        csvFileName = excelPath + "/" + csvFileName + ".csv";
        int columnCount = 0;
        fileWriter = new BufferedWriter(new FileWriter(csvFileName));
        int j = 0;
        while (rs.next()) {
            if (j == 0) {
                columnCount = writeHeaderLine(rs);
            }
            j++;
            String line = "";
            for (int i = 1; i <= columnCount; i++) {
                Object valueObject = rs.getObject(i);
                String valueString = "";
                if (valueObject != null)
                    valueString = valueObject.toString();
                if (valueObject instanceof String) {
                    valueString = "\"" + escapeDoubleQuotes(valueString) + "\"";
                }
                line = line.concat(valueString);
                if (i != columnCount) {
                    line = line.concat(",");
                }
            }
            fileWriter.newLine();
            fileWriter.write(line);
        }
        fileWriter.close();
    }

	public static void getDataFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName,ResultSet rs,String columnName) throws IOException, SQLException {	
		int increase=0;
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 		
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\"+ txtFileName;

     increase++;
     File file = new File(txtFileName1+increase+ ".txt");			
				   try {
				    String content = rs.getString(columnName);
				    file.createNewFile();
				    FileWriter fw = new FileWriter(file.getAbsoluteFile());
				    BufferedWriter bw = new BufferedWriter(fw);
				    bw.write(content);
				    bw.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	
	
	public static void getBLOBDataFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName,ResultSet rs,String columnName) throws IOException, SQLException {	
		int increase=0;
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 		
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\"+ txtFileName;

     increase++;
     File file = new File(txtFileName1+increase+ ".txt");
     FileOutputStream output = new FileOutputStream(file);
				   try {
					//InputStream input = rs.getBinaryStream(columnName); 
					Blob input = rs.getBlob(columnName);
					InputStream binaryInput = input.getBinaryStream();
					long len = input.length();
				    byte[] buffer = new byte[(int) len];
				    while (binaryInput.read(buffer) > 0) {
				    	output.write(buffer);
				    }
				    output.flush();
				    output.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	
	public static void getBLOBDataFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName,ResultSet rs,String columnName,int increase) throws IOException, SQLException {	
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 		
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\"+ txtFileName;

     File file = new File(txtFileName1+increase+ ".txt");
     FileOutputStream output = new FileOutputStream(file);
				   try {
					//InputStream input = rs.getBinaryStream(columnName); 
					Blob input = rs.getBlob(columnName);
					InputStream binaryInput = input.getBinaryStream();
					long len = input.length();
				    byte[] buffer = new byte[(int) len];
				    while (binaryInput.read(buffer) > 0) {
				    	output.write(buffer);
				    }
				    output.flush();
				    output.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	
	public static void getBLOBAckDataFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName,ResultSet rs,String columnName) throws IOException, SQLException {	
		int increase=0;
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 		
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\"+ txtFileName;

     increase++;
     File file = new File(txtFileName1.concat("_")+rs.getString("ACKMESSAGETYPE").concat("_")+increase+ ".txt");
     FileOutputStream output = new FileOutputStream(file);
				   try {
					//InputStream input = rs.getBinaryStream(columnName); 
					Blob input = rs.getBlob(columnName);
					InputStream binaryInput = input.getBinaryStream();
					long len = input.length();
				    byte[] buffer = new byte[(int) len];
				    while (binaryInput.read(buffer) > 0) {
				    	output.write(buffer);
				    }
				    output.flush();
				    output.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	
	public static void getBLOBBankInteractionDataFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName, ResultSet rs,String columnName) throws IOException, SQLException {	
		int increase=0;
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 	
			 increase++;
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\";

     File file = new File(txtFileName1+rs.getString("RELATIONSHIP").concat("_").concat(rs.getString("INVOCATIONPOINT")).concat("_")+increase+ ".txt");
     FileOutputStream output = new FileOutputStream(file);
				   try {
					//InputStream input = rs.getBinaryStream(columnName); 
					Blob input = rs.getBlob(columnName);
					InputStream binaryInput = input.getBinaryStream();
					long len = input.length();
				    byte[] buffer = new byte[(int) len];
				    while (binaryInput.read(buffer) > 0) {
				    	output.write(buffer);
				    }
				    output.flush();
				    output.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	
	public static void getBLOBAckOutputsFromDBInTxtFile(String testScriptName,String outputSubDirName,String txtFileName, ResultSet rs,String columnName) throws IOException, SQLException {	
		int increase=0;
		String testOutput = FileUtilities.createParentDir(FileUtilities.createOutputDirectory(Constants.Path_DBData, "DBOUTPUT"),testScriptName);
		while (rs.next()) { 	
			 increase++;
			String filePath = FileUtilities.createsubDir(testOutput, outputSubDirName);
			System.out.println("filePath "+filePath);
			String txtFileName1 = filePath+"\\";

     File file = new File(txtFileName1+rs.getString("ACKDATA").concat("_")+increase+ ".txt");
     FileOutputStream output = new FileOutputStream(file);
				   try {
					//InputStream input = rs.getBinaryStream(columnName); 
					Blob input = rs.getBlob(columnName);
					InputStream binaryInput = input.getBinaryStream();
					long len = input.length();
				    byte[] buffer = new byte[(int) len];
				    while (binaryInput.read(buffer) > 0) {
				    	output.write(buffer);
				    }
				    output.flush();
				    output.close();
				    System.out.println("Done");
				}catch (IOException e){
				   }
		}
		rs.close();
	}
	public static int writeHeaderLine(ResultSet result) throws SQLException, IOException {
		//write header line containing column names
		ResultSetMetaData metaData = result.getMetaData();
		int numberOfColumns = metaData.getColumnCount();
		String headerLine = "";
		// exclude the first column which is the ID field
		for (int i = 1; i <= numberOfColumns; i++) {
		String columnName = metaData.getColumnName(i);
		headerLine = headerLine.concat(columnName).concat(",");
		}
		fileWriter.write(headerLine.substring(0, headerLine.length() - 1));

		return numberOfColumns;
		}
		 
	public static String escapeDoubleQuotes(String value) {
		 return value.replaceAll("\"", "\"\"");
		}
	
	
	public static void getOFACNotificationsFromDB(String testcaseName, String instructionId) throws ClassNotFoundException, SQLException, IOException {
		
		try {
			String outputInstrId="";
			String originalPaymentRef = "";
			String paymentID = "";
			
			prop = WinSCPFIleUplaod.readPropertiesFile(System.getProperty("user.dir")+"\\TestData\\GlobalData.properties");
			ResultSet rs = executeQuery(prop.getProperty("GetMultiColumnData").replace("INSTRUCTIONID1", instructionId));
			while (rs.next()) {
				paymentID = rs.getString("PAYMENTID");
				originalPaymentRef = rs.getString("ORIGINALPAYMENTREFERENCE");
				//rs.getString("ORIGINALFILEREFERENCE");
				outputInstrId = rs.getString("OUTPUTINSTRUCTIONID");
				System.out.println("PAYMENTID :" + rs.getString("PAYMENTID"));
				System.out.println("ORIGINALPAYMENTREFERENCE :" + rs.getString("ORIGINALPAYMENTREFERENCE"));
				System.out.println("OUTPUTINSTRUCTIONID :" + outputInstrId);
			}
			rs.close();
			rs = executeQuery(prop.getProperty("AccountPostings").replace("PAYMENTID1", paymentID));
			updateDBDataToCSV(testcaseName, "ACCOUNT_POSTINGS", rs);

			rs = executeQuery(prop.getProperty("AckDetails").replace("INSTRID", instructionId));
			getBLOBAckDataFromDBInTxtFile(testcaseName, "ACK", "ACKFile", rs, "ACKDATA");

			rs = executeQuery(prop.getProperty("BankInteraction").replace("PAYMENTID1", paymentID));
			getBLOBBankInteractionDataFromDBInTxtFile(testcaseName, "BANK_INTERACTION", "BECNOTIFICATION", rs,
					"OBJECT");
			
			if (outputInstrId != null) {
				rs = executeQuery(prop.getProperty("UniqueOutRef").replace("OUTINSTRID", outputInstrId));
				getBLOBDataFromDBInTxtFile(testcaseName, "OUTPUTS_POD", testcaseName, rs, "OUTPUTMESSAGE");
			} else {
				rs = executeQuery(prop.getProperty("UniqueOutRef").replace("OUTINSTRID", originalPaymentRef));
				getBLOBDataFromDBInTxtFile(testcaseName, "OUTPUTS_POD", testcaseName, rs, "OUTPUTMESSAGE");
			}
			rs.close();
			List<String> respINSTRIDLst = new ArrayList<String>();
			 rs = executeQuery(prop.getProperty("Confirmation").replace("pmtId", paymentID));
			while (rs.next()) {
				respINSTRIDLst.add(rs.getString("RESPINSTRID"));	
			}
			for(int i =0; i<respINSTRIDLst.size();i++) {
			ResultSet rs1= executeQuery(prop.getProperty("ConfByRespInstrId").replace("respInstrId", respINSTRIDLst.get(i)));				
			getBLOBDataFromDBInTxtFile(testcaseName, "CONFIRMATIONS", "CONFIRMATION_", rs1, "DATA",i);
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
