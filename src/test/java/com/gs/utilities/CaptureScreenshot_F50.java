package com.gs.utilities;



import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.io.Files;
import com.gs.objectRepo.AllConfirmations;
import com.gs.objectRepo.AllPaymentsPage;
import com.gs.objectRepo.RecInspage;


public class CaptureScreenshot_F50 
{
	
	public static JavascriptExecutor js=null;
	
	
	
	public static void getScreenshot(WebDriver driver,String fileName) throws Exception
	{

		File src= ((TakesScreenshot)driver). getScreenshotAs(OutputType.FILE);

		FileUtils. copyFile(src, new File("./Screenshots/"+fileName+".png"));
	}

	public static void getScreenshotInPath(WebDriver driver,String path,String fileName) throws Exception
	{
		File src= ((TakesScreenshot)driver). getScreenshotAs(OutputType.FILE);

		FileUtils. copyFile(src, new File(path+"//"+fileName+".png"));
	}
	
	public static void getExtCommScreen(WebDriver driver,String Screenshots,String pmtId) throws Exception
	{
		js = (JavascriptExecutor)driver;
		//AllPaymentsPage.goToAllPayments(driver).click();
		

		WebElement goToAllPayments = AllPaymentsPage.goToAllPayments(driver);
		WaitLibrary.waitForElementToBeClickable(driver, goToAllPayments, 55).click();


		Thread.sleep(1500);
		//AllPaymentsPage.selectPaymentId(driver, pmtId).click();
		WebElement selectPaymentId = AllPaymentsPage.selectPaymentId(driver, pmtId);
		WaitLibrary.waitForElementToBeClickable(driver, selectPaymentId, 55).click();

		Thread.sleep(1500);
		js.executeScript("arguments[0].click();", AllPaymentsPage.goToExtComm(driver));
		BrowserResolution.scrollDown(driver);
		Thread.sleep(1500);
		BrowserResolution.zoomOut(driver);
		Thread.sleep(1500);
		CaptureScreenshot_F50.getScreenshotInPath(driver, Screenshots, "External_Communications");
		BrowserResolution.defaultResol(driver);
		//AllPaymentsPage.goToAllPayments(driver).click();
		WebElement goToAllPayments1 = AllPaymentsPage.goToAllPayments(driver);
		WaitLibrary.waitForElementToBeClickable(driver, goToAllPayments1, 55).click();

	}
	
	
	public static void getReceivedInstScreenShot(WebDriver driver,String Screenshots) throws Exception
	{
		//RecInspage.recInsTab(driver).click();
		WebElement recInsTab = RecInspage.recInsTab(driver);
		WaitLibrary.waitForElementToBeClickable(driver, recInsTab, 55).click();
		Thread.sleep(1000);
		CaptureScreenshot_F50.getScreenshotInPath(driver,Screenshots, "Received_Instructions");
		
		
	}
	
	public static void getAllConfirmationsScreenShot(WebDriver driver,String Screenshots) throws Exception
	{
		//AllConfirmations.goToAllConfirmations(driver).click();
		WebElement goToAllConfirmations = AllConfirmations.goToAllConfirmations(driver);
		WaitLibrary.waitForElementToBeClickable(driver, goToAllConfirmations, 55).click();
		Thread.sleep(1000);
		CaptureScreenshot_F50.getScreenshotInPath(driver,Screenshots, "All_confirmations");
		
		
	}
	

	   public static String screenshotName;
	   public static void captureScreenshot() throws IOException {
	   File srcFile = ((TakesScreenshot)LoginLogout_F50.driver).getScreenshotAs(OutputType.FILE);
	   java.util.Date dt= new java.util.Date();
	   screenshotName = "./" +"reports/screenshots/" +dt.toString().replace(":", "_").replace(" ", "_") + ".png";
	   File path = new File("./reports/",screenshotName);
	   FileUtils.copyFile(srcFile,path);
	   }
	   
	   public static String captureScreenshot(String screenShotPath) throws IOException {
	   File srcFile = ((TakesScreenshot)LoginLogout_F50.driver).getScreenshotAs(OutputType.FILE);
	 //  java.util.Date dt= new java.util.Date();
	 //  screenshotName = "./" +"reports/screenshots/" +dt.toString().replace(":", "_").replace(" ", "_") + ".png";
	   File path = new File(screenShotPath);
	   FileUtils.copyFile(srcFile,path);
	   return screenShotPath;
	   }

	
}
