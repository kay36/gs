package com.gs.utilities;

import org.openqa.selenium.WebElement;

import com.gs.objectRepo.AllPaymentsPage;
import com.gs.objectRepo.RecInspage;

public class payemetRefresh {

	public static void refreshforsinglestatus(String pmtStatus, String expectedpmtStatus) throws InterruptedException {
		if (!pmtStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (pmtStatus.equals(expectedpmtStatus)) {
					break;
				} else {

					pmtStatus = RecInspage.getTransportNameStatus(LoginLogout_ICCM.driver).getText();
					System.out.println("refreshforsinglestatus" + pmtStatus);
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
				}
			}

			// Assert.assertEquals(pmtStatus, expectedpmtStatus);
		}
	}

	public static void refreshForWaitInRROFStatus(String sRROFStatus, String expectedpmtStatus)
			throws InterruptedException {
		if (!sRROFStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (sRROFStatus.equals(expectedpmtStatus)) {
					break;
				} else {

					sRROFStatus = RecInspage.getStatusWaitInRROF(LoginLogout_ICCM.driver).getText();
					System.out.println("Refresh For WAIT IN RROF Status: " + sRROFStatus);
					BrowserResolution.scrollUp(LoginLogout_ICCM.driver);
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
					BrowserResolution.scrollDown(LoginLogout_ICCM.driver);
				}
			}

			// Assert.assertEquals(pmtStatus, expectedpmtStatus);
		}
	}

	public static void refreshForProcessed(String sRROFStatus, String expectedpmtStatus)
			throws InterruptedException {
		if (!sRROFStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (sRROFStatus.equals(expectedpmtStatus)) {
					break;
				} else {

					sRROFStatus = AllPaymentsPage.getStatusProcessed(LoginLogout_ICCM.driver).getText();
					System.out.println("Refresh For Status: " + sRROFStatus);
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
				}
			}
		}
	}
	
	public static void refreshForWaitInRRFIStatus(String sRROFStatus, String expectedpmtStatus)
			throws InterruptedException {
		if (!sRROFStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (sRROFStatus.equals(expectedpmtStatus)) {
					break;
				} else {

					sRROFStatus = AllPaymentsPage.getStatusWaitInRRFI(LoginLogout_ICCM.driver).getText();
					System.out.println("Refresh For Status: " + sRROFStatus);
					BrowserResolution.scrollUp(LoginLogout_ICCM.driver);
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
					BrowserResolution.scrollDown(LoginLogout_ICCM.driver);
				}
			}
		}
	}
	
	public static void refreshForCompletedStatus(String sRROFStatus, String expectedpmtStatus)
			throws InterruptedException {
		if (!sRROFStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (sRROFStatus.equals(expectedpmtStatus)) {
					break;
				} else {

					sRROFStatus = AllPaymentsPage.status_CompletedRFI(LoginLogout_ICCM.driver).getText();
					System.out.println("Refresh For Status: " + sRROFStatus);
					BrowserResolution.scrollUp(LoginLogout_ICCM.driver);
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
					BrowserResolution.scrollDown(LoginLogout_ICCM.driver);
				}
			}
		}
	}

	public static void refreshfortwostatuses(String pmtStatus, String expectedpmtStatus1, String expectedpmtStatus2)
			throws InterruptedException {
		if (!pmtStatus.equals(expectedpmtStatus1) || !pmtStatus.equals(expectedpmtStatus2)) {
			for (int i = 0; i < 15; i++) {
				if (pmtStatus.equals(expectedpmtStatus1) || pmtStatus.equals(expectedpmtStatus2)) {
					break;
				} else {
					pmtStatus = RecInspage.getTransportNameStatus(LoginLogout_ICCM.driver).getText();
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
				}
			}

			// Assert.assertEquals(pmtStatus, expectedpmtStatus);
		}
	}

	public static void refreshforthreestatuses(String pmtStatus, String expectedpmtStatus1, String expectedpmtStatus2,
			String expectedpmtStatus3) throws InterruptedException {
		if (!pmtStatus.equals(expectedpmtStatus1) || !pmtStatus.equals(expectedpmtStatus2)
				|| !pmtStatus.equals(expectedpmtStatus3)) {
			for (int i = 0; i < 15; i++) {
				if (pmtStatus.equals(expectedpmtStatus1) || pmtStatus.equals(expectedpmtStatus2)
						|| pmtStatus.equals(expectedpmtStatus3)) {
					break;
				} else {
					pmtStatus = RecInspage.getTransportNameStatus(LoginLogout_ICCM.driver).getText();
					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
				}
			}

			// Assert.assertEquals(pmtStatus, expectedpmtStatus);
		}
	}

	public static void refreshforstatementstatus(String rec_fileStatus, String expectedpmtStatus, String fileName)
			throws InterruptedException {
		if (!rec_fileStatus.equals(expectedpmtStatus)) {
			for (int i = 0; i < 15; i++) {
				if (rec_fileStatus.equals(expectedpmtStatus)) {
					break;
				} else {
					WebElement rec_fileStatusid = RecInspage.getTransportNameStatusByFileName(LoginLogout_ICCM.driver,
							fileName);
					rec_fileStatus = WaitLibrary.waitForElementToBeVisible(LoginLogout_ICCM.driver, rec_fileStatusid, 60)
							.getText();

					RecInspage.refresh(LoginLogout_ICCM.driver).click();
					Thread.sleep(10000);
				}
			}

			// Assert.assertEquals(pmtStatus, expectedpmtStatus);
		}
	}

}
