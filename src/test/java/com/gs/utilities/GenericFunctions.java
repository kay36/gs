package com.gs.utilities;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import java.util.Calendar;
//import java.util.List;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.gs.objectRepo.FileUploadPage;

public class GenericFunctions {

	public static JavascriptExecutor js = null;

	public static int TestCaseRow;
	public static String sPreviousDate;
	/*
	 * public static String getCurrentTime() { Calendar cal =
	 * Calendar.getInstance(); SimpleDateFormat sdf = new
	 * SimpleDateFormat("HH:mm:ss"); return sdf.format(cal.getTime());
	 * //System.out.println( sdf.format(cal.getTime()) ); }
	 */

	/**
	 * @return getPreviousCalendarDate
	 */
	public static String getPreviousCalendarDate(int i) {
		Calendar cal = Calendar.getInstance();
		// subtracting a day
		cal.add(Calendar.DATE, i);

		SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
		sPreviousDate = s.format(new Date(cal.getTimeInMillis()));
		System.out.println(sPreviousDate + "-----Entered");

		return sPreviousDate;
	}

	public static String getCurrentTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);

	}

	public static String getCurrentTimeOnly() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);

	}

	public static String getCurrentDateOnly(String format) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);

	}

	public static void updateCurrentDate(String path, String date) throws Exception {
		File file = new File(path);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		FileWriter fw = new FileWriter(path);
		BufferedWriter bw = new BufferedWriter(fw);
		String sCurrentLine;
		// int counter = 0;

		while ((sCurrentLine = br.readLine()) != null) {
			// System.out.println(sCurrentLine);
			if (sCurrentLine.startsWith("1")) {
				sCurrentLine = sCurrentLine.replaceAll(sCurrentLine.substring(22, 30), date);
				bw.write(sCurrentLine + "\n");
				// System.out.println(sCurrentLine);

			}

			if (sCurrentLine.startsWith("5")) {
				sCurrentLine = sCurrentLine.replaceAll(sCurrentLine.substring(63, 71), date);
				sCurrentLine = sCurrentLine.replaceAll(sCurrentLine.substring(71, 78), date);
				bw.write(sCurrentLine + "\n");

				// System.out.println(sCurrentLine);

			}

		}
		br.close();
		bw.flush();
		bw.close();

	}

	public static void updateCTDate(String filepath) throws Exception {
		String date1 = null;
		String date = null;
		String date2 = null;
		File inputFile = new File(filepath);
		BufferedReader br = new BufferedReader(new FileReader(inputFile));
		BufferedWriter bw = null;
		String line1 = null;
		// String line2 = null;
		// int count = 1;
		String line = null;
		while ((line = br.readLine()) != null) {

			if (line.startsWith("1")) {
				date = line.substring(22, 30);
				line = line.replaceAll(date, GenericFunctions.getCurrentDateOnly("yyyyMMdd"));
				bw = new BufferedWriter(new FileWriter(filepath));
				bw.write(line + "\r\n");
			} else if (line.startsWith("5")) {
				date1 = line.substring(69, 77);
				date2 = line.substring(77, 85);
				line = line.replaceAll(date1, GenericFunctions.getCurrentDateOnly("yyyyMMdd"));
				line = line.replaceAll(date2, GenericFunctions.getCurrentDateOnly("yyyyMMdd"));
				bw = new BufferedWriter(new FileWriter(filepath));
				bw.write(line1 + "\r\n");
			} else {
				bw.write(line + "\r\n");
			}

			/*
			 * else if(line.startsWith("5")) { line2=line.replaceAll(date,
			 * genericFunctions.getCurrentDateOnly("yyyyMMdd")); bw.write(line2+"\n"); }
			 */
			// count++;
		}
		br.close();
		bw.flush();
		bw.close();

	}

	public static boolean selectByValue(WebDriver driver, WebElement locator, String value) {
		boolean flag;
		try {
			Select s = new Select(locator);
			s.selectByVisibleText(value);
			System.err.println("Value is:::" + value);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public static void UpdateFlatFile(String filePath, String oldValue, String newValue) throws Exception {

		File fileToBeModified = new File(filePath);

		String oldContent = "";

		BufferedReader reader = null;

		FileWriter writer = null;

		try {
			reader = new BufferedReader(new FileReader(fileToBeModified));

			// Reading all the lines of input text file into oldContent

			String line = reader.readLine();

			while (line != null) {
				oldContent = oldContent + line + System.lineSeparator();

				line = reader.readLine();
			}

			// Replacing oldString with newString in the oldContent

			String newContent = oldContent.replaceAll(oldValue, newValue);

			// Rewriting the input text file with newContent

			writer = new FileWriter(fileToBeModified);

			writer.write(newContent);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// Closing the resources

				reader.close();

				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/*
	 * public static void uploadSelectedFile(WebDriver driver,String
	 * TestCaseName,String sheetName,String AutoITfilePath,String filePath,String
	 * fileName) throws Exception {
	 * 
	 * //System.out.println("TestCase name in UploadSelectedFile fn is :"
	 * +TestCaseName);
	 * //ExcelUtilities.setExcelFile(Constants.File_TestData1,sheetName);
	 * 
	 * //TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.
	 * File_TestData,"TestCases", TestCaseName, Constants.Col_TestCaseName);
	 * //TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.
	 * File_TestData1,sheetName, TestCaseName, Constants.Col_TestCaseName);
	 * //System.out.println("TestCase row number in fileupload sheet is:"
	 * +TestCaseRow);
	 * 
	 * fileUploadPage.fileUploadSec(driver).click(); Thread.sleep(3000);
	 * 
	 * fileUploadPage.setParty(driver).click(); Thread.sleep(1000);
	 * 
	 * //String value = ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_Party); String
	 * party=ExcelUtilities.getCellDataBySheetName(VOCCONSTANTS.File_TestData1,
	 * sheetName, TestCaseName, VOCCONSTANTS.Col_CTOUT_UI_Party);
	 * //System.out.println("File name  :"+value);
	 * //Log.info("Select one party from dropdown list ");
	 * fileUploadPage.setPartyTxtBox(driver).sendKeys(party); Thread.sleep(2000);
	 * fileUploadPage.setPartyTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000);
	 * 
	 * //Log.info("Select one service"); fileUploadPage.setService(driver).click();
	 * Thread.sleep(1000); //String value1 = ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_Service); String
	 * service=ExcelUtilities.getCellDataBySheetName(VOCCONSTANTS. File_TestData1,
	 * sheetName, TestCaseName, VOCCONSTANTS.Col_CTOUT_UI_Service);
	 * //System.out.println("Vlaue2::"+service);
	 * fileUploadPage.setServiceTxtBox(driver).sendKeys(service);
	 * Thread.sleep(2000);
	 * fileUploadPage.setServiceTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000);
	 * 
	 * //Log.info("Select one 'Input format'");
	 * fileUploadPage.setInputFormat(driver).click(); Thread.sleep(1000); //String
	 * Value3=ExcelUtilities.getCellData(TestCaseRow,Constants.Col_InputFormat);
	 * String inputFormat=ExcelUtilities.getCellDataBySheetName(VOCCONSTANTS.
	 * File_TestData1, sheetName, TestCaseName,
	 * VOCCONSTANTS.Col_CTOUT_UI_InputFormat);
	 * //System.out.println("Input Format is :"+inputFormat);
	 * fileUploadPage.setInputFormatTxtBox(driver).sendKeys(inputFormat);
	 * Thread.sleep(2000);
	 * fileUploadPage.setInputFormatTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000); fileUploadPage.setPSA(driver).click();
	 * Thread.sleep(4000); String value4=ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_PSA); System.out.println(value4);
	 * fileUploadPage.setPSATxtBox(driver).sendKeys(value4); Thread.sleep(2000);
	 * fileUploadPage.setPSATxtBox(driver).sendKeys(Keys.ENTER);
	 * //Log.info("Select one file from Drive");
	 * fileUploadPage.chooseFile(driver).click();
	 * 
	 * //Runtime.getRuntime().exec(
	 * "D:\\Tools\\Eclipse\\Library\\AutoIT\\FileUpload.exe"); //String
	 * autoITExecutable =
	 * "D:\\BNYMRTP\\workspace\\BNYM\\All_Outputs\\FileUpload.exe " + RFIFileName;
	 * 
	 * //System.out.println("FilePath in UploadSelected fn is :"+filePath); String
	 * autoITExecutable = AutoITfilePath+" "+filePath+"\\"+fileName;
	 * System.out.println(autoITExecutable);
	 * Runtime.getRuntime().exec(autoITExecutable); Thread.sleep(5000);
	 * fileUploadPage.uploadBtn(driver).click(); Thread.sleep(4000); }
	 */

	/*
	 * public static void uploadSelectedFile(WebDriver driver,String
	 * TestCaseName,String sheetName,String AutoITfilePath,String filePath,String
	 * fileName) throws Exception {
	 * 
	 * //System.out.println("TestCase name in UploadSelectedFile fn is :"
	 * +TestCaseName);
	 * //ExcelUtilities.setExcelFile(Constants.File_TestData1,sheetName);
	 * 
	 * //TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.
	 * File_TestData,"TestCases", TestCaseName, Constants.Col_TestCaseName);
	 * //TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.
	 * File_TestData1,sheetName, TestCaseName, Constants.Col_TestCaseName);
	 * //System.out.println("TestCase row number in fileupload sheet is:"
	 * +TestCaseRow);
	 * 
	 * FileUploadPage.fileUploadSec(driver).click(); Thread.sleep(3000);
	 * 
	 * FileUploadPage.setParty(driver).click(); Thread.sleep(1000);
	 * 
	 * //String value = ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_Party); String
	 * party=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,
	 * sheetName, TestCaseName, Constants.Col_Party);
	 * //System.out.println("File name  :"+value);
	 * //Log.info("Select one party from dropdown list ");
	 * FileUploadPage.setPartyTxtBox(driver).sendKeys(party); Thread.sleep(2000);
	 * FileUploadPage.setPartyTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000);
	 * 
	 * //Log.info("Select one service"); FileUploadPage.setService(driver).click();
	 * Thread.sleep(1000); //String value1 = ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_Service); String
	 * service=ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1,
	 * sheetName, TestCaseName, Constants.Col_Service);
	 * //System.out.println("Vlaue2::"+service);
	 * FileUploadPage.setServiceTxtBox(driver).sendKeys(service);
	 * Thread.sleep(2000);
	 * FileUploadPage.setServiceTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000);
	 * 
	 * //Log.info("Select one 'Input format'");
	 * //FileUploadPage.setInputFormat(driver).click(); js.executeScript("argumen",
	 * arg1) Thread.sleep(1000); //String
	 * Value3=ExcelUtilities.getCellData(TestCaseRow,Constants.Col_InputFormat);
	 * String inputFormat=ExcelUtilities.getCellDataBySheetName(Constants.
	 * File_TestData1, sheetName, TestCaseName, Constants.Col_InputFormat);
	 * //System.out.println("Input Format is :"+inputFormat);
	 * FileUploadPage.setInputFormatTxtBox(driver).sendKeys(inputFormat);
	 * Thread.sleep(2000);
	 * FileUploadPage.setInputFormatTxtBox(driver).sendKeys(Keys.ENTER);
	 * Thread.sleep(1000); FileUploadPage.setPSA(driver).click();
	 * Thread.sleep(4000); String value4=ExcelUtilities.getCellData(TestCaseRow,
	 * Constants.Col_PSA); System.out.println(value4);
	 * FileUploadPage.setPSATxtBox(driver).sendKeys(value4); Thread.sleep(2000);
	 * FileUploadPage.setPSATxtBox(driver).sendKeys(Keys.ENTER);
	 * //Log.info("Select one file from Drive");
	 * FileUploadPage.chooseFile(driver).click(); Thread.sleep(2000); File file =
	 * new File(filePath+"\\"+fileName);
	 * //System.out.println("File absolute path:::"+file.getAbsolutePath());
	 * StringSelection stringSelection = new
	 * StringSelection(file.getAbsolutePath());
	 * System.out.println("stringSelection path:::"+stringSelection);
	 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
	 * stringSelection, stringSelection);
	 * 
	 * Robot robot = new Robot();
	 * 
	 * robot.keyPress(KeyEvent.VK_CONTROL); robot.keyPress(KeyEvent.VK_V);
	 * robot.keyRelease(KeyEvent.VK_V); robot.keyRelease(KeyEvent.VK_CONTROL);
	 * robot.keyPress(KeyEvent.VK_ENTER); robot.keyRelease(KeyEvent.VK_ENTER);
	 * Thread.sleep(2000); FileUploadPage.uploadBtn(driver).click();
	 * Thread.sleep(4000);
	 * 
	 * }
	 */

	public static void uploadSelectedFile(WebDriver driver, String TestCaseName, String sheetName,
			String AutoITfilePath, String filePath, String fileName) throws Exception {
		try {
			System.out.println("TestCaseName::::::::" + TestCaseName);
			System.out.println("sheetName::::::::" + sheetName);
			System.out.println("AutoITfilePath::::::::" + AutoITfilePath);
			System.out.println("filePath::::::::" + filePath);
			System.out.println("fileName::::::::" + fileName);
			// Keys.chord(Keys.CONTROL, "-");
			// Keys.chord(Keys.CONTROL, Keys.SUBTRACT);
			// BrowserResolution.zoomOutWithPer(driver, 70);
			// BrowserResolution.zoomOut(driver);
			// Thread.sleep(3000);
			// System.out.println("TestCase name in UploadSelectedFile fn is
			// :"+TestCaseName);
			// ExcelUtilities.setExcelFile(Constants.File_TestData1,sheetName);

			// TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.File_TestData,"TestCases",
			// TestCaseName, Constants.Col_TestCaseName);
			// TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.File_TestData1,sheetName,
			// TestCaseName, Constants.Col_TestCaseName);
			// System.out.println("TestCase row number in fileupload sheet
			// is:"+TestCaseRow);
			// js.executeScript("arguments[0].click();",
			// FileUploadPage.fileUploadSec(driver));
			// FileUploadPage.fileUploadSec(driver).click();
			// Thread.sleep(4000);

			// FileUploadPage.setParty(driver).click();
			// Thread.sleep(3000);
			WebElement setPrty = FileUploadPage.setParty(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setPrty, 30).click();
			Thread.sleep(1000);
			// String value = ExcelUtilities.getCellData(TestCaseRow,
			// Constants.Col_Party);
			String party = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
					Constants.Col_Party);
			// System.out.println("File name :"+value);
			// Log.info("Select one party from dropdown list ");
			FileUploadPage.setPartyTxtBox(driver).sendKeys(party);
			Thread.sleep(2000);
			FileUploadPage.setPartyTxtBox(driver).sendKeys(Keys.ENTER);
			// Thread.sleep(2000);

			// Log.info("Select one service");
			WebElement setSrvce = FileUploadPage.setService(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setSrvce, 30).click();
			// FileUploadPage.setService(driver).click();
			Thread.sleep(1000);
			// String value1 = ExcelUtilities.getCellData(TestCaseRow,
			// Constants.Col_Service);
			String service = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
					Constants.Col_Service);
			// System.out.println("Vlaue2::"+service);
			FileUploadPage.setServiceTxtBox(driver).sendKeys(service);
			Thread.sleep(2000);
			FileUploadPage.setServiceTxtBox(driver).sendKeys(Keys.ENTER);
			// Thread.sleep(3000);

			// Log.info("Select one Debitor Customer Proprietory Code");
			/*
			 * FileUploadPage.setdrCustPropCode(driver).click(); Thread.sleep(1000);
			 * //String value1 = ExcelUtilities.getCellData(TestCaseRow,
			 * Constants.Col_Service); String
			 * drCustPropCode=ExcelUtilities.getCellDataBySheetName(Constants.
			 * File_TestData1, sheetName, TestCaseName,
			 * Constants.Col_Init_Txn_Debtor_Customer_ProprietaryCode);
			 * //System.out.println("Vlaue2::"+service);
			 * FileUploadPage.setdrCustPropCodeTxtBox(driver).sendKeys( drCustPropCode);
			 * Thread.sleep(2000);
			 * FileUploadPage.setdrCustPropCodeTxtBox(driver).sendKeys(Keys. ENTER);
			 */
			// Thread.sleep(1000);

			// Log.info("Select one 'Input format'");
			WebElement setInputFrmt = FileUploadPage.setInputFormat(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setInputFrmt, 30).click();
			// FileUploadPage.setInputFormat(driver).click();
			Thread.sleep(1000);
			// String
			// Value3=ExcelUtilities.getCellData(TestCaseRow,Constants.Col_InputFormat);
			String inputFormat = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName,
					TestCaseName, Constants.Col_InputFormat);
			// System.out.println("Input Format is :"+inputFormat);
			FileUploadPage.setInputFormatTxtBox(driver).sendKeys(inputFormat);
			Thread.sleep(2000);
			FileUploadPage.setInputFormatTxtBox(driver).sendKeys(Keys.ENTER);
			// Thread.sleep(6000);
			/*
			 * fileUploadPage.setPSA(driver).click(); Thread.sleep(4000); String
			 * value4=ExcelUtilities.getCellData(TestCaseRow, Constants.Col_PSA);
			 * System.out.println(value4);
			 * fileUploadPage.setPSATxtBox(driver).sendKeys(value4); Thread.sleep(2000);
			 * fileUploadPage.setPSATxtBox(driver).sendKeys(Keys.ENTER);
			 */
			// Log.info("Select one file from Drive");
			/*
			 * FileUploadPage.chooseFile(driver).click();
			 * 
			 * //Runtime.getRuntime().exec(
			 * "D:\\Tools\\Eclipse\\Library\\AutoIT\\FileUpload.exe"); //String
			 * autoITExecutable =
			 * "D:\\BNYMRTP\\workspace\\BNYM\\All_Outputs\\FileUpload.exe " + RFIFileName;
			 * 
			 * //System.out.println("FilePath in UploadSelected fn is :" +filePath); String
			 * autoITExecutable = AutoITfilePath+" "+filePath+"\\"+fileName;
			 * System.out.println(autoITExecutable);
			 * Runtime.getRuntime().exec(autoITExecutable); Thread.sleep(5000);
			 * FileUploadPage.uploadBtn(driver).click(); Thread.sleep(4000);
			 */

			// FileUploadPage.chooseFile(driver).click();
			System.out.println("Before choose file");
			// System.out.println(""+FileUploadPage.chooseFile(driver).getText());
			// FileUploadPage.chooseFile(driver).click();
			Actions act = new Actions(driver);
			WebElement wb2 = driver.findElement(By.xpath("//input[@file-model='myFile']"));
			act.click(wb2).build().perform();
			// js.executeScript("arguments[0].click();",
			// FileUploadPage.chooseFile(driver));
			System.out.println("After choose file");
			// driver.findElement(By.id("UploadBtn")).sendKeys(Keys.ENTER);
			Thread.sleep(6000);
			// File file = new File(filePath+"\\"+fileName);
			File file = new File(filePath + "/" + fileName);
			System.out.println("File path:::::::::::::::::::::::::::::::::file::::" + file.getAbsolutePath());
			// System.out.println("File absolute
			// path:::"+file.getAbsolutePath());
			String derived_path = file.getAbsolutePath();// .replace("/home/volpayadmin", ".");
			StringSelection stringSelection = new StringSelection(derived_path);// file.getAbsolutePath());
			System.out.println("stringSelection path:::" + stringSelection);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
			Thread.sleep(1000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
						robot.keyPress(KeyEvent.VK_V);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need

			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need.
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);// for windows required thread.sleep for linux it won't need
			FileUploadPage.uploadBtn(driver).click();
			Thread.sleep(4000);
		} catch (Exception em) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_Elecomments, em.getMessage());
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_status, "Failed");
		}
	}

	public static void uploadSelectedSecondFile(WebDriver driver, String TestCaseName, String sheetName,
			String AutoITfilePath, String filePath, String fileName, int sParty, int sService, int sInputFormat)
			throws Exception {
		try {
			System.out.println("TestCaseName::::::::" + TestCaseName);
			System.out.println("sheetName::::::::" + sheetName);
			System.out.println("AutoITfilePath::::::::" + AutoITfilePath);
			System.out.println("filePath::::::::" + filePath);
			System.out.println("fileName::::::::" + fileName);

			WebElement setPrty = FileUploadPage.setParty(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setPrty, 30).click();
			Thread.sleep(1000);

			String party = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
					sParty);

			FileUploadPage.setPartyTxtBox(driver).sendKeys(party);
			Thread.sleep(2000);
			FileUploadPage.setPartyTxtBox(driver).sendKeys(Keys.ENTER);

			WebElement setSrvce = FileUploadPage.setService(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setSrvce, 30).click();
			Thread.sleep(1000);

			String service = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
					sService);
			FileUploadPage.setServiceTxtBox(driver).sendKeys(service);
			Thread.sleep(2000);
			FileUploadPage.setServiceTxtBox(driver).sendKeys(Keys.ENTER);

			WebElement setInputFrmt = FileUploadPage.setInputFormat(driver);
			WaitLibrary.waitForElementToBeClickable(driver, setInputFrmt, 30).click();
			Thread.sleep(1000);

			String inputFormat = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName,
					TestCaseName, sInputFormat);
			FileUploadPage.setInputFormatTxtBox(driver).sendKeys(inputFormat);
			Thread.sleep(2000);
			FileUploadPage.setInputFormatTxtBox(driver).sendKeys(Keys.ENTER);

			System.out.println("Before choose file");

			Actions act = new Actions(driver);
			WebElement wb2 = driver.findElement(By.xpath("//input[@file-model='myFile']"));
			act.click(wb2).build().perform();

			System.out.println("After choose file");
			Thread.sleep(6000);
			File file = new File(filePath + "/" + fileName);
			System.out.println("File path:::::::::::::::::::::::::::::::::file::::" + file.getAbsolutePath());
			String derived_path = file.getAbsolutePath();// .replace("/home/volpayadmin", ".");
			StringSelection stringSelection = new StringSelection(derived_path);// file.getAbsolutePath());
			System.out.println("stringSelection path:::" + stringSelection);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
			Thread.sleep(1000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyPress(KeyEvent.VK_V);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);// for windows required thread.sleep for linux it won't need
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);// for windows required thread.sleep for linux it won't need
			FileUploadPage.uploadBtn(driver).click();
			Thread.sleep(4000);
		} catch (Exception em) {
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_Elecomments, em.getMessage());
			ExcelUtilities.setCellDataBySheetName(Constants.File_TestData, "TestCases", TestCaseRow,
					Constants.Col_status, "Failed");
		}
	}

	public static void uploadSelectedFileForStatements(WebDriver driver, String TestCaseName, String sheetName,
			String AutoITfilePath, String filePath, String fileName) throws Exception {

		System.out.println("TestCaseName::::::::" + TestCaseName);
		System.out.println("sheetName::::::::" + sheetName);
		System.out.println("AutoITfilePath::::::::" + AutoITfilePath);
		System.out.println("filePath::::::::" + filePath);
		System.out.println("fileName::::::::" + fileName);
		// Keys.chord(Keys.CONTROL, "-");
		// Keys.chord(Keys.CONTROL, Keys.SUBTRACT);
		// BrowserResolution.zoomOutWithPer(driver, 70);
		// BrowserResolution.zoomOut(driver);
		// Thread.sleep(3000);
		// System.out.println("TestCase name in UploadSelectedFile fn is
		// :"+TestCaseName);
		// ExcelUtilities.setExcelFile(Constants.File_TestData1,sheetName);

		// TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.File_TestData,"TestCases",
		// TestCaseName, Constants.Col_TestCaseName);
		// TestCaseRow=ExcelUtilities.getRowContainsBySheetName(Constants.File_TestData1,sheetName,
		// TestCaseName, Constants.Col_TestCaseName);
		// System.out.println("TestCase row number in fileupload sheet
		// is:"+TestCaseRow);
		// js.executeScript("arguments[0].click();",
		// FileUploadPage.fileUploadSec(driver));
		// FileUploadPage.fileUploadSec(driver).click();
		Thread.sleep(3000);

		FileUploadPage.setParty(driver).click();
		Thread.sleep(1000);

		// String value = ExcelUtilities.getCellData(TestCaseRow,
		// Constants.Col_Party);
		String party = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
				Constants.Col_statement_Party);
		// System.out.println("File name :"+value);
		// Log.info("Select one party from dropdown list ");
		FileUploadPage.setPartyTxtBox(driver).sendKeys(party);
		Thread.sleep(2000);
		FileUploadPage.setPartyTxtBox(driver).sendKeys(Keys.ENTER);
		Thread.sleep(1000);

		// Log.info("Select one service");
		FileUploadPage.setService(driver).click();
		Thread.sleep(1000);
		// String value1 = ExcelUtilities.getCellData(TestCaseRow,
		// Constants.Col_Service);
		String service = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
				Constants.Col_statement_Service);
		// System.out.println("Vlaue2::"+service);
		FileUploadPage.setServiceTxtBox(driver).sendKeys(service);
		Thread.sleep(2000);
		FileUploadPage.setServiceTxtBox(driver).sendKeys(Keys.ENTER);
		Thread.sleep(1000);

		// Log.info("Select one Debitor Customer Proprietory Code");
		/*
		 * FileUploadPage.setdrCustPropCode(driver).click(); Thread.sleep(1000);
		 * //String value1 = ExcelUtilities.getCellData(TestCaseRow,
		 * Constants.Col_Service); String
		 * drCustPropCode=ExcelUtilities.getCellDataBySheetName(Constants.
		 * File_TestData1, sheetName, TestCaseName,
		 * Constants.Col_Init_Txn_Debtor_Customer_ProprietaryCode);
		 * //System.out.println("Vlaue2::"+service);
		 * FileUploadPage.setdrCustPropCodeTxtBox(driver).sendKeys( drCustPropCode);
		 * Thread.sleep(2000);
		 * FileUploadPage.setdrCustPropCodeTxtBox(driver).sendKeys(Keys.ENTER);
		 */
		Thread.sleep(1000);

		// Log.info("Select one 'Input format'");
		FileUploadPage.setInputFormat(driver).click();
		Thread.sleep(1000);
		// String
		// Value3=ExcelUtilities.getCellData(TestCaseRow,Constants.Col_InputFormat);
		String inputFormat = ExcelUtilities.getCellDataBySheetName(Constants.File_TestData1, sheetName, TestCaseName,
				Constants.Col_statement_InputFormat);
		// System.out.println("Input Format is :"+inputFormat);
		FileUploadPage.setInputFormatTxtBox(driver).sendKeys(inputFormat);
		Thread.sleep(2000);
		FileUploadPage.setInputFormatTxtBox(driver).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		/*
		 * fileUploadPage.setPSA(driver).click(); Thread.sleep(4000); String
		 * value4=ExcelUtilities.getCellData(TestCaseRow, Constants.Col_PSA);
		 * System.out.println(value4);
		 * fileUploadPage.setPSATxtBox(driver).sendKeys(value4); Thread.sleep(2000);
		 * fileUploadPage.setPSATxtBox(driver).sendKeys(Keys.ENTER);
		 */
		// Log.info("Select one file from Drive");
		/*
		 * FileUploadPage.chooseFile(driver).click();
		 * 
		 * //Runtime.getRuntime().exec(
		 * "D:\\Tools\\Eclipse\\Library\\AutoIT\\FileUpload.exe"); //String
		 * autoITExecutable =
		 * "D:\\BNYMRTP\\workspace\\BNYM\\All_Outputs\\FileUpload.exe " + RFIFileName;
		 * 
		 * //System.out.println("FilePath in UploadSelected fn is :"+filePath); String
		 * autoITExecutable = AutoITfilePath+" "+filePath+"\\"+fileName;
		 * System.out.println(autoITExecutable);
		 * Runtime.getRuntime().exec(autoITExecutable); Thread.sleep(5000);
		 * FileUploadPage.uploadBtn(driver).click(); Thread.sleep(4000);
		 */

		// FileUploadPage.chooseFile(driver).click();
		// Thread.sleep(2000);
		Actions act = new Actions(driver);
		WebElement wb2 = driver.findElement(By.xpath("//input[@file-model='myFile']"));
		act.click(wb2).build().perform();
		File file = new File(filePath + "/" + fileName);
		System.out.println("File path:::::::::::::::::::::::::::::::::file::::" + file.getAbsolutePath());
		// System.out.println("File absolute path:::"+file.getAbsolutePath());
		StringSelection stringSelection = new StringSelection(file.getAbsolutePath());
		System.out.println("stringSelection path:::" + stringSelection);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);

		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_CONTROL);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(1000);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_SHIFT);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		FileUploadPage.uploadBtn(driver).click();
		// Actions act = new Actions(driver);
		// WebElement wb2 =
		// driver.findElement(By.xpath("//input[@file-model='myFile']"));
		// act.click(wb2).build().perform();
		Thread.sleep(7000);
	}

}
