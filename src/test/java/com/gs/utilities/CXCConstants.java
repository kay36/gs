package com.gs.utilities;

public class CXCConstants {
	
	
	  public static final String Path_TestData=System.getenv("TestSuite");
	  public static String DownloadsPath=System.getenv("Downloads");
	  public static String Deployment=System.getenv("Deployment");
	    
	  /*public static final String Path_TestData = "E:\\UdayKumar\\To_Uday\\workspace\\BNYMRTP\\CXC_TestData";
	  public static String DownloadsPath="C:\\Users\\pc\\Downloads";
	  public static String Deployment="E:\\UdayKumar\\BNYM\\BNYM\\Releases\\BNYM_Release-5\\apache-tomcat";*/
	  
	  
	  public static final String Path_CXC_TR = Path_TestData+"\\"+"CXC_Transport";
	  public static final String Path_TR_Confirmations=Path_CXC_TR+"\\"+"Confirmations";
	  
	  
	  
	  public static final String File_TestCases = Path_TestData+"\\"+"CXC_Test_Cases.xlsx";
	  public static final String File_TestData = Path_TestData+"\\"+"CXC_Test_Data.xlsx";
	  
	  
	  
	  
	  //CXC_Transport
	  
	  public static final String Path_CXC_TR_IN_Samples=Path_TestData+"\\"+"CXC_Transport"+"\\"+"Input_Files";
	  
	  
	  //TestCases sheet columns of CXC_Test_cases.xlsx
	  
	//TestCases Sheet of TestData.xlsx workbook
		public static final int Col_TestCaseName = 1;
		public static final int Col_TCDescription=2;
		public static final int Col_URL=3;
		public static final int Col_UserName =4;
		public static final int Col_Password = 5;
		public static final int Col_InputFileName = 6;
		//public static final int Col_Browser = 5;
		public static final int Col_Result = 7;
		public static final int Col_ExecutionTime=8;
		
		public static final int Col_TestOutputs=9;
		public static final int Col_StatusMsg=10;
		public static final int Col_Comments=11;
		
		
		//Application_Info sheet columns
		public static final int Col_ApplicationDetails=1;
		public static final int Col_ApplicationData=2;
		
		
		
		//Deployment_Details
		
		public static String Ack=Deployment+"\\VPH_messages\\Ack\\ALL_RPX_TCH_ACK";
		public static String ALL_RPX_TCH_IN=Deployment+"\\VPH_messages\\Inputs\\ALL_RPX_TCH_IN";
		public static String RPX_ISOPacs008_Tr_IN=Deployment+"\\VPH_messages\\Inputs\\9696620013_RPX_ISOPacs008_Tr_IN";
		
		
		//CXC_Confirmation_Paths
		public static String RPXACH_EchoIn_CONF=Deployment+"\\VPH_messages\\Confirmation\\RPXACH\\EchoIn";
		public static String RPXACH_ConfirmationIn=Deployment+"\\VPH_messages\\Confirmation\\RPXACH\\ConfirmationIn";
		public static String RPXACH_ReturnIn=Deployment+"\\VPH_messages\\Confirmation\\RPXACH\\ReturnIn";
		public static String RPXACH_AdjustmentIn=Deployment+"\\VPH_messages\\Confirmation\\RPXACH\\AdjustmentIn";
		
		//TestData Sheet 'CXC' sheet column names
		public static final int Col_CXC_Echo_Confirmation_FileNames=2;
		public static final int Col_CXC_Confirmation_FileNames=3;
		public static final int Col_CXC_MOP=4;
	  

}
