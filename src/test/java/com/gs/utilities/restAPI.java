package com.gs.utilities;

import io.restassured.RestAssured;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class restAPI extends LoginLogout_ICCM{
	
	public static ResponseBody getpostAPIResponse(String restPath, String requestBody, Headers headers) {
				
		//Specific URI
		RestAssured.baseURI = Constants.baseRESTURL;
		
		//Request Object
		RequestSpecification httpRequest = RestAssured.given().relaxedHTTPSValidation();
		
		// Setting request body
		httpRequest.body(requestBody);
		
		// Setting header
		httpRequest.headers(headers);
		
		//Get Response 
		Response response = httpRequest.request(Method.POST,restPath);
		
		// Get Response body
		ResponseBody body = response. getBody();
		
//		// print response 
//		System.out.println("Response : "+body.asPrettyString());
//							
//		// status code validation
//		System.out.println("Status code : "+response.getStatusCode());
		
		return body;
	}

}
