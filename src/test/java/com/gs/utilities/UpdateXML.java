package com.gs.utilities;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UpdateXML {
	
	public static void updateIN_REMT(String src,String InstrId,String RmtId)
	{
		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName("ar:Refs");
			NodeList nList2 = doc.getElementsByTagName("ar:RmtInf");
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					eElement.getElementsByTagName("ar:InstrId").item(0).setTextContent(InstrId);

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					eElement.getElementsByTagName("ar:RmtId").item(0).setTextContent(RmtId);

				}
			}
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	//To Update camt.026.001.05
	
	public static void updateIN_Camt26(String src,String tagvalue1,String tagvalue2)
	{
		String SRC_Path=src;
		String OrgnlMsgId=tagvalue1;
		String OrgnlInstrId=tagvalue2;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("fi:OrgnlGrpInf");
			NodeList nList2 = doc.getElementsByTagName("fi:IntrBk");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("fi:OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());

				}
			}
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());

				}
			}
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("camt.026.001.05.xml file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	//IN_CAMT35
	public static void updateIN_CAMT35(String src,String InstrId)
	{
		String SRC_Path=src;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName("ac:OrigRefs");
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					eElement.getElementsByTagName("ac:InstrId").item(0).setTextContent(InstrId);

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTP-CAMT035.xml file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	//OUT_CAMT35
	public static void updateOUT_CAMT35(String src,String InstrId)
	{
		String SRC_Path=src;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName("OrigRefs");
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					eElement.getElementsByTagName("InstrId").item(0).setTextContent(InstrId);

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("ISOCAMT035_ForISOCustomer.XML file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	public static void updateOUT_Camt26(String src,String tagvalue1,String tagvalue2)
	{
		String SRC_Path=src;
		String OrgnlMsgId=tagvalue1;
		String OrgnlInstrId=tagvalue2;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("OrgnlGrpInf");
			NodeList nList2 = doc.getElementsByTagName("IntrBk");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());

				}
			}
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());

				}
			}
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("OUT_camt.026.001.05.xml file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	
	public static void updateXMLByValue(String src,String tagvalue)
	{
		String SRC_Path=src;
		String msgID=tagvalue;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList = doc.getElementsByTagName("fi:Undrlyg");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).setTextContent(msgID);


					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("fi:OrgnlInstrId").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("camt.026.001.05.xml file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}

	public static void updateUSRTPACS008ForISOCustomerIncomingByValue(String src,String tagvalue)
	{
		String SRC_Path=src;
		String msgID=tagvalue;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList = doc.getElementsByTagName("ct:RfrdDocInf");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("ct:Nb").item(0).setTextContent(msgID);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPACS008ForISOCustomer_CreditTransferMatchWithROF.xml file updated with provided value");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	//To Update CT-Incoming file after RFP Request
	public static void updateRFPCTIncoming(String src,String txnId)
	{

		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList = doc.getElementsByTagName("ct:PmtId");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("ct:TxId").item(0).setTextContent(txnId);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	
	public static void updateRFPUSRTPPAIN014Incoming(String src,String OrgnlMsgId,String OrgnlPmtInfId )
	{


		String SRC_Path=src;
		//System.out.println("Instruction Id is :"+OrgnlPmtInfId);

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("rp:OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("rp:OrgnlPmtInfAndSts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("rp:OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("rp:OrgnlPmtInfId").item(0).setTextContent(OrgnlPmtInfId);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	
	}
	
	public static void updateRFPISOPAIN014Incoming(String src,String OrgnlMsgId,String OrgnlInstrId )
	{


		String SRC_Path=src;
		//System.out.println("Instruction Id is :"+OrgnlPmtInfId);

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("TxInfAndSts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("ct:Nb : " + eElement.getElementsByTagName("ct:Nb").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	
	}
	
	//To Update USRTPPacs002_Response_Final_RCVD
	public static void updateUSRTPPacs002_Response_Final_RCVD(String src,String tagvalue1)
	{
		String SRC_Path=src;
		String MessageId=tagvalue1;
		//String OrgnlMsgId=tagvalue1;
		//String OrgnlInstrId=tagvalue2;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("ps:OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("ps:TxInfAndSts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlMsgId : " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).setTextContent(MessageId);


					//System.out.println("ps:OrgnlMsgId " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).setTextContent(MessageId);


					//System.out.println("OrgnlInstrId " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPPacs002_Response_Final_RCVD.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	public static void updateUSRTPPacs002_Response(String src,String tagvalue1,String tagvalue2,String status)
	{
		String SRC_Path=src;
		String MessageId=tagvalue1;
		String GrpRefId=tagvalue2;
		//String OrgnlMsgId=tagvalue1;
		//String OrgnlInstrId=tagvalue2;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("ps:OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("ps:TxInfAndSts");
			
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlMsgId : " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).setTextContent(GrpRefId);


					//System.out.println("ps:OrgnlMsgId " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).setTextContent(MessageId);


					//System.out.println("OrgnlInstrId " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());

				}
			}
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:TxSts").item(0).setTextContent(status);


					//System.out.println("OrgnlInstrId " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPPacs002_Response_Final_RCVD.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	//To update ALL_RPX_TCH_RROF_camt.029.001.06_IPAY file
	public static void updateCAMT29XMLByValue(String src,String id,String status)
	{
		String SRC_Path=src;
		String tr_Id=id;
		String tr_Conf=status;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("tr:RslvdCase");
			NodeList nList2 = doc.getElementsByTagName("tr:Sts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("tr:Id").item(0).setTextContent(tr_Id);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Conf- " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());
					eElement.getElementsByTagName("tr:Conf").item(0).setTextContent(tr_Conf);


					//System.out.println("tr:Conf " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("ALL_RPX_TCH_RROF_camt.029.001.06_IPAY.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	
	//To update ALL_RPX_TCH_RROF_camt.029.001.06_IPAY file
	public static void updateCAMT29XMLRROF(String src,String id,String status,String pmtId)
	{
		String SRC_Path=src;
		//String tr_Id=id;
		//String tr_Conf=status;

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			
			NodeList RslvdCase = doc.getElementsByTagName("RslvdCase");
			NodeList Sts = doc.getElementsByTagName("Sts");
			NodeList CxlDtls = doc.getElementsByTagName("CxlDtls");

			for (int temp = 0; temp < RslvdCase.getLength(); temp++) {
				Node nNode = RslvdCase.item(temp);
				

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					
					eElement.getElementsByTagName("Id").item(0).setTextContent(id);
					//eElement.getElementsByTagName("Id").item(1).setTextContent(id);

				}
			}
			
			for (int temp = 0; temp < Sts.getLength(); temp++) {
				Node nNode = Sts.item(temp);
				

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					
					eElement.getElementsByTagName("Conf").item(0).setTextContent(status);

				}
			}
			
			for (int temp = 0; temp < CxlDtls.getLength(); temp++) {
				Node nNode = CxlDtls.item(temp);
				

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					
					eElement.getElementsByTagName("OrgnlInstrId").item(0).setTextContent(pmtId);

				}
			}
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("ALL_RPX_TCH_RROF_camt.029.001.06_IPAY.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	public static void updateCamt56(String src,String OrgnlMsgId,String OrgnlInstrId)
	{

		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("rt:OrgnlGrpInfAndCxl");
			NodeList nList2 = doc.getElementsByTagName("rt:TxInf");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("rt:OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Conf- " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());
					eElement.getElementsByTagName("rt:OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("tr:Conf " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("ALL_RPX_TCH_ROF_ISO_camt.056.001.05.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	
	public static void updateCamt28(String src,String Id)
	{

		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("if:Case");
			
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("if:Id").item(0).setTextContent(Id);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPCamt028.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	
	public static void update_OUT_Camt28(String src,String Id,String InstrId)
	{

		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("Case");
			NodeList nList2 = doc.getElementsByTagName("Inf");
			
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("Id").item(0).setTextContent(Id);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			
			
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("InstrId").item(0).setTextContent(InstrId);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			
			
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			//System.out.println("USRTPCamt028.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	
	public static void updateCamt56_Outgoing(String src,String OrgnlMsgId,String OrgnlInstrId)
	{

		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("OrgnlGrpInfAndCxl");
			NodeList nList2 = doc.getElementsByTagName("TxInf");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("tr:Id " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Conf- " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());
					eElement.getElementsByTagName("OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("tr:Conf " + eElement.getElementsByTagName("tr:Conf").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("Outgoing camt.056.001.05.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	public static String readXMLByTags(String src,String parentTagValue,String subTagValue)
	{
		String SRC_Path=src;
		String value=null;

		try {
			
			String filePath= SRC_Path;
			File inputFile = new File(filePath);
			


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName(parentTagValue);
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					value=eElement.getElementsByTagName(subTagValue).item(0).getTextContent();
				}

			}

		}
		catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		return value;

	}
	
	
	public static void update_USRTPPacs002_Response_Final_RCVD_To_TwoTags(String src,String OrgnlMsgId,String OrgnlInstrId)
	{
		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("ps:OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("ps:TxInfAndSts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlMsgId : " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("ps:OrgnlMsgId " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("OrgnlInstrId " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPPacs002_Response_Final_RCVD.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	
	
	
	public static void updateRFP_USRTPPacs002_Response_Final_RCVD_To_TwoTags(String src,String OrgnlMsgId,String OrgnlInstrId)
	{
		String SRC_Path=src;
		

		try {
			String filePath= SRC_Path;
			File inputFile = new File(filePath);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			//NodeList data= doc.getElementsByTagName("firstname");
			NodeList nList1 = doc.getElementsByTagName("ps:OrgnlGrpInfAndSts");
			NodeList nList2 = doc.getElementsByTagName("ps:TxInfAndSts");
			//System.out.println("----------------------------");

			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlMsgId : " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).setTextContent(OrgnlMsgId);


					//System.out.println("ps:OrgnlMsgId " + eElement.getElementsByTagName("ps:OrgnlMsgId").item(0).getTextContent());

				}
			}
			for (int temp = 0; temp < nList2.getLength(); temp++) {
				Node nNode = nList2.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("OrgnlInstrId : " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());
					eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).setTextContent(OrgnlInstrId);


					//System.out.println("OrgnlInstrId " + eElement.getElementsByTagName("ps:OrgnlInstrId").item(0).getTextContent());

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath));
			transformer.transform(source, result);

			System.out.println("USRTPPacs002_Response_Final_RCVD.xml file updated with provided value");

		}catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	public static String readXMLByFileName(String src,String fileName,String parentTagValue,String subTagValue)
	{
		String SRC_Path=src;
		String value=null;

		try {
			
			//String filePath= SRC_Path;
			String filePath=findFile(SRC_Path, fileName);
			
			File inputFile = new File(filePath);
			
			


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName(parentTagValue);
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					//System.out.println("tr:Id- " + eElement.getElementsByTagName("tr:Id").item(0).getTextContent());
					value=eElement.getElementsByTagName(subTagValue).item(0).getTextContent();
				}

			}

		}
		catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		return value;

	}
	
	public static String findFile(String src,String file)
	{
		int i;
		String folderName = src; 
		File[] listFiles = new File(folderName).listFiles();

		for ( i = 0; i < listFiles.length; i++) {

			if (listFiles[i].isFile()) {
				String fileName = listFiles[i].getName();
				if (fileName.startsWith(file)
						&& fileName.endsWith(".xml")) {
					
					System.out.println("found file" + " " + fileName);
					break;
				}
			}
		}
		return listFiles[i].getAbsolutePath();
	}
	
	/*Provide the tag values in double quotes*/
	public static void setXMLFile_Field(String src,String parentTag,String subTag,String subtagvalue)
	{


		try {
			File inputFile = new File(src);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName(parentTag);
			
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					eElement.getElementsByTagName(subTag).item(0).setTextContent(subtagvalue);

				}
			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(src));
			transformer.transform(source, result);

			System.out.println("Fields <"+subTag+"> updated with value: "+subtagvalue);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	public static String getXMLFile_Field(String src,String parentTag,String subTag)
	{
		String value=null;

		try {
			File inputFile = new File(src);


			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nList1 = doc.getElementsByTagName(parentTag);
			
			for (int temp = 0; temp < nList1.getLength(); temp++) {
				Node nNode = nList1.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					value=eElement.getElementsByTagName(subTag).item(0).getTextContent();

				}
			}
			

			

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		
		return value;
	}
	

}
