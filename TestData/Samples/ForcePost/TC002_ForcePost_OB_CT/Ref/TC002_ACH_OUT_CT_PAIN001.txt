<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.08">
        <CstmrCdtTrfInitn>
            <GrpHdr>
                <MsgId>Outbound CT</MsgId>
                <CreDtTm>2020-12-14T01:22:59.093</CreDtTm>
                <NbOfTxs>1</NbOfTxs>
				<CtrlSum>300</CtrlSum>
                <InitgPty>
                   <Nm>Goldman Sachs FX Business</Nm>
				   <Id>
				   <OrgId>
				   <AnyBIC>GSCRUS30XXX</AnyBIC>
				   <Othr>
				   <Id>BLACKROCK</Id>
				   </Othr>
				   </OrgId>
				   </Id>
				</InitgPty>
            </GrpHdr>
            <PmtInf>
                <PmtInfId>Batch001</PmtInfId>
                <PmtMtd>TRF</PmtMtd>
				<NbOfTxs>1</NbOfTxs>
				<CtrlSum>300</CtrlSum>
                <ReqdExctnDt>
                    <Dt>{{yyyy-MM-dd}}</Dt>
                </ReqdExctnDt>
				<Dbtr>
                    <Nm>Microsofts</Nm> 
                   <Id>
                    <OrgId>
                        <Othr>
                            <Id>8529631475</Id>
                           <SchmeNm>
                           <Cd>CUST</Cd>
                            </SchmeNm>
                        </Othr>
                       </OrgId>
                    </Id>
                </Dbtr>
                <DbtrAcct>
                    <Id>
                        <Othr>
                            <Id>112255446</Id>
                        </Othr>
                    </Id>
					<Ccy>USD</Ccy> 
                </DbtrAcct>
                <DbtrAgt>
				<FinInstnId>
                    <ClrSysMmbId>
								<ClrSysId>
									<Cd>USABA</Cd>
								</ClrSysId>
								<MmbId>026015079</MmbId> <!-- Receiving DFI Identification-->
					</ClrSysMmbId>                 
                </FinInstnId>
				</DbtrAgt>
				
                <CdtTrfTxInf>
                    <PmtId>
                        <EndToEndId>Payment001</EndToEndId>
                    </PmtId>
					<PmtTpInf>
						<LclInstrm>
							<Cd>CCD</Cd>
						</LclInstrm>
						<CtgyPurp>
						  <Prtry>PAYROLL</Prtry>
						</CtgyPurp>
					</PmtTpInf>
                    <Amt>
                        <InstdAmt Ccy="USD">300</InstdAmt>
                    </Amt>
                    <CdtrAgt>
						<FinInstnId>
							<!--<BICFI>BCCBAOLU</BICFI>  Should BIC be Present-->
							<ClrSysMmbId>
								<ClrSysId>
									<Cd>USABA</Cd>
								</ClrSysId>
								<MmbId>121000359</MmbId> <!-- Receiving DFI Identification-->
							</ClrSysMmbId>
						</FinInstnId>
                    </CdtrAgt>	
					<Cdtr>
						<Nm>John T</Nm>
						<PstlAdr>
							<StrtNm>JM ROAD</StrtNm>
							<BldgNb>GS123</BldgNb>
							<PstCd>1024</PstCd>
							<TwnNm>SWIZERLAND</TwnNm>
							<Ctry>CH</Ctry>
							<AdrLine>add1</AdrLine>
							<AdrLine>add2</AdrLine>
						</PstlAdr>
					</Cdtr>
                    <CdtrAcct>
                        <Id>
							<Othr>
								<Id>09900100000999</Id>
							</Othr>
                        </Id>
						<Tp>
							<Cd>CACC</Cd>
						</Tp>
                    </CdtrAcct>
					<Purp>
						<Prtry>PAYROLLTR</Prtry>
					</Purp>
					<RmtInf>
						<Ustrd>ABCD</Ustrd>
					</RmtInf>
                </CdtTrfTxInf>
            </PmtInf>
        </CstmrCdtTrfInitn>
</Document>
