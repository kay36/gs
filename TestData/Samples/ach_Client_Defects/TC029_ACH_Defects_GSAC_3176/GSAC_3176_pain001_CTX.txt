<?xml version="1.0" encoding="UTF-8"?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.08">
        <CstmrCdtTrfInitn>
            <GrpHdr>
                <MsgId>TS51TC01AOA</MsgId>
                <CreDtTm>2020-05-01T01:22:59.093</CreDtTm>
                <NbOfTxs>1</NbOfTxs>
				<CtrlSum>100</CtrlSum>
                <InitgPty>
                   <Nm>IM Name</Nm> <!-- Immediate_Origin Name -->
						<Id>
							<OrgId>
								<Othr>
									<Id>ACH</Id> <!-- Immediate_Origin -->
									<SchmeNm>
										<Cd>CUST</Cd>
									</SchmeNm>
								</Othr>
							</OrgId>
						</Id>
				</InitgPty>
            </GrpHdr>
            <PmtInf>
                <PmtInfId>TS51TC01AOA</PmtInfId> <!-- Batch ID -->
                <PmtMtd>TRF</PmtMtd>
				<NbOfTxs>1</NbOfTxs>
				<CtrlSum>100</CtrlSum>
				<PmtTpInf>
					<InstrPrty>NORM</InstrPrty> <!-- Optional -->
					<SvcLvl>
						<Cd>NURG</Cd>
					</SvcLvl>
					<LclInstrm>
						<Cd>CTX</Cd>
					</LclInstrm>
					<CtgyPurp>
					<Prtry>PAYROLL</Prtry>
				</CtgyPurp>
				</PmtTpInf>
                <ReqdExctnDt>
                    <Dt>2022-05-13</Dt>
                </ReqdExctnDt>
                <Dbtr>
                    <Nm>Blackrock</Nm> <!-- Company Name -->
                    <Id>
                    <OrgId>
                        <Othr>
                            <Id>8529631475</Id>  <!-- Company Id -->
                            <SchmeNm>
                           <Cd>CUST</Cd>
                            </SchmeNm>
                        </Othr>
                       </OrgId>
                    </Id>
                </Dbtr>
                <DbtrAcct>
                    <Id>
                        <Othr>
                            <Id>112255446</Id> <!-- Will be derived -->
                            <SchmeNm> <!-- Optional -->
                           <Cd>AIIN</Cd> <!-- Optional -->
                            </SchmeNm> <!-- Optional -->
                        </Othr>
                    </Id>
					<Ccy>USD</Ccy>
                </DbtrAcct>
                <DbtrAgt>
                    <FinInstnId>
                        <BICFI>GSFXGB2LXXX</BICFI>  <!-- Will BIC come-->
                        <ClrSysMmbId>   <!-- Payer's Bank Identification  -->
                        	<ClrSysId>
                        		<Cd>USABA</Cd>
                        	</ClrSysId>
                        	<MmbId>124085082</MmbId>   <!--Originating DFI ABA or transit routing number-->
                        </ClrSysMmbId>
                    </FinInstnId>
                </DbtrAgt>
                <CdtTrfTxInf>
                    <PmtId>
                        <EndToEndId>TS51TC01AOA</EndToEndId>
                    </PmtId>
                    <Amt>
                        <InstdAmt Ccy="USD">100</InstdAmt>
                    </Amt>
                    <CdtrAgt>
						<FinInstnId>
							<BICFI>BCCBAOLU</BICFI>  <!--Should BIC be Present-->
							<ClrSysMmbId>
								<ClrSysId>
									<Cd>USABA</Cd>
								</ClrSysId>
								<MmbId>738541220</MmbId> <!-- Receiving DFI Identification-->
							</ClrSysMmbId>
						</FinInstnId>
                    </CdtrAgt>	
					<Cdtr>
						<Nm>John T</Nm> <!--Receiving Company Name or Individual Name -->
					</Cdtr>
                    <CdtrAcct>
                        <Id>
                        	<Othr>
                        		<Id>123456763</Id> <!--DFI Account Number The receiver's bank account number-->
                             </Othr>
                         </Id>
                          <Tp>
                             <Cd>CACC</Cd> <!--Transaction Code -->
                           </Tp>
                    </CdtrAcct>
					<Purp>  	<!-- Optional -->
						<Prtry>TXID</Prtry>
					</Purp>
					<RmtInf>
						<Ustrd>ABCD</Ustrd>
						<Ustrd>ABCD</Ustrd>
					</RmtInf>
                </CdtTrfTxInf>
            </PmtInf>
        </CstmrCdtTrfInitn>
</Document>		
