<?xml version="1.0" encoding="UTF-8" ?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.08">
    <CstmrCdtTrfInitn>
        <GrpHdr>
            <MsgId>GSMHGRIZXYCHMYUZC</MsgId>
            <CreDtTm>2020-01-20T01:22:59.093</CreDtTm>
            <NbOfTxs>4</NbOfTxs>
            <CtrlSum>1222.20</CtrlSum>
            <InitgPty>
                <Nm>Continental Petro</Nm>
                <Id>
                    <OrgId>
                        <Othr>
                            <Id>ACH</Id>
                        </Othr>
                    </OrgId></Id>
            </InitgPty>
        </GrpHdr>
        <PmtInf>
            <PmtInfId>IAT CT BATCH 01</PmtInfId>
            <PmtMtd>TRF</PmtMtd>
            <NbOfTxs>4</NbOfTxs>
            <CtrlSum>1222.20</CtrlSum>
            <PmtTpInf>
                <InstrPrty>NORM</InstrPrty>
                <SvcLvl>
                    <Cd>NURG</Cd>
                </SvcLvl>
                <LclInstrm>
                    <Cd>IAT</Cd>
                </LclInstrm>
                <CtgyPurp>
                    <Prtry>REMITTANCE</Prtry>
                </CtgyPurp>
            </PmtTpInf>
            <ReqdExctnDt>
                <Dt>2022-05-03</Dt>
            </ReqdExctnDt>
            <Dbtr>
                <Nm>Continental Petro</Nm>
                <Id>
                    <OrgId>
                        <Othr>
                            <Id>8529631475</Id>
                        </Othr>
                    </OrgId></Id>
            </Dbtr>
            <DbtrAcct>
                <Id>
                    <Othr>
                        <Id>112255446</Id>
                    </Othr></Id>
            </DbtrAcct>
            <DbtrAgt>
                <FinInstnId>
                    <ClrSysMmbId>
                        <ClrSysId>
                            <Cd>USABA</Cd>
                        </ClrSysId>
                        <MmbId>026015079</MmbId>
                    </ClrSysMmbId>
                    <Nm>GS Bank USA,SLC BRANCH</Nm>
                </FinInstnId>
            </DbtrAgt>
            <CdtTrfTxInf>
                <PmtId>
                    <InstrId>TraceNumber 11</InstrId>
                    <EndToEndId>GSNAKP1XKB</EndToEndId>
                </PmtId>
                <Amt>
                    <InstdAmt Ccy="USD" >155.55</InstdAmt>
                </Amt>
                <CdtrAgt>
                    <FinInstnId>
                        <ClrSysMmbId>
                            <ClrSysId>
                                <Cd>USABA</Cd>
                            </ClrSysId>
                            <MmbId>061212510</MmbId>
                        </ClrSysMmbId>
                        <Nm>PNC BANK</Nm>
                    </FinInstnId>
                </CdtrAgt>
                <Cdtr>
                    <Nm>MISSY COOPER</Nm>
                </Cdtr>
                <CdtrAcct>
                    <Id>
                        <Othr>
                            <Id>121232343</Id>
                        </Othr></Id>
                    <Tp>
                        <Cd>CACC</Cd>
                    </Tp>
                </CdtrAcct>
                <Purp>
                    <Prtry>ROYALTY</Prtry>
                </Purp>
                <RmtInf>
                    <Ustrd>SHOW ROYALTY</Ustrd>
                </RmtInf>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Indicator</PlcAndNm>
                    <Envlp>
                        <any>FF</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference_Indicator</PlcAndNm>
                    <Envlp>
                        <any>3</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference</PlcAndNm>
                    <Envlp>
                        <any> </any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Country_Code</PlcAndNm>
                    <Envlp>
                        <any>US</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Originating_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>EUR</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>USD</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Secondary_OFAC_Screening_Indicator</PlcAndNm>
                    <Envlp>
                        <any>0</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>IAT</PlcAndNm>
                    <Envlp>
                        <any>
					<![CDATA[<?xml version="1.0" encoding="UTF-8"?>
					<First_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>10</Addenda_Type_Code>
						<Transaction_Type_Code>MIS</Transaction_Type_Code>
						<Foreign_Payment_Amount>129.92</Foreign_Payment_Amount>
						<Foreign_Trace_Number>1234567</Foreign_Trace_Number>
						<Receiving_Company_Name___Individual_Name>MISSY COOPER</Receiving_Company_Name___Individual_Name>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</First_Addenda>
					<Second_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>11</Addenda_Type_Code>
						<Originator_Name>KLM Airlines</Originator_Name>
						<Originator_Street_Address>1 Schipol Airport</Originator_Street_Address>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Second_Addenda>
					<Third_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>12</Addenda_Type_Code>
						<Originator_City___State___Province>Amsterdam*123 456 789\</Originator_City___State___Province>
						<Originator_Country___Postal_Code>NL*123 456 789\</Originator_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Third_Addenda>
					<Fourth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>13</Addenda_Type_Code>
						<Originating_DFI_Name>ING Bank</Originating_DFI_Name>
						<Originating_DFI_Identification_Number_Qualifier>02</Originating_DFI_Identification_Number_Qualifier>
						<Originating_DFI_Identification>INGBNL2A</Originating_DFI_Identification>
						<Originating_DFI_Branch_Country_Code>NL</Originating_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Fourth_Addenda>
					<Fifth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>14</Addenda_Type_Code>
						<Receiving_DFI_Name>PNC Bank</Receiving_DFI_Name>
						<Receiving_DFI_Identification_Number_Qualifier>01</Receiving_DFI_Identification_Number_Qualifier>
						<Receiving_DFI_Identification>031316967</Receiving_DFI_Identification>
						<Receiving_DFI_Branch_Country_Code>US</Receiving_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Fifth_Addenda>
					<Sixth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>15</Addenda_Type_Code>
						<Receiver_Identification_Number>031316967</Receiver_Identification_Number>
						<Receiver_Street_Address>123 PITTSBURGH, PA</Receiver_Street_Address>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Sixth_Addenda>
					<Seventh_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>16</Addenda_Type_Code>
						<Receiver_City___State___Province>PA</Receiver_City___State___Province>
						<Receiver_Country___Postal_Code>US</Receiver_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Seventh_Addenda>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>THIS IS A TEST IAT PAYMENT</Payment_Related_Information>
						<Addenda_Sequence_Number>1</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>FOR THE FUN OF IT</Payment_Related_Information>
						<Addenda_Sequence_Number>2</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011111</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>]]>
					</any>
                    </Envlp>
                </SplmtryData>
            </CdtTrfTxInf>
            <CdtTrfTxInf>
                <PmtId>
                    <InstrId>TraceNumber 12</InstrId>
                    <EndToEndId>GS1OZXEQAE</EndToEndId>
                </PmtId>
                <Amt>
                    <InstdAmt Ccy="USD" >255.55</InstdAmt>
                </Amt>
                <CdtrAgt>
                    <FinInstnId>
                        <ClrSysMmbId>
                            <ClrSysId>
                                <Cd>USABA</Cd>
                            </ClrSysId>
                            <MmbId>031316271</MmbId>
                        </ClrSysMmbId>
                        <Nm>FIRST NATIONAL BANK OF PENNSYLVANIA</Nm>
                    </FinInstnId>
                </CdtrAgt>
                <Cdtr>
                    <Nm>GEORGIE COOPER</Nm>
                </Cdtr>
                <CdtrAcct>
                    <Id>
                        <Othr>
                            <Id>090989878</Id>
                        </Othr></Id>
                    <Tp>
                        <Cd>CACC</Cd>
                    </Tp>
                </CdtrAcct>
                <Purp>
                    <Prtry>ROYALTY</Prtry>
                </Purp>
                <RmtInf>
                    <Ustrd>ROYALTY FOR EPISODE 47</Ustrd>
                </RmtInf>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Indicator</PlcAndNm>
                    <Envlp>
                        <any>FF</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference_Indicator</PlcAndNm>
                    <Envlp>
                        <any>3</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference</PlcAndNm>
                    <Envlp>
                        <any> </any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Country_Code</PlcAndNm>
                    <Envlp>
                        <any>KY</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Originating_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>GBP</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>USD</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Secondary_OFAC_Screening_Indicator</PlcAndNm>
                    <Envlp>
                        <any>0</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>IAT</PlcAndNm>
                    <Envlp>
                        <any>
					<![CDATA[<?xml version="1.0" encoding="UTF-8"?>
					<First_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>10</Addenda_Type_Code>
						<Transaction_Type_Code>MIS</Transaction_Type_Code>
						<Foreign_Payment_Amount>213.46</Foreign_Payment_Amount>
						<Foreign_Trace_Number>1234567</Foreign_Trace_Number>
						<Receiving_Company_Name___Individual_Name>GEORGIE COOPER</Receiving_Company_Name___Individual_Name>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</First_Addenda>
					<Second_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>11</Addenda_Type_Code>
						<Originator_Name>KLM AIRLINES</Originator_Name>
						<Originator_Street_Address>1 Schipol Airport</Originator_Street_Address>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Second_Addenda>
					<Third_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>12</Addenda_Type_Code>
						<Originator_City___State___Province>Amsterdam*123 456 789\</Originator_City___State___Province>
						<Originator_Country___Postal_Code>NL*123 456 789\</Originator_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Third_Addenda>
					<Fourth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>13</Addenda_Type_Code>
						<Originating_DFI_Name>ING Bank</Originating_DFI_Name>
						<Originating_DFI_Identification_Number_Qualifier>01</Originating_DFI_Identification_Number_Qualifier>
						<Originating_DFI_Identification>INGBNL2A</Originating_DFI_Identification>
						<Originating_DFI_Branch_Country_Code>NL</Originating_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Fourth_Addenda>
					<Fifth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>14</Addenda_Type_Code>
						<Receiving_DFI_Name>FIRST NATIONAL BANK OF PENNSYLVANIA</Receiving_DFI_Name>
						<Receiving_DFI_Identification_Number_Qualifier>01</Receiving_DFI_Identification_Number_Qualifier>
						<Receiving_DFI_Identification>031316271</Receiving_DFI_Identification>
						<Receiving_DFI_Branch_Country_Code>US</Receiving_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Fifth_Addenda>
					<Sixth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>15</Addenda_Type_Code>
						<Receiver_Identification_Number>CAT113</Receiver_Identification_Number>
						<Receiver_Street_Address>666 UPPER RIVER STREET</Receiver_Street_Address>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Sixth_Addenda>
					<Seventh_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>16</Addenda_Type_Code>
						<Receiver_City___State___Province>SMALL TOWN, PA 90908</Receiver_City___State___Province>
						<Receiver_Country___Postal_Code>US</Receiver_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Seventh_Addenda>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>ROYALTY FOR EPISODE 47</Payment_Related_Information>
						<Addenda_Sequence_Number>1</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011222</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>]]>
					</any>
                    </Envlp>
                </SplmtryData>
            </CdtTrfTxInf>
            <CdtTrfTxInf>
                <PmtId>
                    <InstrId>TraceNumber 13</InstrId>
                    <EndToEndId>GSXU3XICTK</EndToEndId>
                </PmtId>
                <Amt>
                    <InstdAmt Ccy="USD" >355.55</InstdAmt>
                </Amt>
                <CdtrAgt>
                    <FinInstnId>
                        <ClrSysMmbId>
                            <ClrSysId>
                                <Cd>USABA</Cd>
                            </ClrSysId>
                            <MmbId>021000021</MmbId>
                        </ClrSysMmbId>
                        <Nm>JPM Chase</Nm>
                    </FinInstnId>
                </CdtrAgt>
                <Cdtr>
                    <Nm>BILLY SPARKS</Nm>
                </Cdtr>
                <CdtrAcct>
                    <Id>
                        <Othr>
                            <Id>232343454</Id>
                        </Othr></Id>
                    <Tp>
                        <Cd>CACC</Cd>
                    </Tp>
                </CdtrAcct>
                <Purp>
                    <Prtry>ROYALTY</Prtry>
                </Purp>
                <RmtInf>
                    <Ustrd>EPISODE 47 ROYALTY</Ustrd>
                </RmtInf>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Indicator</PlcAndNm>
                    <Envlp>
                        <any>FF</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference_Indicator</PlcAndNm>
                    <Envlp>
                        <any>3</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference</PlcAndNm>
                    <Envlp>
                        <any> </any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Country_Code</PlcAndNm>
                    <Envlp>
                        <any>US</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Originating_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>EUR</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>USD</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Secondary_OFAC_Screening_Indicator</PlcAndNm>
                    <Envlp>
                        <any>0</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>IAT</PlcAndNm>
                    <Envlp>
                        <any>
					<![CDATA[<?xml version="1.0" encoding="UTF-8"?>
					<First_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>10</Addenda_Type_Code>
						<Transaction_Type_Code>MIS</Transaction_Type_Code>
						<Foreign_Payment_Amount>296.95</Foreign_Payment_Amount>
						<Foreign_Trace_Number>333111333</Foreign_Trace_Number>
						<Receiving_Company_Name___Individual_Name>BILLY SPARKS</Receiving_Company_Name___Individual_Name>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</First_Addenda>
					<Second_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>11</Addenda_Type_Code>
						<Originator_Name>KLM Airlines</Originator_Name>
						<Originator_Street_Address>1 Schipol Airport</Originator_Street_Address>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Second_Addenda>
					<Third_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>12</Addenda_Type_Code>
						<Originator_City___State___Province>Amsterdam*123 456 789\</Originator_City___State___Province>
						<Originator_Country___Postal_Code>NL*123 456 789\</Originator_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Third_Addenda>
					<Fourth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>13</Addenda_Type_Code>
						<Originating_DFI_Name>ING Bank</Originating_DFI_Name>
						<Originating_DFI_Identification_Number_Qualifier>02</Originating_DFI_Identification_Number_Qualifier>
						<Originating_DFI_Identification>INGBNL2A</Originating_DFI_Identification>
						<Originating_DFI_Branch_Country_Code>NL</Originating_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Fourth_Addenda>
					<Fifth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>14</Addenda_Type_Code>
						<Receiving_DFI_Name>JPM CHASE</Receiving_DFI_Name>
						<Receiving_DFI_Identification_Number_Qualifier>01</Receiving_DFI_Identification_Number_Qualifier>
						<Receiving_DFI_Identification>021000021</Receiving_DFI_Identification>
						<Receiving_DFI_Branch_Country_Code>US</Receiving_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Fifth_Addenda>
					<Sixth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>15</Addenda_Type_Code>
						<Receiver_Identification_Number>031316967</Receiver_Identification_Number>
						<Receiver_Street_Address>123 PITTSBURGH, PA</Receiver_Street_Address>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Sixth_Addenda>
					<Seventh_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>16</Addenda_Type_Code>
						<Receiver_City___State___Province>PA</Receiver_City___State___Province>
						<Receiver_Country___Postal_Code>US</Receiver_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Seventh_Addenda>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>ROYALTY PAYMENT FOR AIRING ON KLM FLIGHTS</Payment_Related_Information>
						<Addenda_Sequence_Number>1</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011333</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>]]>
					</any>
                    </Envlp>
                </SplmtryData>
            </CdtTrfTxInf>
            <CdtTrfTxInf>
                <PmtId>
                    <InstrId>TraceNumber 14</InstrId>
                    <EndToEndId>GSAMY75ROA</EndToEndId>
                </PmtId>
                <Amt>
                    <InstdAmt Ccy="USD" >455.55</InstdAmt>
                </Amt>
                <CdtrAgt>
                    <FinInstnId>
                        <ClrSysMmbId>
                            <ClrSysId>
                                <Cd>USABA</Cd>
                            </ClrSysId>
                            <MmbId>031316967</MmbId>
                        </ClrSysMmbId>
                        <Nm>SYNOVUS BANK</Nm>
                    </FinInstnId>
                </CdtrAgt>
                <Cdtr>
                    <Nm>TAM NGUYEN</Nm>
                </Cdtr>
                <CdtrAcct>
                    <Id>
                        <Othr>
                            <Id>989878767</Id>
                        </Othr></Id>
                    <Tp>
                        <Cd>CACC</Cd>
                    </Tp>
                </CdtrAcct>
                <Purp>
                    <Prtry>ROYALTY</Prtry>
                </Purp>
                <RmtInf>
                    <Ustrd>SHOW ROYALTY</Ustrd>
                </RmtInf>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Indicator</PlcAndNm>
                    <Envlp>
                        <any>FF</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference_Indicator</PlcAndNm>
                    <Envlp>
                        <any>3</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Foreign_Exchange_Reference</PlcAndNm>
                    <Envlp>
                        <any> </any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Country_Code</PlcAndNm>
                    <Envlp>
                        <any>US</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Originating_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>EUR</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>ISO_Destination_Currency_Code</PlcAndNm>
                    <Envlp>
                        <any>USD</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>Secondary_OFAC_Screening_Indicator</PlcAndNm>
                    <Envlp>
                        <any>0</any>
                    </Envlp>
                </SplmtryData>
                <SplmtryData>
                    <PlcAndNm>IAT</PlcAndNm>
                    <Envlp>
                        <any>
					<![CDATA[<?xml version="1.0" encoding="UTF-8"?>
					<First_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>10</Addenda_Type_Code>
						<Transaction_Type_Code>MIS</Transaction_Type_Code>
						<Foreign_Payment_Amount>380.55</Foreign_Payment_Amount>
						<Foreign_Trace_Number>1234567</Foreign_Trace_Number>
						<Receiving_Company_Name___Individual_Name>TAM NGUYEN</Receiving_Company_Name___Individual_Name>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</First_Addenda>
					<Second_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>11</Addenda_Type_Code>
						<Originator_Name>KLM Airlines</Originator_Name>
						<Originator_Street_Address>1 Schipol Airport</Originator_Street_Address>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Second_Addenda>
					<Third_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>12</Addenda_Type_Code>
						<Originator_City___State___Province>Amsterdam*123 456 789\</Originator_City___State___Province>
						<Originator_Country___Postal_Code>NL*123 456 789\</Originator_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Third_Addenda>
					<Fourth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>13</Addenda_Type_Code>
						<Originating_DFI_Name>ING Bank</Originating_DFI_Name>
						<Originating_DFI_Identification_Number_Qualifier>02</Originating_DFI_Identification_Number_Qualifier>
						<Originating_DFI_Identification>INGBNL2A</Originating_DFI_Identification>
						<Originating_DFI_Branch_Country_Code>NL</Originating_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Fourth_Addenda>
					<Fifth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>14</Addenda_Type_Code>
						<Receiving_DFI_Name>PNC Bank</Receiving_DFI_Name>
						<Receiving_DFI_Identification_Number_Qualifier>01</Receiving_DFI_Identification_Number_Qualifier>
						<Receiving_DFI_Identification>031316967</Receiving_DFI_Identification>
						<Receiving_DFI_Branch_Country_Code>US</Receiving_DFI_Branch_Country_Code>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Fifth_Addenda>
					<Sixth_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>15</Addenda_Type_Code>
						<Receiver_Identification_Number>031316967</Receiver_Identification_Number>
						<Receiver_Street_Address>123 PITTSBURGH, PA</Receiver_Street_Address>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Sixth_Addenda>
					<Seventh_Addenda>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>16</Addenda_Type_Code>
						<Receiver_City___State___Province>PA</Receiver_City___State___Province>
						<Receiver_Country___Postal_Code>US</Receiver_Country___Postal_Code>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Seventh_Addenda>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>THIS IS A TEST IAT PAYMENT</Payment_Related_Information>
						<Addenda_Sequence_Number>1</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>
					<Addenda_for_Remittance_Information>
						<Record_Type_Code>7</Record_Type_Code>
						<Addenda_Type_Code>17</Addenda_Type_Code>
						<Payment_Related_Information>FOR THE FUN OF IT</Payment_Related_Information>
						<Addenda_Sequence_Number>2</Addenda_Sequence_Number>
						<Entry_Detail_Sequence_Number>1011444</Entry_Detail_Sequence_Number>
					</Addenda_for_Remittance_Information>]]>
					</any>
                    </Envlp>
                </SplmtryData>
            </CdtTrfTxInf>
        </PmtInf>
    </CstmrCdtTrfInitn>
</Document>
